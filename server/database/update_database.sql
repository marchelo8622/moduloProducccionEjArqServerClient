--MENU

INSERT INTO `ccelbosque_infosac`.`menu` (
`MM` ,
`OPC` ,
`RG` ,
`OPCION` ,
`ACCION` ,
`users` ,
`icono` ,
`demo` ,
`sac` ,
`comentario` ,
`tabla` ,
`accesotabla`
)
VALUES
('27', '446', '00', 'ADMINISTRACION DE LOCALES', ' ', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '447', '01', '<marquee>Actualizar Base de Datos</marquee>', 'modulos/server/update_databases/', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '448', '01', 'Creacion de locales', 'clientes/crear_localesfr.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '449', '01', 'Expensas', 'clientes/expensasfr.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '450', '01', '<marquee>Configurar Productos</marquee>', 'clientes/configurar_productosFR.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '451', '01', 'Registro de Ingresos Excel', 'clientes/registro_ingresosfr.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '452', '01', 'Carga de Excel Locales', 'clientes/excel_localesfr.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '453', '01', '<marquee>Facturacion Masiva</marquee>', 'clientes/facturacion_masivafr.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '454', '01', 'Reporte de Deudas', 'clientes/reporte_deudasfr.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '455', '01', 'Reporte de Facturas', 'clientes/reporte_facturasfr.php', '1111111000000000000', '', 'S', '', '', '', ''),
('27', '456', '01', 'Registro Cuentas Bancarias', 'clientes/pagos_por_transferenciafr.php', '1111111000000000000', '', 'S', '', '', '', '')
ON DUPLICATE KEY UPDATE `OPCION` = VALUES(`OPCION`), `ACCION` = VALUES(`ACCION`); 

-- --------------------------------------------------------

--
-- Estructura tabla `locales`
--

CREATE TABLE IF NOT EXISTS `locales` (
  `id` varchar(255) NOT NULL,
  `tipo_local` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `factor` double(18,10) NOT NULL,
  `estado` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


-- --------------------------------------------------------

--
-- Estructura tabla `locales_productos`
--

CREATE TABLE IF NOT EXISTS `locales_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_local` varchar(255) NOT NULL,
  `id_producto` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Estructura tabla `expensas`
--

CREATE TABLE IF NOT EXISTS `expensas` (
  `id` int(11) NOT NULL ,
  `nombre` varchar(255) NOT NULL,
  `valor` double(18,2) default 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci AUTO_INCREMENT=1;

INSERT INTO `expensas`
(
`id`,
`nombre`
)
VALUES
(1, 'Ordinaria'),
(2, 'Extraordinaria'),
(3, 'Seguro de Riesgos')
ON DUPLICATE KEY UPDATE `nombre` = VALUES(`nombre`);

--
-- Estructura tabla `registro_ingresos`
--

CREATE TABLE IF NOT EXISTS `registro_ingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_local` varchar(255) NOT NULL,
  `interes_mora` double(18,2) DEFAULT '0.00',
  `multas` double(18,2) DEFAULT '0.00',
  `otros` double(18,2) DEFAULT '0.00',
  `total_servicios` double(18,2) DEFAULT '0.00',
  `facturado` tinyint(1) DEFAULT '0',
  `fecha_creado` DATE,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci AUTO_INCREMENT=1;


drop trigger if exists `registro_ingresos_created`;

create trigger `registro_ingresos_created` before insert
    on `registro_ingresos`
    for each row 
    set new.`fecha_creado` = now();


-- --------------------------------------------------------

--
-- Configuracion de productos
--

INSERT INTO `maetab` (`numtab`, `codtab`, `nomtab`, `ad1tab`, `ad2tab`, `ad3tab`, `ad4tab`, `ad5tab`, `ad6tab`, `ad7tab`, `ad8tab`, `block`, `ad9tab`, `ad0tab`) VALUES
('PPL', '', 'Productos para locales', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('PPL', 'm01', 'Interes Mora', 0.00, 0.00, 0, 0.00, 0, 0, 'mora01', 'c', NULL, '', ''),
('PPL', 'mu01', 'Multas', 0.00, 0.00, 0, 0.00, 0, 0, 'multas01', 'd', NULL, '', ''),
('PPL', 'o01', 'Otros', 0.00, 0.00, 0, 0.00, 0, 0, 'otros01', 'e', NULL, '', ''),
('PPL', 'r01', 'Rentas', NULL, NULL, NULL, NULL, NULL, NULL, 'rentas01', 'g', NULL, NULL, NULL),
('PPL', 's01', 'Total Servicios', 0.00, 0.00, 0, 0.00, 0, 0, 'servicio01', 'f', NULL, '', '');

INSERT INTO `maepro` (`codprod01`, `desprod01`, `cve101`, `cve201`, `unidmed01`, `cantmin01`, `cantact01`, `valact01`, `exipromo01`, `precuni01`, `pedpend01`, `orden01`, `refer01`, `canentm01`, `valentm01`, `cansalm01`, `valsalm01`, `canenta01`, `valenta01`, `cansala01`, `valsala01`, `fecape01`, `fecult01`, `fecvta01`, `ubic01`, `precvta01`, `descto101`, `precio201`, `descto201`, `precio301`, `descto301`, `canvtam01`, `valvtam01`, `cosvtam01`, `canvtaa01`, `valvtaa01`, `cosvtaa01`, `prod1alt01`, `prod2alt01`, `proved101`, `proved201`, `med101`, `med201`, `med301`, `factor01`, `cvserie01`, `ctain101`, `ctain201`, `ctain301`, `porciva01`, `prodsinsdo01`, `sinprec01`, `fotoprod01`, `detprod01`, `block`, `UID`, `ultimoacceso`, `idpro`, `catprod01`, `med401`, `med501`, `prodconmed01`, `factorpeso01`, `codbar01`, `unifrac01`, `calidad01`, `color01`, `material01`, `talla01`, `compuesto01`, `catalt01`, `precfob01`, `precio401`, `descto401`, `porigen01`, `rin01`, `marca01`, `alto01`, `ancho01`, `tipoletra01`, `indcarga01`, `indveloc01`, `pr01`, `dis01`, `tipocons01`, `precateg01`, `tipprod01`, `conversion01`, `valhom01`, `ctain401`, `valhom02`, `valhom03`, `valhom04`, `statuspro01`, `parara01`, `prodequiv01`, `regalia01`, `precio501`, `descto501`, `precio601`, `descto601`, `precio701`, `descto701`, `precio801`, `descto801`, `precio901`, `descto901`, `precio1001`, `descto1001`, `precio1101`, `descto1101`, `precio1201`, `descto1201`, `submarca01`, `modelo01`, `clasific01`, `codbarempaque01`, `unidadempaque01`, `dimensionempaque01`, `link01`, `desprod201`, `desprod301`, `peso01`, `prodrel01`, `coefprd01`, `infor01`, `infor02`, `infor03`, `infor04`, `infor05`, `infor06`, `infor07`, `infor08`, `porcenrenta`, `porcicevta01`, `porcicecpra01`, `porcptdaranc01`, `ordimp01`, `peso`, `consignado`, `cant_consignado`) VALUES
('multas01', 'Multas', 'S', 0, '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '2015-08-04 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '', 0.00000, 0.00000, 0.00000, 0.00, 'N', '101090999', '', '101039901', 12.00, 'S', 'S', '', '', 1, 2, '2015-08-04 11:23:27', 247, '1', 0.00000, 0.00000, 'N', '', '', 'S', '', '', '', '', 'N', '', 0.00, 0.0000, 0.00, '', '', '', '', '', '', '', '', '', '', '', 'N', 'S', 0.00, 0.00, '', 0.00, 0.00, 0.00, 'S', '', '', '', 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, NULL, NULL, '', '', '', '', '', '', '', 0.000000, '', NULL, '', '', '', '', '', '', '', '', NULL, 0.00, 0.00, 0.00, '', '', 'N', 0),
('rentas01', 'Rentas', 'S', 0, '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '2015-08-04 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '', 0.00000, 0.00000, 0.00000, 0.00, 'N', '201070130', '', '201070130', 12.00, 'S', 'S', '', '', 0, NULL, '2015-08-04 11:19:11', 245, '1', 0.00000, 0.00000, 'N', '', '', 'S', '', '', '', '', 'N', '', 0.00, 0.0000, 0.00, '', '', '', '', '', '', '', '', '', '', '', 'N', 'S', 0.00, 0.00, '', 0.00, 0.00, 0.00, 'S', '', '', '', 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, NULL, NULL, '', '', '', '', '', '', '', 0.000000, '', NULL, '', '', '', '', '', '', '', '', NULL, 0.00, 0.00, 0.00, '', '', 'N', 0),
('servicio01', 'Servicios', 'S', 0, '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '2015-08-04 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '', 0.00000, 0.00000, 0.00000, 0.00, 'N', '201070501', '', '101080106', 12.00, 'S', 'S', '', '', 0, NULL, '2015-08-04 11:20:10', 246, '1', 0.00000, 0.00000, 'N', '', '', 'S', '', '', '', '', 'N', '', 0.00, 0.0000, 0.00, '', '', '', '', '', '', '', '', '', '', '', 'N', 'S', 0.00, 0.00, '', 0.00, 0.00, 0.00, 'S', '', '', '', 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, 0.0000, 0.00, NULL, NULL, '', '', '', '', '', '', '', 0.000000, '', NULL, '', '', '', '', '', '', '', '', NULL, 0.00, 0.00, 0.00, '', '', 'N', 0);

-- ADD campos a tabla `maecte`
--
ALTER TABLE `maecte` ADD COLUMN `asociar_locales` char(1) NOT NULL DEFAULT 'N';

-- --------------------------------------------------------

--
-- Estructura tabla `productos_meses_cobro`
--

CREATE TABLE IF NOT EXISTS `productos_meses_cobro` (
`codprod01` VARCHAR( 60 ) NOT NULL ,
`mes_cobro` VARCHAR( 2 ) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Configuracion para campo alicuota
--

INSERT INTO `maetab` (`numtab`, `codtab`, `nomtab`, `ad1tab`, `ad2tab`, `ad3tab`, `ad4tab`, `ad5tab`, `ad6tab`, `ad7tab`, `ad8tab`, `block`, `ad9tab`, `ad0tab`) VALUES
('ALO', '01', 'configuracion de alicuota ordinaria', 0.00, 0.00, 1, 0.00, 0, 0, '', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Estructura tabla `clientes_locales_productos`
--

CREATE TABLE IF NOT EXISTS `clientes_locales_productos` (
`id_cliente` VARCHAR(255) NOT NULL ,
`id_local` VARCHAR(255) NOT NULL,
`id_producto` VARCHAR(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Estructura tabla `movpro_deuda`
--

CREATE TABLE IF NOT EXISTS `movpro_deuda` (
`tipo_documento_31` VARCHAR(25),
`numero_factura_31` VARCHAR(25),
`ocurrencia` VARCHAR(4),
`fecha_factura_31` datetime default NULL,
`codigo_producto` VARCHAR(50),
`valor_deuda` double(18,2) default 0,
`valor_sin_iva` double(18,2) default 0,
`saldo` double(18,2) default 0,
`UID` int,
`codigo_bodega` VARCHAR(5)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Estructura tabla `clientes_locales_productos`
--

CREATE TABLE IF NOT EXISTS `movpro_pago` (
`tipo_documento_31` VARCHAR(25),
`numero_factura_31` VARCHAR(25),
`ocurrencia` VARCHAR(4),
`codigo_producto` VARCHAR(50),
`tipo_pago` VARCHAR(10),
`numero_pago` VARCHAR(50),
`fecha_pago` datetime default NULL,
`valor_pago` double(18,2) default 0,
`UID` int,
`cuentas_contables` VARCHAR(255) NOT NULL,
`codigo_bodega` VARCHAR(5)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Estructura tabla `facturas_locales`
--

CREATE TABLE IF NOT EXISTS `facturas_locales` (
`numero_factura` VARCHAR(25),
`id_local` VARCHAR(25)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

# Se aumenta campos para cartera vencida
ALTER TABLE `maepro` ADD `cartera_vencida` VARCHAR(15) NOT NULL AFTER `ctain301`;

--
-- Configuracion para productos suman en total servicios
--

INSERT INTO `maetab` (`numtab`, `codtab`, `nomtab`, `ad1tab`, `ad2tab`, `ad3tab`, `ad4tab`, `ad5tab`, `ad6tab`, `ad7tab`, `ad8tab`, `block`, `ad9tab`, `ad0tab`) VALUES
('PSS', '', 'Productos suman servicios', 0.00, 0.00, 0.00, 0.00, 0, 0, '', '', NULL, '', ''),
('PSS', 'seg01', 'Seguro', 1, 0.00, 0.00, 0.00, 0, 0, 'seguro01', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Estructura tabla `facturas_locales`
--
CREATE TABLE IF NOT EXISTS `persona_transferencia` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre_completo` VARCHAR(255) NOT NULL,
  `tipo_identificacion` CHAR NOT NULL,
  `identificacion` VARCHAR(20) NOT NULL,
  `cuenta_contable` VARCHAR(20) NOT NULL,
  `banco` VARCHAR(20) NOT NULL,
  `cuenta_bancaria` VARCHAR(20) NOT NULL,
  `tipo_cuenta_bancaria` VARCHAR(20) NOT NULL,
  `esta_activo` CHAR NOT NULL DEFAULT 'S',
  `creado_en` DATETIME NOT NULL,
  `actualizado_en` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Estructura tabla `diccionarioCamposTransferenciasOtros`
--
CREATE TABLE IF NOT EXISTS `diccionarioCamposTransferenciasOtros` (
  `tabla` varchar(255) NOT NULL,
  `campoBase` varchar(255) NOT NULL,
  `nombreCampo` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `tipoCampo` varchar(50) NOT NULL COMMENT 'Tipo de dato de la base'

) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcar la base de datos para la tabla `diccionarioCamposTransferenciasOtros`
--

INSERT INTO `diccionarioCamposTransferenciasOtros` (`tabla`, `campoBase`, `nombreCampo`, `descripcion`, `tipoCampo`) VALUES
('persona_transferencia', 'identificacion', 'CodigoProveedor', 'Numero del proveedor', ''),
('persona_transferencia', 'nombre_completo', 'NombreProveedor', 'Nombre del proveedor', ''),
('persona_transferencia', 'identificacion', 'CedulaProveedor', 'Cedula del proveedor', ''),
('persona_transferencia', 'banco', 'BancoProveedor', 'Codigo del banco del proveedor', ''),
('persona_transferencia', 'cuenta_bancaria', 'NumeroCuentaProveedor', 'Numero cuenta banco del proveedor', ''),
('persona_transferencia', 'tipo_cuenta_bancaria', 'TipoCuenta', 'Tipo de cuenta del proveedor', ''),
('persona_transferencia', 'ctabanco20', 'NumeroCuentaEmpresa', '', '');

-- --------------------------------------------------------

# Se aumenta campos para pagos a terceros
ALTER TABLE `movcon2` ADD `identificacion` VARCHAR(15) NULL;

# Se aumenta campos para pagos a terceros
ALTER TABLE `movcon` ADD `identificacion` VARCHAR(15) NULL;

--
-- Se crea tabla clientes_interes_mora
--

CREATE TABLE  `cliente_interes_mora` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`id_local` VARCHAR( 255 ) NOT NULL ,
`id_cliente` VARCHAR( 255 ) NOT NULL ,
`interes_mora` DOUBLE( 18, 2 ) NOT NULL ,
`facturado` SMALLINT NOT NULL ,
`fecha_creacion` DATETIME NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


# Se aumenta campos para pagos a terceros
ALTER TABLE `movpag` ADD `numero_transferencia` INT(11) NOT NULL;
ALTER TABLE `movcon2` ADD `numero_transferencia` INT(11) NOT NULL;