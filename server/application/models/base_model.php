<?php
error_reporting(0);

Class Base_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $db_settings = '';

        $this->load->library('Connection');
        $connection = new Connection();

        if (isset($_GET["base"])) {
            $db_settings = $connection->change($_GET["base"]);
        }

        if ($db_settings != '') {
            $this->db = $this->load->database($db_settings, true);
        }
    }

    private function get_directory() {

        $this->db->select('codtab, ad7tab');
        $this->db->where("numtab = '97' AND codtab != ''");
        return $this->db->get('maetab');
    }

    private function get_matrix_connection() {

        $this->db->select('codtab, ad7tab');
        $this->db->where("numtab = '97' AND codtab = '01' ");
        return $this->db->get('maetab');
    }

    public function get_warehouse_name($DSN) {
        $data = explode(';', $DSN);
        $warehouseData = explode('=', $data[2]);
        return $warehouseData[1];
    }

    public function get_matrix_name() {
        $matrix = $this->get_matrix_connection()->result_array();
        $dsn_matrix = $this->get_warehouse_name($matrix[0]['ad7tab']);

        return $dsn_matrix;
    }

    private function get_bodega_costos_connection() {

        $this->db->select('codtab, ad7tab');
        $this->db->where("numtab = '97' AND ad8tab = 'BMP' ");
        return $this->db->get('maetab');
    }

    public function get_bodega_costos_name() {
        $bod_costos = $this->get_bodega_costos_connection()->result_array();
        $dsn_bod_costos = $this->get_warehouse_name($bod_costos[0]['ad7tab']);
        return $dsn_bod_costos;
    }

}

?>
