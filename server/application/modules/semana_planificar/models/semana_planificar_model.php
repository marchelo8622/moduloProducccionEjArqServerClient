<?php

include_once(APPPATH . "models/base_model.php");

Class Semana_planificar_model extends Base_model {

    function __construct() {
        parent::__construct();
    }

    function get_semana_planificar($desde) {
        $this->db->where("desde", $desde);
        $this->db->from("semana_planificar");
        $this->db->join('maecte', 'id_cliente = codcte01');
        $this->db->join('maepro', 'id_producto = codprod01');
        $result = $this->db->get();
        return $result->result();
    }
    
    function get_semana_planificar_by_from($desde) {
        $this->db->where("desde", $desde);
        $this->db->select('*,SUM(cantidad) AS cantidad_total')->from("semana_planificar");
        $this->db->join('maecte', 'id_cliente = codcte01');
        $this->db->join('maepro', 'id_producto = codprod01');
        $this->db->group_by('id_producto, opcion');
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function post_guardar($datos) {
       // var_dump($datos['data']["url_params"][0]["desde"]);
        $this->db->delete('semana_planificar', array('desde' => $datos['data']["url_params"][0]["desde"]));
        foreach ($datos['data'] as $key => $producto) {
            foreach ($producto as $key_p => $data) {
                $this->db->replace("semana_planificar", $data);
            }
        }
    }

}

?>
