<?php

defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";

class api extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Semana_planificar_model", "Semana_planificar");
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model('producto/Producto_model', 'Producto');
    }

    function semana_planificar_get($desde) {
        $clientes = $this->Semana_planificar->get_semana_planificar($desde);
        echo format_response($clientes);
    }

    function semana_planificar_put() {
        // create a new semana_planificar and respond with a status/errors
        // return data = echo format_response($data);
    }

    function semana_planificar_post() {
        // update an existing semana_planificar and respond with a status/errors
        // return data = echo format_response($data);
    }

    function semana_planificar_delete() {
        // delete a semana_planificar and respond with a status/errors
        // return data = echo format_response($data);
    }

    function clientes_get($id, $nombre) {
        $maecte = $this->Cliente->get_cliente_by($id, $nombre);
        echo format_response($maecte);
    }

    function productos_tipo_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_tipo($codigo, $descripcion, 'null');
        echo format_response($productos);
    }

    function guardar_post() {
        $filter = $this->post();
        $productos = $this->Semana_planificar->post_guardar($filter);
        echo format_response($productos);
    }

}

?>
