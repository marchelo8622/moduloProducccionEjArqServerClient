<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class semana_planificar extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Semana_planificar";
		$data["angular_app"] = "Semana_planificar";
		$this->layout->view("semana_planificar", $data);
	}
}

