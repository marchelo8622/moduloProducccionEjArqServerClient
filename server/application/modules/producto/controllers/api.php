<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Producto_model', 'Producto');
    }
    
    function productos_get($codigo, $descripcion)
    {
        $productos = $this->Producto->get_productos($codigo, $descripcion);
        echo format_response($productos);
    }
    
    function productos_compuestos_get($codigo, $descripcion)
    {
        $productos = $this->Producto->get_productos_compuestos($codigo, $descripcion);
        echo format_response($productos);
    }
    
    function productos_tipo_get($codigo, $descripcion)
    {
        $productos = $this->Producto->get_productos_tipo($codigo, $descripcion, '2');
        echo format_response($productos);
    }
    
}
