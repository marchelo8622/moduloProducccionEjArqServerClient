<?php

include_once(APPPATH . "models/base_model.php");

Class Producto_model extends Base_model {

    function __construct() {
        parent::__construct();
        $this->bod_costos = parent::get_bodega_costos_name();
    }

    function get_bodega_costos()
    {
        return $this->bod_costos;
    }

    function get_productos($codigo, $descripcion) {

        if ($codigo != "null" && $descripcion != "null") {
            $this->db->limit(20);
        }

        if ($codigo != "null") {
            $this->db->like('codprod01', $codigo);
        }

        if ($descripcion != "null") {
            $this->db->like('desprod01', $descripcion);
        }

        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock');
        $this->db->from("maepro");
        $result = $this->db->get();

        return $result->result();
    }

    function get_productos_compuestos($codigo, $descripcion) {

        if ($codigo != "null" && $descripcion != "null") {
            $this->db->limit(20);
        }

        if ($codigo != "null") {
            $this->db->like('codprod01', $codigo);
        }

        if ($descripcion != "null") {
            $this->db->like('desprod01', $descripcion);
        }
        $this->db->where('compuesto01', "S");

        //$this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock, GROUP_CONCAT(codprod20, "\|",cantidad20 SEPARATOR \' \') AS componentes', false, false);
        
        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock, catprod01 as cate');
        
        //$this->db->join('lismae', 'codprod01 = codprod20t');
        $this->db->from("maepro");

        $result = $this->db->get();

        //echo $this->db->last_query();

        return $result->result();
    }

    function get_productos_tipo($codigo, $descripcion, $tipo) {

        if ($codigo != "null" && $descripcion != "null") {
            $this->db->limit(20);
        }

        if ($codigo != "null") {
            $this->db->like('codprod01', $codigo);
        }

        if ($descripcion != "null") {
            $this->db->like('desprod01', $descripcion);
        }

        if ($tipo != "null") {
            $this->db->where('ad3tab', $tipo);
            $this->db->join('maetab', 'orden01 = codtab');
            $this->db->where('numtab', '46');
        }

        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock,porciva01');
        $this->db->from('maepro');
        $this->db->where('orden01 != ', '');
        $this->db->limit(25);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }


    function get_productos_equivalentes($cod_producto_principal) {
        $cadena_productos_equivalentes = $this->get_codigos_productos_equivalentes($cod_producto_principal);        
        $array_productos_equivalentes = explode("|", $cadena_productos_equivalentes[0]['prodequiv01']);

        if(count($array_productos_equivalentes) > 0){
             $string = '';
             for ( $i = 0 ; $i <= count($array_productos_equivalentes) ; $i ++) {
                if($array_productos_equivalentes[$i] != ''){
                    $string .= "'".$array_productos_equivalentes[$i]."',";
                }
             }
        }
        
        $string = substr($string, 0, strlen($string) - 1);
        $resultado = array();
        if($string != ''){
            $sql = $this->db->query('select codprod01 as codigo, desprod01 as descripcion, cantact01 as stock,porciva01
                                     from maepro 
                                     where codprod01 in ('.$string.')');
            $resultado = $sql->result_array();
        }

        return $resultado;
    }

    function get_codigos_productos_equivalentes($cod_producto_principal){

        $this->db->select('prodequiv01');
        $this->db->from('maepro');        
        $this->db->where('codprod01',$cod_producto_principal);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();

    }

    function get_productos_con_receta() {

        $this->db->where('clasific01', "01");
        
        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock');
        
        $this->db->from("maepro");

        $result = $this->db->get();

        return $result->result();
    }

    function get_ultima_compra_by_producto($codigo)
    {
        $this->db->select('PU03 as costo_transaccion, PRECUNI03 as costo_producto');
        $this->db->where('CODPROD03', $codigo);
        $this->db->where('TIPOTRA03', '01');
        $this->db->from($this->bod_costos.".movpro");
        $this->db->order_by('FECMOV03', "DESC");
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();

    }

    function get_ultima_transaccion($codigo)
    {
        $this->db->select('PU03 as costo_transaccion, PRECUNI03 as costo_producto');
        $this->db->where('CODPROD03', $codigo);
        $this->db->where('TIPOTRA03 >= ', '01');
        $this->db->where('TIPOTRA03 <= ', '50');
        $this->db->from($this->bod_costos.".movpro");
        $this->db->order_by('FECMOV03', "DESC");
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();

    }

    function es_compuesto($codigo)
    {
        $this->db->where('codprod01', $codigo);
        $this->db->where('compuesto01', "S");
        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock');
        $this->db->from("maepro");
        $this->db->join('lismae','maepro.codprod01 = codprod20t');
        $result = $this->db->get();

        return $result->result();
    }

    function get_producto_by_codigo($codigo)
    {
        $this->db->select('codprod01 as codigo, desprod01 as descripcion,
                         cantact01 as stock, cantact01 as cantidad_actual,
                         valact01 as valor_actual, unidmed01 as unidad_medida, clasific01 as clasificacion,
                         compuesto01 as compuesto');
        $this->db->from("maepro");
        $this->db->where('codprod01', $codigo);
        $result = $this->db->get();
        $result = $result->result_array();
        return $result[0];

    }

    function get_productos_con_receta_by_categoria($categoria) {       

        $sql = $this->db->query('select codprod01 as codigo, desprod01 as descripcion, cantact01 as stock,"'.$categoria.'" as categoria
            from maepro
            where catprod01 = "'.$categoria.'"
            and clasific01 = "01"
         ');       
        
        return $sql->result();
    }

    function actualizar_inventario_producto($datos)
    {
        $data = array(
            'cantact01' => $datos['cantidad_actual'],
            'valact01' => $datos['valor_actual'],
            'precuni01' => $datos['precio_unitario']
        );
        $this->db->where('codprod01', $datos['codigo']);
        $result = $this->db->update('maepro', $data);
        return $result;

    }

    function get_costo_producto_by_codigo($codigo)
    {
        $costo = $this->get_costo_subrecetas($codigo);
        if($costo == 0)
           $costo = $this->get_costo_producto($codigo);

       return $costo;
    }

    function get_costo_subrecetas($codigo_receta)
    {
            $subReceta = $this->get_componentes($codigo_receta);
            $sumSubreceta = 0;
            if (count($subReceta) > 0){

                foreach ($subReceta as  $dato){
                     $ultimo_costo =  $this->get_costo_producto($dato['codprod20']);
                     
                     $subReceta1 = $this->get_componentes($dato['codprod20']);
                     if (count($subReceta1) > 0){
                        
                        $ultimo_costo = 0;
                         foreach ($subReceta1 as  $dato1) {
                             $ultimo_costo1 =  $this->get_costo_producto($dato1['codprod20']);
                             $subReceta2 = $this->get_componentes($dato1['codprod20']);
                             if (count($subReceta2) > 0){
                                $ultimo_costo1 = 0;
                                foreach ($subReceta2 as  $dato2) {
                                    $ultimo_costo2 =  $this->get_costo_producto($dato2['codprod20']);
                                    $subReceta3 = $this->get_componentes($dato2['codprod20']);
                                    if (count($subReceta2) > 0){
                                        $ultimo_costo2 = 0;
                                        foreach ($subReceta3 as  $dato3) {
                                            $ultimo_costo3 =  $this->get_costo_producto($dato3['codprod20']);

                                            $subReceta4 = $this->get_componentes($dato3['codprod20']);
                                            if (count($subReceta2) > 0){
                                                $ultimo_costo3 = 0;
                                                foreach ($subReceta4 as  $dato4) {
                                                    $ultimo_costo4 =  $this->get_costo_producto($dato4['codprod20']);
                                                    $ultimo_costo3 += ($dato4['cantidad20'] * $ultimo_costo4);
                                                }
                                            }

                                            $ultimo_costo2 += ($dato3['cantidad20'] * $ultimo_costo3);
                                        }
                                    }

                                    $ultimo_costo1 += ($dato2['cantidad20'] * $ultimo_costo2);
                                }
                             }
                             
                             $ultimo_costo += ($dato1['cantidad20'] * $ultimo_costo1);                             
                         }
                     } 

                     $sumSubreceta += ($dato['cantidad20'] * $ultimo_costo); 
                }

            }
        return $sumSubreceta;
    }

    function  get_costo_producto($codigo)
    {
        $costo = 0.00;
        $costoCompra = $this->get_ultima_compra_by_producto($codigo);
        if ($costoCompra != '' && $costoCompra != null && $costoCompra != 0 && count($costoCompra) > 0){
            $costo = $costoCompra[0]->costo_transaccion;
        }
        else{
            $costo_producto = $this->get_ultima_transaccion($codigo);

        if ($costo_producto != '' && $costo_producto != null && $costo_producto != 0 && count($costo_producto) > 0)
            $costo = $costo_producto[0]->costo_producto;
        }

        return $costo;
    }

    function get_componentes($producto_receta,$tabla = 'lismae')
    {
        $this->db->select('*');
        $this->db->from($tabla);
        $this->db->where('codprod20t', $producto_receta);
        $result = $this->db->get();
        return $result->result_array();

    }

    function es_producto_final($codigo)
    {
        $this->db->where('codprod01', $codigo);
        $this->db->where('catprod01', 'PT');
        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock');
        $this->db->from("maepro");
        $result = $this->db->get();

        return $result->result();
    }

    function consulta_stock($codigo){

        $this->db->select('cantact01');
        $this->db->from('maepro');
        $this->db->where('codprod01', $codigo);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();

    }

    function consulta_producto_servicio($codigo){

        $this->db->select('*');
        $this->db->from('maepro');
        $this->db->where('codprod01', $codigo);
        $this->db->where('prodsinsdo01','S');
        $result = $this->db->get();
        $dato = $result->result_array();
         $flag = 0;
         if(count($dato) > 0){
            $flag = 1;
         }

         return $flag;
    }
    function get_producto_por_codigo($codigo)
    {
        $this->db->select('codprod01 as codigo, desprod01 as descripcion,
                         cantact01 as stock, cantact01 as cantidad_actual,
                         valact01 as valor_actual, precuni01 as precio_unitario');
        $this->db->from("maepro");
        $this->db->where('codprod01', $codigo);
        $result = $this->db->get();
        $result = $result->result_array();
        return $result[0];

    }

    function generar_movimiento_producto($datos)
    {
        $this->db->insert("movpro", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }    
}
?>