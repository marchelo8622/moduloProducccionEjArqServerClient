<?php

include_once(APPPATH . "models/base_model.php");

Class Cliente_model extends Base_model {

    private $matriz;

    function __construct() {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }

    function get_cliente() {
        //$this->db->where('codcate BETWEEN "'. 1.01. '" and "'. 1.02.'"');
       $sql = $this->db->query("SELECT * FROM ".$this->matriz.".maecte INNER JOIN ".$this->matriz.".categorias ON codcate = catcte01 AND alias != ''");
       return $sql->result();
    }

    function get_cliente_by($id, $nombre) {
        
        if ($id != "null" && $nombre != "null") {
            $this->db->limit(20);
        }

        if ($id == "null" && $nombre == "null") {
            $this->db->limit(50);
        }


        if ($id != "null") {
            $this->db->like('codcte01', $id);
        }

        if ($nombre != "null") {
            $this->db->like('nomcte01', $nombre);
        }
//        $this->db->where("alias", '2');
        $this->db->select('cascte01, catcte01, cobrcte01, codcate, codcatep, codcte01, condpag01, coniva01, ctacgcte01, desccate, dircte01, emailcte01, loccte01, nomcte01, pagleg01, telcte01, telcte01b, tipcte01, tipocate, totexceso01, vendcte01, sdoact01');
        $this->db->from($this->matriz.".maecte");
        $this->db->join($this->matriz.'.categorias', 'codcate = catcte01');
        $this->db->group_by('codcte01');
        $this->db->order_by('nomcte01','asc');
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }


    function get_cliente_by_codigo($codigo)
    {
        $this->db->select('codcte01 as codigo_cliente, nomcte01 as nombre_cliente, dircte01 as direccion,   loccte01 as localidad, telcte01 as telefono, vendcte01 as vendedor, condpag01 as condPago, emailcte01 as email, catcte01 as categoria, sdoact01 as saldo_actual, totexceso01 as saldo_exceso, pagleg01 as contacto,cascte01 as ci_ruc, (select ad1tab from maetab where catcte01 = ad7tab and numtab = "01" and codtab = "VPP") as porcentaje_portfee');
        $this->db->from($this->matriz.".maecte");
        $this->db->where('codcte01', $codigo);
        $result = $this->db->get();
        //echo $this->db->last_query();
        $result = $result->result();
        return $result[0];
    }

    function retornaPrecio($idCliente, $idProducto, $numVuelo, $tipoAvion){
        $sql = $this->db->query('Select catcte01 from '.$this->matriz.'.maecte where codcte01 = "'.$idCliente.'"');
        $cat = $sql->result_array();
        $categoria = $cat[0]['catcte01'];
        if($categoria = 1.01){
        $sql = $this->db->query('Select precio, cantidad from configurar_productos where configurar_productos.id_tipo_avion="'.$tipoAvion.'" AND configurar_productos.numero_vuelo="'.$numVuelo.'" AND configurar_productos.codigo="'.$idProducto.'"');
            $datos = $sql->result_array();
            if(empty($datos)){
                $tipo_precio = 0;
            }else{
                $cantidad = $datos[0]['cantidad'];
                $precio = $datos[0]['precio'];
                $tipo_precio = $precio / $cantidad;
                $arrayTipoPrecio = array(
                'tipo_precio' => $tipo_precio
                );
            }
   
        if($tipo_precio == 0 || $tipo_precio == ''){
            $this->db->select('precte01');
            $this->db->from($this->matriz.'.maecte');
            $this->db->where('codcte01',$idCliente);
            $sql = $this->db->get();
            $preciocte = $sql-> result_array();
            return $this->clientePrecioProducto($idProducto, $preciocte[0]['precte01'], $numVuelo, $tipoAvion);
            }

        return $arrayTipoPrecio;
        }else{

            $this->db->select('precte01');
            $this->db->from($this->matriz.'.maecte');
            $this->db->where('codcte01',$idCliente);
            $sql = $this->db->get();
            $preciocte = $sql-> result_array();
            return $this->clientePrecioProducto($idProducto, $preciocte[0]['precte01'], $numVuelo, $tipoAvion);
        }
    }

    function clientePrecioProducto($idProducto, $preciocte, $numVuelo, $tipoAvion){
    
            $sql = $this->db->query('Select precvta01, precio201, precio301, precio401, precio501, precio601, precio701, precio801, precio901, precio1001, precio1101, precio1201 from maepro where codprod01 ="'.$idProducto.'" limit 1');
            $datos = $sql->result_array();             
  
    switch ($preciocte) {
        case 1:
            $tipo_precio = $datos[0]['precvta01'];
            break;
        case 2:
            $tipo_precio = $datos[0]['precio201'];
            break;
        case 3:
            $tipo_precio = $datos[0]['precio301'];
            break;
        case 4:
            $tipo_precio = $datos[0]['precio401'];
            break;
        case 5:
            $tipo_precio = $datos[0]['precio501'];
            break;
        case 6:
            $tipo_precio = $datos[0]['precio601'];
            break;
        case 7:
            $tipo_precio = $datos[0]['precio701'];
            break;
        case 8:
            $tipo_precio = $datos[0]['precio801'];
            break;
        case 9:
            $tipo_precio = $datos[0]['precio901'];
            break;
        case 10:
            $tipo_precio = $datos[0]['precio1001'];
            break;
        case 11:
            $tipo_precio = $datos[0]['precio1101'];
            break;
        case 12:
            $tipo_precio = $datos[0]['precio1201'];
            break;
        default:
            $tipo_precio = $datos[0]['precvta01'];
        }
        $arrayTipoPrecio = array(
            'tipo_precio' => $tipo_precio
            );

        return $arrayTipoPrecio;
    }

    function cliente_interno(){

      $this->db->where("numtab", '01'); 
      $this->db->where("codtab", 'CIG'); 
      $this->db->select('ad7tab')->from($this->matriz.".maetab");
      $resultado = $this->db->get();
       $dato = $resultado->result_array();

       return $dato[0]['ad7tab'];
    }

    function get_cliente_by_categoria($id, $nombre, $categoria) {
        
        if ($id != "null" && $nombre != "null") {
            $this->db->limit(20);
        }

        if ($id == "null" && $nombre == "null") {
            $this->db->limit(50);
        }


        if ($id != "null") {
            $this->db->like('codcte01', $id);
        }

        if ($nombre != "null") {
            $this->db->like('nomcte01', $nombre);
        }
//        $this->db->where("alias", '2');
        $this->db->select('cascte01, catcte01, cobrcte01, codcate, codcatep, codcte01, condpag01, coniva01, ctacgcte01, desccate, dircte01, emailcte01, loccte01, nomcte01, pagleg01, telcte01, telcte01b, tipcte01, tipocate, totexceso01, vendcte01');
        $this->db->from($this->matriz.".maecte");
        $this->db->join($this->matriz.'.categorias', 'codcate = catcte01');
        $this->db->where("catcte01", $categoria);
        $this->db->group_by('codcte01');
        $this->db->order_by('nomcte01','asc');
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function actualizar_saldos_cliente($datos)
    {
        $data = array(
            'sdoact01' => $datos['saldo_actual'],
            'acucrm01' => $datos['saldo_actual'],
            'acucre01' => $datos['saldo_actual']
        );
        $this->db->where('codcte01', $datos['codigo_cliente']);
        $result = $this->db->update('maecte', $data);
        return $result;

    }

    function portfee_by_categoria($categoria)
    {

        $this->db->where("numtab", '01'); 
        $this->db->where("codtab", 'VPP'); 
        $this->db->where("ad7tab", $categoria); 
        $this->db->select('ad1tab')->from($this->matriz.".maetab");
        $resultado = $this->db->get();
        $dato = $resultado->result_array();

        return $dato[0];
    }

    function portfee()
    {
        $this->db->where("numtab", '01'); 
        $this->db->where("codtab", 'VPP'); 
        $this->db->select('ad1tab')->from($this->matriz.".maetab");
        $resultado = $this->db->get();
        $dato = $resultado->result_array();

        return $dato[0]['ad1tab'];
    }


}

?>
