<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Cliente_model', 'Cliente');
    }

    function clientes_get(){
        $maecte = $this->Cliente->get_cliente();
        echo format_response($maecte);
    }

}
