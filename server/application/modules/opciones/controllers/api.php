<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Opcion_model', 'Opcion');
    }
    
    function opcion_get()
    {
        $opcion = $this->Opcion->get_opcion();
        echo format_response($opcion);
    }
    
}
