<?php
include_once(APPPATH ."models/base_model.php");
Class Dar_baja_pedidos_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_pedidos_semidespachados($codigo_cliente, $nombre_cliente, $numero_pedido)
    {
        if ($codigo_cliente != "null") {
            $this->db->like('codcte30', $codigo_cliente);
        }

        if ($nombre_cliente != "null") {
            $this->db->like('nomcte30', $nombre_cliente);
        }

        if ($numero_pedido != "null") {
            $this->db->where('nopedido30', $numero_pedido);
        }

        $estados = array('03','15');
        $this->db->select('nopedido30, codprod30, codcte30, fecpedido30, nomcte30, novend30, localid30, condpag30, 
            descto30, iva30, status30, cantped30, precuni30, cantfac30, cantafac30, uid, bodega30, dividir_pedido,maetab.nomtab as estado');
        $this->db->from("maeped30");
        $this->db->join("maetab","maetab.codtab = maeped30.status30 and maetab.numtab = '49'");
        $this->db->where_in("status30", $estados);
        $this->db->order_by("fecpedido30", "DESC");
        $this->db->group_by("codcte30","nopedido30");
        //$this->db->limit(20);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }    
   
}
?>
