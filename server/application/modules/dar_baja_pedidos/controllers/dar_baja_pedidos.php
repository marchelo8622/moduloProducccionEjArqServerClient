<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class dar_baja_pedidos extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Dar_baja_pedidos";
		$data["angular_app"] = "Dar_baja_pedidos";
		$this->layout->view("dar_baja_pedidos", $data);
	}
}

