<?php
//ini_set("display_errors",'1');
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {
    private $serie_sri;
    private $uid;  

    function __construct()
    {
        parent::__construct();
        $this->load->model("Dar_baja_pedidos_model", "dar_baja_pedidos");
        $this->load->model('plan_diario/plan_diario_model', 'plan_diario');
        $this->load->model('producto/producto_model', 'Producto');
        $this->load->model("facturacion_pedidos/Facturacion_pedidos_model", "facturacion_pedidos");      
        $this->pedidos = array();
        $this->serie_sri = $this->get_serie_sri();
        session_start();
        $this->uid = $_SESSION['UID'];
    }

    function pedidos_semidespachados_get($codigo_cliente = 'null', $nombre_cliente = 'null', $numero_pedido = 'null')
    {
        $this->pedidos = $this->dar_baja_pedidos->get_pedidos_semidespachados($codigo_cliente, $nombre_cliente, $numero_pedido);
        echo format_response($this->pedidos);
        
    }



    function dar_baja_pedidos_post()
    {
        $datos = $this->post();        
        $this->generar_egresos_productos($datos['data']);
        $egreso = array('resp'=> 'ok');
        echo format_response($egreso);        
                
    }

    function generar_egresos_productos($datos)
    {
        
        $secuencial_ingreso_pedidos = $this->plan_diario->get_numero_documento('41', '51.3');
        $secuencial_transaccion = (int) $secuencial_ingreso_pedidos + 1;
        $numero_egreso = $this->serie_sri . str_pad($secuencial_transaccion,9,'0',STR_PAD_LEFT);

        foreach ($datos as $key => $producto) {            
            $diferencia_a_fact = $producto['cantped30'] - $producto['cantfac30'];
            if($diferencia_a_fact > 0){
                $datos_producto = $this->Producto->get_producto_por_codigo($producto['codprod30']);
                $valor_transaccion = (float) $datos_producto['precio_unitario'] * (float)$diferencia_a_fact;
                $cantidad_actual = (float)$datos_producto['cantidad_actual'] - (float)$diferencia_a_fact;
                $valor_actual = (float)$datos_producto['valor_actual'] - (float) $valor_transaccion;
                $precio_unitario = 0;
                if ((float)$cantidad_actual > 0)
                    $precio_unitario = (float) $valor_actual / (float) $cantidad_actual;

                $egreso = array(
                    'TIPOTRA03'         => '51.3',
                    'NOCOMP03'          => $numero_egreso,
                    'OCURREN03'         => str_pad($key, 4, '0', STR_PAD_LEFT),
                    'CODPROD03'         => $producto['codprod30'],
                    'FECMOV03'          => date('Y-m-d H:i:s'),
                    'TIPTRAN03'         => '',
                    'CANTID03'          => $diferencia_a_fact,
                    'VALOR03'           => $valor_transaccion,
                    'PU03'              => $datos_producto['precio_unitario'],
                    'CODDEST03'         => '',
                    'CANTACT03'         => $cantidad_actual,
                    'VALACT03'          => $valor_actual,
                    'PRECUNI03'         => $precio_unitario,
                    'UID'               => $this->uid
                    );

                $this->Producto->generar_movimiento_producto($egreso);
                $data = array(
                        'cantidad_actual' => $cantidad_actual,
                        'valor_actual' => $valor_actual,
                        'precio_unitario' => $precio_unitario,
                        'codigo' => $producto['codprod30']
                        );
                $this->Producto->actualizar_inventario_producto($data);
                $this->plan_diario->update_numero_documento('41', '51.3', $secuencial_transaccion);
                
            }  

                 $estado = array(
                    'estado_pedido' =>'04',
                    'numero_pedido' => $producto['nopedido30']
                    );
                $this->facturacion_pedidos->actualizar_estado_pedido($estado);
            
        }

    }

    function get_serie_sri()
    {
        $serie_sri = $this->plan_diario->get_serie_sri();
        return $serie_sri;
    } 
}
?>
