<?php
include_once(APPPATH ."models/base_model.php");
Class Busqueda_contrato_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_busqueda_contrato($id_cliente)
    {
        $this->db->where("campo", 1);
        $this->db->from("tabla");
        $result = $this->db->get();
        return $result->result();

    }

    function edit_busqueda_contrato($busqueda_contrato_id) {
    
        $this->db->where('id');
        $this->db->update('contratos');
        $result = $this->db->get();
        return $result->result();
    }

    function new_busqueda_contrato($busqueda_contrato_id) {
    

        $this->db->where('id');
        $this->db->insert('contratos');
        $result = $this->db->get();
        return $result->result();

    }


   function get_contrato($resultado)
    {
        $this->db->where('codcliente', $resultado);
        $this->db->from("contratos");
        $this->db->order_by('idContrato','desc');
        $result = $this->db->get();
        return $result->result();
    }


}
?>
