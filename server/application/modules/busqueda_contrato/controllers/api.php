<?php
defined("BASEPATH") OR exit("No direct script access allowed");
//ini_set("display_errors",1);
require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {

    function __construct()
    {
       
        parent::__construct();
        $this->load->model("Busqueda_contrato_model", "busqueda_contrato");
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model('categorias/Categoria_model', 'Categoria');
        $this->load->model('contrato/Contrato_model', 'Contrato');
        $this->load->model('avion/Avion_model', 'Avion');
        $this->load->model('Pre_plan/Pre_plan_model', 'Pre_plan');
        $this->load->model('producto/Producto_model', 'Producto');
        $this->load->model('productos_contrato/Productos_contrato_model', 'Productos_contrato');
        $this->load->model('configurar_producto/Configurar_producto_model', 'Configurar_producto');
        $this->load->model('opciones/Opcion_model', 'Opcion');
        $this->load->model('cliente_contrato/Cliente_contrato_model', 'Cliente_contrato');
        $this->load->model('Frecuencia_contrato/Frecuencia_contrato_model', 'Frecuencia_contrato');
    }

    function busqueda_contrato_get()
    {
        $busqueda_contrato_id = $this->get('id');
        $busqueda_contrato = array();

        if(!empty($busqueda_contrato_id)){
            $busqueda_contrato = $this->Busqueda_contrato
                                 ->get_busqueda_contrato_by($busqueda_contrato_id)
                                 ->result_array();
        }
        
        echo format_response($busqueda_contrato); 
    }

    function busqueda_contrato_put()
    {
        // create a new busqueda_contrato and respond with a status/errors
        // return data = echo format_response($data);
    }

    function busqueda_contrato_post()
    {
        // $contratos = $this->post();
        // $contratos = $contratos['data'];
        // $resultado = $this->busqueda_contrato->get_contrato()->result_array();
        // echo format_response($resultado);
    }

    function busqueda_contrato_delete()
    {
        // delete a configurar_producto and respond with a status/errors
        // return data = echo format_response($data);
    }

    function clientes_get($codigo = '') {
        $maecte = $this->Cliente->get_cliente($codigo);
        echo format_response($maecte);
    }
    
    function categoria_get() {
        $categorias = $this->Categoria->get_categoria();
        echo format_response($categorias);
    }                       
    
    function contratos_get($codigo, $tipo, $desde, $hasta) {
        $contratos = $this->Contrato->get_contratos($codigo, $tipo, $desde, $hasta);
        echo format_response($contratos);
        //print_r($contratos);
    }
    function contratos_filtros_get($codigo, $tipo, $desde, $hasta) {
        $contratos = $this->Contrato->get_contratos_filtros($codigo, $tipo, $desde, $hasta);

        if($desde != 'undefined' && $hasta != 'undefined'){

            $nuevo_arreglo_contratos = array();

            foreach ($contratos as $contrato) {

                 $valida_rango_fecha_desde = $this->Pre_plan->check_in_range($desde, $hasta, $contrato->desde);
                 $valida_rango_fecha_hasta = $this->Pre_plan->check_in_range($desde, $hasta, $contrato->hasta);

                 if(($valida_rango_fecha_desde && $valida_rango_fecha_hasta) || (!$valida_rango_fecha_desde && $valida_rango_fecha_hasta) || ($valida_rango_fecha_desde && !$valida_rango_fecha_hasta)){     
                    $nuevo_arreglo_contratos[] = $contrato;
                 }
                
            }

            echo format_response($nuevo_arreglo_contratos);

       }else{

           echo format_response($contratos);

       }
    }

    function avion_get() {
        $avion = $this->Avion->get_avion();
        echo format_response($avion);
    }
    
    function productos_get($codigo) {
        $productos = $this->Producto->get_productos($codigo, $descripcion);
        echo format_response($productos);
    }
    
    function cliente_post() {
        $filter = $this->post();
        var_dump($filter);
    }
    
    function contratos_by_id_get($codigo, $idContrat, $numV, $aerolinea){
        $data = $this->Contrato->get_contrato_by_id($codigo, $idContrat, $numV, $aerolinea);
        echo format_response($data);
    }
    
    function opcion_get(){
        $data['opciones'] = $this->Opcion->get_opcion();
        echo format_response($data);
    }
    function productos_contrato_by_id_get($codigo_producto){
        $productos_contrato = $this->Productos_contrato->get_productos_contrato_by_id($codigo_producto);
        echo format_response($productos_contrato);
    }

    function guardar_contratos_post() {
        $data_principal = $this->post();
        
            $delete_clienteContrato = $this->Cliente_contrato->delete($data_principal['data']['data_clienteContrato'],$idContrato, $codCliente);
            $delete_contrato = $this->Contrato->delete($data_principal['data']['data_contratos'],$idContrato, $codCliente);
            $delete_frecuencia = $this->Frecuencia->delete($data_principal['data']['data_frecuencias'],$idContrato, $codCliente);
            $delete_productos = $this->Productos->delete($data_principal['data']['data_productos'],$idContrato, $codCliente);
            $delete_opciones = $this->Opcion->delete($data_principal['data']['data_opciones'],$idContrato, $codCliente);
            
            $clienteContrato = $this->Cliente_contrato->post_guardar($data_principal['data']['data_clienteContrato']);        
            $contratos = $this->Contrato->post_guardar($data_principal['data']['data_contratos']);
            $frecuencia = $this->Frecuencia_contrato->post_guardar($data_principal['data']['data_frecuencias']);
            $productos = $this->Productos_contrato->post_guardar($data_principal['data']['data_productos']);
            $opciones = $this->Opcion->post_guardar($data_principal['data']['data_opciones']);
            
            echo format_response($data_principal);
    }

    function editar_contratos_put($idContrato, $codCliente) {
            $data_principal = $this->put();    

            $update_clienteContrato = $this->Cliente_contrato->update($data_principal['data']['data_clienteContrato'],$idContrato, $codCliente);
            $update_contrato = $this->Contrato->update($data_principal['data']['data_contratos'],$idContrato, $codCliente);

            $update_frecuencia = $this->Frecuencia_contrato->delete($data_principal['data']['data_frecuencias'],$idContrato, $codCliente);
            $update_productos = $this->Productos_contrato->delete($data_principal['data']['data_productos'],$idContrato, $codCliente);
            $update_opciones = $this->Opcion->delete($data_principal['data']['data_opciones'],$idContrato, $codCliente); 
            
            $frecuencia = $this->Frecuencia_contrato->post_guardar($data_principal['data']['data_frecuencias']);
            $productos = $this->Productos_contrato->post_guardar($data_principal['data']['data_productos']);
            $opciones = $this->Opcion->post_guardar($data_principal['data']['data_opciones']);


            echo format_response($data_principal);
    }

    function eliminar_contratos_delete($idContrato, $codCliente) {

            $data_principal = $this->delete();    

            $delete_clienteContrato = $this->Cliente_contrato->delete($data_principal['data']['data_frecuencias'],$idContrato, $codCliente);
            $delete_contratos = $this->Contrato->delete($data_principal['data']['data_productos'],$idContrato, $codCliente);
            $delete_frecuencia = $this->Frecuencia_contrato->delete($data_principal['data']['data_frecuencias'],$idContrato, $codCliente);
            $delete_productos = $this->Productos_contrato->delete($data_principal['data']['data_productos'],$idContrato, $codCliente);
            $delete_opciones = $this->Opcion->delete($data_principal['data']['data_opciones'],$idContrato, $codCliente); 
            echo format_response($data_principal);
    }

    function crear_contratos_post() {
        $data_principal = $this->post();        
            
            $clienteContrato = $this->Cliente_contrato->post_guardar($data_principal['data']['data_clienteContrato']);        
            $contratos = $this->Contrato->post_guardar($data_principal['data']['data_contratos']);
            $frecuencia = $this->Frecuencia_contrato->post_guardar($data_principal['data']['data_frecuencias']);
            $productos = $this->Productos_contrato->post_guardar($data_principal['data']['data_productos']);
            $opciones = $this->Opcion->post_guardar($data_principal['data']['data_opciones']);

            echo format_response($data_principal);
    }

    function filtro_productos_by_categoria_get($descripcion, $codigo, $alias){
        $productos = $this->Productos_contrato->get_filtro_productos_by_categoria($descripcion, $codigo, $alias);
        echo format_response($productos);
    }
    function productos_tipo_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_tipo($codigo, urldecode($descripcion), 'null');
        echo format_response($productos);
    }

    function categoria_by_id_get($tipo){
        $categorias_contrato = $this->Categoria->get_categoria_by_id($tipo);
        echo format_response($categorias_contrato);
    }
    function productos_by_categoria_get($categoria, $alias){
        $productos_categoria = $this->Productos_contrato->get_productos_by_categoria($categoria, $alias);        
        echo format_response($productos_categoria);
    }

    function productos_by_categoria2_get($categoria, $alias,$cliente,$avion,$vuelo){
        $productos_categoria = $this->Productos_contrato->get_productos_by_categoria($categoria, $alias);
        $new_array = array();
        
        foreach ($productos_categoria as $value) {
            
            $prod_config = $this->Configurar_producto->get_producto_configurado($value->codigo, $cliente,$avion,$vuelo);
            
            if(count($prod_config) > 0){

               $new_array[] = array(
                    'codigo' => $value->codigo,
                    'descripcion' => $value->descripcion,
                    'stock' => $value->stock
                ); 
            }
        }

        $finalArray = (object)$new_array;

        echo format_response($finalArray);
    }

    function productos_contrato_opcion_get($codigo){
        $opcion = $this->Productos_contrato->get_productos_contrato_opcion($codigo);
        echo format_response($opcion);    
    }

    function cliente_by_id_get($codigo, $idContrat){
        $cliente = $this->Clientes->get_cliente_by_id($codigo, $idContrat);
        echo format_response($cliente);    
    }

    function contrato_by_datos_get($codigo){
        $contrato = $this->Contrato->get_contrato_by_datos($codigo);
        echo format_response($contrato);    
    }

    function categoria_by_client_get($codigo){
        $categoria = $this->Categoria->get_categoria_by_client($codigo);
        echo format_response($categoria);    
    }

    function avion_by_cod_get($codigo,$cliente){
       
        $array_consolidado = array();        
        $avion = $this->Avion->get_avion_by_cod($codigo);
        $num_vuelo = $this->Configurar_producto->get_num_de_vuelo($codigo,$cliente);
        
        if(count($avion) > 0 && count($num_vuelo) > 0){

            $array_consolidado[] = array(
                    'numtab' => $avion[0]['numtab'],
                    'codtab' => $avion[0]['codtab'],
                    'nomtab' => $avion[0]['nomtab'],
                    'ad1tab' => $avion[0]['ad1tab'],
                    'ad2tab' => $avion[0]['ad2tab'],
                    'ad3tab' => $avion[0]['ad3tab'],
                    'ad4tab' => $avion[0]['ad4tab'],
                    'ad5tab' => $avion[0]['ad5tab'],
                    'ad6tab' => $avion[0]['ad6tab'],
                    'ad7tab' => $avion[0]['ad7tab'],
                    'ad8tab' => $avion[0]['ad8tab'],
                    'block' => $avion[0]['block'],
                    'ad9tab' => $avion[0]['ad9tab'],
                    'ad0tab' => $avion[0]['ad0tab'],
                    'numero_vuelo' => $num_vuelo[0]['numero_vuelo']
                );
        }
                
        echo format_response($array_consolidado);

    }

    function configuracion_get($numVuelo, $tipoAvion, $idProducto){
        $configuracion = $this->Configurar_producto->get_configuracion($numVuelo, $tipoAvion, $idProducto);
        echo format_response($configuracion);
        //print_r($configuracion);
    }

    function contrato_by_client_get($codigo){

        $contrato = $this->Contrato->get_contrato_by_client($codigo);
        if(count($contrato)==0){
            $idContrato = 1;
            $idContratoFinal = str_pad(round($idContrato,0),9,'0',STR_PAD_LEFT);
        
        }else{
            $idContrato = $contrato[0]["idContrato"] + 1;
            $idContratoFinal = str_pad(round($idContrato,0),9,'0',STR_PAD_LEFT);
        //print_r($idContratoFinal);
        }   
        echo format_response($idContratoFinal); 
    }
    function retornaPrecio_get($idCliente, $idProducto, $numVuelo, $tipoAvion){
        $precios = $this->Cliente->retornaPrecio($idCliente, $idProducto, $numVuelo, $tipoAvion);
        echo format_response($precios);

    }

    function genera_pdf_contratos_post(){
        session_start();
        $uid = $_SESSION['user'];

        $filter = $this->post();

        $contrato = $filter['data'];
        $codCliente = $filter['codigo'];
        $tipo = $filter['tipo'];
        $desde = $filter['desde'];
        $hasta = $filter['hasta'];
        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Marcelo Montalvo');
        $pdf->SetTitle('Servicio Contratado');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
        $pdf->setFontSubsetting(true);
 
        $pdf->SetFont('helvetica', '', 8, '', true);

        $pdf->AddPage();
 
// Establecemos el contenido para imprimir
        
        //preparamos y maquetamos el contenido a crear
        $html = '';
        $html .= "<style type=text/css>";
        $html .= "table  , th {border: 1px solid black; text-align:left; } td{border: 1px solid black;}";
        $html .= "th{color: #000; background-color: #fff;}";
        $html .= ".alineacion{text-align:center; vAlign=middle; font-weight: bold;}";
        $html .= ".sbordes{scope='row' rowspan='10'}";
        $html .= "tr{color: #000;}";
        $html .= "h1{color: black; font-weight: bold;  text-align:left;}";
        $html .= "</style>";

        //$html .= "<h2>Localidades de ".$prov."</h2><h4>Actualmente: ".count($provincias)." localidades</h4>";
        $html .= "<h3>GODDARD CATERING GROUP QUITO S.A.</h3>";
        $html .= "<h4>Resumen Precios Clientes</h4>";
         $html .= "<table cellspacing='0' cellpadding='4' width='100%'>";
        $infoCabecera = $this->Contrato->get_contratos($codCliente, $tipo, $desde, $hasta);
        $html .= "<tr>
                    <td width='30%'>Cliente: ".$infoCabecera[0]['nomCliente']."<br>Ci: ".$infoCabecera[0]['codigo']."
                    </td>

                    <td class='sbordes'></td>

                    <td width='30%'>Tipo Avion: ".$infoCabecera[0]['tipo']."<br># Vuelo: ".$infoCabecera[0]['numV']."</td>

                    <td class='sbordes'></td>

                    <td width='30%'>Contacto<br>
                    </td>
                 </tr>";
        $html .= '<tr><td colspan="5" height="12" border="0">&nbsp;</td></tr>';
        $html .= "</table>";
         $html .= "<table cellspacing='0' cellpadding='4' border='1' width='100%'>";
        $html .= '<tr bgcolor="#0066CC"; >
                    <th class="alineacion" width="8%">CODIGO</th>
                    <th class="alineacion" width="12%">DESCRIPCION</th>
                    <th class="alineacion" width="7%">CLASE</th>
                    <th class="alineacion" width="9%">PORCION</th>
                    <th class="alineacion" width="9%">PRECIO ACTUAL</th>
                    <th class="alineacion" width="11%">FECHA CREACION</th>
                    <th class="alineacion" width="10%">UID</th>
                    <th class="alineacion" width="9%">FECHA INICIO</th>
                    <th class="alineacion" width="9%">FECHA FINAL</th>
                    <th class="alineacion" width="16%">OBSERVACIONES</th>
                </tr>';
        
        $infocontrato = $this->Contrato->get_contrato_by_id_contrato($contrato, $codCliente);
        //$infoconfiguracion = $this->Configurar_producto->get_configuracion($numVuelo, $tipoAvion, $idProducto);
        foreach ($infocontrato  as $valor) 
        {            
             if($valor["precio"]== 0 || $valor["precio"]== ''){
                $precio = $valor["opprecio"];
            }else{
                $precio = number_format($valor["precio"] / $valor["cantidad"],2,'.','');
            }
            $html .= '<tr>
                        <td class="sbordes">' . $valor["codigo_producto"] . '</td>
                        <td class="sbordes">' . $valor["descripcion"] . '</td>
                        <td class="sbordes">' . $valor["nomtab"] . '</td>
                        <td class="sbordes">1 EA</td>
                        <td class="sbordes">' .$precio. '</td>
                        <td class="sbordes">' . $valor["fecha_creacion"] . '</td>
                        <td class="sbordes">'.$uid.'</td>
                        <td class="sbordes">' . $valor["fecha_inicio"] . '</td>
                        <td class="sbordes">' . $valor["fecha_fin"] . '</td>
                        <td class="sbordes"></td>
                    </tr>';
        }
        $html .= "</table>";
        $html .= "<table cellspacing='0' cellpadding='4' width='100%'>";
            $html .= '<tr>
                        <td colspan="5" height="12">Elaborado por:<br>Firma:</td>
                        <td colspan="5" height="12">Autorizado por:<br>Firma:</td>    
                        
                    </tr>';

        $html .= "</table>";

// Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 1, $ln = 1, $fill = 0, $reseth = true, $align = '');
 

        $nombre_archivo = utf8_decode("contratoGoddard".$contrato.".pdf");
        $pdf->Output($nombre_archivo, 'I');
    


    }

}
?>
