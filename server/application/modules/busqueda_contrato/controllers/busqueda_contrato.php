<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class busqueda_contrato extends MX_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('contrato/Contrato_model', 'Contrato');
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Busqueda_contrato";
		$data["angular_app"] = "Busqueda_contrato";
		$this->layout->view("busqueda_contrato", $data);

	}

	
}

