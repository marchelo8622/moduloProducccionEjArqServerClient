<?php

defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
error_reporting(0);
//ini_set("display_errors",1);

class api extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Pre_plan_model", "Pre_plan");
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model('producto/Producto_model', 'Producto');
    }


    function pre_plan_get($desde, $hasta) {
        session_start();
        $uid = $_SESSION['UID'];
        $datos_depurados_contratos = array();
        $data_contratos = array();
        //$prePlan = array();
        $prePlan = $this->Pre_plan->get_pre_plan($desde, $hasta);
        if(count($prePlan) == 0){
           $prePlan = Array(); 
        }

        
        $data_contratos = $this->Pre_plan->get_contratos($desde, $hasta);
        
        
        $datos_depurados_contratos = $this->depuracion_contratos_rangos_fecha($data_contratos,$desde,$hasta);
        $prePlanContratos = array();

        if(empty($datos_depurados_contratos['criterio2']) && empty($datos_depurados_contratos['criterio3']) && !empty($datos_depurados_contratos['criterio1'])){
           $prePlanContratos = $datos_depurados_contratos['criterio1'];
        }        

        if(!empty($datos_depurados_contratos['criterio2']) && empty($datos_depurados_contratos['criterio3']) && empty($datos_depurados_contratos['criterio1'])){
            $prePlanContratos = $datos_depurados_contratos['criterio2'];
        }

        if(empty($datos_depurados_contratos['criterio2']) && !empty($datos_depurados_contratos['criterio3']) && empty($datos_depurados_contratos['criterio1'])){
            $prePlanContratos = $datos_depurados_contratos['criterio3'];
        }

        if(!empty($datos_depurados_contratos['criterio2']) && !empty($datos_depurados_contratos['criterio3']) && !empty($datos_depurados_contratos['criterio1'])){
            $prePlanContratos = array_merge($datos_depurados_contratos['criterio1'],$datos_depurados_contratos['criterio2'],$datos_depurados_contratos['criterio3']);
        }

        if(empty($datos_depurados_contratos['criterio2']) && !empty($datos_depurados_contratos['criterio3']) && !empty($datos_depurados_contratos['criterio1'])){
            $prePlanContratos = array_merge($datos_depurados_contratos['criterio1'],$datos_depurados_contratos['criterio3']);
        }

        if(!empty($datos_depurados_contratos['criterio2']) && !empty($datos_depurados_contratos['criterio3']) && empty($datos_depurados_contratos['criterio1'])){
            $prePlanContratos = array_merge($datos_depurados_contratos['criterio2'],$datos_depurados_contratos['criterio3']);
        }

        if(!empty($datos_depurados_contratos['criterio2']) && empty($datos_depurados_contratos['criterio3']) && !empty($datos_depurados_contratos['criterio1'])){
            $prePlanContratos = array_merge($datos_depurados_contratos['criterio1'],$datos_depurados_contratos['criterio2']);
        }


        if(count($prePlanContratos) > 0){
        


        foreach ($prePlanContratos as  $valor) {
            $arrayPrePlanContratosLunes = Array();

            if($valor["lunes"] == 1){
                $arrayPrePlanContratosLunes = Array(

                    'id_cliente' => $valor["codcliente"],
                    'name_client' => $valor["name_client"],
                    'id_producto' => $valor["idProducto"],
                    'name_product' => $valor["name_product"],
                    'dia' => 1,
                    'opcion' => $valor["indiceOpciones"],
                    'cantidad' => $valor["cantidad"],
                    'codigo' => $valor["idContrato"],
                    'estado' => 0

                    );
            }
            
            $arrayPrePlanContratosMartes = Array();
            if($valor["martes"] == 1){
                $arrayPrePlanContratosMartes = Array(
                    'id_cliente' => $valor["codcliente"],
                    'name_client' => $valor["name_client"],
                    'id_producto' => $valor["idProducto"],
                    'name_product' => $valor["name_product"],
                    'dia' => 2,
                    'opcion' => $valor["indiceOpciones"],
                    'cantidad' => $valor["cantidad"],
                    'codigo' => $valor["idContrato"],
                    'estado' => 0

                    );
            }


            $arrayPrePlanContratosMiercoles = Array();
            if($valor["miercoles"] == 1){
                $arrayPrePlanContratosMiercoles = Array(
                    'id_cliente' => $valor["codcliente"],
                    'name_client' => $valor["name_client"],
                    'id_producto' => $valor["idProducto"],
                    'name_product' => $valor["name_product"],
                    'dia' => 3,
                    'opcion' => $valor["indiceOpciones"],
                    'cantidad' => $valor["cantidad"],
                    'codigo' => $valor["idContrato"],
                    'estado' => 0

                    );
            }

            $arrayPrePlanContratosJueves = Array();
            if($valor["jueves"] == 1){
                $arrayPrePlanContratosJueves = Array(
                    'id_cliente' => $valor["codcliente"],
                    'name_client' => $valor["name_client"],
                    'id_producto' => $valor["idProducto"],
                    'name_product' => $valor["name_product"],
                    'dia' => 4,
                    'opcion' => $valor["indiceOpciones"],
                    'cantidad' => $valor["cantidad"],
                    'codigo' => $valor["idContrato"],
                    'estado' => 0

                    );
            }
            
            $arrayPrePlanContratosViernes = Array();
            if($valor["viernes"] == 1){
                $arrayPrePlanContratosViernes = Array(
                    'id_cliente' => $valor["codcliente"],
                    'name_client' => $valor["name_client"],
                    'id_producto' => $valor["idProducto"],
                    'name_product' => $valor["name_product"],
                    'dia' => 5,
                    'opcion' => $valor["indiceOpciones"],
                    'cantidad' => $valor["cantidad"],
                    'codigo' => $valor["idContrato"],
                    'estado' => 0

                    );
            }
              $arrayPrePlanContratosSabado = Array();
            if($valor["sabado"] == 1){
                $arrayPrePlanContratosSabado = Array(
                    'id_cliente' => $valor["codcliente"],
                    'name_client' => $valor["name_client"],
                    'id_producto' => $valor["idProducto"],
                    'name_product' => $valor["name_product"],
                    'dia' => 6,
                    'opcion' => $valor["indiceOpciones"],
                    'cantidad' => $valor["cantidad"],
                    'codigo' => $valor["idContrato"],
                    'estado' => 0

                    );
            }

            $arrayPrePlanContratosDomingo = Array();

            if($valor["domingo"] == 1){
                $arrayPrePlanContratosDomingo = Array(
                    'id_cliente' => $valor["codcliente"],
                    'name_client' => $valor["name_client"],
                    'id_producto' => $valor["idProducto"],
                    'name_product' => $valor["name_product"],
                    'dia' => 7,
                    'opcion' => $valor["indiceOpciones"],
                    'cantidad' => $valor["cantidad"],
                    'codigo' => $valor["idContrato"],
                    'estado' => 0

                    );
            }

            //$arrayPrePlanContratosFin[] = array_merge($arrayPrePlanContratosLunes,$arrayPrePlanContratosMartes,$arrayPrePlanContratosMiercoles,$arrayPrePlanContratosJueves,$arrayPrePlanContratosViernes,$arrayPrePlanContratosSabado,$arrayPrePlanContratosDomingo);

              $arrayPrePlanContratosFin[] = Array(

                    'lunes' => $arrayPrePlanContratosLunes,
                    'martes' => $arrayPrePlanContratosMartes,
                    'miercoles' => $arrayPrePlanContratosMiercoles,
                    'jueves' => $arrayPrePlanContratosJueves,
                    'viernes' => $arrayPrePlanContratosViernes,
                    'sabado' => $arrayPrePlanContratosSabado,
                    'domingo' => $arrayPrePlanContratosDomingo
                );
            
        }



        foreach ($arrayPrePlanContratosFin as $valorArray) {
            foreach ($valorArray as  $valorContrato) {

                if(!empty($valorContrato)){

                $arrayUnNivelPrePlanContratos[] = Array(


                    'id_cliente' => $valorContrato["id_cliente"],
                    'name_client' => $valorContrato["name_client"],
                    'id_producto' => $valorContrato["id_producto"],
                    'name_product' => $valorContrato["name_product"],
                    'dia' => $valorContrato["dia"],
                    'opcion' => $valorContrato["opcion"],
                    'cantidad' => $valorContrato["cantidad"],
                    'codigo' => $valorContrato["codigo"],
                    'estado' => $valorContrato["estado"]



                    );

                }
                
            }
        }

       }else{

          $arrayUnNivelPrePlanContratos = Array();
       }

        $arrayMergeUnNivelPrePlanContratosFinal = Array(

            'datosPrePlan' => $prePlan,
            'datos Contratos' => $arrayUnNivelPrePlanContratos


            );

        $arrayFinalUnNivelPrePlanContratos = array();
        foreach ($arrayMergeUnNivelPrePlanContratosFinal as  $arrayValue) {
            foreach ($arrayValue as $value) {
               
                //$verifica_datos_pre_plan = $this->Pre_plan->verifica_contratos_pre_plan($desde, $hasta, $value["id_cliente"], $value["codigo"]);

                            if(!empty($value)){

                                $arrayFinalUnNivelPrePlanContratos[] = Array(

                                    'id_cliente' => $value["id_cliente"],
                                    'name_client' => $value["name_client"],
                                    'id_producto' => $value["id_producto"],
                                    'name_product' => $value["name_product"],
                                    'dia' => $value["dia"],
                                    'opcion' => $value["opcion"],
                                    'cantidad' => $value["cantidad"],
                                    'codigo' => $value["codigo"],
                                    'estado' => $value["estado"],
                                    'uid'   => $uid
                                );

                            }

                    
               

            }
             
        }

        $datos_temp_pre_plan = Array();

        if(count($arrayFinalUnNivelPrePlanContratos) > 0){
            
            if($this->Pre_plan->insertar_pre_plan_temporal($arrayFinalUnNivelPrePlanContratos)){
                $datos_temp_pre_plan = $this->Pre_plan->consulta_pre_plan_temporal($uid,$desde, $hasta);
                $this->Pre_plan->eliminar_pre_plan_temporal($uid);

            }
        }

      
        echo format_response($datos_temp_pre_plan);

    }


    function depuracion_contratos_rangos_fecha($datos_contratos,$desde,$hasta){
        $nuevo_arreglo_contratos_criterio1 = array();
        $nuevo_arreglo_contratos_criterio2 = array();
        $nuevo_arreglo_contratos_criterio3 = array();
        $array_criterios = array();

        foreach ($datos_contratos as $contrato) {
            $valida_rango_fecha_desde = $this->Pre_plan->check_in_range($contrato['desde'], $contrato['hasta'], $desde);
            $valida_rango_fecha_hasta = $this->Pre_plan->check_in_range($contrato['desde'], $contrato['hasta'], $hasta);

            if($valida_rango_fecha_desde && $valida_rango_fecha_hasta){     
                $nuevo_arreglo_contratos_criterio1[] = $contrato;
            }

            if(!$valida_rango_fecha_desde && $valida_rango_fecha_hasta){
                $nuevo_arreglo_contratos_criterio2[] = $contrato;
            }

            if($valida_rango_fecha_desde && !$valida_rango_fecha_hasta){

                $nuevo_arreglo_contratos_criterio3[] = $contrato;
            }
        }


        $array_criterios = array(
             'criterio1' => $nuevo_arreglo_contratos_criterio1,
             'criterio2' => $nuevo_arreglo_contratos_criterio2,
             'criterio3' => $nuevo_arreglo_contratos_criterio3
            );

        $array_criterios = $this->cambia_elementos_array_criterios_filtros_contratos($array_criterios,$desde,$hasta);


        return $array_criterios;

        
    }

    function cambia_elementos_array_criterios_filtros_contratos($array_criterios,$desde,$hasta){            

             
             $dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
             $tmp_criterio2 = array();
             if(count($array_criterios['criterio2'])){
                 foreach ($array_criterios['criterio2'] as $key => $valor) {                    
                         
                         $dia_desde = $dias[date('N', strtotime($valor['finicio']))];
                         
                     
                     
                         if($dia_desde == 'Martes'){
                            $valor['lunes'] = 0; 
                         }


                         if($dia_desde == 'Miercoles'){
                            $valor['lunes'] = 0;
                            $valor['martes'] = 0;  
                         }

                         if($dia_desde == 'Jueves'){
                            $valor['lunes'] = 0;
                            $valor['martes'] = 0;
                            $valor['miercoles'] = 0;   
                         }

                         if($dia_desde == 'Viernes'){
                            $valor['lunes'] = 0;
                            $valor['martes'] = 0;
                            $valor['miercoles'] = 0;
                            $valor['jueves'] = 0;    
                         }

                         if($dia_desde == 'Sabado'){
                            $valor['lunes'] = 0;
                            $valor['martes'] = 0;
                            $valor['miercoles'] = 0;
                            $valor['jueves'] = 0;
                            $valor['viernes'] = 0;     
                         }

                         if($dia_desde == 'Domingo'){
                            $valor['lunes'] = 0;
                            $valor['martes'] = 0;
                            $valor['miercoles'] = 0;
                            $valor['jueves'] = 0;
                            $valor['viernes'] = 0;
                            $valor['sabado'] = 0;     
                         }
                     

                     $tmp_criterio2[] = array(
                                            'codcliente' => $valor['codcliente'],
                                            'numVuelo' => $valor['numVuelo'],
                                            'tipoAvion' => $valor['tipoAvion'],
                                            'cantCiclos' => $valor['cantCiclos'],
                                            'categoria' => $valor['categoria'],
                                            'finicio' => $valor['finicio'],
                                            'ffin' => $valor['ffin'],
                                            'idContrato' => $valor['idContrato'],
                                            'idCliente' => $valor['idCliente'],
                                            'ciclos' => $valor['ciclos'],
                                            'duracion' => $valor['duracion'],
                                            'terminacionCiclo' => $valor['terminacionCiclo'],
                                            'dias' => $valor['dias'],
                                            'desde' => $valor['desde'],
                                            'hasta' => $valor['hasta'],
                                            'idFrecuencia' => $valor['idFrecuencia'],
                                            'lunes' => $valor['lunes'],
                                            'martes' => $valor['martes'],
                                            'miercoles' => $valor['miercoles'],
                                            'jueves' => $valor['jueves'],
                                            'viernes' => $valor['viernes'],
                                            'sabado' => $valor['sabado'],
                                            'domingo' => $valor['domingo'],
                                            'idProducto' => $valor['idProducto'],
                                            'descripcion' => $valor['descripcion'],
                                            'indiceProducto' => $valor['indiceProducto'],
                                            'precio' => $valor['precio'],
                                            'opcion' => $valor['opcion'],
                                            'cantidad' => $valor['cantidad'],
                                            'indiceOpciones' => $valor['indiceOpciones'],
                                            'name_client' => $valor['name_client'],
                                            'name_product' => $valor['name_product']

                                            );


                 }
             }

             $tmp_criterio3 = array();

             if(count($array_criterios['criterio3'])){
                 foreach ($array_criterios['criterio3'] as $key => $valor) {

                         
                         $dia_hasta = $dias[date('N', strtotime($valor['ffin']))];

                         if($dia_hasta == 'Sabado'){
                            $valor['domingo'] = 0; 
                         }


                         if($dia_hasta == 'Viernes'){
                            $valor['domingo'] = 0;
                            $valor['sabado'] = 0;  
                         }

                         if($dia_hasta == 'Jueves'){
                            $valor['domingo'] = 0;
                            $valor['sabado'] = 0;
                            $valor['viernes'] = 0;   
                         }

                         if($dia_hasta == 'Miercoles'){
                            $valor['domingo'] = 0;
                            $valor['sabado'] = 0;
                            $valor['viernes'] = 0;
                            $valor['jueves'] = 0;     
                         }

                         if($dia_hasta == 'Martes'){
                            $valor['domingo'] = 0;
                            $valor['sabado'] = 0;
                            $valor['viernes'] = 0;
                            $valor['jueves'] = 0;
                            $valor['miercoles'] = 0;      
                         }

                         if($dia_hasta == 'lunes'){
                            $valor['domingo'] = 0;
                            $valor['sabado'] = 0;
                            $valor['viernes'] = 0;
                            $valor['jueves'] = 0;
                            $valor['miercoles'] = 0;
                            $valor['martes'] = 0;     
                         }
                     

                     $tmp_criterio3[] = array(
                                            'codcliente' => $valor['codcliente'],
                                            'numVuelo' => $valor['numVuelo'],
                                            'tipoAvion' => $valor['tipoAvion'],
                                            'cantCiclos' => $valor['cantCiclos'],
                                            'categoria' => $valor['categoria'],
                                            'finicio' => $valor['finicio'],
                                            'ffin' => $valor['ffin'],
                                            'idContrato' => $valor['idContrato'],
                                            'idCliente' => $valor['idCliente'],
                                            'ciclos' => $valor['ciclos'],
                                            'duracion' => $valor['duracion'],
                                            'terminacionCiclo' => $valor['terminacionCiclo'],
                                            'dias' => $valor['dias'],
                                            'desde' => $valor['desde'],
                                            'hasta' => $valor['hasta'],
                                            'idFrecuencia' => $valor['idFrecuencia'],
                                            'lunes' => $valor['lunes'],
                                            'martes' => $valor['martes'],
                                            'miercoles' => $valor['miercoles'],
                                            'jueves' => $valor['jueves'],
                                            'viernes' => $valor['viernes'],
                                            'sabado' => $valor['sabado'],
                                            'domingo' => $valor['domingo'],
                                            'idProducto' => $valor['idProducto'],
                                            'descripcion' => $valor['descripcion'],
                                            'indiceProducto' => $valor['indiceProducto'],
                                            'precio' => $valor['precio'],
                                            'opcion' => $valor['opcion'],
                                            'cantidad' => $valor['cantidad'],
                                            'indiceOpciones' => $valor['indiceOpciones'],
                                            'name_client' => $valor['name_client'],
                                            'name_product' => $valor['name_product']

                                            );
                     
                     
                     
                 }
             }

             $array_criterios = array(
                 'criterio1' => $array_criterios['criterio1'],
                 'criterio2' => $tmp_criterio2,
                 'criterio3' => $tmp_criterio3
             ); 

             

             return $array_criterios;

    }

         function orderMultiDimensionalArray ($array, $campo, $invertir) {
            $posicion = array();
            $newRow = array();
            foreach ($array as $key => $row) {
                    $posicion[$key]  = $row[$campo];
                    $newRow[$key] = $row;
            }
            if ($invertir) {
                arsort($posicion);
            }
            else {
                asort($posicion);
            }
            $arrayRetorno = array();
            foreach ($posicion as $key => $pos) {
                $arrayRetorno[] = $newRow[$key];
            }
            return $arrayRetorno;
        }

   

    function pre_plan_post() {
        $filter = $this->post();
        $productos = $this->Planificacion_semanal->post_guardar($filter);
        echo format_response($productos);
    }
    
    function guardar_post() {
        $filter = $this->post();
        $productos = $this->Pre_plan->post_guardar($filter['data']);
        echo format_response($productos);
    }

   

    function clientes_get($id, $nombre) {
        $maecte = $this->Cliente->get_cliente_by($id, urldecode($nombre));
        echo format_response($maecte);
    }

     function productos_tipo_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_tipo($codigo, urldecode($descripcion), 'null');
        echo format_response($productos);
    }
    
    }

?>
