<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class pre_plan extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Pre Plan Semanal";
		$data["angular_app"] = "Pre_plan";
		$this->layout->view("pre_plan", $data);
	}
}

