<script src="<?= base_url()?>../client/src/app/pre_plan/module.js"></script>
<script src="<?= base_url()?>../client/src/app/pre_plan/pre_plan_model.js"></script>
<script src="<?= base_url()?>../client/src/app/pre_plan/controllers/pre_plan.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>

<script>
    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }
    
</script>