<?php

include_once(APPPATH . "models/base_model.php");
//error_reporting(0);

Class Pre_plan_model extends Base_model 
{
    private $matriz;

    function __construct() {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }

    function get_pre_plan($desde, $hasta) {
                
        $sql = $this->db->query("select id_cliente, nomcte01 AS name_client, id_producto, desprod01 AS name_product, dia, opcion, cantidad, codigo, estado_preplan as estado
                                 from planificacion_produccion
                                 join ".$this->matriz.".maecte on id_cliente = codcte01
                                 join maepro on id_producto = codprod01
                                 where '".$desde."' >= desde and '".$hasta."' <= hasta
                                 ");       

        return $sql->result_array();
        
    }

    function get_pre_plan_por_dia($fecha) {

        $dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
        $dia = $dias[date('N', strtotime($fecha))];
        
        $this->db->select('id_cliente, nomcte01 AS name_client, id_producto, 
                desprod01 AS name_product, dia, opcion, cantidad,cantidad_dispatch, cantidad_entregada, codigo, estado_preplan as estado,desde,hasta,estado_despacho')->from("planificacion_produccion");
        $this->db->join($this->matriz.'.maecte', 'id_cliente = codcte01');
        $this->db->join('maepro', 'id_producto = codprod01');
        $this->db->where("dia", $this->dia_semana_numero($dia));
         $this->db->order_by('opcion');
        //$this->db->where("hasta <=", $hasta);
        
        $result = $this->db->get();
         $data = array();
         //echo $this->db->last_query();
        foreach ($result->result_array() as  $valor) {

            if($this->check_in_range($valor['desde'], $valor['hasta'], $fecha)){
                $data[] = array(

                         'id_cliente' => $valor['id_cliente'],
                         'name_client' => $valor['name_client'],
                         'id_producto' => $valor['id_producto'],
                         'name_product' => $valor['name_product'],
                         'dia' => $valor['dia'],
                         'opcion' => $valor['opcion'],
                         'cantidad' => $valor['cantidad'],
                         'cantidad_dispatch' => $valor['cantidad_dispatch'],
                         'cantidad_entregada' => $valor['cantidad_entregada'],
                         'codigo' => $valor['codigo'],
                         'estado' => $valor['estado'],
                         'estado_despacho' => $valor['estado_despacho'],
                         'desde' => $valor['desde'],
                         'hasta' => $valor['hasta']


                    );
            }
        }




        return  $data;
        
    }

    function check_in_range($start_date, $end_date, $evaluame) {
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($evaluame);

        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    function dia_semana_numero($dia){

        $numdia = 0;
        switch ($dia) {
            
            case 'Lunes':
                    $numdia = 1;
                    break;
            case 'Martes':
                    $numdia = 2;
                    break;
            case 'Miercoles':
                    $numdia = 3;
                    break;
            case 'Jueves':
                    $numdia = 4;
                    break;
            case 'Viernes':
                    $numdia = 5;
                    break;
            case 'Sabado':
                    $numdia = 6;
                    break;
            case 'Domingo':
                    $numdia = 7;
                    break;
            
            
        }


        return $numdia;


    }

     function get_contratos($desde, $hasta){
        
        $sql = $this->db->query('SELECT contratos.*,clienteContrato.*,frecuenciaContrato.*,productosContrato.*,opciones.idCliente,opciones.numVuelo,opciones.idFrecuencia,opciones.idProducto,opciones.precio,opciones.opcion,(opciones.cantidad+opciones.servicioAdicional) as cantidad,opciones.idContrato,opciones.indiceProducto,opciones.indiceOpciones,maecte.nomcte01 as name_client,maepro.desprod01 as name_product
                                FROM 
                                contratos
                                inner join clienteContrato on contratos.codCliente = clienteContrato.idCliente and contratos.idContrato = clienteContrato.idContrato
                                inner join frecuenciaContrato on frecuenciaContrato.idCliente = contratos.codCliente  and frecuenciaContrato.idContrato =  contratos.idContrato
                                inner join productosContrato on productosContrato.idCliente = contratos.codCliente and productosContrato.idContrato =  contratos.idContrato and productosContrato.idFrecuencia = frecuenciaContrato.idFrecuencia
                                inner join opciones on opciones.idCliente = contratos.codCliente and opciones.idContrato =  contratos.idContrato and opciones.idProducto = productosContrato.idProducto and productosContrato.idFrecuencia = frecuenciaContrato.idFrecuencia
                                inner join '.$this->matriz.'.maecte on maecte.codcte01 = contratos.codCliente
                                inner join maepro on maepro.codprod01 = productosContrato.idProducto
                                ');
                                //where  "'.$desde.'" >= contratos.finicio or  "'.$hasta.'" <= contratos.ffin ');

        //echo $this->db->last_query();

        return $sql->result_array();

    }


    function get_planificacion_by_from($desde, $hasta) {
        
        $sql = $this->db->query("select planificacion_produccion.*,SUM(cantidad) AS cantidad_total,desprod01,codprod01 
                                 from planificacion_produccion
                                 join maepro on id_producto = codprod01
                                 where '".$desde."' >= desde and '".$hasta."' <= hasta and estado_preplan = 0
                                 group by id_producto, opcion, dia
                                 order by dia, id_producto, opcion
                                 ");

        return $sql->result();
    }

    function post_guardar($datos) {
        //$this->db->delete('planificacion_produccion', array('desde' => $datos['url_params'][0]['desde'], 'hasta' => $datos['url_params'][0]['hasta'],'estado_preplan' => 0));
        
        $this->elimina_planificacion_produccion($datos['url_params'][0]['desde'],$datos['url_params'][0]['hasta']);
        foreach ($datos['url_params'] as $key => $producto) {
            $this->db->replace("planificacion_produccion", $producto);
            //echo $this->db->last_query();
        }
    }

    function verifica_contratos_pre_plan($desde, $hasta, $cliente, $contrato){
        $this->db->select('*');
        $this->db->from('planificacion_produccion');
        $this->db->where('desde' ,$desde);
        $this->db->where('hasta' ,$hasta);
        $this->db->where('id_cliente', $cliente);
        $this->db->where('codigo', $contrato);
       // echo $this->db->last_query();
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        $flag = 0;
        if(count($resultado)>0){
            $flag = 1;
        }
        return $flag;
    }

    function insertar_pre_plan_temporal($array){
      $insert = $this->db->insert_batch("tmp_pre_plan",$array);
      return $insert;
    }

    function eliminar_pre_plan_temporal($uid){
      $this->db->where('uid', $uid);
      $this->db->delete("tmp_pre_plan");
    }

    function consulta_pre_plan_temporal($uid, $desde, $hasta){
        $sql = $this->db->query('select * from tmp_pre_plan where uid ='.$uid.' group by id_cliente,id_producto,dia,opcion,codigo order by id_cliente,id_producto,dia,opcion,codigo');
        $datos_temp = array();
        foreach ($sql->result_array() as $value) {
            $datos_temp[] = Array(

                                    'id_cliente' => $value["id_cliente"],
                                    'name_client' => $value["name_client"],
                                    'id_producto' => $value["id_producto"],
                                    'name_product' => $value["name_product"],
                                    'dia' => $value["dia"],
                                    'opcion' => $value["opcion"],
                                    'cantidad' => $value["cantidad"],
                                    'codigo' => $value["codigo"],
                                    'estado' => $this->consulta_estado_pre_plan($value["id_cliente"],$value["id_producto"],$value["dia"],$value["opcion"],$value["codigo"],$desde, $hasta),
                                    'uid'   => $uid
                                );

        }
        
        return $datos_temp;

    }
    function consulta_estado_pre_plan($id_cliente, $id_producto, $dia, $opcion, $codigo, $desde, $hasta){
        $this->db->select('estado_preplan as estado');
        $this->db->from('planificacion_produccion');
        $this->db->where('id_cliente',$id_cliente);
        $this->db->where('id_producto',$id_producto);
        $this->db->where('dia',$dia);
        $this->db->where('opcion',$opcion);
        $this->db->where('codigo',$codigo);
        $this->db->where('desde',$desde);
        $this->db->where('hasta',$hasta);
        //echo $this->db->last_query()."<br>";
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        $estado = 0;
        if(count($resultado) > 0){
            if($resultado[0]['estado'] == 1){
                $estado = $resultado[0]['estado'];
            }
        }
        return $estado;
    }

    function elimina_planificacion_produccion($desde,$hasta){

      $this->db->query("delete from planificacion_produccion where '".$desde."' >= desde and '".$hasta."' <= hasta and estado_preplan = 0");


    }

}

?>
