<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class reporte_plan_semanal extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Reporte_plan_semanal";
		$data["angular_app"] = "Reporte_plan_semanal";
		$this->layout->view("reporte_plan_semanal", $data);
	}
}

