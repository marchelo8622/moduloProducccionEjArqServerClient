<?php

defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Reporte_plan_semanal_model", "Reporte_plan_semanal");
        $this->load->model('semana_planificar/Semana_planificar_model', 'Semana_planificar');
        //$this->load->model("Planificacion_semanal_model", "Planificacion_semanal");
        $this->load->model('producto/Producto_model', 'Producto');
        $this->load->model('Pre_plan/Pre_plan_model', 'Pre_plan');
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model("configurar_producto_venta/Configurar_producto_venta_model", "configurar_producto_venta");
        
    }
 
    
    function productos_tipo_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_tipo($codigo, urldecode($descripcion), 'null');
        foreach ($productos as  $valor) {
            $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($valor->codigo);
            $json = json_encode($config_prod_final);

            $data[] = array(
                    'codigo' => $valor->codigo,
                    'descripcion' => $valor->descripcion,
                    'stock' => $valor->stock,                    
                    'configuraciones_prod_final' => $json
            );
        }
        echo format_response($data);
    }

    function planificacion_by_from_get($desde,$hasta) {
        $plan = $this->Reporte_plan_semanal->get_planificacion_by_from($desde,$hasta);
        $data = array();
        foreach ($plan as  $valor) {

            $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($valor->codprod01);
            $json = json_encode($config_prod_final);


            $data[] = array(
                    'id_producto' => $valor->id_producto,
                    'id_receta' => $valor->id_receta,
                    'dia' => $valor->dia,
                    'opcion' => $valor->opcion,
                    'cantidad' => $valor->cantidad,
                    'desde' => $valor->desde,
                    'hasta' => $valor->id_producto,
                    'desprod01' => $valor->desprod01,
                    'codprod01' => $valor->codprod01,
                    'cantidad_total' => $valor->cantidad_total,
                    'configuraciones_prod_final' => $json
            );
        }
        echo format_response($data);
    }

    
    function guardar_post() {
        $filter = $this->post();
        $productos = $this->Reporte_plan_semanal->post_guardar($filter);
        echo format_response($productos);
    }

    function productos_compuestos_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_compuestos($codigo, $descripcion);
        echo format_response($productos);
    }

    function productos_compuestos_consolidado_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_compuestos_consolidado($codigo, $descripcion);
        echo format_response($productos);
    }
    
    function pre_plan_get($desde,$hasta) {
        $prePlan = $this->Pre_plan->get_planificacion_by_from($desde,$hasta);
         $data = array();
        foreach ($prePlan as  $valor) {

            $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($valor->codprod01);
            $json = json_encode($config_prod_final);

            $id_receta = array();
            $data[] = array(
                    'id_producto' => $valor->id_producto,
                    'id_receta' => json_encode($id_receta),
                    'id_cliente' => $valor->id_cliente,
                    'dia' => $valor->dia,
                    'opcion' => $valor->opcion,
                    'cantidad' => $valor->cantidad,
                    'desde' => $valor->desde,
                    'hasta' => $valor->id_producto,
                    'desprod01' => $valor->desprod01,
                    'codprod01' => $valor->codprod01,
                    'cantidad_total' => $valor->cantidad_total,
                    'configuraciones_prod_final' => $json
            );
        }
        echo format_response($data);
        
    }

     function mostrar_fecha_completa($fecha){
            $subfecha=split("-",$fecha);
               //for($i=0;$subfecha[$i];$i++);
            $año=$subfecha[0];
            $mes=$subfecha[1];
            $dia=$subfecha[2];

            $dia2=date( "d", mktime(0,0,0,$mes,$dia,$año));
            $mes2=date( "m", mktime(0,0,0,$mes,$dia,$año));
            $año2=date( "Y", mktime(0,0,0,$mes,$dia,$año));
            $dia_sem=date( "w", mktime(0,0,0,$mes,$dia,$año));

               switch($dia_sem) {
                  case "0":   // Bloque 1
                     $dia_sem3='Domingo';
                           break;
                  case "1":   // Bloque 1
                     $dia_sem3='Lunes';
                           break;
                    case "2":   // Bloque 1
                     $dia_sem3='Martes';
                           break;
                    case "3":   // Bloque 1
                     $dia_sem3='Miercoles';
                           break;
                    case "4":   // Bloque 1
                     $dia_sem3='Jueves';
                           break;
                    case "5":   // Bloque 1
                     $dia_sem3='Viernes';
                           break;
                    case "6":   // Bloque 1
                     $dia_sem3='Sabado';
                           break;
                  default:   // Bloque 3
                     };
              
                switch($mes2) {
                    case "1":   // Bloque 1
                       $mes3='Enero';
                             break;
                    case "2":   // Bloque 1
                       $mes3='Febrero';
                       break;
                    case "3":   // Bloque 1
                       $mes3='Marzo';
                       break;
                   case "4":   // Bloque 1
                       $mes3='Abril'; 
                       break;  
                   case "5":   // Bloque 1
                           $mes3='Mayo';
                       break;   
                   case "6":   // Bloque 1
                           $mes3='Junio';   
                                 break;
                   case "7":   // Bloque 1
                           $mes3='Julio';
                       break;
                   case "8":   // Bloque 1
                           $mes3='Agosto';
                       break;
                   case "9":   // Bloque 1
                           $mes3='Septiembre';
                       break;
                   case "10":   // Bloque 1
                           $mes3='Octubre';
                       break;
                   case "11":   // Bloque 1
                           $mes3='Noviembre';
                       break;           
                   case "12":   // Bloque 1
                     $mes3='Diciembre';  
                 break;
                  default:   // Bloque 3
                 break;
                     };
              
              
            $fecha_texto=$dia_sem3.' '.$dia2.' '.'de'.' '.$mes3.' '.'del'.' '.$año2;

            return $fecha_texto;
    }

    function devuelve_dia($fecha){
        $fechats = strtotime($fecha); //a timestamp

        //el parametro w en la funcion date indica que queremos el dia de la semana
        //lo devuelve en numero 0 domingo, 1 lunes,....

        return date('w', $fechats);
    }   


    function imprimir_plan_semanal_post(){

        $filter = $this->post();
        $jsonDatos = $filter['datos']; 
        $arrayDatosImprimir = json_decode($jsonDatos,true);
        $arrayfec = $this->formatea_fecha_reporte($arrayDatosImprimir[0]['desde'], $arrayDatosImprimir[0]['hasta'], $arrayDatosImprimir[0]['dia']); 

        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Marcelo Montalvo');
        $pdf->SetTitle('Consolidado de materia prima');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
        $pdf->setFontSubsetting(true);
 
        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
 
        $html = '';
        $html .= "<style type=text/css>";

        $html .= "table  , td {border: 1px solid black;    border-collapse: collapse;}";
        $html .= "h1{color: red; font-weight: bold;  text-align:center;}";
        /*$html .= "td{background-color: #AAC7E3; color: #fff}";*/
        $html .= "</style>";
        $html .= "<h1>".$this->mostrar_fecha_completa($arrayfec[0])."</h1>";
        $html .= "<table cellspacing='0' cellpadding='4' border='1' width='100%'>";
       
        
        $infoPlan = $arrayDatosImprimir;

        foreach ($infoPlan  as $valor) 
        {     

            $nomProducto = $this->Reporte_plan_semanal->get_nombre_producto($valor["id_producto"]);

            $array_recetas = json_decode($valor["id_receta"],true);
            foreach ($array_recetas as $receta){

            $nomProductoReceta = $this->Reporte_plan_semanal->get_nombre_producto($receta["receta"]);


            $html .= '  <tr bgcolor="#0066CC">
                            <td colspan="5" align="center">' . $nomProducto[0]["desprod01"] . '</td>
                          </tr>
                          <tr>
                            <td colspan="3" width="50%" align="center">'. $nomProductoReceta[0]["desprod01"] .'</td>
                            <td width="30%" align="right">PAX A ELABORAR</td>
                            <td width="20%" align="center"> '. $valor["cantidad"] .'</td>
                          </tr>
                          <tr>
                            <td colspan="5" height="12">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" width="158"><b>INGREDIENTES</b></td>
                            <td align="center"><b>CANT. UNID</b></td>
                            <td align="center"><b>CANT. TOT</b></td>
                            <td align="center" width="80" ><b>COST. UNIT</b></td>
                            <td align="center"><b>COST. TOTAL</b></td>
                          </tr>';
                $prodCompuestos = $this->Reporte_plan_semanal->get_productos_compuestos($receta["receta"]);
                $costoTotal = 0;
                foreach ($prodCompuestos as $prodCompuesto) {
                    
                
                $costoUnitarioProdCompuesto = $this->Reporte_plan_semanal->get_nombre_producto($prodCompuesto["codprod20"]);
            $html .=      '<tr>
                            <td>'.$prodCompuesto["desprod20"].'</td>
                            <td align="right">'.$prodCompuesto["cantidad20"].'</td>
                            <td align="right">'.$prodCompuesto["cantidad20"] * $valor["cantidad"].'</td>
                            <td align="right">'.$costoUnitarioProdCompuesto[0]["valact01"].'</td>
                            <td align="right">'.number_format($costoUnitarioProdCompuesto[0]["valact01"] * $valor["cantidad"],2,".",",").'</td>
                          </tr>';

            
                $costoTotal += $costoUnitarioProdCompuesto[0]["valact01"] * $valor["cantidad"];
                }  



            $html .=  '<tr>
                        <td colspan="3">&nbsp;</td>
                        <td align="center"><b> TOTAL</b></td>
                        <td align="right">'.number_format($costoTotal,2,".",",").'</td>
                      </tr>'; 

                }        
  
            }
        $html .= "</table>";
 
        $pdf->writeHTML($html, true, false, true, false, '');


        $nombre_archivo = utf8_decode("plansemanal.pdf");
        $pdf->Output($nombre_archivo, 'I');
    


    }
     function agrupa_suma_array_por_dos_claves($array, $campo, $campo2) {
        $nuevo = array();
        if(count($array)>0){
         $nuevo = array();
         foreach ($array as $parte) {
              $clave[] = $parte[$campo];
         }
      $unico = array_unique($clave);

        foreach ($unico as $un) {
            $suma = 0;
             foreach ($array as $original) {
                  if ($un == $original[$campo]) {
                    $suma = $suma + $original[$campo2];
                  }
             }
          $ele['codprod20'] = $un;
          $ele['cantidad20'] = $suma;
          array_push($nuevo, $ele);
          $suma = 0;
        }
    }
       return $nuevo;
    }
    function formatea_fecha_reporte($fechauno,$fechados,$numDiaImprimir){
        // $fechauno = $arrayDatosImprimir[0]['desde'];
        // $fechados = $arrayDatosImprimir[0]['hasta'];

        // $numDiaImprimir = $arrayDatosImprimir[0]['dia'];

        $fechaaamostar = $fechauno;
        while(strtotime($fechados) >= strtotime($fechauno))
        {
            if(strtotime($fechados) != strtotime($fechaaamostar)){
                if($numDiaImprimir == $this->devuelve_dia($fechaaamostar)){
              $arrayfec[] = $fechaaamostar;
                }
            $fechaaamostar = date("Y-m-d", strtotime($fechaaamostar . " + 1 day"));
            }else{

                if($numDiaImprimir == $this->devuelve_dia($fechaaamostar)){
                   array_push($arrayfec, $fechaaamostar);
                 }
            break;
            }

        }
        return $arrayfec;

    }
    function imprimir_consolidado_post(){
        $filter = $this->post();
        $jsonDatos = $filter['datos']; 
        $arrayDatosImprimir = json_decode($jsonDatos,true);

        $arrayfec = $this->formatea_fecha_reporte($arrayDatosImprimir[0]['desde'], $arrayDatosImprimir[0]['hasta'], $arrayDatosImprimir[0]['dia']); 
         
        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Marcelo Montalvo');
        $pdf->SetTitle('Consolidado de materia prima');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
        $pdf->setFontSubsetting(true);
 
        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
 
        $html = '';
        $html .= "<style type=text/css>";

        $html .= "table  , td {border: 1px solid black;    border-collapse: collapse;}";
        $html .= "h1{color: red; font-weight: bold;  text-align:center;}";
        /*$html .= "td{background-color: #AAC7E3; color: #fff}";*/
        $html .= "</style>";
        $html .= "<h1>".$this->mostrar_fecha_completa($arrayfec[0])."</h1>";
        $html .= "<table cellspacing='0' cellpadding='4' border='1' width='100%'>";
       
        
        $infoPlan = $arrayDatosImprimir;

            $html .= '  
                          <tr bgcolor="#0066CC">
                            <td align="center" width="316"><b>DESCRIPCION</b></td>
                            <td align="center" width="158"><b>CANTIDAD</b></td>
                          </tr>';
        foreach ($infoPlan  as $valor) 
        {     

                $array_recetas = json_decode($valor["id_receta"],true);
                foreach ($array_recetas as $receta){
                  $prodCompuestos[] = $this->Reporte_plan_semanal->get_productos_compuestos_consolidado($receta["receta"], $valor['cantidad']);
                }
        } 
                $datosCompuestosUnNivel = array();
                if(count($prodCompuestos)>0){
                    foreach ($prodCompuestos as $prodCompuesto) {
                            foreach ($prodCompuesto as $valor) {
                                $datosCompuestosUnNivel[]= $arrayName = array(
                                    'codprod20' => $valor['codprod20'],
                                    'cantidad20' => $valor['cantidad'] * $valor['cantidad20']);
                            }
                    }
                }
            $datosConsolidado = $this->agrupa_suma_array_por_dos_claves($datosCompuestosUnNivel, 'codprod20', 'cantidad20');
           
                foreach ($datosConsolidado as $prodCompuesto) {
                    $nombreCompuest = $this->Reporte_plan_semanal->get_nombre_producto($prodCompuesto["codprod20"]);
                    $nombreC = '???';
                    $cantC = '0';

                    if($nombreCompuest != ''){
                        $nombreC = $nombreCompuest[0]['desprod01'];     
                    }

                    if( isset($prodCompuesto["cantidad20"])){
                        $cantC = $prodCompuesto["cantidad20"];
                    }
                
            $html .=      '<tr width="100%">
                            <td>Total '.$nombreC.'</td>
                            <td align="right">'.$cantC.  '</td>
                          </tr>';
                    
            }  

        $html .= "</table>";
 
        $pdf->writeHTML($html, true, false, true, false, '');


        $nombre_archivo = utf8_decode("plansemanal.pdf");
        $pdf->Output($nombre_archivo, 'I');

    }

    function cliente_interno_get(){

        echo format_response($this->Cliente->cliente_interno());


    }


    
}
?>
