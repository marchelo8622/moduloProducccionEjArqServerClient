<?php
include_once(APPPATH ."models/base_model.php");
Class Reporte_plan_semanal_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }
    
    
    function get_planificacion_by_from($desde,$hasta) {
        $this->db->where("desde >=", $desde);
        $this->db->where("hasta <=", $hasta);
        //$this->db->select('*,SUM(cantidad) AS cantidad_total')->from("plan_semanal");

        $this->db->select('planificacion_produccion.*,desprod01,codprod01,cantidad AS cantidad_total')->from("planificacion_produccion");
//        $this->db->join('maecte', 'id_cliente = codcte01');
        $this->db->join('maepro', 'id_producto = codprod01');
        $this->db->group_by('id_producto,opcion,dia');
        $this->db->order_by('dia,id_producto, opcion');
        //$this->db->group_by('id_producto, opcion, dia');
        //$this->db->order_by('dia');
        $result = $this->db->get();

        //print_r($result->result());
        return $result->result();
    }

    

    function add_requisicion_pac($datos) {
        $ocu =0;
        foreach ($datos['data'] as  $producto) {
            $arreglo_requisicion[] =Array(
                   'nopedido31' => 'sem'.$producto['desde'].'_'.$producto['hasta'],
                   'codprod31' =>  $producto['id_receta'],
                   'ocurren31' =>  str_pad(round($ocu,0),2,'0',STR_PAD_LEFT),
                   'fecpedido31' => date('Y-m-d H:i:s'),
                   'referencia31' => 'NA',
                   'localid31' => '01',
                   'status31' => '01',
                   'cantped31' => $producto['cantidad'],
                   'cantafac31' => $producto['cantidad'],
                   'uid31' => 2,
                   'bodega31' => 01,
                   'fecestado31' =>date('Y-m-d H:i:s')
                ); 

         $ocu++;
        }

        $this->db->insert_batch("maereq31", $arreglo_requisicion);
    

    }

    function get_nombre_producto($codigo){

      $this->db->where("codprod01", $codigo); 
      $this->db->select('desprod01,valact01')->from("maepro");
      $resultado = $this->db->get(); 
      //echo $this->db->last_query();
      return $resultado->result_array();

    }

    function get_productos_compuestos($codigo){

      $this->db->where("codprod20t", $codigo); 
      $this->db->select('codprod20t,codprod20,desprod20,unidmed20,cantidad20')->from("lismae");
      $resultado = $this->db->get();
      return $resultado->result_array();

    }
    function get_productos_compuestos_consolidado($codigo, $cantidad){
      $this->db->where("codprod20t", $codigo); 
      $this->db->select('codprod20t,codprod20,desprod20,unidmed20,cantidad20')->from("lismae");
      $resultado = $this->db->get();
      //echo $this->db->last_query();
      $consolidado = $resultado->result_array();
      $arrayConsolidado = array();
      if(count($arrayConsolidado) >0){
      foreach ($consolidado as $datos) {
        $arrayConsolidado[]=array(
          'codprod20t' => $datos['codprod20t'],
          'codprod20' => $datos['codprod20'],
          'desprod20' => $datos['desprod20'],
          'unidmed20' => $datos['unidmed20'],
          'cantidad20' => $datos['cantidad20'],
          'cantidad' => $cantidad
          );
      }
    }
      //print_r($arrayConsolidado);

      return $arrayConsolidado;
    }
}
?>
