<script src="<?= base_url()?>../client/src/app/planificacion_semanal/module.js"></script>
<script src="<?= base_url()?>../client/src/app/planificacion_semanal/planificacion_semanal_model.js"></script>
<script src="<?= base_url()?>../client/src/app/reporte_plan_semanal/module.js"></script>
<script src="<?= base_url()?>../client/src/app/reporte_plan_semanal/reporte_plan_semanal_model.js"></script>
<script src="<?= base_url()?>../client/src/app/reporte_plan_semanal/controllers/reporte_plan_semanal.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>
<script type="text/javascript">
    function base_consultar() {
        $("#btn_send_filter").click();
    }
    
    function base_excel() {
        $("#btn_export_excel").click();
    }

    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }
    function base_imprimir() {
       window.print();
    
    }
</script>
