<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Categoria_model', 'Categoria');
    }
    
    function categoria_get()
    {
        $categoria = $this->Categoria->get_categoria();
        echo format_response($categoria);
    }
    
    function categoria_by_id_get($tipo){
        $categorias_contrato = $this->Categoria->get_categoria_by_id($tipo);
        echo format_response($categorias_contrato);
    }
}
