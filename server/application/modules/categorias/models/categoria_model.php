<?php
include_once(APPPATH ."models/base_model.php");
Class Categoria_model extends Base_model
{
    private $matriz;

    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    
    function get_categoria()
    {
        $this->db->where('tipocate', '03');
        $this->db->where('alias !=','');
        $this->db->where('codcatep =','0');
        $this->db->from($this->matriz.".categorias");
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function get_categoria_by_id($tipo){
        $this->db->select('tipocate, codcate, desccate, alias');
        $this->db->from($this->matriz.".categorias");
        $this->db->where('desccate', $tipo);
        $result = $this->db->get();
        return $result->result();
    }

    function get_categoria_by_client($codigo){
        $result = $this->db->query('Select desccate, codcate, alias from '.$this->matriz.'.categorias INNER JOIN '.$this->matriz.'.maecte ON catcte01 = codcate where '.$this->matriz.'.maecte.codcte01 ='.$codigo.' and  codcatep = 0 ');
        
        return $result->result();
    }


}
?>
