<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class plan_diario extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Plan_diario";
		$data["angular_app"] = "Plan_diario";
		$this->layout->view("plan_diario", $data);
	}
}

