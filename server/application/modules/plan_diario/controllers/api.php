<?php
error_reporting(0);
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {

    private $uid = '';
    private $fecha_plan_diario;
    private $codigo_categoria;
    private $respuesta = array();
    private $validacion_receta = array();
    private $validacion_produccion = array();
    private $secuencial_produccion;
    private $numero_produccion;
    private $secuencial_requisicion;
    private $numero_requisicion;
    private $serie_sri;
    private $codigo_cliente_interno;
    private $bodega_materia_prima;
    private $clienteGenerico = array();
    private $ocu_req;
    private $IDB;
    private $tipo_cocina;
    private $clasificacion_prod_final;
    private $codigo_receta;
    private $nombre_receta;
    private $proceso_produccion = array();


    function __construct()
    {
          parent::__construct();
          $this->load->model("Plan_diario_model", "plan_diario");
          $this->load->model('categorias/Categoria_model', 'Categoria');
          $this->load->model('clientes/Cliente_model', 'Cliente');
          $this->load->model('producto/Producto_model', 'Producto');
          $this->load->model('creador_recetas/Creador_recetas_model', 'creador_recetas');
          $this->load->model("configurador_umedidas/Configurador_umedidas_model", "configurador_umedidas");
          $this->load->model("configurar_producto_venta/Configurar_producto_venta_model", "configurar_producto_venta");
          $this->response_false();
          $this->serie_sri = $this->get_serie_sri();
          $this->codigo_cliente_interno = $this->cliente_interno_get();
          $this->bodega_materia_prima = $this->get_cod_bodega_materia_prima();
          $this->clienteGenerico = $this->Cliente->get_cliente_by_codigo($this->codigo_cliente_interno);
          $this->ocu_req = 0;
          $this->tipo_cocina = '';
          $this->clasificacion_prod_final = '';
          $this->codigo_receta = '';
          $this->nombre_receta = '';
          session_start();
          $this->uid = $_SESSION['UID'];
    }

    function response_false($error = 'No se Proceso')
    {
        $response = array();
        $response['respuesta'] = false;
        $response['error'] = $error;
        $this->respuesta['respuesta'] = $response;
    }

    function response_true($respuesta = true)
    {
        $response = array();
        $response['respuesta'] = $respuesta;
        $this->respuesta['respuesta'] = $response;
    }

    function categoria_get() {
        $categorias = $this->Categoria->get_categoria();
        echo format_response($categorias);
    }

    function productos_plan_diario_get($dia, $codigo_categoria)
    {
        $plan_diario = $this->plan_diario->get_productos_pre_plan_por_dia($dia, $codigo_categoria);
        echo format_response($plan_diario);
    }

    function buscar_orden_produccion_plan_diario_get($dia, $codigo_categoria)
    {
        $orden_produccion = $this->plan_diario->buscar_orden_produccion($dia, $codigo_categoria);
        if (empty($orden_produccion)){
            $orden_produccion = array(
                array(
                        'numero_produccion'     => "",
                        'estado_produccion'     => 0
                    )
                );
          }
        echo format_response($orden_produccion);
    }

    function clientes_plan_diario_get($dia, $codigo_producto)
    {
        $clientes_plan_diario = $this->plan_diario->get_clientes_preplan_por_dia($dia, $codigo_producto);
        echo format_response($clientes_plan_diario);
    }

     function clientes_get($id, $nombre, $categoria) {
        $maecte = $this->Cliente->get_cliente_by_categoria($id, urldecode($nombre), $categoria);
        echo format_response($maecte);
    }

    function total_productos_por_dia_get($fecha){
         $datos_totalizar = $this->plan_diario->get_total_productos_por_dia($fecha);
         echo format_response($datos_totalizar);
    }

    function crear_op_de_producto_receta_post()
    {
        $this->IDB = $this->input->get('IDB');
        $data = $this->post();
        $datos = $data['data'];
        $this->fecha_plan_diario = $datos['fecha'];
        $this->codigo_categoria = $datos['codigo_categoria'];
        $this->secuencial_produccion = $this->get_numero_produccion();
        $this->numero_produccion = $this->serie_sri . str_pad($this->secuencial_produccion,9,'0',STR_PAD_LEFT);
        
        $validacion_receta = $this->validar_recetas_get();
        if ($this->validacion_receta['validacion'])
        {
            $this->insertar_orden_produccion();
            $this->update_numero_produccion();
        }else{
            $error = $this->validacion_receta['producto']. $this->validacion_receta['error'];
            $this->response_false($error);
        }
        echo format_response($this->respuesta['respuesta']);
    }

    function insertar_orden_produccion()
    {
        $recetas = $this->plan_diario->get_productos_para_produccion($this->fecha_plan_diario, $this->codigo_categoria);
        foreach ($recetas as $key => $receta_producto) {
            $opciones = json_decode($receta_producto['id_receta']);
            foreach ($opciones as $key => $producto_receta) {
                if ($producto_receta->receta)
                {
                $this->generar_tmp_produccion($producto_receta, $receta_producto['cantidad'], $receta_producto['id_producto']);
                }
            }
            $datos = array(
                    'id_producto' => $receta_producto['id_producto'],
                    'dia' => $receta_producto['dia'],
                    'opcion' => $receta_producto['opcion']
                    );
            $this->plan_diario->ingresar_numero_produccion_en_plan_diario($datos, $this->numero_produccion, $this->fecha_plan_diario);
        }
        $this->generar_orden_produccion();
        $this->borrar_temporales_produccion();
        $this->plan_diario->eliminar_detalle_orden_produccion($this->numero_produccion);
        $this->generar_detalle_produccion();
        $this->borrar_temporales_lismaehis();
        $this->response_true();
    }


    function generar_tmp_produccion($receta, $cantidad, $producto_final)
    {
        $datos_produccion = 
            array(
            'nopedido30' => $this->numero_produccion,
            'codprod30' => $receta->receta,
            'cantped30' => $cantidad,
            'categoria' => $receta->cat,
            'nombre_producto' => $receta->nom_receta,
            'producto_final' => $producto_final
            );
        $produccion = $this->plan_diario->generar_tmp_produccion($datos_produccion);
    }

    function generar_orden_produccion()
    {
        $tmp_maeprd = $this->obtener_detalle_tmp_produccion_get();
        foreach ($tmp_maeprd as $key => $value) {
            $datos_documento_produccion = 
                array(
                'nopedido30' => $this->numero_produccion,
                'codprod30' => $value['codprod30'],
                'ocurren30' => str_pad($key, 2, '0', STR_PAD_LEFT),
                'codcte30' => $this->clienteGenerico->codigo_cliente,
                'cvectenegro30' => 'X',
                'fecpedido30' => date('Y-m-d H:i:s'),
                'nomcte30' => $this->clienteGenerico->nombre_cliente,
                'status30' => '02',
                'cantped30' => $value['cantped30'],
                'cantafac30' => $value['cantped30'],
                'uid' => '',
                'cvtransfer30' => 'N',
                'bodega30' => $this->IDB,
                'UIDap' => '',
                'fecestado30' => date('Y-m-d H:i:s'),
                'fecdespacho30' => $this->fecha_plan_diario,
                'liqprd30' => 'N'
                );
            $produccion = $this->plan_diario->generar_produccion($datos_documento_produccion);
            $this->generar_tmp_lismaehis($key, $value['codprod30'], $value['cantped30'], $value['producto_final']);
        }
    }

    function obtener_detalle_tmp_produccion_get()
    {
        $detalle = $this->plan_diario->obtener_detalle_tmp_produccion($this->numero_produccion);
        return $detalle;
    }

    function generar_tmp_lismaehis($ocu, $producto_receta, $cantidad_producir, $producto_final)
    {
        $receta = $this->plan_diario->get_componentes_lismaehis($producto_receta, $this->numero_produccion);
        if (count($receta) <= 0)
            $receta = $this->plan_diario->get_componentes($producto_receta);
        $tipo_cocina = '';
        $cocina = $this->plan_diario->get_tipo_cocina_by_producto($producto_receta);
        if (!empty($cocina[0]['tipo_cocina']))
            $tipo_cocina = $cocina[0]['tipo_cocina'];

        $producto_terminado = $this->get_producto_by_codigo($producto_final);
        $clasificacion = $producto_terminado['clasificacion'];
        if (count($receta) > 0)
        {
            foreach ($receta as $key => $producto_compenente) {
                $producto = array(
                            'numord20' => $this->numero_produccion,
                            'codprod20t' => $producto_receta,
                            'clasificacion' => $clasificacion,
                            'tipo_cocina' => $tipo_cocina,
                            'codprod20' => $producto_compenente['codprod20'],
                            'ocucomp20' => str_pad($ocu, 2, '0', STR_PAD_LEFT),
                            'desprod20' => $producto_compenente['desprod20'],
                            'unidmed20' => $producto_compenente['unidmed20'],
                            'cantidad20' => $producto_compenente['cantidad20'],
                            'cant_producir' => $cantidad_producir,
                            'cantfalta20' => '0.00',
                            'costo20' => $producto_compenente['costo20'],
                            'fecmod20' => date('Y-m-d H:i:s'),
                            'tipo20' => 'P',
                            'bodega20' => $this->IDB
                            );
            $this->plan_diario->generar_tmp_lismaehis($producto);
            }
        }else{
            $datos_producto = $this->get_producto_by_codigo($producto_receta);
            $costo = $this->Producto->get_costo_producto($datos_producto['codigo']);
            $producto = array(
                            'numord20' => $this->numero_produccion,
                            'codprod20t' => $producto_receta,
                            'clasificacion' => $clasificacion,
                            'tipo_cocina' => $tipo_cocina,
                            'codprod20' => $producto_receta,
                            'ocucomp20' => str_pad($ocu, 2, '0', STR_PAD_LEFT),
                            'desprod20' => $datos_producto['descripcion'],
                            'unidmed20' => $datos_producto['unidad_medida'],
                            'cantidad20' => 1,
                            'cant_producir' => $cantidad_producir,
                            'cantfalta20' => '0.00',
                            'costo20' => $costo,
                            'fecmod20' => date('Y-m-d H:i:s'),
                            'tipo20' => 'P',
                            'bodega20' => $this->IDB
                            );
            $this->plan_diario->generar_tmp_lismaehis($producto);

        }

    }

    function calcular_equivalencia_receta($cantidad_ingrediente, $cantidad_producir, $equivalencia)
    {
        $cant_aux = (float) $cantidad_ingrediente / (float)$equivalencia;
        $cantidad_total = number_format((float)$cantidad_producir * $cant_aux, 6, '.', '');
        return $cantidad_total;
    }

    function generar_detalle_produccion()
    {
        $tmp_lismaehis = $this->obtener_detalle_tmp_lismaehis();

            foreach ($tmp_lismaehis as $key => $producto_compenente) {
                $producto = array(
                            'numord20' => $this->numero_produccion,
                            'codprod20t' => $producto_compenente['codprod20t'],
                            'codprod20' => $producto_compenente['codprod20'],
                            'ocurren20' => str_pad($key, 2, '0', STR_PAD_LEFT),
                            'ocucomp20' => $producto_compenente['ocucomp20'],
                            'desprod20' => $producto_compenente['desprod20'],
                            'unidmed20' => $producto_compenente['unidmed20'],
                            'cantidad20' => $producto_compenente['cantidad20'],
                            'cantfalta20' => '0.00',
                            'costo20' => $producto_compenente['costo20'],
                            'fecmod20' => date('Y-m-d H:i:s'),
                            'tipo20' => 'P',
                            'bodega20' => $this->IDB,
                            'circunferencia20' => '0',
                            'tipo_cocina' => $producto_compenente['tipo_cocina'],
                            'cant_producir' => $producto_compenente['cant_producir'],
                            'clasificacion' => $producto_compenente['clasificacion']
                            );
                $this->plan_diario->generar_detalle_produccion($producto);
            }
    }

    function generar_requisiciones_by_produccion_post()
    {
        $this->IDB = $this->input->get('IDB');
        $data = $this->post();
        $datos = $data['data'];
        $this->numero_produccion = $datos['numero_produccion'];
        $this->fecha_plan_diario = $datos['fecha'];

        $this->generar_requisicion_por_cocina();
        $this->response_true();
        echo format_response($this->respuesta['respuesta']);

    }

    function generar_requisicion_por_cocina()
    {

        $clasificaciones = $this->clasificaciones_by_produccion_get();
        foreach ($clasificaciones as $key => $value) {
        $this->clasificacion_prod_final = $value['clasificacion'];
        $numero_cocinas = $this->numero_cocinas_by_produccion_get();

        foreach ($numero_cocinas as $key => $cocina) {
            $this->tipo_cocina = $cocina['tipo_cocina'];
            $tmp_lismaehis = $this->obtener_detalle_lismaehis_by_cocina_get();
            if (count($tmp_lismaehis) > 0)
            {
                $this->secuencial_requisicion = $this->get_numero_requisicion();
                $this->numero_requisicion = "PD-".$this->fecha_plan_diario."-".$this->secuencial_requisicion;
                $this->ocu_req = 0;
                $this->generar_requisicion($tmp_lismaehis);
            }
        }
        }

    }

    function generar_requisicion($tmp_lismaehis)
    {
        foreach ($tmp_lismaehis as $key => $producto_compenente) 
            {
                $cantidad_a_producir = $producto_compenente['cant_producir'];
                $subReceta1 = $this->plan_diario->get_componentes($producto_compenente['codprod20']);
                $this->codigo_receta = $producto_compenente['codprod20t'];
                $producto_receta = $this->get_producto_by_codigo($producto_compenente['codprod20t']);
                $this->nombre_receta = $producto_receta['descripcion'];
                $ocu_receta = $producto_compenente['ocucomp20'];
                $codigo_subreceta = '';
                if (count($subReceta1) > 0)
                {
                    foreach ($subReceta1 as $ocu => $producto_compenente1) 
                    {
                        $codigo_subreceta = $producto_compenente1['codprod20t'];
                        $subReceta2 = $this->plan_diario->get_componentes($producto_compenente1['codprod20']);
                        if (count($subReceta2) > 0)
                        {
                            foreach ($subReceta2 as $key => $producto_compenente2) 
                            {
                                $subReceta3 = $this->plan_diario->get_componentes($producto_compenente2['codprod20']);
                                if (count($subReceta3) > 0)
                                {
                                    foreach ($subReceta3 as $key => $producto_compenente3) 
                                    {
                                        $subReceta4 = $this->plan_diario->get_componentes($producto_compenente3['codprod20']);
                                        if (count($subReceta4) > 0)
                                        {
                                            foreach ($subReceta4 as $key => $producto_compenente4) 
                                            {
                                                $this->built_datos_requisicion($producto_compenente4, $cantidad_a_producir, $ocu_receta, $codigo_subreceta);
                                            }

                                        }else{
                                            $this->built_datos_requisicion($producto_compenente3, $cantidad_a_producir, $ocu_receta, $codigo_subreceta);
                                        }
                                    }

                                }else{
                                    $this->built_datos_requisicion($producto_compenente2, $cantidad_a_producir, $ocu_receta, $codigo_subreceta);
                                }
                            }

                        }else{
                            $this->built_datos_requisicion($producto_compenente1, $cantidad_a_producir, $ocu_receta, $codigo_subreceta);
                        }
                    }
                }else{
                    $this->built_datos_requisicion($producto_compenente, $cantidad_a_producir, $ocu_receta, $codigo_subreceta);
                }

            }
    }

    function built_datos_requisicion($producto_compenente, $cantidad_a_producir, $ocu_receta, $codigo_subreceta)
    {
        $nombre_cocina = $this->nombre_cocina_get();
        $requisicion = array(
                            'numdoc' => $this->numero_requisicion,
                            'ocu' => str_pad($this->ocu_req, 2, '0', STR_PAD_LEFT),
                            'sec' => $this->secuencial_requisicion,
                            'numordenp' => $this->numero_produccion,
                            'codcliente' => $this->clienteGenerico->codigo_cliente,
                            'fecreg' => date('Y-m-d H:i:s'),
                            'codprod' => $producto_compenente['codprod20'],
                            'desprod' => $producto_compenente['desprod20'],
                            'cantsol' => number_format($producto_compenente['cantidad20'] * $cantidad_a_producir, 2, '.', ''),
                            'cantent' => '0.00',
                            'cant_a_devolver' => '0.00',
                            'bodega' =>  $this->IDB,
                            'UID' => '',
                            'observacion' => $nombre_cocina,
                            'bodegareq' => $this->bodega_materia_prima,
                            'codigo_receta' => $this->codigo_receta,
                            'nombre_receta' => $this->nombre_receta,
                            'ocu_receta' => $ocu_receta,
                            'codigo_subreceta' => $codigo_subreceta,
                            'tipo_cocina' => $this->tipo_cocina,
                            'origen' => 'plan_diario',
                            'clasificacion' => $this->clasificacion_prod_final
                            );
                $this->plan_diario->generar_detalle_requisicion($requisicion);
        $this->ocu_req++;
    }

    function nombre_cocina_get()
    {
        $nombre_cocina = ($this->tipo_cocina != "") ? $this->creador_recetas->get_tipo_cocina_by_codigo($this->tipo_cocina) : "";
        if (empty($nombre_cocina)){
            $nombre_cocina[0]['nombre_cocina'] = "SIN COCINA";
          }
        return $nombre_cocina[0]['nombre_cocina'];
    }
    function obtener_detalle_tmp_lismaehis()
    {
        $detalle = $this->plan_diario->obtener_detalle_tmp_lismaehis($this->numero_produccion);
        return $detalle;

    }

    function obtener_detalle_lismaehis_by_cocina_get()
    {
        $detalle = $this->plan_diario->get_obtener_detalle_lismaehis_by_cocina($this->numero_produccion, $this->tipo_cocina, $this->clasificacion_prod_final);
        return $detalle;
    }

    function clasificaciones_by_produccion_get()
    {
        $datos = $this->plan_diario->clasificaciones_by_produccion_get($this->numero_produccion);
        return $datos;
    }

    function numero_cocinas_by_produccion_get()
    {
        $numero_cocinas = $this->plan_diario->get_numero_cocinas_by_produccion($this->numero_produccion, $this->clasificacion_prod_final);
        return $numero_cocinas;
    }

    function borrar_temporales_lismaehis()
    {
        $delete = $this->plan_diario->borrar_temporales_lismaehis($this->numero_produccion);
        return $delete;
    }

    function borrar_temporales_produccion()
    {
        $delete = $this->plan_diario->borrar_temporales_produccion($this->numero_produccion);
        return $delete;
    }

    function get_serie_sri()
    {
        $serie_sri = $this->plan_diario->get_serie_sri();
        return $serie_sri;
    }

    function get_numero_produccion()
    {
        $numero_produccion = $this->plan_diario->get_numero_documento('71', '93');
        return (int) $numero_produccion + 1;
    }

    function get_numero_requisicion()
    {
        $numero_requisicion = $this->plan_diario->get_numero_requisicion();
        return (int) $numero_requisicion + 1;
    }

    function update_numero_produccion()
    {
        $this->plan_diario->update_numero_documento('71', '93', $this->secuencial_produccion);
    }

    function get_cod_bodega_materia_prima()
    {
        $bodega_materia_prima = $this->plan_diario->get_bodega_materia_prima();
        return $bodega_materia_prima['codigo_bodega'];
    }

    function validar_entrega_requisiciones()
    {
        $requisiciones = $this->plan_diario->get_requisicion_by_produccion($this->numero_produccion);
        $validacion = $this->comparar_cantidades_requisicion2($requisiciones);
        return $validacion;
    }

    function comparar_cantidades_requisicion2($requisiciones)
    {
        $entrega_requisicion = true;
        foreach ($requisiciones as $key => $value) {
            if ((float)$value['cantent'] < (float)$value['cantsol'])
                {
                    $entrega_requisicion = false;
                    break;
                }

        }
        return $entrega_requisicion;
    }

    function validar_entrega_requisiciones_get($numero_produccion)
    {
        $this->validar_produccion();
        if ($this->validacion_produccion['validacion'])
        {

            $requisiciones = $this->plan_diario->get_requisicion_by_produccion($numero_produccion);
            if (count($requisiciones) > 0)
                $this->comparar_cantidades_requisicion($requisiciones);
        }
        echo format_response($this->validacion_produccion);
    }

    function comparar_cantidades_requisicion($requisiciones)
    {
        $this->validacion_produccion['validacion'] = true;
        foreach ($requisiciones as $key => $value) {
            if ((float)$value['cantent'] < (float)$value['cantsol'])
                {
                    $this->validacion_produccion['validacion'] = false;
                    $this->validacion_produccion['error'] = 'Falta Entrega de Requisiciones!';
                    break;
                }

        }
        return $entrega_requisicion;
    }

    function estado_requisiciones_get($numero_produccion)
    {
        $estados = array();
        $requisiciones = $this->plan_diario->get_num_requisicion_by_produccion($numero_produccion);
        if (count($requisiciones) > 0)
        {
            foreach ($requisiciones as $key => $requisicion) {
                $estados[] = $this->estado_by_requisicion($requisicion, $numero_produccion);
            }
            echo format_response($estados);
        }else{
            echo format_response(false);
        }
    }

    function estado_by_requisicion($requisicion, $numero_produccion)
    {
        $estado = array();
        $flag = true;
        $detalle_requisicion = $this->plan_diario->get_requisicion_by_numero($numero_produccion, $requisicion);
        foreach ($detalle_requisicion as $key => $value) {
            if ((float)$value['cantent'] < (float)$value['cantsol'])
            {
                $flag = false;
                $estado['requisicion'] = $requisicion['numdoc'];
                $estado['estado'] = 'Falta Entregar';
                break;
            }
        }

        if ($flag)
        {
            $estado['requisicion'] = $requisicion['numdoc'];
            $estado['estado'] = 'Entregada';
        }

        return $estado;
    }

    function producir_plan_diario_get($numero_produccion)
    {
        $this->numero_produccion = $numero_produccion;
        $this->set_respuesta();
        $validacion = $this->validar_entrega_requisiciones();
        if ($validacion)
        {
            $estado_produccion = $this->plan_diario->get_estado_produccion_plan_diario($this->numero_produccion);
            if ($estado_produccion[0]['estado_produccion'] == '0')
            {
                $this->producir_productos_receta();
            }

            $estado_produccion = $this->plan_diario->get_estado_produccion_plan_diario($this->numero_produccion);
            if ($estado_produccion[0]['estado_produccion'] == '2')
            {
                $this->proceso_produccion['producto_error'] = '';
                $this->proceso_produccion['error'] = 'Plan Diario ya esta producido!';

            }
            if ($estado_produccion[0]['estado_produccion'] == '1')
            {

                $this->valida_materia_prima_recetas();
                if ($this->validacion_produccion['validacion'])
                {
                    $this->producir_productos_finales();
     
                }
            }
            echo format_response($this->proceso_produccion);
        }else{
            echo format_response($this->proceso_produccion);
        }


    }

    function producir_productos_receta()
    {
        $this->proceso_produccion['recetas'] = true;
        $documento_egreso = $this->generar_egreso_produccion_recetas();
        $this->proceso_produccion['documentos'][] = array('numero_documento' => $documento_egreso,
                                        'tipo_documento' => '76');
        $documentos_ingreso = $this->generar_ingreso_produccion_recetas();
        $this->proceso_produccion['documentos'][] = array('numero_documento' => $documentos_ingreso,
                                        'tipo_documento' => '06');
        $this->plan_diario->actualizar_estado_produccion($this->numero_produccion, 1);
    }

    function producir_productos_finales()
    {
        $this->proceso_produccion['finales'] = true;
        $documento_egreso = $this->generar_egreso_produccion_pfinal();
        $this->proceso_produccion['documentos'][] = array('numero_documento' => $documento_egreso,
                                        'tipo_documento' => '76');
        $documentos_ingreso = $this->generar_ingreso_produccion_pfinal();
        $this->proceso_produccion['documentos'][] = array('numero_documento' => $documentos_ingreso,
                                        'tipo_documento' => '06');
        $this->plan_diario->actualizar_estado_produccion($this->numero_produccion, 2);
    }

    function set_respuesta()
    {
        $this->proceso_produccion = array();
        $this->proceso_produccion['recetas'] = false;
        $this->proceso_produccion['finales'] = false;
    }

    function generar_egreso_produccion_recetas()
    {
        $secuencial = $this->get_numero_egreso_produccion();
        $numero_egreso_produccion = $this->serie_sri . str_pad($secuencial,9,'0',STR_PAD_LEFT);
        
        $productos_materia_prima = $this->obtener_materia_prima_producto_receta_get($this->numero_produccion);
        $ocu = 0;

        foreach ($productos_materia_prima as $key => $producto) {
            $datos_producto = $this->get_producto_by_codigo($producto['codprod']);
            $costo = $this->Producto->get_costo_producto_by_codigo($producto['codprod']);

            $valor_transaccion = (float) $costo * (float)$producto['cantsol'];
            $costo_unitario = (float)$valor_transaccion / (float)$producto['cantsol'];
            $cantidad_actual = (float)$datos_producto['cantidad_actual'] - (float)$producto['cantsol'];
            $valor_actual = (float)$datos_producto['valor_actual'] - (float) $valor_transaccion;
            $precio_unitario = 0;
            if ((float)$cantidad_actual > 0)
                $precio_unitario = (float) $valor_actual / (float) $cantidad_actual;
            $documento_egreso = array(
                    'TIPOTRA03' => '76',
                    'NOCOMP03' => $numero_egreso_produccion,
                    'OCURREN03' => str_pad($ocu, 4, '0', STR_PAD_LEFT),
                    'CODPROD03' => $producto['codprod'],
                    'FECMOV03' => date('Y-m-d H:i:s'),
                    'CANTID03' => $producto['cantsol'],
                    'VALOR03' => $valor_transaccion,
                    'PU03' => $costo_unitario,
                    'CODDEST03' => '9999',
                    'CANTACT03' => $cantidad_actual,
                    'VALACT03' => $valor_actual,
                    'PRECUNI03' => $precio_unitario,
                    'NOFACT03' => $producto['numero_requisicion'],
                    'nomprodesp03' => $producto['producto_receta'],
                    'nopedido03' => $producto['numero_produccion'],
                    'detalle03' => 'Egreso de Produccion Automatico',
                    'cvanulado03' => 'N',
                    'UID' => $this->uid
                    );
            $documento = $this->plan_diario->generar_egreso_produccion($documento_egreso);
            $datos = array(
                    'cantidad_actual' => $cantidad_actual,
                    'valor_actual' => $valor_actual,
                    'precio_unitario' => $precio_unitario,
                    'codigo' => $producto['codprod']
                    );
            $this->Producto->actualizar_inventario_producto($datos);
            $ocu ++;
        }

        $this->update_numero_egreso_produccion($secuencial);
        return $numero_egreso_produccion;
    }

    function obtener_materia_prima_producto_receta_get($numero_produccion)
    {
        $productos_materia_prima = $this->plan_diario->obtener_materia_prima_producto_receta($numero_produccion);
        return $productos_materia_prima;
    }

    function generar_egreso_produccion_pfinal()
    {
        $secuencial = $this->get_numero_egreso_produccion();
        $numero_egreso_produccion = $this->serie_sri . str_pad($secuencial,9,'0',STR_PAD_LEFT);
        
        $productos_materia_prima = $this->plan_diario->get_producto_receta($this->numero_produccion);
        $ocu = 0;
        $fecha = date('Y-m-d H:i:s');
        $nuevafecha = date('Y-m-d H:i:s',strtotime('+2 seconds', strtotime($fecha)));
        foreach ($productos_materia_prima as $key => $producto) {
            $datos_producto = $this->get_producto_by_codigo($producto['codigo_receta']);
            if ($datos_producto['compuesto'] == 'S')
            {
            $costo = $this->Producto->get_costo_producto_by_codigo($producto['codigo_receta']);

            $valor_transaccion = (float) $costo * (float)$producto['cantidad_producida'];
            $costo_unitario = (float)$valor_transaccion / (float)$producto['cantidad_producida'];
            $cantidad_actual = (float)$datos_producto['cantidad_actual'] - (float)$producto['cantidad_producida'];
            $valor_actual = (float)$datos_producto['valor_actual'] - (float) $valor_transaccion;
            $precio_unitario = 0;
            if ((float)$cantidad_actual > 0)
                $precio_unitario = (float) $valor_actual / (float) $cantidad_actual;
            $documento_egreso = array(
                    'TIPOTRA03' => '76',
                    'NOCOMP03' => $numero_egreso_produccion,
                    'OCURREN03' => str_pad($ocu, 4, '0', STR_PAD_LEFT),
                    'CODPROD03' => $producto['codigo_receta'],
                    'FECMOV03' => $nuevafecha,
                    'CANTID03' => $producto['cantidad_producida'],
                    'VALOR03' => $valor_transaccion,
                    'PU03' => $costo_unitario,
                    'CODDEST03' => '9999',
                    'CANTACT03' => $cantidad_actual,
                    'VALACT03' => $valor_actual,
                    'PRECUNI03' => $precio_unitario,
                    'NOFACT03' => '',
                    'nomprodesp03' => $producto['producto_receta'],
                    'nopedido03' => $this->numero_produccion,
                    'detalle03' => 'Egreso de Produccion Automatico para Productos Finales',
                    'cvanulado03' => 'N',
                    'UID' => $this->uid
                    );
            $documento = $this->plan_diario->generar_egreso_produccion($documento_egreso);
            $datos = array(
                    'cantidad_actual' => $cantidad_actual,
                    'valor_actual' => $valor_actual,
                    'precio_unitario' => $precio_unitario,
                    'codigo' => $producto['codigo_receta']
                    );
            $this->Producto->actualizar_inventario_producto($datos);
            $ocu ++;
            }   
        }

        $this->update_numero_egreso_produccion($secuencial);
        return $numero_egreso_produccion;
    }

    function generar_ingreso_produccion_recetas()
    {
        $secuencial = $this->get_numero_ingreso_produccion();
        $numero_ingreso_produccion = $this->serie_sri . str_pad($secuencial,9,'0',STR_PAD_LEFT);
        $productos_receta = $this->plan_diario->get_producto_receta($this->numero_produccion);
        $ocu = 0;
        $fecha = date('Y-m-d H:i:s');
        $nuevafecha = date('Y-m-d H:i:s',strtotime('+2 seconds', strtotime($fecha)));
        foreach ($productos_receta as $key => $producto) {
            $datos_producto = $this->get_producto_by_codigo($producto['codigo_receta']);
            if ($datos_producto['compuesto'] == 'S')
            {
            $costo = $this->Producto->get_costo_producto_by_codigo($producto['codigo_receta']);

            $valor_transaccion = (float) $costo * (float) $producto['cantidad_producida'];
            $costo_unitario = (float)$valor_transaccion / (float) $producto['cantidad_producida'];
            $cantidad_actual = (float)$datos_producto['cantidad_actual'] + (float)$producto['cantidad_producida'];
            $valor_actual = (float)$datos_producto['valor_actual'] + (float) $valor_transaccion;
            $precio_unitario = (float) $valor_actual / (float) $cantidad_actual;
            $documento_ingreso = array(
                    'TIPOTRA03' => '06',
                    'NOCOMP03' => $numero_ingreso_produccion,
                    'OCURREN03' => str_pad($ocu, 4, '0', STR_PAD_LEFT),
                    'CODPROD03' => $producto['codigo_receta'],
                    'FECMOV03' => $nuevafecha,
                    'CANTID03' => $producto['cantidad_producida'],
                    'VALOR03' => $valor_transaccion,
                    'PU03' => $costo_unitario,
                    'CODDEST03' => '9999',
                    'CANTACT03' => $cantidad_actual,
                    'VALACT03' => $valor_actual,
                    'PRECUNI03' => $precio_unitario,
                    'nomdest03' => $this->numero_produccion,
                    'nopedido03' => $this->numero_produccion,
                    'detalle03' => $this->numero_produccion.' Producto : '.$producto['codigo_receta']. ' Ingreso de Produccion Automatico',
                    'cvanulado03' => 'N',
                    'UID' => $this->uid
                    );
            $documento = $this->plan_diario->generar_ingreso_produccion($documento_ingreso);

            $datos_produccion = array(
                                'numero_produccion' => $this->numero_produccion,
                                'cantidad_producida' => $producto['cantidad_producida'],
                                'codigo_receta' => $producto['codigo_receta']
                                );
            $this->plan_diario->actualizar_producto_produccion($datos_produccion);
            $datos = array(
                    'cantidad_actual' => $cantidad_actual,
                    'valor_actual' => $valor_actual,
                    'precio_unitario' => $precio_unitario,
                    'codigo' => $producto['codigo_receta']
                    );
            $this->Producto->actualizar_inventario_producto($datos);
            $ocu ++;
            }   
        }
        $this->update_numero_ingreso_produccion($secuencial);
        return $numero_ingreso_produccion;
    }

    function generar_ingreso_produccion_pfinal()
    {
        $secuencial = $this->get_numero_ingreso_produccion();
        $numero_ingreso_produccion = $this->serie_sri . str_pad($secuencial,9,'0',STR_PAD_LEFT);
        $productos_finales = $this->plan_diario->get_productos_finales($this->numero_produccion);
        $ocu = 0;
        $fecha = date('Y-m-d H:i:s');
        foreach ($productos_finales as $key => $producto) {
            $datos_producto = $this->get_producto_by_codigo($producto['id_producto']);
            $costo = $this->get_costo_producto_final($producto['id_receta']);

            $valor_transaccion = (float) $costo * (float) $producto['cantidad'];
            $costo_unitario = (float)$valor_transaccion / (float) $producto['cantidad'];
            $cantidad_actual = (float)$datos_producto['cantidad_actual'] + (float)$producto['cantidad'];
            $valor_actual = (float)$datos_producto['valor_actual'] + (float) $valor_transaccion;
            $precio_unitario = (float) $valor_actual / (float) $cantidad_actual;
            $documento_ingreso = array(
                    'TIPOTRA03' => '06',
                    'NOCOMP03' => $numero_ingreso_produccion,
                    'OCURREN03' => str_pad($ocu, 4, '0', STR_PAD_LEFT),
                    'CODPROD03' => $producto['id_producto'],
                    'FECMOV03' => $fecha,
                    'CANTID03' => $producto['cantidad'],
                    'VALOR03' => $valor_transaccion,
                    'PU03' => $costo_unitario,
                    'CODDEST03' => '9999',
                    'CANTACT03' => $cantidad_actual,
                    'VALACT03' => $valor_actual,
                    'PRECUNI03' => $precio_unitario,
                    'nomdest03' => $this->numero_produccion,
                    'nopedido03' => $this->numero_produccion,
                    'detalle03' => $this->numero_produccion.' Producto : '.$producto['id_producto']. ' Ingreso de Produccion Automatico',
                    'cvanulado03' => 'N',
                    'UID' => $this->uid
                    );
            $documento = $this->plan_diario->generar_ingreso_produccion($documento_ingreso);
            $datos = array(
                    'cantidad_actual' => $cantidad_actual,
                    'valor_actual' => $valor_actual,
                    'precio_unitario' => $precio_unitario,
                    'codigo' => $producto['id_producto']
                    );
            $this->Producto->actualizar_inventario_producto($datos);
            $ocu ++;
        }
        $this->update_numero_ingreso_produccion($secuencial);
        return $numero_ingreso_produccion;
    }

    function get_costo_producto_final($id_receta)
    {
        $array_receta = json_decode($id_receta,true);
        $resultSubRecetas = 0;
        foreach ($array_receta as  $valor){

            $costo = $this->Producto->get_costo_producto_by_codigo($valor['receta'],$tabla);

            $resultSubRecetas += $costo;
        }
        return $resultSubRecetas;
    }

    function get_producto_by_codigo($codigo)
    {
        $producto = $this->Producto->get_producto_by_codigo($codigo);
        return $producto;
    }

    function get_numero_egreso_produccion()
    {
        $numero_egreso_produccion = $this->plan_diario->get_numero_documento('41', '76');
        return (int) $numero_egreso_produccion + 1;
    }

    function update_numero_egreso_produccion($numero)
    {
        $this->plan_diario->update_numero_documento('41', '76', $numero);
    }

    function get_numero_ingreso_produccion()
    {
        $numero_egreso_produccion = $this->plan_diario->get_numero_documento('41', '06');
        return (int) $numero_egreso_produccion + 1;
    }

    function update_numero_ingreso_produccion($numero)
    {
        $this->plan_diario->update_numero_documento('41', '06', $numero);
    }

    function guardar_post(){

        $datos = $this->post();
        $numero_produccion = $datos['data']['numero_produccion'];

        foreach ($datos['data']['data1'] as  $valor) {
            $valida_registro_planificacion = $this->plan_diario->consulta_plan_diario($valor);
            if($valida_registro_planificacion == 1)
                $this->plan_diario->update_plan_diario($valor);
            else
                $this->plan_diario->guardar_plan_diario($valor);
        }


        if(count($datos['data']['data_opciones_eliminadas']) > 0){

            $this->plan_diario->elimiar_opciones($datos['data']['data_opciones_eliminadas']);

        }

        if(count($datos['data']['data_clientes_eliminados']) > 0){

            $this->plan_diario->elimiar_clientes($datos['data']['data_clientes_eliminados']);

        }



        if ($numero_produccion != ""){
            $this->IDB = $this->input->get('IDB');
            $this->numero_produccion = $numero_produccion;
            $this->fecha_plan_diario = $datos['data']['fecha'];
            $this->codigo_categoria = $datos['data']['codigo_categoria'];
            $this->eliminar_orden_produccion();
            $this->insertar_orden_produccion();

            $requisiciones = $this->plan_diario->get_num_requisicion_by_produccion($numero_produccion);
            foreach ($requisiciones as $key => $requisicion) {
                $this->numero_requisicion = $requisicion['numdoc'];
                $this->secuencial_requisicion = $requisicion['sec'];
                $this->clasificacion_prod_final = $requisicion['clasificacion'];
                $this->tipo_cocina = $requisicion['tipo_cocina'];
                
                $productos_requisicion = $this->plan_diario->get_requisicion_by_numero($numero_produccion,$requisicion);
                $cantidad_entrante = $this->total_cantidad_entrante($productos_requisicion);
                if ((float)$cantidad_entrante <= 0)
                {
                    $this->eliminar_orden_requisicion();
                    $tmp_lismaehis = $this->obtener_detalle_lismaehis_by_cocina_get();
                    if (count($tmp_lismaehis) > 0)
                    {
                        $this->ocu_req = 0;
                        $this->generar_requisicion($tmp_lismaehis);
                    }
                }else{
                    $productos_receta = $this->plan_diario->recetas_lismaehis_by_cocina_get($this->numero_produccion ,$this->tipo_cocina, $requisicion['clasificacion']);
                    $this->editar_datos_requisicion($productos_receta);
                }

            }

        } 
        echo format_response(true);
    }

    function editar_datos_requisicion($productos_receta)
    {
        foreach ($productos_receta as $key => $producto) {
            $receta = $this->plan_diario->get_componentes_produccion($producto['codprod20t'], $this->numero_produccion);

            foreach ($receta as $key => $ingrediente) {
                $ocu_receta = $ingrediente['ocucomp20'];
                $this->tipo_cocina = $ingrediente['tipo_cocina'];
                $cant_producir = $ingrediente['cant_producir'];
                $this->codigo_receta = $ingrediente['codprod20t'];
                $producto_receta = $this->get_producto_by_codigo($ingrediente['codprod20t']);
                $this->nombre_receta = $producto_receta['descripcion'];
                $existe_ingrediente = false;
                $existe_ingrediente = $this->plan_diario->verificar_ingrediente_en_requisicion($this->numero_produccion, $this->numero_requisicion, $ingrediente, $this->codigo_receta);
                if (!$existe_ingrediente)
                    $existe_ingrediente = $this->plan_diario->verificar_ingrediente_en_requisicion2($this->numero_produccion, $this->numero_requisicion, $ingrediente, $this->codigo_receta);

                if ($existe_ingrediente)
                {
                    $subReceta1 = $this->plan_diario->get_componentes($ingrediente['codprod20']);
                    if (count($subReceta1) > 0)
                    {
                        foreach ($subReceta1 as $key => $ingrediente1) {
                            $codigo_subreceta = $ingrediente1['codprod20t'];
                            $existe_ingrediente = false;
                            $existe_ingrediente = $this->plan_diario->verificar_subreceta_en_requisicion($this->numero_produccion, $this->numero_requisicion, $ingrediente1);
                            if ($existe_ingrediente)
                            {
                                $this->validar_cantidades_pedidas($ingrediente1, $cant_producir, $codigo_subreceta);
                            }
                            else{
                                $ocu = $this->plan_diario->ocurrencia_nuevo_ingrediente($this->numero_produccion, $this->numero_requisicion);
                                $this->ocu_req = (int)$ocu[0]['ocu'];
                                $this->built_datos_requisicion($ingrediente1, $cant_producir, $ocu_receta, $codigo_subreceta);
                            }
                        }
                    }else{
                        $this->validar_cantidades_pedidas($ingrediente, $cant_producir, '');
                    }
                }else{
                    $ocu = $this->plan_diario->ocurrencia_nuevo_ingrediente($this->numero_produccion, $this->numero_requisicion);
                    $this->ocu_req = (int)$ocu[0]['ocu'];
                    $producto = array($ingrediente);
                    $this->generar_requisicion($producto);
                }
            }
        }
        
    }

    function validar_cantidades_pedidas($ingrediente, $cant_producir, $codigo_subreceta)
    {
        $cant_solicitada = number_format($ingrediente['cantidad20'] * $cant_producir, 2, '.', '');
        $cantidades = $this->plan_diario->get_cantidad_pedida_by_producto($this->numero_produccion, $this->numero_requisicion, $ingrediente, $this->codigo_receta, $codigo_subreceta);
        $cant_pedida = (count($cantidades) > 0) ? $cantidades[0]['cantsol'] : 0 ;
        $cant_entregada = ((float)$cantidades[0]['cantent'] > 0) ? (float) $cantidades[0]['cantent'] : 0 ;
        
        if ((float)$cant_solicitada > (float)$cant_pedida)
        {
            $cant_devolver = 0;
            $cant_diferencia = number_format((float)$cant_solicitada - (float)$cant_pedida, 2, '.', '');
            $cant_devolver = ((float)$cantidades[0]['cant_a_devolver'] > 0) ? (float) $cantidades[0]['cant_a_devolver'] : 0 ;
            if ($cant_devolver > 0)
                $cant_devolver = number_format((float)$cant_devolver - (float)$cant_diferencia, 2, '.', '');
            $this->actualizar_producto_requisicion($cant_solicitada, $cant_devolver, $ingrediente['codprod20'], $codigo_subreceta);
        }
        if ((float)$cant_solicitada < (float)$cant_pedida)
        {
            $cant_devolver = 0;
            if ($cant_entregada > $cant_solicitada)
                $cant_devolver = number_format((float)$cant_entregada - (float)$cant_solicitada, 2, '.', '');
            $this->actualizar_producto_requisicion($cant_solicitada, $cant_devolver, $ingrediente['codprod20'], $codigo_subreceta);
        }

    }

    function actualizar_producto_requisicion($cant_solicitada, $cant_devolver, $producto, $codigo_subreceta)
    {
        $datos = array(
                'cantsol' => $cant_solicitada,
                'cant_a_devolver' => $cant_devolver
                );
        $this->plan_diario->actualizar_producto_requisicion($datos, $this->numero_produccion, $this->numero_requisicion, $producto, $this->codigo_receta, $this->clasificacion_prod_final, $codigo_subreceta);
        return;
    }

    function eliminar_orden_requisicion()
    {
        $this->plan_diario->eliminar_orden_requisicion($this->numero_produccion, $this->numero_requisicion);
    }

    function eliminar_orden_produccion()
    {
        $this->plan_diario->eliminar_tmp_orden_produccion($this->numero_produccion);
        $this->plan_diario->eliminar_orden_produccion($this->numero_produccion);
    }

    function total_cantidad_entrante($requisicion)
    {
        $total = 0.00;
        foreach ($requisicion as $key => $producto) {
            $total += (float)$producto['cantent'];
        }
        return $total;

    }

    function cliente_interno_get()
    {
        $codigo_cliente_interno = $this->Cliente->cliente_interno();
        return $codigo_cliente_interno;
    }

    function genera_pdf_requisicion_post()
    {
        setlocale(LC_TIME, 'es_EC.UTF-8');
        $filter = $this->post();
        $orden_produccion = $filter['codigo'];
        $fecha = $filter['fecha'];
        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edison Calderon');
        $pdf->SetTitle('Receta');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);

        $numero_requisiciones = $this->num_requisicion_by_produccion($orden_produccion);

        $page = 1;
        foreach ($numero_requisiciones as $key => $num_requisicion) {
        $cabecera = false;
        if ($page == 1) {
            $cabecera = true;
            $pdf->AddPage();
            $clas = $num_requisicion['clasificacion'];
        }
        else{
            if ($clas != $num_requisicion['clasificacion']){
                $cabecera = true;
                $pdf->AddPage();
                $clas = $num_requisicion['clasificacion'];
            }
        }
        $html = '';
        $html .= "<style type=text/css>";
        $html .= "table  , td {border: 0px solid black; border-collapse: collapse;}";
        $html .= "h2{color: red; font-weight: bold;  text-align:center;}";
        $html .= "</style>";
        if ($cabecera)
        {
        $html .= "<h2>Requisicion a Bodega para ".strftime("%A, %d de %B de %Y",strtotime($fecha))."</h2>";
        $html .= "<h2>".$this->get_clasificacion($num_requisicion['clasificacion'])."</h2>";
        }
        $html .= "<table width='100%'>";
        
        $html .= '<tr>
                    <td align="center" width="50%"><b>COCINA</b></td>
                    <td align="center" width="25%">'.$num_requisicion['observacion'].'</td>
                    <td align="center" width="25%"><b>Num: '.$num_requisicion['numdoc'].'</b></td>
                  </tr>';    
        $html .= '<tr>
                    <td colspan = "2" align="center" width="34%"><h5><b>RECETAS</b></h5></td>
                    <td align="center" width="20%"><h5><b>INGREDIENTE</b></h5></td>
                    <td align="center" width="10%"><h5><b>U. MEDIDA</b></h5></td>
                    <td align="center" width="12%"><h5><b>CANT. PEDIDA</b></h5></td>
                    <td align="center" width="12%"><h5><b>CANT. ENTREGADA</b></h5></td>
                    <td align="center" width="12%"><h5><b>CANT. EN BODEGA</b></h5></td>
                  </tr>';

        $recetas_en_produccion = $this->productos_receta_by_requisicion_get($orden_produccion, $num_requisicion['numdoc']);
        foreach ($recetas_en_produccion as $key => $receta) {
            $detalle_receta = $this->detalle_receta_by_requisicion_get($orden_produccion, $num_requisicion['numdoc'], $receta['codigo_receta'], $receta['ocu_receta']);
            $datos_producto_re = $this->get_producto_by_codigo($receta['codigo_receta']);
            $cant_detalle_receta = count($detalle_receta);
            $flag = 1;
            $imprimir_sub = false;
            $buscar_sub = true;
            $producto_sub = 0;
            $cant_ingre_impreso = 0;
            $cant_ingre_subreceta = 0;
            $colspan = 2;
            $cant_subreceta = 0;
            //ingredientes
            foreach ($detalle_receta  as $producto_receta) 
            {
                //consulta de subrecetas
                if ($buscar_sub)
                {

                    $subreceta = $this->productos_subreceta_by_requisicion_get($orden_produccion, $num_requisicion['numdoc'], $producto_receta['codigo_receta']);
                    $cant_subreceta = count($subreceta);
                    if ( (int) $cant_subreceta > 0)
                    {
                        $colspan = 1;
                        $imprimir_sub = true;
                        $datos_producto_subre = $this->get_producto_by_codigo($producto_receta['codigo_subreceta']);
                        
                        $detalle_subreceta = $this->detalle_subreceta_by_requisicion_get($orden_produccion, $num_requisicion['numdoc'], $producto_receta['codigo_receta'], $producto_receta['ocu_receta'], $producto_receta['codigo_subreceta']);
                        $cant_ingre_subreceta = count($detalle_subreceta);
                        if ($cant_ingre_subreceta > 1)
                            $buscar_sub = false;
                    }
                }

                $html .='<tr>';
                if ($flag != 0)
                $html .= '<td rowspan="'.$cant_detalle_receta.'" colspan ="'.$colspan.'"><h5>'.$datos_producto_re['descripcion'].'</h5></td>';
                $datos_producto = $this->get_producto_by_codigo($producto_receta['codprod']);

                //subreceta
                if ($imprimir_sub)
                $html .= '<td rowspan="'.$cant_ingre_subreceta.'"><h6>'.$datos_producto_subre['descripcion'].'</h6></td>';

                $html .= '<td><h6>Total '.$producto_receta['desprod'] .'</h6></td>';
                $html .= '<td align="left"><h6>'.$datos_producto['unidad_medida'].'</h6></td>';
                $html .= '<td align="right">'.number_format($producto_receta['cantsol'],2).'</td>';
                $html .= '<td align="right">'.number_format($producto_receta['cantent'],2).'</td>';
                $html .= '<td align="right">'.number_format($datos_producto['stock'],2).'</td>';
                $html .= '</tr>';
                $flag = 0;

                if ((int)$cant_subreceta > 0)
                {
                    $cant_ingre_impreso++;
                    if ((int)$cant_ingre_impreso == (int)$cant_ingre_subreceta)
                    {
                        $buscar_sub = true;
                        $imprimir_sub = false;
                        $producto_sub++;
                        $cant_ingre_impreso = 0;
                    }else{
                        $imprimir_sub = false;
                    }
                }
                $colspan = 2;
            }
        }

        $html .= '</table>';
        $html .= '</br>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '');
        $page++;
        }
        $nombre_archivo = utf8_decode("receta.pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    function validar_recetas_get()
    {
        $this->validacion_receta['validacion'] = true;
        $this->validacion_receta['producto'] = '';

        if ($this->serie_sri == '' || $this->serie_sri == null)
        {
            $this->validacion_receta['validacion'] = false;
            $this->validacion_receta['error'] = " No esta configurada la serie de la Bodega";
            return;
        }

        if ($this->clienteGenerico->codigo_cliente == "" || $this->clienteGenerico->codigo_cliente == " " || $this->clienteGenerico->codigo_cliente == null)
        {
            $this->validacion_receta['validacion'] = false;
            $this->validacion_receta['error'] = " No esta configurado Codigo Cliente Interno o No existe Cliente configurado";
            return;
        }

        $documento_configurado = $this->plan_diario->get_numero_documento('71', '93');
        if ($documento_configurado == '' || $documento_configurado == null)
        {
            $this->validacion_receta['validacion'] = false;
            $this->validacion_receta['error'] = " No esta configurado secuencial de documento de Produccion (71 - 93)";
            return;
        }

        $existe_produccion = $this->plan_diario->consultar_si_existe_produccion($this->numero_produccion);
        if ($existe_produccion)
        {
            $this->validacion_receta['validacion'] = false;
            $this->validacion_receta['error'] = $this->numero_produccion . ": Numero Produccion ya existe";
            return;

        }
        $recetas = $this->plan_diario->get_productos_para_produccion($this->fecha_plan_diario, $this->codigo_categoria);
        if (count($recetas) > 0 && is_array($recetas))
        {

        foreach ($recetas as $key => $receta_producto) {
            $opciones = json_decode($receta_producto['id_receta']);
            foreach ($opciones as $key => $producto_receta) {
                $this->validacion_receta['producto'] = $producto_receta->nom_receta;
                $exist_receta = $this->creador_recetas->verificar_existencia_receta($producto_receta->receta);
                if ($exist_receta)
                {

                $receta = $this->plan_diario->get_componentes($producto_receta->receta);
                if (count($receta) <= 0)
                {
                    $this->validacion_receta['validacion'] = false;
                    $this->validacion_receta['error'] = " No tiene receta";
                    break;
                }
                
                $cocina = $this->plan_diario->get_tipo_cocina_by_producto($producto_receta->receta);
                  if (empty($cocina[0]['tipo_cocina'])){
                    $this->validacion_receta['validacion'] = false;
                    $this->validacion_receta['error'] = " No tiene tipo de Cocina";
                    break;
                  }
                }
            }
            if (!$this->validacion_receta['validacion'])
                break;
        }
        }else{
            $this->validacion_receta['validacion'] = false;
            $this->validacion_receta['error'] = " No se encontraron datos";
            return;
        }
    }

    function num_requisicion_by_produccion($orden_produccion)
    {
        $numero_requisiciones = $this->plan_diario->get_num_requisicion_by_produccion($orden_produccion);
        return $numero_requisiciones;
    }

    function num_requisicion_by_produccion_get($orden_produccion)
    {
        $validacion = false;
        $requisiciones = $this->plan_diario->get_num_requisicion_by_produccion($orden_produccion);
        if (count($requisiciones) > 0)
            $validacion = true;

        echo format_response($validacion);
    }

    function productos_receta_by_requisicion_get($orden_produccion, $numero_requisicion)
    {
        $productos_receta = $this->plan_diario->get_productos_receta_by_requisicion($orden_produccion, $numero_requisicion);
        return $productos_receta;
    }

    function detalle_receta_by_requisicion_get($orden_produccion, $num_requisicion, $codigo_receta, $ocu_receta)
    {
        $detalle_req = $this->plan_diario->get_requisicion_by_requisicion($orden_produccion, $num_requisicion, $codigo_receta, $ocu_receta);
        return $detalle_req;
    }

    function productos_subreceta_by_requisicion_get($orden_produccion, $numero_requisicion, $codigo_receta)
    {
        $productos_subreceta = $this->plan_diario->get_productos_subreceta_by_requisicion($orden_produccion, $numero_requisicion, $codigo_receta);
        return $productos_subreceta;
    }

    function detalle_subreceta_by_requisicion_get($orden_produccion, $num_requisicion, $codigo_receta, $ocu_receta, $codigo_subreceta)
    {
        $detalle_req = $this->plan_diario->get_detalle_subreceta_by_requisicion($orden_produccion, $num_requisicion, $codigo_receta, $ocu_receta, $codigo_subreceta);
        return $detalle_req;
    }

    function categorias_producto_final_get($codigo_producto)
    {
        $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($codigo_producto);
        echo format_response($config_prod_final);
    }

    function productos_con_recetas_by_categoria_get($categoria)
    {
        $productos = $this->Producto->get_productos_con_receta_by_categoria($categoria);
        echo format_response($productos);
    }

    function modificar_receta_produccion_post()
    {
        $this->IDB = $this->input->get('IDB');
        $datos = $this->post();
        $numero_produccion = $datos['data']['numero_produccion'];

        foreach ($datos['data']['data2'] as $key => $value) {
          $this->plan_diario->eliminar_ingrediente($value, $numero_produccion);
        }

        foreach ($datos['data']['data1'] as $key => $value) {
            $receta = array();
            $receta['codigo_producto'] = $value['codigo_producto'];
            $receta['codigo_ingrediente'] = $value['codigo_ingrediente'];
            $existe_receta = $this->plan_diario->verificar_receta($receta, $numero_produccion);
            if ($existe_receta)
            {
            $existe_ingrediente = $this->plan_diario->verificar_ingrediente_receta($receta, $numero_produccion);
            if ($existe_ingrediente){
                $producto = array(
                            'unidmed20' => $value['unidad_medida'],
                            'cantidad20' => $value['cantidad_unitaria'],
                            'costo20' => $value['costo_producto'],
                            );
                $this->plan_diario->actualizar_receta($producto, $receta, $numero_produccion);
            }else{

                $data_receta = $this->plan_diario->ocurrencia_detalle_produccion_get($receta, $numero_produccion);
                $ocurren20 = (int)$data_receta[0]['ocurren20'] + 1;
                $ocucomp20 = (int)$data_receta[0]['ocucomp20'];
                $producto = array(
                            'numord20' => $numero_produccion,
                            'codprod20t' => $value['codigo_producto'],
                            'codprod20' => $value['codigo_ingrediente'],
                            'ocurren20' => str_pad($ocurren20, 2, '0', STR_PAD_LEFT),
                            'ocucomp20' => str_pad($ocucomp20, 2, '0', STR_PAD_LEFT),
                            'desprod20' => $value['descripcion_ingrediente'],
                            'unidmed20' => $value['unidad_medida'],
                            'cantidad20' => $value['cantidad_unitaria'],
                            'cantfalta20' => '0.00',
                            'costo20' => $value['costo_producto'],
                            'fecmod20' => date('Y-m-d H:i:s'),
                            'tipo20' => 'P',
                            'bodega20' => $this->IDB,
                            'circunferencia20' => '0',
                            'tipo_cocina' => $data_receta[0]['tipo_cocina'],
                            'cant_producir' => $data_receta[0]['cant_producir']
                            );
                $this->plan_diario->guardar_receta($producto);
            }
            }
        }
            echo $this->response('ok',200);
    }

    function get_clasificacion($codigo)
    {
        $clasificacion = 'SIN CLASIFICACION';
        if ($codigo != "" || $codigo != NULL)
            $clasificacion = $this->plan_diario->get_clasificacion($codigo);

        return $clasificacion;

    }

    function validar_produccion()
    {
        $this->validacion_produccion['validacion'] = true;
        $documento_configurado = $this->plan_diario->get_numero_documento('41', '76');
        if ($documento_configurado == '' || $documento_configurado == null)
        {
            $this->validacion_produccion['validacion'] = false;
            $this->validacion_produccion['error'] = " No esta configurado secuencial de documento de Egreso de Produccion (76)";
            return;
        }

        $documento_configurado = $this->plan_diario->get_numero_documento('41', '06');
        if ($documento_configurado == '' || $documento_configurado == null)
        {
            $this->validacion_produccion['validacion'] = false;
            $this->validacion_produccion['error'] = " No esta configurado secuencial de documento de Ingreso de Produccion (06)";
            return;
        }
    }

    function valida_materia_prima_recetas()
    {
        $this->validacion_produccion = array();
        $this->validacion_produccion['validacion'] = true;
        $productos_receta = $this->plan_diario->get_producto_receta($this->numero_produccion);
        foreach ($productos_receta as $key => $producto) {
            $datos_producto = $this->get_producto_by_codigo($producto['codigo_receta']);

            if ((float)$datos_producto['cantidad_actual'] < (float)$producto['cantidad_producida'])
            {
                $this->validacion_produccion['validacion'] = false;
                $this->proceso_produccion['producto_error'] = $datos_producto['descripcion'];
                $this->proceso_produccion['error'] = 'No hay cantidad suficiente: ';
                break;
            }
        }
    }

    function genera_pdf_planDiario_get()
    {

        

        setlocale(LC_TIME, 'es_EC.UTF-8');
        /*$filter = $this->post();
        $orden_produccion = $filter['codigo'];*/
        $fecha = "2016-01-01";//$filter['fecha'];
        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Provedatos');
        $pdf->SetTitle('Receta');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->AddPage();
        
        $html = '';

        $html .= "<h2>FECHA: ".strftime("%A, %d de %B de %Y",strtotime($fecha))."</h2>";

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '');
                
        $nombre_archivo = utf8_decode("receta.pdf");
        $pdf->Output($nombre_archivo, 'I');

    }
   
}
?>
