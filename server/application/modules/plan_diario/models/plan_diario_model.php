<?php
include_once(APPPATH ."models/base_model.php");
Class Plan_diario_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    
    

    function get_productos_pre_plan_por_dia($fecha, $codigo_categoria) {

        $dia = date('w', strtotime($fecha));
        $dia = ($dia != "0") ? $dia : '7' ;
        
        $sql = $this->db->query('SELECT `id_producto`, `desprod01` AS name_product, `dia`, clasific01 as clasificacion
                            FROM (`planificacion_produccion`)
                            JOIN '.$this->matriz.'.maecte ON `id_cliente` = `codcte01` AND catcte01 = "'.$codigo_categoria.'"
                            JOIN `maepro` ON `id_producto` = `codprod01`
                            WHERE `dia` =  "'.$dia.'"
                            AND productoFinal = "S"
                            AND "'.$fecha.'" >= `desde`
                            AND "'.$fecha.'" <= `hasta` 
                            GROUP BY `id_producto`, `dia`');
        //echo $this->db->last_query();
        return  $sql->result_array();
    }

    function buscar_orden_produccion($fecha, $codigo_categoria)
    {
        $dia = date('w', strtotime($fecha));
        $dia = ($dia != "0") ? $dia : '7' ;
        
        $sql = $this->db->query('SELECT orden_produccion as numero_produccion,  estado_produccion
                            FROM (`planificacion_produccion`)
                            JOIN '.$this->matriz.'.maecte ON `id_cliente` = `codcte01` AND catcte01 = "'.$codigo_categoria.'"
                            JOIN `maepro` ON `id_producto` = `codprod01`
                            WHERE `dia` =  "'.$dia.'"
                            AND "'.$fecha.'" >= `desde`
                            AND "'.$fecha.'" <= `hasta` 
                            AND `orden_produccion` <> "" 
                            GROUP BY `dia`, orden_produccion');
        //echo $this->db->last_query();
        return  $sql->result_array();
    }

    function get_productos_para_produccion($fecha, $codigo_categoria) {

        $dia = date('w', strtotime($fecha));
        $dia = ($dia != "0") ? $dia : '7' ;
        
        $sql = $this->db->query('SELECT `id_producto`, `desprod01` AS name_product, sum(cantidad) as cantidad, `dia`, id_receta, opcion
                            FROM (`planificacion_produccion`)
                            JOIN '.$this->matriz.'.maecte ON `id_cliente` = `codcte01` AND catcte01 = "'.$codigo_categoria.'"
                            JOIN `maepro` ON `id_producto` = `codprod01`
                            WHERE `dia` =  "'.$dia.'"
                            AND "'.$fecha.'" >= `desde`
                            AND "'.$fecha.'" <= `hasta`
                            GROUP BY `id_producto`, `opcion`');
        //echo $this->db->last_query();
        return  $sql->result_array();
    }


    function get_total_productos_por_dia($fecha) {

        $dia = date('w', strtotime($fecha));
        $dia = ($dia != "0") ? $dia : '7' ;
        
        $sql = $this->db->query('SELECT `id_producto`, `desprod01` AS name_product, `dia`, cantidad,codcate,desccate,opcion
                            FROM (`planificacion_produccion`)
                            JOIN '.$this->matriz.'.maecte ON id_cliente = codcte01 
                            JOIN '.$this->matriz.'.categorias ON maecte.catcte01 = categorias.codcate
                            JOIN `maepro` ON `id_producto` = `codprod01`
                            WHERE `dia` =  "'.$dia.'"
                            AND "'.$fecha.'" >= `desde`
                            AND "'.$fecha.'" <= `hasta` 
                            GROUP BY `id_producto`, dia, codcate');
        
        //echo $this->db->last_query();
        return  $sql->result_array();
    }

     

    function get_clientes_preplan_por_dia($fecha, $codigo_producto) {

        $dia = date('w', strtotime($fecha));
        $dia = ($dia != "0") ? $dia : '7' ;

        $sql = $this->db->query('SELECT id_cliente, nomcte01 AS name_client, desprod01 AS name_product, dia,id_receta, cantidad_dispatch, estado_produccion,  cantidad_entregada, opcion,id_cliente,codigo,id_producto, cantidad, estado_preplan as estado, desde, hasta
                            FROM (`planificacion_produccion`)
                            JOIN '.$this->matriz.'.maecte ON `id_cliente` = `codcte01`
                            JOIN maepro on id_producto = codprod01                          
                            WHERE `dia` =  "'.$dia.'"
                            and id_producto = "'.$codigo_producto.'"
                            AND "'.$fecha.'" >= `desde`
                            AND "'.$fecha.'" <= `hasta`
                            and estado_preplan = "1"
                            order by opcion
                            ');
        //echo $this->db->last_query();

      
        return  $sql->result_array();
    }

    function check_in_range($start_date, $end_date, $evaluame) {
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($evaluame);

        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    function generar_produccion($datos)
    {

        $this->db->insert("maeprd30", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function generar_detalle_produccion($datos)
    {
        $this->db->insert("lismaehis", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function generar_tmp_lismaehis($datos)
    {
        $this->db->insert("tmp_lismaehis", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function generar_detalle_requisicion($datos)
    {
        $this->db->insert($this->matriz.".reqprod", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function get_componentes($producto_receta,$tabla = 'lismae')
    {
        $this->db->select('*');
        $this->db->from($tabla);
        $this->db->where('codprod20t', $producto_receta);
        $result = $this->db->get();
        return $result->result_array();

    }

    function get_componentes_produccion($producto_receta, $numero_produccion)
    {
        $this->db->select('*');
        $this->db->from('lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $this->db->where('codprod20t', $producto_receta);
        $result = $this->db->get();
        return $result->result_array();
    }

    function get_numero_documento($numtab, $codtab)
    {
      
        $this->db->where("numtab", $numtab);
        $this->db->where("codtab", $codtab);
        $this->db->select('ad1tab')->from("maetab");
        $sql = $this->db->get();
        $resultado = $sql->result_array();

        return $resultado[0]['ad1tab'];

    }

    function update_numero_documento($numtab, $codtab, $valor)
    {
      $data = array(
               'ad1tab' => $valor               
              );

      $this->db->where("numtab", $numtab);
      $this->db->where("codtab", $codtab);
      $this->db->update('maetab', $data); 
    }

    function get_serie_sri()
    {

        $this->db->select('ad7tab');
        $this->db->from('maetab');
        $this->db->where('numtab', '01');
        $this->db->where('codtab', '26');
        $result = $this->db->get();
        $result =  $result->result_array();
        return $result[0]['ad7tab'];
    }

    function get_numero_requisicion()
    {
        $fecha = date('Y-m-d');
        $sql = $this->db->query('SELECT IFNULL(max(sec), 0) as secuencial FROM '.$this->matriz.'.reqprod WHERE `fecreg` >= "'.$fecha.'"');
        //echo $this->db->last_query();
        $result =  $sql->result_array();
        return $result[0]['secuencial'];
    }


    function get_bodega_materia_prima()
    {
        $this->db->select('codtab as codigo_bodega, ad7tab as conexion');
        $this->db->from($this->matriz.'.maetab');
        $this->db->where('ad8tab', 'BMP');
        $result = $this->db->get();
        $result =  $result->result_array();
        return $result[0];
    }

    function get_requisicion_by_produccion($numero_produccion)
    {
        $this->db->select('codprod, desprod, cantsol, cantent');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $result = $this->db->get();
        return $result->result_array();
    }

    function get_num_requisicion_by_produccion($numero_produccion)
    {
        $this->db->select('numdoc, observacion, tipo_cocina, sec, clasificacion');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->group_by('numdoc');
        $result = $this->db->get();
        return $result->result_array();
    }

    function get_requisicion_by_requisicion($numero_produccion, $requisicion, $codigo_receta, $ocu_receta)
    {
        $this->db->select('codprod, desprod, cantsol, cantent, codigo_receta, codigo_subreceta, ocu_receta');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->where('numdoc', $requisicion);
        $this->db->where('codigo_receta', $codigo_receta);
        $this->db->where('ocu_receta', $ocu_receta);
        $result = $this->db->get();
        return $result->result_array();
    }

    function get_requisicion_by_numero($numero_produccion, $requisicion)
    {
        $this->db->select('codprod, desprod, cantsol, cantent, codigo_receta, codigo_subreceta, ocu_receta');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->where('numdoc', $requisicion['numdoc']);
        $this->db->where('tipo_cocina', $requisicion['tipo_cocina']);
        $this->db->where('sec', $requisicion['sec']);
        $result = $this->db->get();
        return $result->result_array();
    }

    function actualizar_producto_produccion($datos)
    {
        $data = array(
            'status30' => '04',
            'cantfac30' => $datos['cantidad_producida'],
            'cantafac30' => '0'
        );
        $this->db->where('nopedido30', $datos['numero_produccion']);
        $this->db->where('codprod30', $datos['codigo_receta']);
        $result = $this->db->update('maeprd30', $data);
        return $result;

    }

    function obtener_materia_prima_producto_receta($numero_produccion)
    {
        $this->db->select('codprod, sum(cantsol) as cantsol, numdoc as numero_requisicion, 
                            numordenp as numero_produccion, codigo_receta as producto_receta, codigo_receta, desprod');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->group_by('codprod');
        $result = $this->db->get();
        return $result->result_array();

    }

    function generar_egreso_produccion($datos)
    {
        $this->db->insert("movpro", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function generar_ingreso_produccion($datos)
    {
        $this->db->insert("movpro", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function get_producto_receta($numero_produccion)
    {
        $this->db->select('codprod30 as codigo_receta, codcte30 as codigo_cliente, 
                        nomcte30 as nombre_cliente, cantped30 as cantidad_producida');
        $this->db->from('maeprd30');
        $this->db->where('nopedido30', $numero_produccion);
        $result = $this->db->get();
        return $result->result_array();
    }


    function update_plan_diario($valor){

        $dia = date('w', strtotime($valor['fecha']));
        $dia = ($dia != "0") ? $dia : '7' ;    

        $array_update = array(
                         'cantidad' => $valor['cantidad'],
                         'cantidad_entregada' =>  $valor['cantidad_entregada'],
                         'id_receta' =>  $valor['id_receta'],
                         'estado_produccion' => $valor['estado_produccion'],
                         'orden_produccion' => $valor['orden_produccion']                    
                    ); 

        $this->db->where("desde", $valor['desde']);
        $this->db->where("hasta", $valor['hasta']);
        $this->db->where("id_cliente", $valor['id_cliente']);
        $this->db->where("id_producto", $valor['id_producto']);
        $this->db->where("opcion", $valor['opcion']);
        //$this->db->where("estado_despacho", 1);
        $this->db->where("estado_preplan", 1);
        $this->db->where("dia", $dia);
        $this->db->update('planificacion_produccion',$array_update);


    }


    function guardar_plan_diario($valor){
        $dia = date('w', strtotime($valor['fecha']));
        $dia = ($dia != "0") ? $dia : '7' ;

        $array_insert = array(
                 'cantidad_dispatch' => $valor['cantidad_dispatch'],
                 'estado_despacho' => 1,
                 'desde' => $valor['desde'],
                 'hasta' => $valor['hasta'],
                 'opcion' => $valor['opcion'],
                 'id_cliente' => $valor['id_cliente'],
                 'estado_preplan' => 1,
                 'dia' => $dia,
                 'cantidad' => $valor['cantidad'],
                 'cantidad_entregada' => $valor['cantidad_entregada'],
                 'id_producto' => $valor['id_producto'],
                 'id_receta' => $valor['id_receta']
            );
        $this->db->insert('planificacion_produccion',$array_insert);

    }

     function consulta_plan_diario($datos){

        
        $dia = date('w', strtotime($datos['fecha']));
        $dia = ($dia != "0") ? $dia : '7' ;

        $this->db->select('*')->from("planificacion_produccion");
     
        $this->db->where("desde", $datos['desde']);
        $this->db->where("hasta", $datos['hasta']);
        $this->db->where("id_cliente", $datos['id_cliente']);
        $this->db->where("id_producto", $datos['id_producto']);
        $this->db->where("opcion", $datos['opcion']);
        //$this->db->where("estado_despacho", 0);
        $this->db->where("estado_preplan", 1);
        $this->db->where("dia", $dia);
        
        $result = $this->db->get();
         //echo $this->db->last_query();
        $flag = 0;
        if(count($result->result_array()) > 0){

                $flag = 1;
        }

        return $flag;

    }

    function consulta_receta_ultima_opcion($datos){     
        $dia = date('w', strtotime($datos['fecha']));
        $dia = ($dia != "0") ? $dia : '7' ;

        $this->db->select('id_receta,desde,hasta')->from("planificacion_produccion");
     
        $this->db->where("desde", $datos['desde']);
        $this->db->where("hasta", $datos['hasta']);
        $this->db->where("id_cliente", $datos['id_cliente']);
        $this->db->where("id_producto", $datos['id_producto']);
        //$this->db->where("opcion", $datos['opcion']);
        //$this->db->where("estado_despacho", 0);
        $this->db->where("estado_preplan", 1);
        $this->db->where("dia", $dia);
        $this->db->order_by("opcion", "desc");
        $this->db->limit(1);
        
        $result = $this->db->get();
        //echo $this->db->last_query();
        
        return $result->result_array();

    }

    function ingresar_numero_produccion_en_plan_diario($datos, $numero_produccion, $fecha)
    {
        $sql = $this->db->query('UPDATE `planificacion_produccion` SET orden_produccion = "'.$numero_produccion.'"
                            WHERE `dia` =  "'.$datos['dia'].'"
                            AND id_producto = "'.$datos['id_producto'].'"
                            AND opcion = "'.$datos['opcion'].'"
                            AND "'.$fecha.'" >= `desde`
                            AND "'.$fecha.'" <= `hasta`');
        //echo $this->db->last_query();
    }

    function obtener_detalle_tmp_lismaehis($numero_produccion)
    {
        $this->db->select('*');
        $this->db->from('tmp_lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $result = $this->db->get();

        return $result->result_array();
    }

    function get_componentes_lismaehis($receta, $numero_produccion)
    {
        $this->db->select('*');
        $this->db->from('lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $this->db->where('codprod20t', $receta);
        $result = $this->db->get();

        return $result->result_array();

    }

    function borrar_temporales_lismaehis($numero_produccion)
    {
        $this->db->where('numord20',$numero_produccion);
        $result = $this->db->delete('tmp_lismaehis');
        return $result;
    }

    function get_tipo_cocina_by_producto($codigo)
    {
        $this->db->select('tipo_cocina');
        $this->db->where("codigo_producto", $codigo);
        $this->db->from("producto_receta");
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        return $resultado;
    }

    function get_numero_cocinas_by_produccion($numero_produccion, $clasificacion)
    {
        $this->db->select('tipo_cocina, clasificacion');
        $this->db->from('lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $this->db->where('clasificacion', $clasificacion);
        $this->db->group_by('tipo_cocina');
        $result = $this->db->get();

        return $result->result_array();
    }

    function get_obtener_detalle_lismaehis_by_cocina($numero_produccion, $tipo_cocina, $clasificacion)
    {
        $this->db->select('*');
        $this->db->from('lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $this->db->where('tipo_cocina', $tipo_cocina);
        $this->db->where('clasificacion', $clasificacion);
        $result = $this->db->get();

        return $result->result_array();
    }

    function recetas_lismaehis_by_cocina_get($numero_produccion, $tipo_cocina, $clasificacion)
    {
        $this->db->select('codprod20t, tipo_cocina');
        $this->db->from('lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $this->db->where('tipo_cocina', $tipo_cocina);
        $this->db->where('clasificacion', $clasificacion);
        $this->db->group_by('codprod20t');
        $result = $this->db->get();

        return $result->result_array();

    }

    function get_productos_receta_by_requisicion($numero_produccion, $requisicion)
    {
        $this->db->select('codigo_receta, ocu_receta');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->where('numdoc', $requisicion);
        $this->db->group_by('ocu_receta, codigo_receta');
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function get_productos_subreceta_by_requisicion($numero_produccion, $requisicion, $codigo_receta)
    {
        $this->db->select('codigo_receta, ocu_receta, codigo_subreceta');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->where('numdoc', $requisicion);
        $this->db->where('codigo_receta', $codigo_receta);
        $this->db->where('codigo_subreceta != ""');
        $this->db->group_by('codigo_subreceta');
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function get_detalle_subreceta_by_requisicion($numero_produccion, $requisicion, $codigo_receta, $ocu_receta, $codigo_subreceta)
    {
        $this->db->select('codprod, desprod, cantsol, cantent');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->where('numdoc', $requisicion);
        $this->db->where('codigo_receta', $codigo_receta);
        $this->db->where('ocu_receta', $ocu_receta);
        $this->db->where('codigo_subreceta', $codigo_subreceta);
        $result = $this->db->get();
        return $result->result_array();
    }

    function consultar_si_existe_produccion($numero_produccion)
    {
        $this->db->select('nopedido30');
        $this->db->from('maeprd30');
        $this->db->where('nopedido30', $numero_produccion);
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function verificar_ingrediente_receta($receta, $orden_produccion)
    {
        $this->db->select('codprod20t');
        $this->db->from('lismaehis');
        $this->db->where('codprod20t', $receta['codigo_producto']);
        $this->db->where('codprod20', $receta['codigo_ingrediente']);
        $this->db->where('numord20',$orden_produccion);
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function verificar_receta($receta, $orden_produccion)
    {
        $this->db->select('codprod20t');
        $this->db->from('lismaehis');
        $this->db->where('codprod20t', $receta['codigo_producto']);
        $this->db->where('numord20',$orden_produccion);
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function eliminar_ingrediente($parametro, $orden_produccion)
    {
        $this->db->where('codprod20t',$parametro['codigo_producto']);
        $this->db->where('codprod20',$parametro['codigo_ingrediente']);
        $this->db->where('numord20',$orden_produccion);
        $result = $this->db->delete('lismaehis');
        return $result;
    }

    function actualizar_receta($datos, $parametro, $orden_produccion)
    {
        $this->db->where('codprod20t', $parametro['codigo_producto']);
        $this->db->where('codprod20', $parametro['codigo_ingrediente']);
        $this->db->where('numord20',$orden_produccion);
        $result = $this->db->update('lismaehis', $datos);
        //echo $this->db->last_query();
        return $result;
    }

    function guardar_receta($datos)
    {
        $result = $this->db->insert("lismaehis", $datos);
        //echo $this->db->last_query();
        return $result;
    }

    function ocurrencia_detalle_produccion_get($receta, $orden_produccion)
    {
        $this->db->select_max('ocurren20');
        $this->db->select('ocucomp20, tipo_cocina, cant_producir');
        $this->db->from('lismaehis');
        $this->db->where('numord20',$orden_produccion);
        $this->db->where('codprod20t', $receta['codigo_producto']);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function generar_tmp_produccion($datos)
    {
        $this->db->insert("tmp_maeprd30", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function obtener_detalle_tmp_produccion($codigo)
    {
        $this->db->select('codprod30, producto_final');
        $this->db->select_sum('cantped30');
        $this->db->where("nopedido30", $codigo);
        $this->db->from("tmp_maeprd30");
        $this->db->group_by(" codprod30 ");
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        //echo $this->db->last_query();
        return $resultado;
    }

    function borrar_temporales_produccion($numero_produccion)
    {
        $this->db->where('nopedido30',$numero_produccion);
        $result = $this->db->delete('tmp_maeprd30');
        return $result;
    }


    function eliminar_orden_produccion($numero_produccion)
    {
        $this->db->where('nopedido30',$numero_produccion);
        $result = $this->db->delete('maeprd30');
        return $result;
    }

    function eliminar_detalle_orden_produccion($numero_produccion)
    {
        $this->db->where('numord20',$numero_produccion);
        $result = $this->db->delete('lismaehis');
        return $result;
    }
    
    function eliminar_tmp_orden_produccion($numero_produccion)
    {
        $this->db->where('numord20',$numero_produccion);
        $result = $this->db->delete('tmp_lismaehis');
        return $result;
    }
    function elimiar_opciones($data){

        foreach ($data as $valor) {

            $dia = date('w', strtotime($valor['dia']));
            $dia = ($dia != "0") ? $dia : '7' ;

            $this->db->query('delete from planificacion_produccion 
                              where id_producto = "'.$valor['producto'].'" 
                              and dia = "'.$dia.'" 
                              and opcion = "'.$valor['opcion']['code'].'" 
                              and "'.$valor['dia'].'" >= desde 
                              and "'.$valor['dia'].'" <= hasta');
        }

    }

    function elimiar_clientes($data){
        
        foreach ($data as $valor) {

            $dia = date('w', strtotime($valor['dia']));
            $dia = ($dia != "0") ? $dia : '7' ;

            $this->db->query('delete from planificacion_produccion 
                              where id_producto = "'.$valor['producto'].'" 
                              and dia = "'.$dia.'"
                              and id_cliente = "'.$valor['cliente']['code'].'" 
                              and opcion = "'.$valor['opcion'].'" 
                              and "'.$valor['dia'].'" >= desde 
                              and "'.$valor['dia'].'" <= hasta');
            
        }
    }

    function eliminar_orden_requisicion($numero_produccion, $numero_requisicion)
    {
        $this->db->where('numordenp',$numero_produccion);
        $this->db->where('numdoc',$numero_requisicion);
        $result = $this->db->delete($this->matriz.'.reqprod');
        return $result;
    }

    function get_cantidad_pedida_by_producto($numero_produccion, $requisicion, $producto, $receta, $codigo_subreceta)
    {
        $this->db->select('cantsol, cantent, cant_a_devolver');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp', $numero_produccion);
        $this->db->where('numdoc', $requisicion);
        $this->db->where('codprod', $producto['codprod20']);
        $this->db->where('codigo_receta', $receta);
        $this->db->where('codigo_subreceta', $codigo_subreceta);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function actualizar_producto_requisicion($datos, $numero_produccion, $numero_requisicion, $producto, $receta, $clasificacion, $codigo_subreceta)
    {
        $this->db->where('numordenp', $numero_produccion);
        $this->db->where('numdoc', $numero_requisicion);
        $this->db->where('codprod',$producto);
        $this->db->where('codigo_receta', $receta);
        $this->db->where('codigo_subreceta', $codigo_subreceta);
        $this->db->where('clasificacion', $clasificacion);
        $result = $this->db->update($this->matriz.'.reqprod', $datos);
        //echo $this->db->last_query();
        return $result;

    }
    
    function get_ingredientes_by_produccion($numero_produccion)
    {
        $this->db->select('*');
        $this->db->from('lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $result = $this->db->get();

        return $result->result_array();

    }

    function verificar_ingrediente_en_requisicion($orden_produccion, $numero_requisicion, $ingrediente)
    {
        $this->db->select('codprod');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp',$orden_produccion);
        $this->db->where('numdoc',$numero_requisicion);
        $this->db->where('codprod', $ingrediente['codprod20']);
        $this->db->where('tipo_cocina', $ingrediente['tipo_cocina']);
        $this->db->limit(1);
        $result = $this->db->get();
        return $this->db->affected_rows();
    }

    function verificar_ingrediente_en_requisicion2($orden_produccion, $numero_requisicion, $ingrediente)
    {
        $this->db->select('codprod');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp',$orden_produccion);
        $this->db->where('numdoc',$numero_requisicion);
        $this->db->where('codigo_subreceta', $ingrediente['codprod20']);
        $this->db->where('tipo_cocina', $ingrediente['tipo_cocina']);
        $this->db->limit(1);
        $result = $this->db->get();
        return $this->db->affected_rows();
    }

    function verificar_subreceta_en_requisicion($orden_produccion, $numero_requisicion, $ingrediente)
    {
        $this->db->select('codprod');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp',$orden_produccion);
        $this->db->where('numdoc',$numero_requisicion);
        $this->db->where('codprod', $ingrediente['codprod20']);
        $this->db->where('codigo_subreceta', $ingrediente['codprod20t']);
        $this->db->limit(1);
        $result = $this->db->get();
        return $this->db->affected_rows();
    }

    function ocurrencia_nuevo_ingrediente($orden_produccion, $numero_requisicion, $ingrediente)
    {
        $this->db->select('count(*) as ocu');
        $this->db->from($this->matriz.'.reqprod');
        $this->db->where('numordenp',$orden_produccion);
        $this->db->where('numdoc',$numero_requisicion);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();
    }

    function clasificaciones_by_produccion_get($numero_produccion)
    {
        $this->db->select('clasificacion');
        $this->db->from('lismaehis');
        $this->db->where('numord20', $numero_produccion);
        $this->db->group_by('clasificacion');
        $result = $this->db->get();

        return $result->result_array();
    }

    function get_clasificacion($codtab)
    {
      
        $this->db->where("numtab", "7313");
        $this->db->where("codtab", $codtab);
        $this->db->select('nomtab')->from("maetab");
        $sql = $this->db->get();
        $resultado = $sql->result_array();

        return $resultado[0]['nomtab'];

    }

    function actualizar_estado_produccion($numero_produccion, $estado)
    {
        $data = array(
            'estado_produccion' => $estado
        );
        $this->db->where('orden_produccion', $numero_produccion);
        $result = $this->db->update('planificacion_produccion', $data);
        return $result;
    }

    function get_estado_produccion_plan_diario($numero_produccion)
    {
        $this->db->select('estado_produccion');
        $this->db->from('planificacion_produccion');
        $this->db->where("orden_produccion", $numero_produccion);
        $this->db->group_by('estado_produccion');
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        return $resultado;
    }

    function get_productos_finales($numero_produccion)
    {

        $this->db->select('id_producto, id_receta');
        $this->db->select_sum('cantidad');
        $this->db->from('planificacion_produccion');
        $this->db->where("orden_produccion", $numero_produccion);
        $this->db->group_by('id_producto');
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        return $resultado;

    }

}
?>
