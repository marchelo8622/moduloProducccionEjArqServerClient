<script src="<?= base_url()?>../client/src/app/creador_recetas/module.js"></script>
<script src="<?= base_url()?>../client/src/app/creador_recetas/creador_recetas_model.js"></script>
<script src="<?= base_url()?>../client/src/app/planificacion_semanal/module.js"></script>
<script src="<?= base_url()?>../client/src/app/planificacion_semanal/planificacion_semanal_model.js"></script>
<script src="<?= base_url()?>../client/src/app/plan_diario/module.js"></script>
<script src="<?= base_url()?>../client/src/app/plan_diario/plan_diario_model.js"></script>
<script src="<?= base_url()?>../client/src/app/plan_diario/controllers/plan_diario.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>
<script>
    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }
    
</script>


