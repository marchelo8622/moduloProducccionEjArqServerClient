<?php
include_once(APPPATH ."models/base_model.php");
Class Facturacion_pedidos_model extends Base_model
{
    private $matriz;
    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    
    function get_pedidos_aprobados($codigo_cliente, $nombre_cliente, $numero_pedido)
    {
        if ($codigo_cliente != "null") {
            $this->db->like('codcte30', $codigo_cliente);
        }

        if ($nombre_cliente != "null") {
            $this->db->like('nomcte30', $nombre_cliente);
        }

        if ($numero_pedido != "null") {
            $this->db->where('nopedido30', $numero_pedido);
        }

        $estados = array('02', '03');
        $this->db->select('nopedido30, codprod30, codcte30, fecpedido30, nomcte30, novend30, localid30, condpag30, 
            descto30, iva30, status30, cantped30, precuni30, cantfac30, cantafac30, uid, bodega30, dividir_pedido');
        $this->db->from("maeped30");
        $this->db->where_in("status30", $estados);
        $this->db->order_by("fecpedido30", "DESC");
        $this->db->group_by("codcte30");
        //$this->db->limit(20);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function get_pedidos_aprobados_by_cliente($codigo_cliente)
    {
        $sql = $this->db->query("select nopedido30,codcte30,condpag30, (sum(cantped30*precuni30)-sum(desctoxprod30)) as totalpedido, descto30,dividir_pedido from maeped30 where codcte30 = '".$codigo_cliente."' and status30 in ('02', '03') group by nopedido30  order by fecpedido30 DESC");
        //echo $this->db->last_query();
        return $sql->result();
    }

   /* function get_pedido_by_numero($numero_pedido,$cliente)
    {

        $this->db->select('nopedido30, codprod30, codcte30, fecpedido30, nomcte30, novend30, localid30, condpag30, 
            descto30, iva30, status30, cantped30, precuni30, cantfac30, cantafac30, cantafac30 - cantfac30 as aFacturar  , maeped30.uid, bodega30, dividir_pedido, desprod01 as nom_producto');
        $this->db->from("maeped30");        
        $this->db->join('maepro', 'codprod01 = codprod30');
        $this->db->where('nopedido30', $numero_pedido);
        $this->db->where('codcte30', $cliente);
        
        //$this->db->group_by("nopedido30");
        $result = $this->db->get();
        //echo $this->db->last_query();
        //print_r($result->result_array());
        return $result->result_array();

    }*/

    
    function get_pedido_by_numero($numero_pedido,$cliente)
    {
        $sql = $this->db->query("select nopedido30, codprod30,ocurren30, codcte30, fecpedido30, nomcte30, novend30, localid30,condpag30, descto30, iva30, status30, piva30, recargos30, desctofp30, desctoxprod30 , cantped30, precuni30, cantfac30, cantafac30, cantafac30 - cantfac30 as aFacturar,  '0' as total , maeped30.uid, bodega30, dividir_pedido, desprod01 as nom_producto,'' as prod_principal_equiv
            from maeped30
            inner join maepro on maepro.codprod01 = maeped30.codprod30
            where nopedido30 = '".$numero_pedido."' and codcte30 = '".$cliente."'");
        return $sql->result_array();
    }




    function get_estado_pedido($numero_pedido)
    {
        $this->db->select('status30, dividir_pedido');
        $this->db->from("maeped30");
        $this->db->where('nopedido30', $numero_pedido);
        $this->db->group_by("nopedido30");
        $result = $this->db->get();
        return $result->result();

    }

    function dividir_pedido($numero_pedido)
    {
        $data = array(
               'dividir_pedido' => 1               
              );

          $this->db->where("nopedido30", $numero_pedido);
          $result = $this->db->update('maeped30', $data);
          return $result;
    }

    function consultar_si_existe_factura($numero_factura)
    {
        $this->db->select('nofact31');
        $this->db->from('maefac');
        $this->db->where('nofact31', $numero_factura);
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function registrar_factura($datos)
    {
        $this->db->insert("maefac", $datos);
        return $this->db->affected_rows();
    }

    function registrar_pago_factura($datos)
    {
        $this->db->insert("movpro", $datos);
        return $this->db->affected_rows();
    }

    function registrar_movimientos_productos($datos)
    {
        $this->db->insert("movpro", $datos);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function registrar_movimientos_clientes($datos)
    {
        $this->db->insert($this->matriz.".movcte", $datos);
        return $this->db->affected_rows();
    }

    function actualizar_cantidades_pedido($datos)
    {
        $data = array(
            'cantfac30'     => $datos['cantidad_facturada'],
            'cantafac30'     => $datos['cantidad_pedida'] - $datos['cantidad_facturada']
        );
        $this->db->where('nopedido30', $datos['numero_pedido']);
        $this->db->where('codprod30', $datos['codigo_producto']);
        $this->db->where('ocurren30', $datos['ocurrencia']);
        $result = $this->db->update('maeped30', $data);
        return $result;
    }

    function actualizar_estado_pedido($datos)
    {
        $data = array(
            'status30'     => $datos['estado_pedido']
        );
        $this->db->where('nopedido30', $datos['numero_pedido']);
        $result = $this->db->update('maeped30', $data);
        return $result;
    }

    function get_vendedores(){
        $sql = $this->db->query("Select codtab,nomtab from maetab where numtab='73' AND (ad2tab IS NULL OR ad2tab !=9) and codtab<>'' order by nomtab");
        return $sql->result_array();

    }
    function get_condiciones_pago(){
        $sql = $this->db->query("Select codtab,nomtab, ad2tab from maetab where numtab='72' and codtab<>'' order by nomtab");
        return $sql->result_array();

    }
    
    function get_impresoras(){
        $sql = $this->db->query("Select codtab,nomtab from maetab where numtab='98'and not(codtab='') order by nomtab");
        return $sql->result_array();
    }

    function get_formatos(){
        $sql = $this->db->query("Select codform01,codform01 from estrform where codform01 like 'FAC%'and codform01!='FACC' group by codform01 order by codform01");
        return $sql->result_array();
    }

    function get_localidades(){
        $sql = $this->db->query("Select codtab,nomtab from maetab where numtab='74' and codtab<>'' order by nomtab");
        return $sql->result_array();
    }
    
    function get_transportistas(){
        $sql = $this->db->query("Select codtab,nomtab from maetab where numtab='82'and not(codtab='') order by nomtab");
        return $sql->result_array();
    }
    
    function get_ayudantes(){
        $sql = $this->db->query("Select codtab,nomtab from maetab where numtab='84'and not(codtab='') order by nomtab");
        return $sql->result_array();
    }

    function get_formas_pago(){
        $sql = $this->db->query("select codtab,nomtab from maetab where numtab ='78' and codtab<>'' order by nomtab");
        return $sql->result_array();
    }

    function get_data_condicion_pago($codigo){
        $sql = $this->db->query("Select ad1tab as num_cuotas, ad3tab as tipo_pago, ad2tab as dias from maetab where numtab='72' and codtab = '".$codigo."' order by nomtab");
        return $sql->result_array();

    }

    function inserta_movimientos_temporales_producto($nocomp,$ocurrencia,$codProd,$cant,$localidad,$fecha){
        $insert = $this->db->query("insert into movtmppro (tipotra03, nocomp03, ocurren03, codprod03, cantid03, fecmov03, coddest03) values('80', '".$nocomp."', '".$ocurrencia."', '".$codProd."', '".$cant."', '".$fecha."', '".$localidad."')");
        return $insert;
    }

    function actualiza_movimientos_temporales_producto($nocomp,$ocurrencia,$codProd,$cant){
        $this->db->query("update movtmppro set cantid03 = '".$cant."' where tipotra03 = '80' and  nocomp03 = '".$nocomp."' and  ocurren03 = '".$ocurrencia."' and  codprod03 = '".$codProd."'");
        
    }

    function elimina_movimientos_temporales_producto($nocomp,$ocurrencia,$codProd){
       $this->db->query("delete from movtmppro where tipotra03 = '80' and nocomp03 = '".$nocomp."' and ocurren03 = '".$ocurrencia."' and codprod03 = '".$codProd."'");      
    }

    function elimina_movimientos_temporales_producto_by_nocomp($nocomp){
       $this->db->query("delete from movtmppro where tipotra03 = '80' and nocomp03 = '".$nocomp."'");      
    }

    function consulta_mov_temporal($nocomp,$ocurrencia,$codProd){
        $sql = $this->db->query("select * from movtmppro where tipotra03 = '80' and nocomp03 = '".$nocomp."' and ocurren03 = '".$ocurrencia."' and codprod03 = '".$codProd."'");
        $datos = $sql->result_array();
        $flag = 0;
        if(count($datos) > 0){
            $flag = 1;
        }
        return $flag;
    }

    function get_nocomp_temporal(){
        $sql = $this->db->query("select max(nocomp03) as nocomp from movtmppro");
        $dato =  $sql->result_array();
        $nocomp = 0;
        if(count($dato) > 0){

            $nocomp = $dato[0]['nocomp'];

        }
        return $nocomp;
    }

    function elimina_all_movimientos_temporales_producto($nocomp){
       $this->db->query("delete from movtmppro where tipotra03 = '80' and nocomp03 = '".$nocomp."'");      
    }

    function sum_stock_movimientos_temporales_producto($nocomp){
       $sql = $this->db->query("select sum(cantid03) as totSum from movtmppro where tipotra03 = '80' and nocomp03 = '".$nocomp."'");
       $dato = $sql->result_array();
       $sumTot = 0;
       if(count($dato)>0){
            $sumTot = $dato[0]['totSum'];
       } 

       return $sumTot;   
    }

        
}
?>
