<script src="<?= base_url()?>../client/src/app/facturacion_pedidos/module.js"></script>
<script src="<?= base_url()?>../client/src/app/facturacion_pedidos/facturacion_pedidos_model.js"></script>
<script src="<?= base_url()?>../client/src/app/facturacion_pedidos/controllers/facturacion_pedidos.controller.js"></script>
<script src="<?= base_url()?>../client/src/app/facturacion_pedidos/controllers/detalle_facturacion_pedidos.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>

<script>
    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }
    
</script>


