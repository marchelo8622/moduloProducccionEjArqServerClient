<?php
//error_reporting(0);
ini_set("display_errors",1);
defined("BASEPATH") OR exit("No direct script access allowed");
require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {
    private $pedidos;
    private $validacion;
    private $serie_sri;
    private $secuencial_factura;
    private $numero_factura;
    private $codigo_cliente;
    private $datos_cliente;
    private $respuesta = array();
    private $datos_factura = array();
    private $secuencial_pago;
    private $numero_pago;
    private $estado_pedido;

    function __construct()
    {
        parent::__construct();
        $this->load->model("Facturacion_pedidos_model", "facturacion_pedidos");
        $this->load->model('plan_diario/plan_diario_model', 'plan_diario');
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model('producto/Producto_model', 'Producto');
        $this->load->model("dispatch/dispatch_model", "dispatch");
        $this->pedidos = array();
        $this->validaciones = array();
        $this->serie_sri = $this->get_serie_sri();
        $this->response_false();
        session_start();
        $this->uid = $_SESSION['UID'];
    }

    function response_false($error = 'No se Proceso')
    {
        $response = array();
        $response['respuesta'] = false;
        $response['error'] = $error;
        $this->respuesta['respuesta'] = $response;
    }

    function response_true($respuesta = true)
    {
        $response = array();
        $response['respuesta'] = $respuesta;
        $this->respuesta['respuesta'] = $response;
    }

    function get_serie_sri()
    {
        $serie_sri = $this->plan_diario->get_serie_sri();
        return $serie_sri;
    }
 
    function pedidos_aprobados_get($codigo_cliente = 'null', $nombre_cliente = 'null', $numero_pedido = 'null')
    {
        $this->pedidos = $this->facturacion_pedidos->get_pedidos_aprobados($codigo_cliente, $nombre_cliente, $numero_pedido);
        echo format_response($this->pedidos);
        
    }

    function pedidos_by_condicion_post($cliente)
    {
        $datos = $this->post();
        $array_pedidos_seleccionados = array();
        
        if(count($datos['data']) > 0){
            foreach ($datos['data'] as $valor) {
                $array_pedidos_seleccionados = array_merge($this->facturacion_pedidos->get_pedido_by_numero($valor['nopedido30'],$cliente),$array_pedidos_seleccionados);
            }
        } 

        echo format_response($array_pedidos_seleccionados);
        
    }

    function estado_pedido_get($numero_pedido = 'null')
    {
        $this->pedido = $this->facturacion_pedidos->get_estado_pedido($numero_pedido);
        echo format_response($this->pedido);
        
    }
 
    function estado_pedido_put()
    {
        $datos = $this->put();
        $numero_pedido = $datos['data']['numero_pedido'];
        $this->result = $this->facturacion_pedidos->dividir_pedido($numero_pedido);
        echo format_response($this->result);
    }
 
    function facturacion_pedidos_post()
    {
        $data_fac = $this->post();
        $this->datos_factura = $data_fac['data']['items'][0];       
        $nocomp = $data_fac['data']['nocomp'];
        //print_r( $this->datos_factura);       
        $this->codigo_cliente = $this->datos_factura['codigo_cliente'];
        $this->validaciones_generales();
        if ($this->validacion['validacion'])
            $this->validar_datos();
        
        if ($this->validacion['validacion'])
        {
            $this->registrar_factura();
            $this->update_numero_factura();
            $this->registrar_movimientos_productos();
            $this->registrar_movimiento_cliente();

            if ($this->datos_factura['condicion_pago'] == '0')
            {
                $this->registrar_pago_factura();
                $this->update_numero_pago();
            }
            if($nocomp != ''){
                $this->facturacion_pedidos->elimina_all_movimientos_temporales_producto($nocomp);
            }
            $this->actualizar_cantidades_pedido();

            $this->response_true($this->numero_factura);

        }else{
            $error = $this->validacion['error'];
            $this->response_false($error);

        }
        echo format_response($this->respuesta['respuesta']);

    }

    function registrar_factura()
    {
        $factura = array(
            'tipodocto31'   => '02',
            'nofact31'      => $this->numero_factura,
            'nocte31'       => $this->datos_cliente->codigo_cliente,
            'nomcte31'      => $this->datos_cliente->nombre_cliente,
            'localid31'     => $this->datos_factura['localidad'],
            'vtabta31'      => $this->datos_factura['venta_bruta'],
            'descto31'      => $this->datos_factura['desc_cliente'],
            'flete31'       => $this->datos_factura['valor_portfee'],
            'itm31'         => $this->datos_factura['iva'],
            'novend31'      => $this->datos_factura['codigo_vendedor'],
            'fecfact31'     => date('Y-m-d H:i:s'),
            'condpag31'     => $this->datos_factura['condicion_pago'],
            'nopagos31'     => $this->datos_factura['numero_pagos'],
            'formapago31'   => $this->datos_factura['forma_pago'],
            'status31'      => 1,
            'cvanulado31'   => 1,
            'nopedido31'    => $this->datos_factura['productos'][0]['numero_pedido'],
            'ruc31'         => $this->datos_cliente->codigo_cliente,
            'tel31'         => $this->datos_cliente->telefono,
            'cvtransfer31'  => 'N',
            'desctofp31'    => $this->datos_factura['desc_forma_pago'],
            'catcte31'      => $this->datos_cliente->categoria,
            'UID'           => $this->uid,
            'recargos31'    => $this->datos_factura['recargos'],
            'totsiniva31'   => $this->datos_factura['subtotal_siniva']
            );

        $this->facturacion_pedidos->registrar_factura($factura);

    }

    function registrar_movimientos_productos()
    {
        foreach ($this->datos_factura['productos'] as $key => $producto) 
        {
            $datos_producto = $this->Producto->get_producto_by_codigo($producto['codigo_producto']);
            $costo_unitario = (float) $datos_producto['valor_actual'] / (float) $datos_producto['cantidad_actual'];
            $cantidad_actual = (float)$datos_producto['cantidad_actual'] - (float)$producto['cantidad_facturar'];
            $valor_transaccion = (float) $costo_unitario * (float)$producto['cantidad_facturar'];
            
            $valor_actual = (float)$cantidad_actual * (float) $costo_unitario;
            if($cantidad_actual == '-0'){
                $valor_actual = 0;
            }            
            $precio_unitario = 0;
            if ((float)$cantidad_actual > 0)
                $precio_unitario = (float) $valor_actual / (float) $cantidad_actual;
            $iva_producto = ($producto['con_iva'] == 'S') ? $this->datos_factura['iva'] : 0 ;
            $precio_venta = (float) $producto['valor'] * (float)$producto['cantidad_facturar'];
            $movimiento = array(
                'TIPOTRA03'     => '80',
                'NOCOMP03'      => $this->numero_factura,
                'OCURREN03'     => str_pad($key, 2, '0', STR_PAD_LEFT),
                'CODPROD03'     => $producto['codigo_producto'],
                'FECMOV03'      => date('Y-m-d H:i:s'),
                'CANTID03'      => $producto['cantidad_facturar'],
                'VALOR03'       => $valor_transaccion,
                'PU03'          => $costo_unitario,
                'CANTACT03'     => $cantidad_actual,
                'VALACT03'      => $valor_actual,
                'DESCVTA03'     => 0,
                'PRECUNI03'     => $precio_unitario,
                'PRECVTA03'     => $precio_venta,
                'nomdest03'     => $this->datos_cliente->codigo_cliente,
                'nopedido03'    => $producto['numero_pedido'],
                'desctotvta03'  => $producto['descuento'],
                'desctotfp03'   => $this->datos_factura['desc_forma_pago'],
                'UID'           => $this->uid,
                'cvanulado03'   => 'N',
                'ocuped03'      => $producto['ocurrencia'],                
                'iva03'         => $iva_producto,
                'prod_principal_equiv' => $producto['prod_principal_equiv']
                );
            $this->facturacion_pedidos->registrar_movimientos_productos($movimiento);
            $datos = array(
                    'cantidad_actual' => $cantidad_actual,
                    'valor_actual' => $valor_actual,
                    'precio_unitario' => $precio_unitario,
                    'codigo' => $producto['codigo_producto']
                    );
            $this->Producto->actualizar_inventario_producto($datos);
        }
    }

    function registrar_movimiento_cliente()
    {
        foreach ($this->datos_factura['vencimientos'] as $key => $vencimiento) 
        {
            $this->datos_cliente = $this->Cliente->get_cliente_by_codigo($this->datos_cliente->codigo_cliente);
            $saldo_actual = (float)$this->datos_cliente->saldo_actual + (float)$this->datos_factura['total_factura'];

        $movimiento = array(
            'codcte43'      => $this->datos_cliente->codigo_cliente,
            'tipodoc43'     => '02',
            'numdoc43'      => $this->numero_factura,
            'ocurren43'     => str_pad($key, 2, '0', STR_PAD_LEFT),
            'fecdoc43'      => date('Y-m-d H:i:s'),
            'numvencob43'   => '01',
            'fedoc43'       => date('Y-m-d H:i:s'),
            'totdoc43'      => $this->datos_factura['total_factura'],
            'detalle43'     => $vencimiento['detalle'],
            'autocompra43'  => '',
            'numcuotasord43'=> $key,
            'valormov43'    => $vencimiento['cobro'],
            'saldoregmov43' => $vencimiento['saldo'],
            'feccobro43'    => $vencimiento['fecha_cobro'],
            'codpagounif43' => $this->datos_factura['condicion_pago'],
            'saldoexceso43' => $this->datos_cliente->saldo_exceso,
            'saldocte43'    => $saldo_actual,
            'UID'           => $this->uid,
            'cvtransfer43'  => 'N',
            'fecvendoc43'   => $vencimiento['fecha_cobro'],
            'cvanulado43'   => 'N',
            'cierre'        => 'N'
            );
        $this->facturacion_pedidos->registrar_movimientos_clientes($movimiento);
        $datos = array(
                    'saldo_actual' => $saldo_actual,
                    'codigo_cliente' => $this->datos_cliente->codigo_cliente
                    );
        $this->Cliente->actualizar_saldos_cliente($datos);
        }
    }

    function registrar_pago_factura()
    {
        $this->secuencial_pago();
        foreach ($this->datos_factura['pagos'] as $key => $pago) 
        {
            $this->datos_cliente = $this->Cliente->get_cliente_by_codigo($this->datos_cliente->codigo_cliente);
            $saldo_actual = (float)$this->datos_cliente->saldo_actual - (float)$pago['valor'];

            $pago = array(
            'codcte43'      => $this->datos_cliente->codigo_cliente,
            'tipodoc43'     => '80',
            'numdoc43'      => $this->numero_pago,
            'ocurren43'     => str_pad($key, 2, '0', STR_PAD_LEFT),
            'fecdoc43'      => date('Y-m-d H:i:s'),
            'numvencob43'   => $this->datos_factura['codigo_vendedor'],
            'totdoc43'      => $this->datos_factura['total_factura'],
            'tipodocdb43'   => '02',
            'numdocdb43'    => $this->numero_factura,
            'ocurrecdocdb43'=> '00',
            'valorabono43'  => $pago['valor'],
            'efectcheque43' => $pago['forma_pago'],
            'saldoexceso43' => $this->datos_cliente->saldo_exceso,
            'saldocte43'    => $saldo_actual,
            'UID'           => $this->uid,
            'cvtransfer43'  => 'N',
            'cvanulado43'   => 'N'
            );
        $this->facturacion_pedidos->registrar_movimientos_clientes($pago);

        $datos = array(
                    'saldo_actual' => $saldo_actual,
                    'codigo_cliente' => $this->datos_cliente->codigo_cliente
                    );
        $this->Cliente->actualizar_saldos_cliente($datos);
        }

    }

    function actualizar_cantidades_pedido()
    {
        $this->estado_pedido = '04';
        $flag = true;
        $pedido = '';
        foreach ($this->datos_factura['productos'] as $key => $producto) 
        {
            if ($flag)
            {
                $pedido = $producto['numero_pedido'];
                $flag = false;
            }

            if ($pedido != $producto['numero_pedido'])
            {
                $this->estado_pedido = '04';
                $pedido = $producto['numero_pedido'];
            }
            $total_facturada = (float)$producto['cantidad_facturar'] + (float)$producto['cantidad_facturada'];
            
            $datos = array(
                    'cantidad_facturada'    => $total_facturada,
                    'cantidad_pedida'    => $producto['cantidad_pedida'],
                    'codigo_producto'       => $producto['codigo_producto'],
                    'numero_pedido'         => $producto['numero_pedido'],
                    'ocurrencia'            => $producto['ocurrencia']
                    );
            $this->facturacion_pedidos->actualizar_cantidades_pedido($datos);
            $status = $this->get_estado_pedido($total_facturada, $producto['cantidad_pedida']);
            if ($this->datos_factura['dividir_factura'] == true)
                $this->facturacion_pedidos->dividir_pedido($producto['numero_pedido']);
            $datos2 = array(
                'estado_pedido'         => $status,
                'numero_pedido'         => $producto['numero_pedido']
                );
            $this->facturacion_pedidos->actualizar_estado_pedido($datos2);
        }

    }

    function get_estado_pedido($total_facturada, $cantidad_pedida)
    {
        $status = $this->estado_pedido;
        if ($total_facturada < (float)$cantidad_pedida && $this->datos_factura['dividir_factura'] == true)
                $status = '03';
        if ($total_facturada < (float)$cantidad_pedida && $this->datos_factura['dividir_factura'] != true)
                $status = '15';
        if ($status != '04')
            $this->estado_pedido = $status;
        return $status;
    }

    function validar_datos()
    {
        $this->validacion['validacion'] = true;
        if (count($this->datos_factura) > 0 && is_array($this->datos_factura))
        {

            foreach ($this->datos_factura['productos'] as $key => $producto) 
            {
                if ($producto['valor'] <= 0) 
                {
                    $this->validacion['validacion'] = false;
                    $this->validacion['error'] = " No se puede facturar con valor 0";
                    break;
                }

                if (!$this->validacion['validacion'])
                    break;
            }
        }else{
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No se encontraron datos";
            return;
        }
        $subtotal_coniva = 0;
        $subtotal_siniva = 0;
        foreach ($this->datos_factura['productos'] as $key => $producto) 
        {
            if ($producto['con_iva'] == 'S') 
                $subtotal_coniva += $producto['total'];
            else
                $subtotal_siniva += $producto['total'];
        }

        foreach ($this->datos_factura['productos'] as $key => $producto) 
        {
            if ((float)$producto['cantidad_facturar'] > (float)$producto['cantidad_pedida']) 
            {
                $this->validacion['validacion'] = false;
                $this->validacion['error'] = " No se puede facturar una cantidad mayor a la pedida";
                break;
            }

            if (!$this->validacion['validacion'])
                break;
        }

        if ($this->datos_factura['subtotal_coniva'] != $subtotal_coniva)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No cuadra subtotal con iva";
            return;
        }

        if ($this->datos_factura['subtotal_siniva'] != $subtotal_siniva)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No cuadra subtotal sin iva";
            return;
        }

        if ($this->datos_factura['condicion_pago'] == '0' && count($this->datos_factura['condicion_pago']) <= 0)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No se encuentran los pagos";
            return;
        }

    }

    function validaciones_generales()
    {
        $this->validacion['validacion'] = true;

        if ($this->serie_sri == '' || $this->serie_sri == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No esta configurada la serie de la Bodega";
            return;
        }

        $this->datos_cliente = $this->Cliente->get_cliente_by_codigo($this->codigo_cliente);
        if (count($this->datos_cliente) < 0 || $this->datos_cliente == null || $this->datos_cliente == '')
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No existe el cliente";
            return;
        }

        $this->secuencial_factura_get();
        if ($this->secuencial_factura == '' || $this->secuencial_factura == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No esta configurado secuencial de documento de Factura (71 - 02)";
            return;
        }

        $existe_factura = $this->facturacion_pedidos->consultar_si_existe_factura($this->numero_factura);
        if ($existe_factura)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = $this->numero_factura . " Numero Factura ya existe";
            return;

        }

    }

    function secuencial_factura_get()
    {
        $secuencial_factura = $this->plan_diario->get_numero_documento('71', '02');
        $this->secuencial_factura = (int) $secuencial_factura + 1;
        $this->numero_factura = $this->serie_sri . str_pad($this->secuencial_factura,9,'0',STR_PAD_LEFT);
    }

    function update_numero_factura()
    {
        $this->plan_diario->update_numero_documento('71', '02', $this->secuencial_factura);
    }

    function secuencial_pago()
    {
        $secuencial_pago = $this->plan_diario->get_numero_documento('71', '80');
        $this->secuencial_pago = (int) $secuencial_pago + 1;
        $this->numero_pago = $this->serie_sri . str_pad($this->secuencial_pago,9,'0',STR_PAD_LEFT);
    }

    function update_numero_pago()
    {
        $this->plan_diario->update_numero_documento('71', '80', $this->secuencial_pago);
    }

    function cargar_datos()
    {
        $facturar = array();
        $facturar['codigo_cliente']     = '1790727203001';
        $facturar['venta_bruta']        = 172;
        $facturar['subtotal_coniva']    = 172;
        $facturar['subtotal_siniva']    = 0;
        $facturar['total_iva']          = 18.58;
        $facturar['iva']                = 12;
        $facturar['desc_cliente']       = 17.2;
        $facturar['desc_forma_pago']    = 0;
        $facturar['desc_total']         = 0;
        $facturar['recargos']           = 0;
        $facturar['valor_portfee']      = 0;
        $facturar['total_factura']      = 173.38;
        $facturar['localidad']          = '02';
        $facturar['codigo_vendedor']    = '01';
        $facturar['condicion_pago']     = '0';
        $facturar['numero_pagos']       = 0;
        $facturar['forma_pago']         = '4';
        $facturar['dividir_factura']    = true;
        $facturar['productos']          = array(
             array(
                'numero_pedido'     => '001-001-000000016',
                'codigo_producto'   => 'alm01',
                'ocurrencia'        => '00',
                'con_iva'           => 'S',
                'cantidad_pedida'   => 20,
                'cantidad_facturar' => 20,
                'cantidad_facturada'=> 0,
                'valor'             => 3,
                'descuento'         => 6,
                'total'             => 60
                ),
                array(
                'numero_pedido'     => '001-001-000000017',
                'codigo_producto'   => 'alm01',
                'ocurrencia'        => '00',
                'con_iva'           => 'S',
                'cantidad_pedida'   => 20,
                'cantidad_facturar' => 20,
                'cantidad_facturada'=> 0,
                'valor'             => 3,
                'descuento'         => 6,
                'total'             => 60
                ),
                array(
                'numero_pedido'     => '001-001-000000017',
                'codigo_producto'   => 'CEN09',
                'ocurrencia'        => '01',
                'con_iva'           => 'S',
                'cantidad_pedida'   => 20,
                'cantidad_facturar' => 20,
                'cantidad_facturada'=> 0,
                'valor'             => 2.50,
                'descuento'         => 5,
                'total'             => 50
                ),
                array(
                'numero_pedido'     => '001-001-000000017',
                'codigo_producto'   => 'DS-DES-0041',
                'ocurrencia'        => '02',
                'con_iva'           => 'S',
                'cantidad_pedida'   => 100,
                'cantidad_facturar' => 100,
                'cantidad_facturada'=> 0,
                'valor'             => 0.02,
                'descuento'         => 0.2,
                'total'             => 2
                )

            );

        $facturar['vencimientos']       = array(
                array(
                'fecha_cobro'       => '2016-11-24',
                'cobro'             => 173.38,
                'saldo'             => 0,
                'detalle'           => 'PED001-001-0000000016 1/1'
                )/*,
                array(
                'fecha_cobro'       => '2016-12-23',
                'cobro'             => 41.13,
                'saldo'             => 0,
                'detalle'           => 'PED001-001-0000000016 1/1'
                ),
                array(
                'fecha_cobro'       => '2017-01-23',
                'cobro'             => 41.13,
                'saldo'             => 0,
                'detalle'           => 'PED001-001-0000000016 1/1'
                ),
                array(
                'fecha_cobro'       => '2017-02-23',
                'cobro'             => 41.12,
                'saldo'             => 0,
                'detalle'           => 'PED001-001-0000000016 1/1'
                )*/
            );

        $facturar['pagos']          = array(
                array(
                    'valor'         => 173.38,
                    'forma_pago'    => '2'
                    )
            );

        print_r($facturar);

        return $facturar;
    }

    
    function pedidos_aprobados_by_cliente_get($codigo_cliente)
    {
        $this->pedidos = $this->facturacion_pedidos->get_pedidos_aprobados_by_cliente($codigo_cliente);
        echo format_response($this->pedidos);
        
    }

    function vendedores_get(){
        $vendedores = $this->facturacion_pedidos->get_vendedores();
        echo format_response($vendedores);       

    }
    function condiciones_pago_get(){
        $condiciones_pago = $this->facturacion_pedidos->get_condiciones_pago();
        echo format_response($condiciones_pago);
    }
    
    function impresoras_get(){
        $impresoras = $this->facturacion_pedidos->get_impresoras();
        echo format_response($impresoras);
    }

    function formatos_get(){
        $formatos = $this->facturacion_pedidos->get_formatos();
        echo format_response($formatos);
    }

    function localidades_get(){
        $localidades = $this->facturacion_pedidos->get_localidades();
        echo format_response($localidades);
    }
    
    function transportistas_get(){
        $transportistas = $this->facturacion_pedidos->get_transportistas();
        echo format_response($transportistas);
    }
    
    function ayudantes_get(){
        $ayudantes = $this->facturacion_pedidos->get_ayudantes();
        echo format_response($ayudantes);
    }

    function cliente_by_codigo_get($codigo_cliente){

        $datos_cliente = $this->Cliente->get_cliente_by_codigo($codigo_cliente);
        echo format_response($datos_cliente);

    }

    function clientes_get($id, $nombre) {
        $maecte = $this->Cliente->get_cliente_by($id, urldecode($nombre));

        $array_maecte = array();
        if(count($maecte) > 0){
            foreach ($maecte as $value) {
                $portfee = $this->portfee_by_categoria_get($value->catcte01);
                $array_maecte [] = array(
                    'codigo_cliente' => $value->codcte01,
                    'nombre_cliente' => $value->nomcte01,
                    'ci_ruc' =>$value->cascte01,
                    'direccion' => $value->dircte01,
                    'localidad' => $value->loccte01,
                    'telefono' => $value->telcte01,
                    'vendedor' => $value->vendcte01,
                    'condPago' => $value->condpag01,
                    'email' => $value->emailcte01,
                    'categoria' => $value->catcte01,
                    'saldo_actual' => $value->sdoact01,
                    'saldo_exceso' => $value->totexceso01,
                    'contacto' => $value->pagleg01,
                    'porcentaje_portfee' => $portfee
                    );
            }
        }
           
        echo format_response($array_maecte);
    }

    function productos_tipo_get($codigo, $descripcion,$flag,$cod_producto_principal = '') {
        if($flag == 0){
            $productos = $this->Producto->get_productos_tipo($codigo, urldecode($descripcion), 'null');
        }else{
            $productos = $this->Producto->get_productos_equivalentes($cod_producto_principal);
        }
        echo format_response($productos);
    }


    function portfee_by_categoria_get($categoria)
    {
        $portfee = 0;
        $valor_portfee = $this->Cliente->portfee_by_categoria($categoria);
        if ($valor_portfee != NULL || count($valor_portfee) > 0)
            $portfee = (float)$valor_portfee['ad1tab'];
        return $portfee;

    }

    function portfee_get()
    {
        $valor_portfee = $this->Cliente->portfee();
        echo format_response($valor_portfee);
    }
    function iva_default_get(){
        $iva = $this->dispatch->iva_default();
        $array_iva = array('iva' => $iva,'fecha_formato1'=> date('Y-m-d'),'fecha_formato2'=> date('d-m-Y') );
        echo format_response($array_iva);

    }

    function formas_pago_get(){
        $formas_pago = $this->facturacion_pedidos->get_formas_pago();        
        echo format_response($formas_pago);
    }

    function data_condicion_pago_get($cod_condicion){

        $data = $this->facturacion_pedidos->get_data_condicion_pago($cod_condicion);        
        echo format_response($data);

    }

    function validacion_stock_post(){
        $datos = $this->post();        
        $array_productos_sin_stock = array();

        if(count($datos['data']['items']) > 0){
            foreach ($datos['data']['items'] as $valor) {
                $es_servicio =  $this->Producto->consulta_producto_servicio($valor['codprod30']);
                if($es_servicio == 0){                    
                    $cantidad_stock_temp = $this->facturacion_pedidos->sum_stock_movimientos_temporales_producto($datos['data']['nocomp']);
                                        
                    $cantidad_stock = $this->Producto->consulta_stock($valor['codprod30']);                    
                    $cantidad_stock_final = $cantidad_stock[0]['cantact01'] + ((float)$cantidad_stock_temp);
                    
                    if($valor['aFacturar'] > $cantidad_stock[0]['cantact01']){
                        $array_productos_sin_stock[] = array(
                            'codProductoSinStock' => $valor['codprod30']
                            );
                    }
                }           
            }
        }               
        echo format_response($array_productos_sin_stock);

    } 

    function nocomp_temporal_producto_post(){
        $datos = $this->post();
        $nocomp = $this->facturacion_pedidos->get_nocomp_temporal() + 1;
        echo json_encode($nocomp);
    }   

    function inserta_movimientos_temporales_producto_post(){
        $datos = $this->post(); 
        $valida_mov_temp = $this->facturacion_pedidos->consulta_mov_temporal($datos['nocomp'],$datos['ocurrencia'],$datos['codProd']); 

        if($valida_mov_temp == 0){ 

            $this->facturacion_pedidos->inserta_movimientos_temporales_producto($datos['nocomp'],$datos['ocurrencia'],$datos['codProd'],$datos['cant']*-1,$datos['localidad'],date('Y-m-d H:i:s'));
        }else{

            $this->actualiza_movimientos_temporales_producto($datos['nocomp'],$datos['ocurrencia'],$datos['codProd'],$datos['cant']*-1);

        }
    }

    function elimina_movimientos_temporales_producto_post(){
        $datos = $this->post();           
        $this->facturacion_pedidos->elimina_movimientos_temporales_producto($datos['nocomp'],$datos['ocurrencia'],$datos['codProd']);
    }

    function elimina_movimientos_temporales_producto_by_nocomp_post(){
        $datos = $this->post();           
        $this->facturacion_pedidos->elimina_movimientos_temporales_producto_by_nocomp($datos['nocomp']);
    }

    function actualiza_movimientos_temporales_producto($nocomp,$ocurrencia,$codProd){
                   
        $this->facturacion_pedidos->actualiza_movimientos_temporales_producto($nocomp,$ocurrencia,$codProd);
    }
    
}
?>
