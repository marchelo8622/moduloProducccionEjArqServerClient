<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class facturacion_pedidos extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Facturacion_pedidos";
		$data["angular_app"] = "Facturacion_pedidos";
		$this->layout->view("facturacion_pedidos", $data);
	}
}

