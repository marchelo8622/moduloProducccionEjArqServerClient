<?php
include_once(APPPATH ."models/base_model.php");
Class Portfee_model extends Base_model
{
    private $matriz;
    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    
    function get_facturas($fecha_desde, $fecha_hasta, $codigo_cliente,$estado)
    {
        if ($codigo_cliente != "null") {
            $this->db->like('nocte31', $codigo_cliente);
        }

        if ($fecha_desde != 'null'){
            $fecha_desde .= " 00:00:00";
            $fecha_hasta .= " 23:59:59";
            $this->db->where('fecfact31 BETWEEN "'. date('Y-m-d H:i:s', strtotime($fecha_desde)). '" and "'. date('Y-m-d H:i:s', strtotime($fecha_hasta)).'"');
        }

        $this->db->select('tipodocto31 as tipo_documento,
                            nofact31 as numero_factura,
                            nocte31 as codigo_cliente,
                            nomcte31 as nombre_cliente,
                            localid31 as localidad,
                            vtabta31 as venta_bruta,
                            descto31 as descuento,
                            flete31 as valor_portfee,
                            itm31 as iva,
                            novend31 as vendedor,
                            fecfact31 as fecha_factura,
                            condpag31 as condicion_pago,
                            nopagos31 as numero_pagos,
                            formapago31 as forma_pago,
                            ruc31 as ruc,
                            catcte31 as categoria_cliente,
                            recargos31 as valor_recargos,
                            ice31 as valor_ice,
                            totsiniva31 as total_sin_iva,
                            estado_portfee,
                            proveedor_portfee
                        ');
        $this->db->from("maefac");
        $this->db->where("flete31 > 0");
        $this->db->where("estado_portfee", 0);
        $this->db->where("proveedor_portfee", "");
        $this->db->group_by("nofact31");
        $this->db->order_by("nofact31", "ASC");
        $this->db->limit(20);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function get_facturas_reporte($fecha_desde, $fecha_hasta, $codigo_cliente,$estado)
    {
        if ($codigo_cliente != "null") {
            $this->db->like('nocte31', $codigo_cliente);
        }

        if ($fecha_desde != 'null'){
            $fecha_desde .= " 00:00:00";
            $fecha_hasta .= " 23:59:59";
            $this->db->where('fecfact31 BETWEEN "'. date('Y-m-d H:i:s', strtotime($fecha_desde)). '" and "'. date('Y-m-d H:i:s', strtotime($fecha_hasta)).'"');
        }

        if($estado != "T"){
            $this->db->where("estado_portfee", $estado);
        }

        $this->db->select('tipodocto31 as tipo_documento,
                            nofact31 as numero_factura,
                            nocte31 as codigo_cliente,
                            nomcte31 as nombre_cliente,
                            localid31 as localidad,
                            vtabta31 as venta_bruta,
                            descto31 as descuento,
                            flete31 as valor_portfee,
                            itm31 as iva,
                            novend31 as vendedor,
                            fecfact31 as fecha_factura,
                            condpag31 as condicion_pago,
                            nopagos31 as numero_pagos,
                            formapago31 as forma_pago,
                            ruc31 as ruc,
                            catcte31 as categoria_cliente,
                            recargos31 as valor_recargos,
                            ice31 as valor_ice,
                            totsiniva31 as total_sin_iva,
                            estado_portfee,
                            proveedor_portfee
                        ');
        $this->db->from("maefac");
        $this->db->where("flete31 > 0");        
        $this->db->where("proveedor_portfee", "");
        $this->db->group_by("nofact31");
        $this->db->order_by("nofact31", "ASC");
        //$this->db->limit(20);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function get_codigo_proveedor_portfee()
    {
        $this->db->where("numtab", '01'); 
        $this->db->where("codtab", 'PRPF'); 
        $this->db->select('ad7tab as codigo_proveedor, ad8tab as forma_pago')->from($this->matriz.".maetab");
        $resultado = $this->db->get();
        $dato = $resultado->result_array();

        return $dato[0];
    }

    function get_datos_proveedor($codigo)
    {
        $this->db->select('codcte01 as codigo_proveedor, nomcte01 as nombre_proveedor, sdoact01 as saldo_actual, emailcte01 as email, totexceso01 as total_exceso');
        $this->db->from($this->matriz.".maepag");
        $this->db->where('codcte01', $codigo);
        $result = $this->db->get();
        //echo $this->db->last_query();
        $result = $result->result();
        return $result[0];
    }

    function registrar_anticipo($datos)
    {
        $this->db->insert($this->matriz.".movpag2", $datos);
        return $this->db->affected_rows();
    }

    function actualizar_saldos_proveedor($datos)
    {
        $data = array(
            'totexceso01' => - $datos['saldo_exceso']
        );
        $this->db->where('codcte01', $datos['codigo_proveedor']);
        $result = $this->db->update('maepag', $data);
        return $result;
    }

    function actualizar_estado_factura($datos)
    {
        $data = array(
            'estado_portfee' => 1,
            'valor_portfee'  => $datos['valor_portfee'],
            'proveedor_portfee' => $datos['codigo_proveedor']
        );
        $this->db->where('nofact31', $datos['numero_factura']);
        $this->db->where('nocte31', $datos['codigo_cliente']);
        $result = $this->db->update('maefac', $data);
        return $result;

    }
}
?>
