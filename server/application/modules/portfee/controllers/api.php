<?php
error_reporting(0);
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {
    private $datos_facturas = array();
    private $datos_proveedor = array();
    private $validacion = array();
    private $cod_proveedor_portfee;
    private $respuesta = array();
    private $secuencial_anticipo;
    private $secuencial_anticipo_val;
    private $numero_anticipo;
    private $forma_pago;

    function __construct()
    {
        parent::__construct();
        $this->load->model("Portfee_model", "portfee");
        $this->load->model('plan_diario/plan_diario_model', 'plan_diario');
        $this->datos_proveedor_portfee();        
        $this->serie_sri = $this->get_serie_sri();
        session_start();
        $this->uid = $_SESSION['UID'];
    }

    function datos_proveedor_portfee()
    {
        $datos = $this->portfee->get_codigo_proveedor_portfee();
        $this->cod_proveedor_portfee = $datos['codigo_proveedor'];
        $this->forma_pago = $datos['forma_pago'];
    }

    function response_false($error = 'No se Proceso')
    {
        $this->respuesta['respuesta'] = false;
        $this->respuesta['error'] = $error;
    }

    function response_true($respuesta = true)
    {
        $this->respuesta['respuesta'] = $respuesta;
    }

    function get_serie_sri()
    {
        $serie_sri = $this->plan_diario->get_serie_sri();
        return $serie_sri;
    }
 
    function facturas_get($fecha_desde = 'null', $fecha_hasta = 'null', $codigo_cliente = 'null', $estado = 'null')
    {
        $this->facturas = $this->portfee->get_facturas($fecha_desde, $fecha_hasta, $codigo_cliente, $estado);
        echo format_response($this->facturas);
        
    }

    function facturas_reporte_get($fecha_desde = 'null', $fecha_hasta = 'null', $codigo_cliente = 'null', $estado = 'null')
    {
        $this->facturas = $this->portfee->get_facturas_reporte($fecha_desde, $fecha_hasta, $codigo_cliente, $estado);
        echo format_response($this->facturas);
        
    }

    function crear_anticipo_post()
    {
        $data = $this->post();
        $this->datos_facturas = $data['data'];
        $this->validaciones_generales();
        if ($this->validacion['validacion'])
        {
            $this->generar_anticipo();
            $this->update_numero_anticipo();
            $this->actualizar_facturas();
            $this->response_true();

        }else{
            $error = $this->validacion['error'];
            $this->response_false($error);

        }
        echo format_response($this->respuesta);
    }

    function generar_anticipo()
    {
        $saldo_exceso = (float)$this->datos_facturas['total_portfee'] + abs($this->datos_proveedor->total_exceso); 
        $anticipo = array(
            'codcte43'      => $this->datos_proveedor->codigo_proveedor,
            'tipodoc43'     => '71.5',
            'numdoc43'      => $this->numero_anticipo,
            'ocurren43'     => '0000',
            'fecdoc43'      => date('Y-m-d H:i:s'),
            'numvencob43'   => '001',
            'totdoc43'      => $this->datos_facturas['total_portfee'],
            'saldoregmov43' => $this->datos_facturas['total_portfee'],
            'tipodocdb43'   => '71.5',
            'valorabono43'  => $this->datos_facturas['total_portfee'],
            'efectcheque43' => $this->forma_pago,
            'saldoexceso43' => - $saldo_exceso,
            'UID'           => $this->uid,
            'cvtransfer43'  => 'N',
            'fecvencompra43'=> date('Y-m-d'),
            'cvanulado43'   => 'N'
            );

        $this->portfee->registrar_anticipo($anticipo);
        $this->respuesta['documentos'][] = array('numero_documento' => $this->numero_anticipo,
                                        'tipo_documento' => '71.5');
        $datos = array(
                    'saldo_exceso' => $saldo_exceso,
                    'codigo_proveedor' => $this->datos_proveedor->codigo_proveedor
                    );
        $this->portfee->actualizar_saldos_proveedor($datos);
    }

    function actualizar_facturas()
    {
        foreach ($this->datos_facturas['facturas'] as $key => $factura) 
        {
            $datos = array(
                    'numero_factura' => $factura['numero_factura'],
                    'codigo_cliente' => $factura['codigo_cliente'],
                    'valor_portfee' => $factura['valor_portfee'],
                    'codigo_proveedor' => $this->datos_proveedor->codigo_proveedor
                    );
            $this->portfee->actualizar_estado_factura($datos);
        }
    }

    function validaciones_generales()
    {
        $this->validacion['validacion'] = true;

        if (count($this->datos_facturas) <= 0 || $this->datos_facturas == null || $this->datos_facturas == undefined)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No se encontraron datos";
            return;
        }

        if ($this->serie_sri == '' || $this->serie_sri == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No esta configurada la serie de la Bodega";
            return;
        }

        if ($this->cod_proveedor_portfee == "" || $this->cod_proveedor_portfee == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " Falta configurar codigo de proveedor PortFee";
            return;
        }

        if ($this->forma_pago == "" || $this->forma_pago == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " Falta configurar forma de pago para generar anticipo";
            return;
        }

        $this->datos_proveedor = $this->portfee->get_datos_proveedor($this->cod_proveedor_portfee);
        if (count($this->datos_proveedor) < 0 || $this->datos_proveedor == null || $this->datos_proveedor == '')
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " Codigo de Proveedor PortFee: Proveedor no existe";
            return;
        }

        if ($this->datos_facturas['facturas'] == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No hay facturas que procesar";
            return;
        }

        $total_anticipo = 0;
        foreach ($this->datos_facturas['facturas'] as $key => $factura) 
        {
            $total_anticipo += (float)$factura['valor_portfee'];
        }

        if ($this->datos_facturas['total_portfee'] == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No hay valor PortFee";
            return;
        }

        if ($this->datos_facturas['total_portfee'] != $total_anticipo)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No cuadra total del valor de anticipo: PortFee";
            return;
        }

        $this->secuencial_anticipo_get();
        if ($this->secuencial_anticipo_val == '' || $this->secuencial_anticipo_val == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No esta configurado secuencial de documento de Anticipo (61 - 71.5)";
            return;
        }

    }

    function secuencial_anticipo_get()
    {
        $this->secuencial_anticipo_val = $this->plan_diario->get_numero_documento('61', '71.5');
        $this->secuencial_anticipo = (int) $this->secuencial_anticipo_val + 1;
        $this->numero_anticipo = $this->serie_sri . str_pad($this->secuencial_anticipo,9,'0',STR_PAD_LEFT);
    }

    function update_numero_anticipo()
    {
        $this->plan_diario->update_numero_documento('61', '71.5', $this->secuencial_anticipo);
    }

    function genera_excel_post(){

        $filter = $this->post();  
        $fecha_desde = $filter['fecha_desde'];
        $fecha_hasta = $filter['fecha_hasta'];
        if($filter['codigo_cliente']){
            $codigo_cliente =$filter['codigo_cliente'];
        }else{
            $codigo_cliente = 'null';
        }
        $estado = $filter['estado'];
            
        $datos_portfee = $this->facturas = $this->portfee->get_facturas_reporte($fecha_desde, $fecha_hasta, $codigo_cliente, $estado);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Cod. Cliente')
        ->setCellValue('B1', 'Nombre')
        ->setCellValue('C1', 'Fecha Fact.')
        ->setCellValue('D1', 'Factura')
        ->setCellValue('E1', 'Total Factura')
        ->setCellValue('F1', 'Valor PortFee')
        ->setCellValue('G1', 'Anticipo generado');
          if (count($datos_portfee) > 0) {
                    $j = 2;
                    foreach ($datos_portfee as $valor) {
                            if($valor['estado_portfee'] == 0){
                                $valor['estado_portfee'] = 'NO';
                            }else{
                                $valor['estado_portfee'] = 'SI';
                            }         
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$j, $valor['codigo_cliente'])                            
                            ->setCellValue('C'.$j, $valor['nombre_cliente'])
                            ->setCellValue('D'.$j, $valor['fecha_factura'])
                            ->setCellValue('E'.$j, $valor['numero_factura'])
                            ->setCellValue('F'.$j, $valor['venta_bruta'])
                            ->setCellValue('G'.$j, $valor['valor_portfee'])
                            ->setCellValue('B'.$j, $valor['estado_portfee']);
                            $j++;
                    }
            }     

            $objPHPExcel->setActiveSheetIndex(0);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            //ob_end_clean();
            header('Content-Type: Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="ReportePortfee.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');

            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
            header ('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            $objWriter->save('php://output');     

    }

    function genera_pdf_post(){

        $filter = $this->post();  
        $fecha_desde = $filter['fecha_desde'];
        $fecha_hasta = $filter['fecha_hasta'];
        if($filter['codigo_cliente']){
            $codigo_cliente =$filter['codigo_cliente'];
        }else{
            $codigo_cliente = 'null';
        }
        $estado = $filter['estado'];
            
        $datos_portfee = $this->facturas = $this->portfee->get_facturas_reporte($fecha_desde, $fecha_hasta, $codigo_cliente, $estado);

        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Marcelo Montalvo');
        $pdf->SetTitle('Receta');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
        $pdf->setFontSubsetting(true);
 
        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
 
        $html = '';
        $html .= "<style type=text/css>";

        $html .= "table  , td {border: 1px solid black;    border-collapse: collapse;}";
        $html .= "h1{color: red; font-weight: bold;  text-align:center;}";
        $html .= "</style>";
        
        $html .= "<table cellspacing='0' cellpadding='4' border='1' width='100%'>";
        $html .= '<tr>
                    <td align="center" width="20%"><b>Cod. Cliente</b></td>
                    <td align="center" width="20%"><b>Nombre</b></td>
                    <td align="center" width="20%"><b>Fecha Fact.</b></td>
                    <td align="center" width="20%"><b>Factura</b></td>
                    <td align="center" width="20%"><b>Valor Portfee</b></td>
                  </tr>';

        $receta = $this->creador_recetas->get_receta_by_producto($codigo_producto);
        $costoTotalReceta = 0;
        $costoReceta = 0;
        foreach ($datos_portfee as $valor) 
        {    

            if($valor['valor_portfee'] == 0){
                $valor['valor_portfee'] = 'NO';
            }else{
                $valor['valor_portfee'] = 'SI';
            }

            $html .='<tr>
                        <td>'.$valor['codigo_cliente'] .'</td>
                        <td>'.$valor['nombre_cliente'].'</td>
                        <td>'.$valor['fecha_factura'].'</td>
                        <td>'.$valor['numero_factura'].'</td>
                        <td>'.$valor['venta_bruta'].'</td>
                        <td>'.$valor['valor_portfee'].'</td>
                        <td>'.$valor['estado_portfee'].'</td>
                    </tr>';
           
        }
               
        $html .= '</table>';
        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 1, $ln = 1, $fill = 0, $reseth = true, $align = '');
        $nombre_archivo = utf8_decode("reportePortfee.pdf");
        $pdf->Output($nombre_archivo, 'I');     

    }

}
?>
