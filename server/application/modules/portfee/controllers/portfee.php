<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class portfee extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Portfee";
		$data["angular_app"] = "Portfee";
		$this->layout->view("portfee", $data);
	}

	public function reporte()
	{
		$data["title_for_layout"] = "Reporte Portfee";
		$data["angular_app"] = "Portfee";
		$this->layout->view("reporte_portfee", $data);
	}
}

