<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class requisiciones_produccion extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Requisiciones_produccion";
		$data["angular_app"] = "Requisiciones_produccion";
		$this->layout->view("requisiciones_produccion", $data);
	}
}

