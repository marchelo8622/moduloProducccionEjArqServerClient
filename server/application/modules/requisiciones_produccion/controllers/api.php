<?php
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {
    private $IDB;
    private $ocu_req;
    private $numero_requisicion;
    private $secuencial_requisicion;
    private $bodega_materia_prima;
    private $codigo_cliente;
    private $numero_produccion;
    private $observacion_requisicion;

    function __construct()
    {
        parent::__construct();
        $this->load->model("Requisiciones_produccion_model", "requisiciones_produccion");
        $this->load->model('producto/Producto_model', 'Producto');
        $this->load->model("creador_recetas/Creador_recetas_model", "creador_recetas");
        $this->load->model("plan_diario/Plan_diario_model", "plan_diario");
        $this->IDB = $this->input->get('IDB');
        $this->ocu_req = 0;
        $this->observacion_requisicion = '';
    }
 
    function ordenes_produccion_get($fecha)
    {

        $ordenes = $this->requisiciones_produccion->get_ordenes_produccion($fecha);
        echo format_response($ordenes);
    }
 
    function detalle_orden_produccion_get($orden_produccion)
    {
        $detalle_produccion = $this->requisiciones_produccion->get_detalle_produccion($orden_produccion);
        $receta = array();
        foreach ($detalle_produccion as $key => $value) {
            $producto = $this->Producto->get_producto_by_codigo($value->codprod30);
            $receta[$key]['label'] = $producto['descripcion'];
            $receta[$key]['codigo_producto'] = $producto['codigo'];
            $compuesto = $this->Producto->es_compuesto($value->codprod30);
            if ($compuesto)
            {
                $productos = array();
                $subreceta = $this->creador_recetas->get_receta_by_producto($value->codprod30);
                foreach ($subreceta as $key2 => $sub) {
                    $productos[$key2]['label'] = $sub->descripcion_ingrediente;
                    $productos[$key2]['codigo_producto'] = $sub->codigo_ingrediente;
                    $compuesto = $this->Producto->es_compuesto($sub->codigo_ingrediente);
                    if ($compuesto)
                    {
                        $productos2 = array();
                        $subreceta2 = $this->creador_recetas->get_receta_by_producto($sub->codigo_ingrediente);
                        foreach ($subreceta2 as $key3 => $sub2) {
                            $productos2[$key3]['label'] = $sub2->descripcion_ingrediente;
                            $productos2[$key3]['codigo_producto'] = $sub2->codigo_ingrediente;

                        }
                        $productos[$key2]['children'] = $productos2;
                    }
                }
                $receta[$key]['children'] = $productos;
            }
        }
        //print_r($receta);
        echo format_response($receta);
    }
 
    function requisiciones_produccion_post()
    {
        $datos = $this->post();
        $this->numero_produccion = $datos['data']['orden_produccion'];
        $this->observacion_requisicion = $datos['data']['observacion'];
        $fecha = date("Y-m-d");
        $secuencial_requisicion = $this->plan_diario->get_numero_requisicion();
        $this->secuencial_requisicion = $secuencial_requisicion + 1;
        $this->numero_requisicion = "PD-".$fecha."-".$this->secuencial_requisicion;
        $this->codigo_cliente = $this->get_cliente_by_produccion_get();
        foreach ($datos['data']['productos'] as $key => $value) {
            $receta = $this->plan_diario->get_componentes($value['codigo_producto']);
            if (count($receta) > 0)
            {
                foreach ($receta as $key => $producto_compenente) {
                    $producto = array(
                                'numord20' => $this->numero_produccion,
                                'codprod20t' => $value['codigo_producto'],
                                'codprod20' => $producto_compenente['codprod20'],
                                'ocucomp20' => str_pad($key, 2, '0', STR_PAD_LEFT),
                                'desprod20' => $producto_compenente['desprod20'],
                                'unidmed20' => $producto_compenente['unidmed20'],
                                'cantidad20' => $producto_compenente['cantidad20'],
                                'cant_producir' => $value['cantidad_solicitada'],
                                'cantfalta20' => '0.00',
                                'costo20' => $producto_compenente['costo20'],
                                'fecmod20' => date('Y-m-d H:i:s'),
                                'tipo20' => 'P',
                                'bodega20' => $this->IDB
                                );
                $this->plan_diario->generar_tmp_lismaehis($producto);
                }
            }else{
                $datos_producto = $this->Producto->get_producto_by_codigo($value['codigo_producto']);
                $costo = $this->get_costo_producto($datos_producto['codigo']);
                $producto = array(
                                'numord20' => $this->numero_produccion,
                                'codprod20t' => $value['codigo_producto'],
                                'codprod20' => $value['codigo_producto'],
                                'ocucomp20' => str_pad($key, 2, '0', STR_PAD_LEFT),
                                'desprod20' => $datos_producto['descripcion'],
                                'unidmed20' => $datos_producto['unidad_medida'],
                                'cantidad20' => 1,
                                'cant_producir' => $value['cantidad_solicitada'],
                                'cantfalta20' => '0.00',
                                'costo20' => $costo,
                                'fecmod20' => date('Y-m-d H:i:s'),
                                'tipo20' => 'P',
                                'bodega20' => $this->IDB
                                );
                $this->plan_diario->generar_tmp_lismaehis($producto);
        }

          
        }

        $this->generar_requisicion();
        $this->plan_diario->borrar_temporales_lismaehis($this->numero_produccion);
        echo format_response(true);
    }

    function generar_requisicion()
    {
        $bodega_materia_prima = $this->plan_diario->get_bodega_materia_prima();
        $this->bodega_materia_prima = $bodega_materia_prima['codigo_bodega'];
        $nombre_cocina = '';

        $tmp_lismaehis = $this->plan_diario->obtener_detalle_tmp_lismaehis($this->numero_produccion);

            foreach ($tmp_lismaehis as $key => $producto_compenente) 
            {
                $cantidad_a_producir = $producto_compenente['cant_producir'];
                $subReceta1 = $this->plan_diario->get_componentes($producto_compenente['codprod20']);
                if (count($subReceta1) > 0)
                {
                    foreach ($subReceta1 as $key => $producto_compenente) 
                    {
                        $subReceta2 = $this->plan_diario->get_componentes($producto_compenente['codprod20']);
                        if (count($subReceta2) > 0)
                        {
                            foreach ($subReceta2 as $key => $producto_compenente) 
                            {
                                $subReceta3 = $this->plan_diario->get_componentes($producto_compenente['codprod20']);
                                if (count($subReceta3) > 0)
                                {
                                    foreach ($subReceta3 as $key => $producto_compenente) 
                                    {
                                        $subReceta4 = $this->plan_diario->get_componentes($producto_compenente['codprod20']);
                                        if (count($subReceta4) > 0)
                                        {
                                            foreach ($subReceta4 as $key => $producto_compenente) 
                                            {
                                                $this->built_datos_requisicion($nombre_cocina, $producto_compenente, $cantidad_a_producir);
                                            }

                                        }else{
                                            $this->built_datos_requisicion($nombre_cocina, $producto_compenente, $cantidad_a_producir);
                                        }
                                    }

                                }else{
                                    $this->built_datos_requisicion($nombre_cocina, $producto_compenente, $cantidad_a_producir);
                                }
                            }

                        }else{
                            $this->built_datos_requisicion($nombre_cocina, $producto_compenente, $cantidad_a_producir);
                        }
                    }
                }else{
                    $this->built_datos_requisicion($nombre_cocina, $producto_compenente, $cantidad_a_producir);
                }

            }

    }

    function built_datos_requisicion($nombre_cocina, $producto_compenente, $cantidad_a_producir)
    {
        $requisicion = array(
                            'numdoc' => $this->numero_requisicion,
                            'ocu' => str_pad($this->ocu_req, 2, '0', STR_PAD_LEFT),
                            'sec' => $this->secuencial_requisicion,
                            'numordenp' => $this->numero_produccion,
                            'codcliente' => $this->codigo_cliente,
                            'fecreg' => date('Y-m-d H:i:s'),
                            'codprod' => $producto_compenente['codprod20'],
                            'desprod' => $producto_compenente['desprod20'],
                            'cantsol' => number_format($producto_compenente['cantidad20'] * $cantidad_a_producir, 6, '.', ''),
                            'cantent' => '0.00',
                            'bodega' =>  $this->IDB,
                            'UID' => '',
                            'observacion' => $this->observacion_requisicion,
                            'bodegareq' => $this->bodega_materia_prima
                            );
                $this->plan_diario->generar_detalle_requisicion($requisicion);
        $this->ocu_req++;
    }

    function get_cliente_by_produccion_get()
    {
        $codigo_cliente = $this->requisiciones_produccion->get_cliente_by_produccion($this->numero_produccion);
        return $codigo_cliente->codcte30;
    }

    function  get_costo_producto($codigo)
    {
        $costo = '0.00';
        $costoCompra = $this->Producto->get_ultima_compra_by_producto($codigo);
        if ($costoCompra != '' && $costoCompra != null && $costoCompra != 0 && count($costoCompra) > 0){
            $costo = $costoCompra[0]->costo_transaccion;
        }
        else{
            $costoTransaccion = $this->Producto->get_ultima_transaccion($codigo);

        if ($costoTransaccion != '' && $costoTransaccion != null && $costoTransaccion != 0 && count($costoTransaccion) > 0)
            $costo = $costoTransaccion[0]->costo_transaccion;
        }

        return $costo;
    }
 
    function requisiciones_produccion_delete()
    {
        // delete a requisiciones_produccion and respond with a status/errors
        // return data = echo format_response($data);
    }
}
?>
