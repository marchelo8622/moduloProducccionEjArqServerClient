<?php
include_once(APPPATH ."models/base_model.php");
Class Requisiciones_produccion_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_ordenes_produccion($fecha)
    {
        $fecha_desde = $fecha." 00:00:00";
        $fecha_hasta = $fecha." 23:59:59";
        $this->db->select('nopedido30, fecpedido30, fecdespacho30, nomcte30');
        $this->db->where("fecdespacho30 >=", $fecha_desde);
        $this->db->where("fecdespacho30 <=", $fecha_hasta);
        $this->db->where("status30", "02");
        $this->db->from("maeprd30");
        $this->db->group_by('nopedido30');
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function get_detalle_produccion($orden_produccion)
    {
        $this->db->select('nopedido30, fecpedido30, nomcte30, codprod30, cantped30');
        $this->db->where("nopedido30", $orden_produccion);
        $this->db->from("maeprd30");
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function get_cliente_by_produccion($orden_produccion)
    {
        $this->db->select('codcte30');
        $this->db->where("nopedido30", $orden_produccion);
        $this->db->from("maeprd30");
        $this->db->group_by('codcte30');
        $result = $this->db->get();
        //echo $this->db->last_query();
        $resultado = $result->result();
        return $resultado[0];

    }
}
?>
