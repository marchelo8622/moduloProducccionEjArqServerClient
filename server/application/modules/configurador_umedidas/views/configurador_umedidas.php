<script src="<?= base_url()?>../client/src/app/configurador_umedidas/module.js"></script>
<script src="<?= base_url()?>../client/src/app/configurador_umedidas/configurador_umedidas_model.js"></script>
<script src="<?= base_url()?>../client/src/app/configurador_umedidas/controllers/configurador_umedidas.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>

<script>
    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }
    
</script>


