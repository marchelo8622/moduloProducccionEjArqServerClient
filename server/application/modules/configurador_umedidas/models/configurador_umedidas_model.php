<?php
include_once(APPPATH ."models/base_model.php");
Class Configurador_umedidas_model extends Base_model
{
    private $matriz;

    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }

    function get_unidades_medida()
    {
        $this->db->from($this->matriz.".unidades_medida");
        $this->db-> limit(10);
        $result = $this->db->get();
        return $result->result();
    }

    function get_unidades_medida_by_nombre($parametro)
    {
        $this->db->from($this->matriz.".unidades_medida");
        $this->db->like('unidad_medida_principal', $parametro);
        $result = $this->db->get();
        return $result->result();
    }

    function get_unidades_medida_by_codigo($parametro)
    {
        $this->db->from($this->matriz.".unidades_medida");
        $this->db->where('id_um', $parametro);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function guardar_unidad_medida($datos)
    {
        $result = $this->db->insert($this->matriz.".unidades_medida", $datos);
        return $result;
    }

    function eliminar_unidad_medida($parametro)
    {

        $this->db->where('id_um',$parametro);
        $result = $this->db->delete($this->matriz.'.unidades_medida');
        return $result;
    }

    function actualizar_unidad_medida($id, $unidad_medida_principal, $abreviatura_um_principal, $unidad_medida_secundaria, $abreviatura_um_secundaria, $equivalencia) {
        $data = array(
            'unidad_medida_principal' => $unidad_medida_principal,
            'abreviatura_um_principal' => $abreviatura_um_principal,
            'unidad_medida_secundaria' => $unidad_medida_secundaria,
            'abreviatura_um_secundaria' => $abreviatura_um_secundaria,
            'equivalencia' => $equivalencia
        );
        $this->db->where('id_um', $id);
        $result = $this->db->update($this->matriz.'.unidades_medida', $data);
        return $result;
    }

    function unidad_medida_utilizada($parametro)
    {
        $this->db->select('unidmed20');
        $this->db->from("lismae");
        $this->db->where('unidmed20', $parametro);
        $this->db->group_by('unidmed20');
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->num_rows();
    }
}
?>
