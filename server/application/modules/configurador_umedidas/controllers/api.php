<?php
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("Configurador_umedidas_model", "configurador_umedidas");
    }
 
    function unidades_medidas_get()
    {
        $umedidas = $this->configurador_umedidas->get_unidades_medida();
        echo format_response($umedidas);
    }

    function buscar_unidades_medidas_get($descripcion)
    {
        $umedidas = $this->configurador_umedidas->get_unidades_medida_by_nombre($descripcion);
        echo format_response($umedidas);
    }
 
    function actualizar_unidades_medidas_post()
    {
        $datos = $this->post();
        $id = $datos['data']['id_um'];
        $unidad_medida_principal = $datos['data']['unidad_medida_principal'];
        $abreviatura_um_principal = $datos['data']['abreviatura_um_principal'];
        $unidad_medida_secundaria = $datos['data']['unidad_medida_secundaria'];
        $abreviatura_um_secundaria = $datos['data']['abreviatura_um_secundaria'];
        $equivalencia = $datos['data']['equivalencia'];
        if ($this->configurador_umedidas->actualizar_unidad_medida($id, $unidad_medida_principal, $abreviatura_um_principal, $unidad_medida_secundaria, $abreviatura_um_secundaria, $equivalencia))
            echo $this->response('ok',200);
        else
            echo $this->response('false',400);
    }
 
    function unidades_medidas_post()
    {
        $datos = $this->post();
        if ($this->configurador_umedidas->guardar_unidad_medida($datos['data']))
            echo $this->response('ok',200);
        else
            echo $this->response('false',400);

    }
 
    function unidades_medidas_delete($parametro)
    {
        $umedidas = $this->configurador_umedidas->eliminar_unidad_medida($parametro);
        echo format_response($umedidas);
    }

    function unidad_medida_utilizada_get($codigo)
    {
        $umedidas = $this->configurador_umedidas->unidad_medida_utilizada($codigo);
        echo format_response($umedidas);
    }
}
?>
