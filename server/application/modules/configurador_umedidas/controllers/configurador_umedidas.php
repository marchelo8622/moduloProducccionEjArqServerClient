<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class configurador_umedidas extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Configurador_umedidas";
		$data["angular_app"] = "Configurador_umedidas";
		$this->layout->view("configurador_umedidas", $data);
	}
}

