<?php
ini_set("display_errors", '1');
error_reporting( E_ALL );
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("Reporte_dispatch_model", "reporte_dispatch");
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model('producto/Producto_model', 'Producto');
    }

    function clientes_get() {
        $maecte = $this->Cliente->get_cliente();
        echo format_response($maecte);
    }

    function productos_tipo_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_tipo($codigo, urldecode($descripcion), 'null');
        echo format_response($productos);
    }

    function dispatch_filtros_get($dia, $cliente, $producto,$filtro_orden) {
           $datos_dispatch = $this->reporte_dispatch->get_datos_dispatch($dia, $cliente, $producto,$filtro_orden);
           echo format_response($datos_dispatch);
    }

    function genera_excel_despacho_post(){

        $filter = $this->post();        
        $datos_dispatch = $this->reporte_dispatch->get_datos_dispatch($filter['fecha'], $filter['cliente'], $filter['producto'],$filter['filtro_orden']);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Nombre Cliente')
        ->setCellValue('B1', 'Codigo Cliente')
        ->setCellValue('C1', 'Nom. Prod')
        ->setCellValue('D1', 'Cod. Prod')
        ->setCellValue('E1', 'Cant. Producida')
        ->setCellValue('F1', 'Cant. Dispatch')
        ->setCellValue('G1', 'Cant. Entregada')
        ->setCellValue('H1', 'Fecha Despacho')
        ->setCellValue('I1', 'Usuario');     
            if (count($datos_dispatch) > 0) {
                    $j = 2;
                    foreach ($datos_dispatch as $valor) {
                            if($valor['fecha_dispatch'] == '0000-00-00 00:00:00'){
                                $valor['fecha_dispatch'] = 'PENDIENTE';
                            }         
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$j, $valor['nomcte01'])
                            ->setCellValue('B'.$j, $valor['id_cliente'])
                            ->setCellValue('C'.$j, $valor['desprod01'])
                            ->setCellValue('D'.$j, $valor['codprod01'])
                            ->setCellValue('E'.$j, $valor['cantidad'])
                            ->setCellValue('F'.$j, $valor['cantidad_dispatch'])
                            ->setCellValue('G'.$j, $valor['cantidad_entregada'])
                            ->setCellValue('H'.$j, $valor['fecha_dispatch'])
                            ->setCellValue('I'.$j, $valor['usuario_dispatch']);

                            $j++;
                    }
            }       

            $objPHPExcel->setActiveSheetIndex(0);



            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            //ob_end_clean();

            header('Content-Type: Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="ReporteDispatch.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');

            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
            header ('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            $objWriter->save('php://output');        

    }
 
    
}
?>
