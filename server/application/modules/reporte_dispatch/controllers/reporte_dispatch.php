<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class reporte_dispatch extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Reporte_dispatch";
		$data["angular_app"] = "Reporte_dispatch";
		$this->layout->view("reporte_dispatch", $data);
	}
}

