<script src="<?= base_url()?>../client/src/app/reporte_dispatch/module.js"></script>
<script src="<?= base_url()?>../client/src/app/reporte_dispatch/reporte_dispatch_model.js"></script>
<script src="<?= base_url()?>../client/src/app/reporte_dispatch/controllers/reporte_dispatch.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>

<script>
    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }
    
</script>

