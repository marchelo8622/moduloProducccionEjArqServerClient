<?php
include_once(APPPATH ."models/base_model.php");
Class Reporte_dispatch_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    
    function get_datos_dispatch($fecha, $cliente, $producto,$filtro_orden){

        $dia = date('w', strtotime($fecha));
        $dia = ($dia != "0") ? $dia : '7' ;

        $condicion = '';
        if ($cliente == "undefined" && $producto != "undefined") {
            $condicion = "and id_producto = '".$producto."'";
        }

        if ($cliente != "undefined" && $producto == "undefined") {
             $condicion = "and id_cliente = '".$producto."'";
        }

        if ($cliente != "undefined" && $producto != "undefined") {
             $condicion = "and id_cliente = '".$producto."' and id_cliente = '".$cliente."'";
        }

        $order_by = "";
        if($filtro_orden != "undefined"){

            $order_by = " order by ".$filtro_orden." ASC";
        }


        $sql = $this->db->query("select planificacion_produccion.*,maepro.codprod01,maepro.desprod01,maecte.nomcte01 
                                 from planificacion_produccion
                                 inner join maepro on id_producto = codprod01
                                 inner join ".$this->matriz.".maecte on codcte01  = id_cliente
                                 where '".$fecha."' >= desde 
                                 and '".$fecha."' <= hasta 
                                 and dia = '".$dia."' ".$condicion."".$order_by."");

        //echo $this->db->last_query();

        return $sql->result_array();
    }
}
?>
