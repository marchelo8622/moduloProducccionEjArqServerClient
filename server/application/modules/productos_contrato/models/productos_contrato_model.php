<?php
include_once(APPPATH ."models/base_model.php");
Class Productos_contrato_model extends Base_model
{
    private $matriz;

    function __construct()
    {
        //ini_set("display_errors", 1);
        error_reporting(0);
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    function get_productos_contrato()
    {
        if ($idProducto != "undefined") {
            $this->db->limit(20);
        }
        $this->db->select('idProducto as codigo');
        $this->db->from("productosContrato");
        $result = $this->db->get();
        return $result->result();
    }
    function get_productos_contrato_by_id($codigo){
        $this->db->from('maepro');
        $this->db->join('configurar_productos', 'maepro.codprod01 = configurar_productos.codigo_producto');
        //$this->db->group_by('configurar_productos.codigo_producto');
        $this->db->where('configurar_productos.id_Cliente', $codigo);
        $result = $this->db->get();
        return $result->result();
    }
    function get_productos_contrato_opcion($codigo){
        $this->db->select('*');
        $this->db->from('productosContrato');
        $this->db->group_by('idProducto');
        $this->db->where('idCliente', $codigo);
        $result = $this->db->get();
        return $result->result();
    }
    function codigosTipoAvion($alias){
        $sql = $this->db->query('SELECT codtab
            FROM '.$this->matriz.'.maetab
            WHERE numtab =46
            AND ad3tab ="'.$alias.'"');
        
        $codigos = $sql->result_array();
            $data = '';
        foreach ($codigos as $codigoTipoAvion) {


            $data .= $codigoTipoAvion['codtab'].',';
        }
        $dataFinal = substr($data, 0, strlen($data) - 1);
        return $dataFinal;

    }

    function get_productos_by_categoria($categoria, $alias){
        $codigosTipoAvion = $this->codigosTipoAvion($alias);
        $sql = $this->db->query('SELECT `codprod01` AS codigo, `desprod01` AS descripcion, `cantact01` AS stock
            FROM maepro
            WHERE orden01 IN ('.$codigosTipoAvion.') limit 20');
        //echo $this->db->last_query();
        return $sql->result();
    }

    function get_filtro_productos_by_categoria($descripcion, $codigo, $alias){
        $codigosTipoAvion = $this->codigosTipoAvion($alias);
        if ($codigo != "null") {
            $this->db->like('codprod01', $codigo);
        }

        if ($descripcion != "null") {
            $this->db->like('desprod01', $descripcion);
        }

        if ($codigosTipoAvion != "") {
            $this->db->where_in('orden01', $codigosTipoAvion);
        }

        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock');
        $this->db->from('maepro');
        $this->db->limit(20);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function delete($data_delete,$idContrato, $codCliente){
        $this->db->where('idContrato', $idContrato);
        $this->db->where('idCliente', $codCliente);
        $this->db->delete("productosContrato");        
    }

    function post_guardar($productos) {
        foreach ($productos as $producto) {
            $this->db->insert("productosContrato", $producto);
        }
    }

}
?>
