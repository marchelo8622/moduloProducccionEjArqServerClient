<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Productos_contrato_model', 'Productos_contrato');
    }
    
    function productos_contrato_get()
    {
        $productos_contrato = $this->Productos_contrato->get_productos_contrato();
        echo format_response($productos_contrato);
    }
    
}
