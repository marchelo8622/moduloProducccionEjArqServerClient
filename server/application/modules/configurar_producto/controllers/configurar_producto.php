<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class configurar_producto extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Configurar_producto";
		$data["angular_app"] = "Configurar_producto";
		$this->layout->view("configurar_producto", $data);
	}
}

