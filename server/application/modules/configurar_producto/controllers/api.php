<?php

defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";

class api extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Configurar_producto_model", "Configurar_producto");
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model('avion/Avion_model', 'Avion');
        $this->load->model('producto/Producto_model', 'Producto');
    }

    function configurar_producto_get($id_cliente,$tipo_avion,$numero_vuelo) {
        $productos = $this->Configurar_producto->get_configurar_producto($id_cliente, $tipo_avion,$numero_vuelo);
        echo format_response($productos);
    }

    function configurar_producto_put() {
        // create a new configurar_producto and respond with a status/errors
        // return data = echo format_response($data);
    }

    function configurar_producto_post() {
        // update an existing configurar_producto and respond with a status/errors
        // return data = echo format_response($data);
    }

    function cliente_post() {
        $filter = $this->post();
        //var_dump($filter);
        // update an existing configurar_producto and respond with a status/errors
        // return data = echo format_response($data);
    }

    function configurar_producto_delete() {
        // delete a configurar_producto and respond with a status/errors
        // return data = echo format_response($data);
    }

    function clientes_get() {
        $maecte = $this->Cliente->get_cliente();
        echo format_response($maecte);
    }

    function avion_get() {
        $avion = $this->Avion->get_avion();
        echo format_response($avion);
    }

    function productos_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos($codigo, $descripcion);
        echo format_response($productos);
    }

    function guardar_configuracion_post() {
        $filter = $this->post();
        $productos = $this->Configurar_producto->post_guardar($filter);
        echo format_response($productos);
    }
    
    function productos_tipo_get($codigo, $descripcion)
    {
        $productos = $this->Producto->get_productos_tipo($codigo, $descripcion, '2');
        echo format_response($productos);
    }



}

?>
