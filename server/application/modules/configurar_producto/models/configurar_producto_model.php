<?php

include_once(APPPATH . "models/base_model.php");

Class Configurar_producto_model extends Base_model {

    function __construct() {
        parent::__construct();
    }

    function get_configurar_producto($id_cliente, $tipo_avion, $numero_vuelo) {
        $this->db->from("configurar_productos");
        $this->db->where("id_cliente", $id_cliente);
        $this->db->where("id_tipo_avion", $tipo_avion);
        $this->db->where("numero_vuelo", $numero_vuelo);
        $this->db->where("fecha_creacion", "(SELECT 
                        fecha_creacion
                    FROM 
                        configurar_productos 
                        WHERE id_cliente = '$id_cliente' 
                        AND id_tipo_avion = '$tipo_avion' 
                        AND numero_vuelo = '$numero_vuelo'
                    ORDER BY 
                        fecha_creacion DESC LIMIT 1)", false, false);
        $result = $this->db->get();
        return $result->result();
    }

    function post_guardar($datos) {
        $fecha = date("Y-m-d H:i:s");

        foreach ($datos['data'] as $key => $producto) {
            $producto['fecha_creacion'] = $fecha;
            $this->db->insert("configurar_productos", $producto);
        }
    }

    function get_configuracion($numVuelo, $tipoAvion, $idProducto){
        $this->db->select('*');
        $this->db->from('configurar_productos');
        $this->db->where('configurar_productos.id_tipo_avion', $tipoAvion);
        $this->db->where('configurar_productos.numero_vuelo', $numVuelo);
        $this->db->where('configurar_productos.codigo', $idProducto);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();

        }


    function get_num_de_vuelo($tipoAvion,$id_cliente){

        $this->db->select('numero_vuelo');
        $this->db->from('configurar_productos');
        $this->db->where('configurar_productos.id_tipo_avion', $tipoAvion);        
        $this->db->where('configurar_productos.id_cliente', $id_cliente);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();


    }

     function get_producto_configurado($producto,$cliente,$avion,$vuelo){

        $this->db->select('*');
        $this->db->from('configurar_productos');
        $this->db->where('configurar_productos.id_cliente', $cliente);
        $this->db->where('configurar_productos.codigo', $producto);
        $this->db->where('configurar_productos.id_tipo_avion', $avion);
        $this->db->where('configurar_productos.numero_vuelo ', $vuelo);        
        $result = $this->db->get();
       // echo $this->db->last_query();
        return $result->result_array();

    }



    }

    

?>
