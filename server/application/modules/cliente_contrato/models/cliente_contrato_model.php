<?php
include_once(APPPATH ."models/base_model.php");
Class Cliente_contrato_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_cliente_contrato()
    {
        if ($idCliente != "undefined" && $numVuelo != "undefined") {
            $this->db->limit(20);
        }

        if ($idCliente != "undefined") {
            $this->db->like('id_Cliente', $idCliente);
        }

        if ($numVuelo != "undefined") {
            $this->db->like('numVuelo', $numVuelo);
        }

        $this->db->select('*');
        $this->db->from("clienteContrato");
        $result = $this->db->get();
        return $result->result();
    }

    function update($data,$idContrato, $codCliente){
        $this->db->where('idContrato', $idContrato);
        $this->db->where('idCliente', $codCliente);       
        $this->db->update('clienteContrato', $data);

    }
    
    function post_guardar($datos) {
            $this->db->insert("clienteContrato", $datos);
    }

    function delete($data_delete,$idContrato, $codCliente){
        $this->db->where('idContrato', $idContrato);
        $this->db->where('idCliente', $codCliente);
        $this->db->delete("clienteContrato");
        //echo $hits->db->last_query();
        
    }
}
?>
