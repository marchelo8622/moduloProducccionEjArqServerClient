<?php
include_once(APPPATH ."models/base_model.php");
Class Avion_model extends Base_model
{
    private $matriz;

    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    
    function get_avion()
    {
        $this->db->where('numtab', '1001');
        $this->db->where('codtab !=','');
        $this->db->from($this->matriz.".maetab");
        $result = $this->db->get();
        return $result->result();
    }
       function get_avion_by_cod($codigo)
    {
        $this->db->where('codtab', $codigo);
        $this->db->where('numtab', '1001');
        $this->db->where('codtab !=','');
        $this->db->from($this->matriz.".maetab");
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result_array();
    }
}
?>
