<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Avion_model', 'Avion');
    }
    
    function avion_get()
    {
        $avion = $this->Avion->get_avion();
        echo format_response($avion);
    }
    
}
