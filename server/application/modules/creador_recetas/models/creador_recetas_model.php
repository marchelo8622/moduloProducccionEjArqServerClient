<?php
include_once(APPPATH ."models/base_model.php");
Class Creador_recetas_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
    
    function get_receta_by_producto($codigo)
    {
        $this->db->select('codprod20t as codigo_producto, codprod20 as codigo_ingrediente, desprod20 as descripcion_ingrediente, 
                            unidmed20 as unidad_medida, cantidad20 as cantidad_ingrediente, costo20 as costo_producto,
                            fecha20 as fecha_ingreso, tipo20 as tipo_ingrediente');
        $this->db->where("codprod20t", $codigo);
        $this->db->from("lismae");
        $result = $this->db->get();
        return $result->result();
    }

    function get_receta_produccion_by_producto($codigo, $numero_produccion)
    {
        $this->db->select('codprod20t as codigo_producto, codprod20 as codigo_ingrediente, desprod20 as descripcion_ingrediente, 
                            unidmed20 as unidad_medida, cantidad20 as cantidad_ingrediente, costo20 as costo_producto,
                            fecha20 as fecha_ingreso, tipo20 as tipo_ingrediente');
        $this->db->where("codprod20t", $codigo);
        $this->db->where("numord20", $numero_produccion);
        $this->db->from("lismaehis");
        $result = $this->db->get();
        return $result->result();
    }

    function get_info_producto_receta($codigo)
    {
        $this->db->select('sirve_a, tipo_cocina as codigo_cocina, nomtab as nombre_cocina');
        $this->db->from('producto_receta');
        $this->db->join($this->matriz.'.maetab', 'codtab = tipo_cocina');
        $this->db->where('codigo_producto', $codigo);
        $this->db->where('numtab', 'TCR');
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function get_productos_para_receta($codigo, $descripcion, $tipo) {

        if ($codigo != "null") {
            $this->db->like('codprod01', $codigo);
        }

        if ($descripcion != "null") {
            $this->db->like('desprod01', $descripcion);
        }

        if ($tipo != "null") {
            $this->db->where('ad3tab', $tipo);
            $this->db->join('maetab', 'orden01 = codtab');
            $this->db->where('numtab', '46');
        }

        $this->db->select('codprod01 as codigo, desprod01 as descripcion, cantact01 as stock');
        $this->db->from('maepro');
        $this->db->where('(orden01 = 1 or orden01 = 2)');
        $this->db->limit(20);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }

    function guardar_receta($datos)
    {
        $result = $this->db->insert("lismae", $datos);
        return $result;
    }

    function actualizar_receta($datos)
    {
        $this->db->where('codprod20t', $datos['codprod20t']);
        $this->db->where('codprod20', $datos['codprod20']);
        $result = $this->db->update('lismae', $datos);
        return $result;
    }

    function eliminar_receta($parametro)
    {
        $this->db->where('codprod20t',$parametro);
        $result = $this->db->delete('lismae');
        return $result;
    }

    function eliminar_ingrediente($parametro)
    {
        $this->db->where('codprod20t',$parametro['codigo_producto']);
        $this->db->where('codprod20',$parametro['codigo_ingrediente']);
        $result = $this->db->delete('lismae');
        return $result;
    }

    function verificar_existencia_receta($parametro)
    {
        $this->db->select('codigo_producto');
        $this->db->from('producto_receta');
        $this->db->where('codigo_producto', $parametro);
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function verificar_ingrediente_receta($receta)
    {
        $this->db->select('codprod20t');
        $this->db->from('lismae');
        $this->db->where('codprod20t', $receta['codprod20t']);
        $this->db->where('codprod20', $receta['codprod20']);
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function insertar_producto_receta($datos)
    {
        $result = $this->db->insert("producto_receta", $datos);
        return $result;
    }

    function actualizar_producto_receta($datos) {
        $data = array(
            'sirve_a' => $datos['sirve_a'],
            'tipo_cocina' => $datos['tipo_cocina']
        );
        $this->db->where('codigo_producto', $datos['codigo_producto']);
        $result = $this->db->update('producto_receta', $data);
        return $result;
    }

    function get_tipo_cocinas()
    {
        $this->db->select('codtab as codigo_cocina, nomtab as nombre_cocina');
        $this->db->where("numtab", "TCR");
        $this->db->where("codtab !=", "");
        $this->db->from("maetab");
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        return $resultado;

    }

    function get_tipo_cocina_by_codigo($codigo)
    {
        $this->db->select('nomtab as nombre_cocina');
        $this->db->where("numtab", "TCR");
        $this->db->where("codtab", $codigo);
        $this->db->from("maetab");
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        return $resultado;
    }
}
?>
