<script src="<?= base_url()?>../client/src/app/creador_recetas/module.js"></script>
<script src="<?= base_url()?>../client/src/app/creador_recetas/creador_recetas_model.js"></script>
<script src="<?= base_url()?>../client/src/app/creador_recetas/controllers/creador_recetas.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>

<script>
    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }
    
</script>


