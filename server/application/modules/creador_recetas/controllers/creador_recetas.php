<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class creador_recetas extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Creador_recetas";
		$data["angular_app"] = "Creador_recetas";
		$this->layout->view("creador_recetas", $data);
	}
}

