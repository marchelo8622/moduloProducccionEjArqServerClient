<?php
error_reporting(0);
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("Creador_recetas_model", "creador_recetas");
        $this->load->model('producto/Producto_model', 'Producto');
        $this->load->model("configurador_umedidas/Configurador_umedidas_model", "configurador_umedidas");
        $this->load->model('plan_diario/plan_diario_model', 'plan_diario');
    }

    function bodega_costos_get()
    {
        echo format_response($this->Producto->get_bodega_costos());
    }

    function productos_con_recetas_get()
    {
        $productos = $this->Producto->get_productos_con_receta();
        echo format_response($productos);
    }
 
    function receta_by_producto_get($codigo)
    {
        $receta = $this->creador_recetas->get_receta_by_producto($codigo);
        $this->built_receta($receta);
    }

    function receta_produccion_by_producto_get($codigo, $numero_produccion)
    {
        $receta_produccion = $this->creador_recetas->get_receta_produccion_by_producto($codigo, $numero_produccion);
        if (count($receta_produccion) <= 0)
            $receta_produccion = $this->creador_recetas->get_receta_by_producto($codigo);
        
        $this->built_receta($receta_produccion);
    }

    function built_receta($receta)
    {
        $new_receta = array();
        foreach ($receta as $key => $value) {
            $datos = new stdClass();
            $umedida = $this->unidades_medida_by_codigo_get($value->unidad_medida);
            $datos->codigo_producto = $value->codigo_producto;
            $datos->codigo_ingrediente = $value->codigo_ingrediente;
            $datos->descripcion_ingrediente = $value->descripcion_ingrediente;
            $datos->unidad_medida = $value->unidad_medida;
            $cantidad_ingrediente = number_format($value->cantidad_ingrediente * $umedida[0]->equivalencia, 6, '.', '');
            $datos->cantidad_ingrediente = $cantidad_ingrediente;
            $info_receta = $this->info_producto_receta($value->codigo_producto);
            $datos->cantidad_total = $this->calcular_cantidad_total($cantidad_ingrediente, $umedida[0]->equivalencia, $info_receta[0]->sirve_a);
            $datos->cantidad_unitaria = number_format($value->cantidad_ingrediente, 6, '.', '');
            $costo_producto_compuesto = $this->calcular_costo_producto_receta_get($value->codigo_ingrediente);
            $datos->costo_producto = $costo_producto_compuesto;
            $costo_ingrediente = ($datos->cantidad_total / $info_receta[0]->sirve_a ) * $costo_producto_compuesto;
            $datos->costo_ingrediente = number_format($costo_ingrediente, 6, '.', '');
            $datos->costo_total = number_format($costo_ingrediente * $info_receta[0]->sirve_a, 6, '.', '');
            $datos->fecha_ingreso = $value->fecha_ingreso;
            $datos->tipo_ingrediente = $value->tipo_ingrediente;
            $datos->equivalencia = (count($umedida) > 0) ? $umedida[0]->equivalencia : "0";
            $new_receta[] = $datos;
        }
        echo format_response($new_receta);
    }

    function unidades_medida_by_codigo_get($codigo)
    {
        $umedida = array();
        $umedida = $this->configurador_umedidas->get_unidades_medida_by_codigo($codigo);
        if(empty($umedida))
        {
            $info = new stdClass();
            $info->equivalencia = 0;
            $umedida[] = $info;
        }

        return $umedida;
    }

    function info_producto_receta_get($codigo)
    {
        $info_receta = array();
        $info_receta = $this->creador_recetas->get_info_producto_receta($codigo);
        if (empty($info_receta))
        {
            $info = new stdClass();
            $info->sirve_a = 1;
            $info->codigo_cocina = "";
            $info->nombre_cocina = "";
            $info_receta[] = $info;
        }
        echo format_response($info_receta);
    }

    function info_producto_receta($codigo)
    {
        $info_receta = array();
        $info_receta = $this->creador_recetas->get_info_producto_receta($codigo);
        if (empty($info_receta))
        {
            $info = new stdClass();
            $info->sirve_a = 1;
            $info->codigo_cocina = "";
            $info->nombre_cocina = "";
            $info_receta[] = $info;
        }
        return $info_receta;
    }

    private function calcular_cantidad_total($cantidad_ingrediente, $equivalencia, $cantidad_a_servir)
    { 
        $cantidad_total = 0.00;
        if ($equivalencia > 0)
        {
            $cant_aux = (float)$cantidad_ingrediente / (float)$equivalencia;
            $cantidad_total = number_format((float)$cantidad_a_servir * $cant_aux, 6, '.', '');
        }
        return $cantidad_total;
    }

    function unidad_medida_get($codigo_umedida)
    {
        $umedida = $this->configurador_umedidas->get_unidades_medida_by_codigo($codigo_umedida);
        echo format_response($umedida);
    }

    function productos_para_receta_get($codigo, $descripcion) {
        $productos = $this->creador_recetas->get_productos_para_receta($codigo, $descripcion, 'null');
        $productos_para_receta = array();
        foreach ($productos as $key => $value) {
            $producto = new stdClass();
            $producto->codigo = $value->codigo;
            $producto->descripcion = $value->descripcion;
            $producto->stock = $value->stock;
            $costo_producto_compuesto = $this->calcular_costo_producto_receta_get($value->codigo);
            $producto->costo = $costo_producto_compuesto;
            $productos_para_receta[] = $producto;
        }
        echo format_response($productos_para_receta);
    }

    function unidades_medidas_get()
    {
        $umedidas = $this->configurador_umedidas->get_unidades_medida();
        echo format_response($umedidas);
    }
 
    function creador_recetas_put()
    {
        // create a new creador_recetas and respond with a status/errors
        // return data = echo format_response($data);
    }
 
    function creador_recetas_post()
    {
        $datos = $this->post();
        foreach ($datos['data']['data2'] as $key => $value) {
          $this->creador_recetas->eliminar_ingrediente($value);
        }
        $receta = array();
        foreach ($datos['data']['data1'] as $key => $value) {
            $receta['codprod20t'] = $value['codigo_producto'];
            $receta['codprod20'] = $value['codigo_ingrediente'];
            $receta['codprod20a'] = '';
            $receta['desprod20'] = $value['descripcion_ingrediente'];
            $receta['cve120'] = '';
            $receta['cve220'] = '';
            $receta['unidmed20'] = $value['unidad_medida'];
            $receta['cantidad20'] = $value['cantidad_unitaria'];
            $receta['cantfalta20'] = '';
            $receta['costo20'] = $value['costo_producto'];
            $receta['fecha20'] = date('Y-m-d H:i:s');
            $receta['tipo20'] = 'C';
            $receta['bodega20'] = '';

            $exist_receta = $this->verificar_ingrediente_receta($receta);
            if ($exist_receta)
                $this->creador_recetas->actualizar_receta($receta);
            else
                $this->creador_recetas->guardar_receta($receta);
          
        }
            echo $this->response('ok',200);
    }

    function receta_producto_post()
    {
        $datos = $this->post();
        $exist_receta = $this->verificar_existencia_receta($datos['data']['codigo_producto']);
        if ($exist_receta)
            $receta = $this->creador_recetas->actualizar_producto_receta($datos['data']);
        else
            $receta = $this->creador_recetas->insertar_producto_receta($datos['data']);
        echo $this->response('ok',200);
    }

    private function verificar_existencia_receta($codigo_producto) 
    {
        $receta = $this->creador_recetas->verificar_existencia_receta($codigo_producto);
        return $receta;
    }

    private function verificar_ingrediente_receta($codigo_producto) 
    {
        $receta = $this->creador_recetas->verificar_ingrediente_receta($codigo_producto);
        return $receta;
    }

    function eliminar_receta_delete($parametro)
    {
        $receta = $this->creador_recetas->eliminar_receta($parametro);
        echo format_response($receta);
    }

    function genera_pdf_receta_post(){
        /*session_start();
        $uid = $_SESSION['user'];*/
        setlocale(LC_TIME, 'es_EC.UTF-8');//para mostrar la fecha hora local
        $filter = $this->post();
        $codigo_producto = $filter['codigo'];
        $producto = $this->Producto->get_producto_by_codigo($codigo_producto);
        $info_receta = $this->info_producto_receta($codigo_producto);
        $cocina = $this->tipo_cocina_by_codigo_get($info_receta[0]->codigo_cocina);

        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Marcelo Montalvo');
        $pdf->SetTitle('Receta');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
        $pdf->setFontSubsetting(true);
 
        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
 
        $html = '';
        $html .= "<style type=text/css>";

        $html .= "table  , td {border: 1px solid black;    border-collapse: collapse;}";
        $html .= "h1{color: red; font-weight: bold;  text-align:center;}";
        $html .= "</style>";
        $html .= "<h1>".strftime("%A, %d de %B de %Y %R")."</h1>";
        $html .= "<table cellspacing='0' cellpadding='4' border='1' width='100%'>";
        $html .= '<tr>
                    <td colspan="2" width="40%" align="center">'. $producto['descripcion'] .'</td>
                    <td width="20%" align="center">'. $cocina[0]['nombre_cocina'] .'</td>
                    <td width="20%" align="right">PAX A ELABORAR</td>
                    <td width="20%" align="center"> '. $info_receta[0]->sirve_a .'</td>
                </tr>';
        $html .= '<tr>
                    <td colspan="5" height="12">&nbsp;</td>
                  </tr>';
        $html .= '<tr>
                    <td align="center" width="20%"><b>INGREDIENTES</b></td>
                    <td align="center" width="20%"><b>CANT. UNID</b></td>
                    <td align="center" width="20%"><b>CANT. TOT</b></td>
                    <td align="center" width="20%"><b>COST. UNIT</b></td>
                    <td align="center" width="20%"><b>COST. TOTAL</b></td>
                  </tr>';

        $receta = $this->creador_recetas->get_receta_by_producto($codigo_producto);
        $costoTotalReceta = 0;
        $costoReceta = 0;
        foreach ($receta  as $value) 
        {            
            $umedida = $this->unidades_medida_by_codigo_get($value->unidad_medida);
            $cantidad_ingrediente = number_format($value->cantidad_ingrediente * $umedida[0]->equivalencia, 6, '.', '');
            $cantidad_total = $this->calcular_cantidad_total($cantidad_ingrediente, $umedida[0]->equivalencia, $info_receta[0]->sirve_a);
            $costo_ingrediente = number_format(($cantidad_total / $info_receta[0]->sirve_a ) * $value->costo_producto, 6, '.', '');
            $costo_total = number_format($costo_ingrediente * $info_receta[0]->sirve_a, 6, '.', '');
            $html .='<tr>
                    <td><h6>'.$value->descripcion_ingrediente .'</h6></td>
                    <td align="right">'.$cantidad_ingrediente.'</td>
                    <td align="right">'.$cantidad_total.'</td>
                    <td align="right">'.$costo_ingrediente.'</td>
                    <td align="right">'.$costo_total.'</td>
                  </tr>';
            $costoReceta += $costo_ingrediente;
            $costoTotalReceta += $costo_total;
        }
         $html .=  '<tr>
                        <td colspan="2">&nbsp;</td>
                        <td align="center"><b> TOTALES:</b></td>
                        <td align="right">'.number_format($costoReceta,6,".",",").'</td>
                        <td align="right">'.number_format($costoTotalReceta,6,".",",").'</td>
                      </tr>'; 
        $html .= '</table>';
        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 1, $ln = 1, $fill = 0, $reseth = true, $align = '');
        $nombre_archivo = utf8_decode("receta.pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    function genera_excel_receta_post()
     {
        setlocale(LC_TIME, 'es_EC.UTF-8');//para mostrar la fecha hora local
        $filter = $this->post();
        $codigo_producto = $filter['codigo'];
        $producto = $this->Producto->get_producto_by_codigo($codigo_producto);
        $info_receta = $this->info_producto_receta($codigo_producto);
        $cocina = $this->tipo_cocina_by_codigo_get($info_receta[0]->codigo_cocina);

        $this->load->library('Excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()
        ->setCreator("Cattivo")
        ->setLastModifiedBy("Cattivo")
        ->setTitle("Documento Excel de Prueba")
        ->setSubject("Documento Excel de Prueba")
        ->setDescription("Clientes.")
        ->setKeywords("Excel Office 2007 openxml php")
        ->setCategory("Reportes");

        // Agregar Informacion
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
        $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', strftime("%A, %d de %B de %Y %R"));

        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A2:B2')
        ->setCellValue('A2', 'Producto: '.$producto['descripcion'])
        ->setCellValue('A3', 'Código: '.$codigo_producto)
        ->setCellValue('C2', $cocina[0]['nombre_cocina'])
        ->setCellValue('D2', 'PAX A ELABORAR')
        ->setCellValue('E2', $info_receta[0]->sirve_a);

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A5', 'INGREDIENTES')
        ->setCellValue('B5', 'CANT. UNID')
        ->setCellValue('C5', 'CANT. TOT')
        ->setCellValue('D5', 'COST. UNIT')
        ->setCellValue('E5', 'COST. TOT');

        $receta = $this->creador_recetas->get_receta_by_producto($codigo_producto);

        $i=5;
        foreach($receta as $value) {
            $umedida = $this->unidades_medida_by_codigo_get($value->unidad_medida);
            $cantidad_ingrediente = number_format($value->cantidad_ingrediente * $umedida[0]->equivalencia, 6, '.', '');
            $cantidad_total = $this->calcular_cantidad_total($cantidad_ingrediente, $umedida[0]->equivalencia, $info_receta[0]->sirve_a);
            $costo_ingrediente = number_format(($cantidad_total / $info_receta[0]->sirve_a ) * $value->costo_producto, 6, '.', '');
            $costo_total = number_format($costo_ingrediente * $info_receta[0]->sirve_a, 6, '.', '');

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, $value->descripcion_ingrediente)
        ->setCellValue('B'.$i, $cantidad_ingrediente)
        ->setCellValue('C'.$i, $cantidad_total)
        ->setCellValue('D'.$i, $costo_ingrediente)
        ->setCellValue('E'.$i, (float) $costo_total);
        $i++;
        }
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('D'.$i, 'TOTAL: ')
        ->setCellValue('E'.$i, '=SUM(E5:E'.($i - 1).')');
        // Renombrar Hoja
        $objPHPExcel->getActiveSheet()->setTitle('Receta');

        // Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
        $objPHPExcel->setActiveSheetIndex(0);

        // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="receta.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

     }

    function tipo_cocinas_get()
    {
      $cocinas = $this->creador_recetas->get_tipo_cocinas();
      echo format_response($cocinas);
    }

    function tipo_cocina_by_codigo_get($codigo)
    {
        $cocina = array();
      $cocina = $this->creador_recetas->get_tipo_cocina_by_codigo($codigo);
      if (empty($cocina)){
        $cocina[]['nombre_cocina'] = "";
      }
      return $cocina;
    }

    function calcular_costo_producto_receta_get($codigo_producto)
    {
        $costo_receta = $this->Producto->get_costo_producto_by_codigo($codigo_producto);
        return $costo_receta;
    }

}
?>
