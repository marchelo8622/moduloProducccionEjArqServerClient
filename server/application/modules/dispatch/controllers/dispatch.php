<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class dispatch extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Dispatch";
		$data["angular_app"] = "Dispatch";
		$this->layout->view("dispatch", $data);
	}
}

