<?php
//ini_set('display_errors', 1);
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {
    private $validacion = array();
    private $secuencial_guia;
    private $secuencial_guia_val;
    private $numero_guia;
    private $uid = '';
    private $IDB = '';
    private $respuesta = array();
    private $cantidad_mercaderia = 0;
    private $codigo_cliente_interno;
    private $clienteGenerico = array();
    private $bodega_materia_prima;

    function __construct()
    {
        parent::__construct();
        $this->load->model("Dispatch_model", "dispatch");
        $this->load->model("contrato/Contrato_model", "contrato");
        $this->load->model("pre_plan/Pre_plan_model", "Pre_plan");
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model('producto/Producto_model', 'Producto');
        $this->load->model('plan_diario/plan_diario_model', 'plan_diario');
        $this->serie_sri = $this->get_serie_sri();
        $this->bodega_materia_prima = $this->get_cod_bodega_materia_prima();
        session_start();
        $this->uid = $_SESSION['UID'];
        $this->IDB = $this->input->get('IDB');
    }

    function get_cod_bodega_materia_prima()
    {
        $bodega_materia_prima = $this->plan_diario->get_bodega_materia_prima();
        return $bodega_materia_prima['codigo_bodega'];
    }

    function get_serie_sri()
    {
        $serie_sri = $this->plan_diario->get_serie_sri();
        return $serie_sri;
    }

    function response_false($error = 'No se Proceso')
    {
        $this->respuesta['respuesta'] = false;
        $this->respuesta['error'] = $error;
    }

    function pre_plan_por_dia_get($fecha) {

        $prePlan = $this->Pre_plan->get_pre_plan_por_dia($fecha);
        echo format_response($prePlan);

    }

    function clientes_get($id, $nombre) {
        $maecte = $this->Cliente->get_cliente_by($id, urldecode($nombre));
        echo format_response($maecte);
    }

    function productos_tipo_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_tipo($codigo, urldecode($descripcion), 'null');
        echo format_response($productos);
    }

    function guardar_dispatch_post() {
        $datos = $this->post();
        if(count($datos['data']['clientes']) > 0){
            $this->dispatch->eliminar_clientes($datos['data']['clientes']);
        }

        if(count($datos['data']['productos']) > 0){
            $this->dispatch->eliminar_productos($datos['data']['productos']);
        }

        if(count($datos['data']['opciones']) > 0){
            $this->dispatch->eliminar_opciones($datos['data']['opciones']);
        }

      foreach ($datos['data']['data1'] as  $valor) {
            $es_compuesto = $this->Producto->es_compuesto($valor['id_producto']);
            if (count($es_compuesto) <= 0)
            {
                $es_producto_final = $this->Producto->es_producto_final($valor['id_producto']);
                if (count($es_producto_final) <= 0)
                {

                    $requisicion_sin_produccion = $this->dispatch->consulta_requisicion_sin_produccion($valor['id_producto'], $valor['fecha']);
                    if ($requisicion_sin_produccion == null)
                    {
                        $this->generar_requisicion($valor);
                    }else{
                        $this->actualizar_requisicion($valor, $requisicion_sin_produccion);
                    }
                }
            }
        
            $valida_registro_planificacion = $this->dispatch->consulta_dispatch($valor);          
            $dia = date('w', strtotime($valor['fecha']));
            $dia = ($dia != "0") ? $dia : '7' ;
             
            if($valida_registro_planificacion == 1){

                $this->dispatch->actualizar_distpatch($valor, $dia);

            }else{
                $id_receta = "[{}]";
                $dato_nueva_opcion = $this->dispatch->consulta_receta_ultima_opcion($valor);
                if (count($dato_nueva_opcion) > 0)
                    $id_receta = $dato_nueva_opcion[0]['id_receta'];
                
                $array_insert = array(

                         'estado_despacho' => 0,
                         'desde' => $valor['fecha'],
                         'hasta' => $valor['fecha'],
                         'opcion' => $valor['opcion'],
                         'id_cliente' => $valor['id_cliente'],
                         'estado_preplan' => 1,
                         'dia' => $dia,
                         'cantidad' => $valor['cantidad_dispatch'],
                         'cantidad_dispatch' => $valor['cantidad_dispatch'],
                         'cantidad_entregada' => $valor['cantidad_entregada'],
                         'id_producto' => $valor['id_producto'],
                         'id_receta' => $id_receta
                    );

            $this->dispatch->guardar_distpatch($array_insert);
            }

        }
        echo format_response(true);
    }

    function generar_requisicion($datos)
    {
        $this->codigo_cliente_interno = $this->cliente_interno_get();
        $this->clienteGenerico = $this->Cliente->get_cliente_by_codigo($this->codigo_cliente_interno);
        $datos_producto = $this->Producto->get_producto_by_codigo($datos['id_producto']);
        $requisicion = array(
                            'numdoc' => "PD-".$datos['fecha']."-".$datos['id_producto'],
                            'ocu' => '00',
                            'sec' => '1',
                            'numordenp' => $datos_producto['descripcion'],
                            'codcliente' => $this->clienteGenerico->codigo_cliente,
                            'fecreg' => $datos['fecha'],
                            'codprod' => $datos['id_producto'],
                            'desprod' => $datos_producto['descripcion'],
                            'cantsol' => number_format($datos['cantidad_dispatch'], 2, '.', ''),
                            'cantent' => '0.00',
                            'cant_a_devolver' => '0.00',
                            'bodega' =>  $this->IDB,
                            'UID' => '',
                            'observacion' => 'Requisicion de Producto Simple por Dispatch',
                            'bodegareq' => $this->bodega_materia_prima,
                            'origen' => 'dispatch'
                            );
        $this->plan_diario->generar_detalle_requisicion($requisicion);
    }

    function actualizar_requisicion($datos, $requisicion)
    {
        $cant_solicitada    = $datos['cantidad_dispatch'];
        $cant_pedida        = $requisicion['cantsol'];
        $cant_entregada     = $requisicion['cantent'];
        $cant_a_devolver    = $requisicion['cant_a_devolver'];
        $cant_devolver = 0;

        if ((float)$cant_solicitada > (float)$cant_pedida)
        {
            $cant_diferencia = number_format((float)$cant_solicitada - (float)$cant_pedida, 2, '.', '');
            $cant_devolver = ((float)$cant_a_devolver > 0) ? (float) $cant_a_devolver : 0 ;
            if ($cant_devolver > 0)
                $cant_devolver = number_format((float)$cant_devolver - (float)$cant_diferencia, 2, '.', '');
        }

        if ((float)$cant_solicitada < (float)$cant_pedida)
        {
            if ($cant_entregada > $cant_solicitada)
                $cant_devolver = number_format((float)$cant_entregada - (float)$cant_solicitada, 2, '.', '');
        }

        $data = array(
                'cantsol' => $cant_solicitada,
                'cant_a_devolver' => $cant_devolver
                );
        $numero_requisicion = $requisicion['numdoc'];
        $producto           = $requisicion['codprod'];
        $fecha              = $datos['fecha']. " 00:00:00";
        $this->dispatch->actualizar_producto_requisicion($data, $numero_requisicion, $producto, $fecha);
        return;

    }

    function cliente_interno_get()
    {
        $codigo_cliente_interno = $this->Cliente->cliente_interno();
        return $codigo_cliente_interno;
    }

    function dispatch_post() {
        $datos = $this->post();
        $array_nuevo_insertar = $this->agrupar_productos($datos);
        
        $productos = $this->generar_pedido($array_nuevo_insertar,$this->input->get('SerieSRI'),$this->input->get('NUM_SRI'),$this->input->get('IDB'),$datos['data']);
        echo format_response($productos);
    }

    function agrupar_productos($datos)
    {
        $array_nuevo_datos = array();
        foreach ($datos['data'] as $key => $valor) {
            $array_nuevo_datos[$valor['id_producto']][] = array(
                           'cantidad_dispatch' => $valor['cantidad_dispatch'],
                           'cantidad_entregada' => $valor['cantidad_entregada'],
                           'cod_cliente' => $valor['id_cliente'],
                           'opcion'=> $valor['opcion'],
                           'fecha' => $valor['fecha'],
                           'codigo' => $valor['codigo'],
                           'desde' => $valor['desde'],
                           'hasta' => $valor['hasta'] 
                           );
        }

        $array_nuevo_insertar = array();

        foreach ($array_nuevo_datos as $key => $nuevo_dato) {
            $sum_cant_d = 0;
            $sum_cant_e = 0;
            foreach ($nuevo_dato as $value) {
                $sum_cant_d += $value['cantidad_dispatch'];
                $sum_cant_e += $value['cantidad_entregada'];
            }

            $this->cantidad_mercaderia = $this->cantidad_mercaderia + $sum_cant_d;
            $array_nuevo_insertar[] = array(
                      'codigo_producto' => $key,
                      'cantidad_dispatch' => $sum_cant_d,
                      'cantidad_entregada' => $sum_cant_e,
                      'cod_cliente' => $nuevo_dato[0]['cod_cliente'],
                      'contrato' => $nuevo_dato[0]['codigo']

                );
        }
        return $array_nuevo_insertar;

    }


    function generar_pedido($datos,$SerieSRI,$NUM_SRI,$IDB,$datos_planificacion){
        session_start();
        $uid = $_SESSION['UID'];
        $nombreUsuario = $_SESSION['user'];        
        $datos_pedido = array();
        $ocurrencia = 0;
        $hisuid = $nombreUsuario.' --> REGISTRADO |';
        $secuencial = $this->dispatch->secuencial_pedido();
        $secuencial_pedido = $SerieSRI.str_pad($secuencial, $NUM_SRI, "0", STR_PAD_LEFT);
        
        foreach ($datos as $valor) {
            $dato_cliente = $this->Cliente->get_cliente_by_codigo($valor['cod_cliente']);
            $dato_producto = $this->Producto->get_producto_by_codigo($valor['codigo_producto']);
            $precio_contrato = $this->contrato->get_precio_contrato($valor['contrato'],$valor['cod_cliente'],$valor['codigo_producto']);
            $precio = 0;
            if(count($precio_contrato) > 0){

                    $precio = $precio_contrato[0]['precio'];

            }else{
                    $precio = 0;

            }
            $datos_pedido[] = array(
                                   'nopedido30' => $secuencial_pedido,
                                   'codprod30' => $valor['codigo_producto'] ,
                                   'ocurren30' => str_pad($ocurrencia, 2, "0", STR_PAD_LEFT),
                                   'codcte30' => $valor['cod_cliente'],
                                   'fecpedido30' => date('Y-m-d'),
                                   'nomcte30' => $dato_cliente->nombre_cliente,
                                   'novend30' => $dato_cliente->vendedor,
                                   'localid30' => $dato_cliente->localidad,
                                   'condpag30' => $dato_cliente->condPago,
                                   'descto30' => 0,
                                   'emailcte30' =>$dato_cliente->email,
                                   'iva30' => $this->dispatch->iva_default(),
                                   'flete30' =>  0,
                                   'telcte30' => $dato_cliente->telefono,
                                   'dircte30' => $dato_cliente->direccion,
                                   'cantped30' => $valor['cantidad_entregada'],
                                   'precuni30' => $precio,
                                   'cantafac30' => $valor['cantidad_entregada'],
                                   'piva30' => $this->dispatch->iva_default(),
                                   'uid' => $uid,
                                   'fleteini30' => 0,
                                   'bodega30' => $IDB,
                                   'fecestado30'=> date('Y-m-d'),
                                   'fecdespacho30' => date('Y-m-d'),
                                   'nomprod30' => $dato_producto['descripcion'],
                                   'hisuid' => $hisuid,
                                   'status30' => '01'                                   
                              );
           $ocurrencia++;
        }

        //print_r($datos_pedido);

        if(count($datos_pedido) > 0){
            if($this->dispatch->inserta_pedido($datos_pedido)){
                $this->dispatch->actualiza_secuencial_pedido($secuencial);

                foreach ($datos_planificacion as $dato) {

                    $this->dispatch->actualiza_estado_dispatch($dato,$nombreUsuario);
                   
                }

            }
        } 

    }

    function guia_remision_post()
    {
        setlocale(LC_TIME, 'es_EC.UTF-8');
        $this->IDB = $this->input->get('IDB');
        $datos = $this->post();
        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edison Calderon');
        $pdf->SetTitle('Receta');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setFontSubsetting(true);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();
        //imagen
        $pdf->setJPEGQuality(75);
        $pdf->Image(dirname(__FILE__).'/../../../../../../iconos/GoddartCatering.png', 8, 10, 40, 30, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        $pdf->SetFont('helvetica', 'B', 7, '', true);
        $pdf->text(15, 36, 'GODDART CATERING GROUP QUITO S.A.');
        $pdf->SetFont('helvetica', 'B', 6, '', true);
        $pdf->text(15, 39, 'CONTRIBUYENTE ESPECIAL');
        $pdf->SetFont('helvetica', 'B', 5, '', true);
        $pdf->text(15, 41, 'Resolucion No.5368 del 2 de Junio de 1995');
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->text(15, 43, 'RUC. 1790011860001');

        //encabezado central
        $pdf->SetXY(75, 20);
        $pdf->SetFont('helvetica', 'B', 9, '', true);
        $pdf->MultiCell(60, 4, 'PLANTA DE CATERING:', 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(75, 25);
        $pdf->SetFont('helvetica', '', 7, '', true);
        $pdf->MultiCell(60, 4, 'Edificion de Catering No 1,', 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(75, 28);
        $pdf->MultiCell(60, 4, '(Junto a la Torre de Control Aeropuertaria)', 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(75, 31);
        $pdf->MultiCell(60, 4, 'Nuevo Aeropuerto Internacional de Quito', 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(75, 34);
        $pdf->MultiCell(60, 4, 'Tababela, Quito - Ecuador + P.O. BOX: 17-07-9923', 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(75, 37);
        $pdf->MultiCell(60, 4, 'PBX: (593-2) 3945820 / 3945821 / 3945822 - 9', 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(75, 40);
        $pdf->MultiCell(60, 4, 'E-mail: uio.info@goddartcatering.com', 0, 'C', 0, 0, '', '', true);

        //encabezado derecho
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(145, 20, 45, 25, 3.50, '1111');
        $pdf->SetXY(138, 21);
        $pdf->SetFont('helvetica', 'B', 10, '', true);
        $pdf->MultiCell(60, 4, 'GUIA DE REMISIÓN', 0, 'C', 0, 0, '', '', true);
        //$pdf->SetXY(139, 25);
        $pdf->SetFont('helvetica', '', 10, '', true);
        //$pdf->MultiCell(60, 4, 'SERIE '.$this->input->get('SerieSRI'), 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(148, 28);
        $pdf->MultiCell(40, 4, $datos['guia_remision'], 0, 'C', 0, 0, '', '', true);

        //autorizacion SRI
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(15, 49, 82, 5, 3, '1111');
        $pdf->SetXY(15, 50);
        $pdf->SetFont('helvetica', '', 7, '', true);
        $pdf->MultiCell(100, 4, 'AUTORIZACIÓN SRI No. 1117383614  FECHA AUT. 07/Agosto/2015', 0, 'L', 0, 0, '', '', true);

        //MOTIVO DE TRASLADO
        //MARCO
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(15, 55, 175, 45, 3, '1111');
        //DETALLE
        $pdf->SetFont('helvetica', '', 6, '', true);
        $line = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        $pdf->SetXY(17, 57);
        $pdf->MultiCell(23, 4, 'Fecha de Emisión:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(37, 59, 100, 59, $line);
        $pdf->SetXY(100, 57);
        $pdf->MultiCell(23, 4, 'No. Factura:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(115, 59, 188, 59, $line);

        $pdf->SetXY(17, 62);
        $pdf->MultiCell(35, 4, 'Fecha de Iniciación del Traslado:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(50, 64, 100, 64, $line);
        $pdf->SetXY(100, 62);
        $pdf->MultiCell(35, 4, 'Tipo de Comprobante de Venta:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(133, 64, 188, 64, $line);

        $pdf->SetXY(17, 67);
        $pdf->MultiCell(40, 4, 'Fecha de Terminación del Traslado:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(53, 69, 100, 69, $line);
        $pdf->SetXY(100, 67);
        $pdf->MultiCell(40, 4, 'Fecha de Emisión Comprob./Vent.:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(135, 69, 188, 69, $line);

        $pdf->SetXY(17, 72);
        $pdf->MultiCell(40, 4, 'Comprobante de Venta No.:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(45, 74, 100, 74, $line);
        $pdf->SetXY(100, 72);
        $pdf->MultiCell(35, 4, 'Autorización No.:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(118, 74, 188, 74, $line);

        $pdf->SetFont('helvetica', 'B', 8, '', true);
        $pdf->SetXY(17, 75);
        $pdf->MultiCell(40, 4, 'MOTIVO DE TRASLADO:', 0, 'L', 0, 0, '', '', true);
        $pdf->SetFont('helvetica', 'B', 7, '', true);
        $pdf->RoundedRect(25, 79, 5, 4, 0, '');
        $pdf->SetXY(35, 80);
        $pdf->MultiCell(20, 4, 'VENTA', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(25, 84, 5, 4, 0, '');
        $pdf->SetXY(35, 85);
        $pdf->MultiCell(20, 4, 'COMPRA', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(25, 89, 5, 4, 0, '');
        $pdf->SetXY(35, 90);
        $pdf->MultiCell(30, 4, 'TRANSFORMACIÓN', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(25, 94, 5, 4, 0, '');
        $pdf->SetXY(35, 95);
        $pdf->MultiCell(30, 4, 'CONSIGNACIÓN', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(70, 79, 5, 4, 0, '');
        $pdf->SetXY(80, 79);
        $pdf->MultiCell(50, 4, 'TRASLADO ENTRE ESTABLECIMIENTO DE UNA MISMA EMPRESA', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(70, 89, 5, 4, 0, '');
        $pdf->SetXY(80, 90);
        $pdf->MultiCell(60, 4, 'TRASLADO POR EMISOR ITINERANTE DE COMPROBANTE DE VENTA', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(150, 79, 5, 4, 0, '');
        $pdf->SetXY(160, 80);
        $pdf->MultiCell(30, 4, 'DEVOLUCIÓN', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(150, 84, 5, 4, 0, '');
        $pdf->SetXY(160, 85);
        $pdf->MultiCell(30, 4, 'IMPORTACIÓN', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(150, 89, 5, 4, 0, '');
        $pdf->SetXY(160, 90);
        $pdf->MultiCell(30, 4, 'EXPORTACIÓN', 0, 'L', 0, 0, '', '', true);

        $pdf->RoundedRect(150, 94, 5, 4, 0, '');
        $pdf->SetXY(160, 95);
        $pdf->MultiCell(30, 4, 'OTROS', 0, 'L', 0, 0, '', '', true);

        //DESTINATARIO
        //MARCO
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(15, 101, 175, 32, 3, '1111');
        $pdf->SetXY(17, 103);
        $pdf->MultiCell(30, 4, 'FECHA DE EMISIÓN:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(45, 106, 188, 106, $line);

        $pdf->SetXY(17, 108);
        $pdf->MultiCell(30, 4, 'PUNTO DE PARTIDA:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(45, 111, 188, 111, $line);

        $pdf->SetFont('helvetica', 'B', 8, '', true);
        $pdf->SetXY(17, 113);
        $pdf->MultiCell(30, 4, 'DESTINATARIO:', 0, 'L', 0, 0, '', '', true);

        $pdf->SetFont('helvetica', 'B', 7, '', true);
        $pdf->SetXY(17, 118);
        $pdf->MultiCell(50, 4, 'NOMBRE O RAZÓN SOCIAL:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(55, 120, 188, 120, $line);

        $pdf->SetXY(17, 123);
        $pdf->MultiCell(30, 4, 'R.U.C / C.I.:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(37, 125, 100, 125, $line);
        $pdf->SetXY(100, 123);
        $pdf->MultiCell(20, 4, 'Placa No.:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(115, 125, 188, 125, $line);

        $pdf->SetXY(17, 128);
        $pdf->MultiCell(50, 4, 'IDENTIFICACIÓN REMITENTE:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(55, 130, 188, 130, $line);

        //BIENES TRANSPORTADOS
        //MARCO
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(15, 134, 175, 140, 3, '1111');
        $pdf->RoundedRect(18, 140, 169, 110, 3, '1111');
        $line2 = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        $pdf->Line(38, 140, 38, 250, $line2);
        $pdf->Line(18, 147, 187, 147, $line2);

        $pdf->SetFont('helvetica', 'B', 8, '', true);
        $pdf->SetXY(17, 136);
        $pdf->MultiCell(40, 4, 'BIENES TRANSPORTADOS:', 0, 'L', 0, 0, '', '', true);

        $pdf->SetXY(22, 142);
        $pdf->MultiCell(20, 4, 'CANT.', 0, 'L', 0, 0, '', '', true);

        $pdf->SetXY(80, 142);
        $pdf->MultiCell(70, 4, 'D  E  S  C  R  I  P  C  I  Ó  N', 0, 'L', 0, 0, '', '', true);

        //FIRMAS
        $pdf->Line(18, 262, 70, 262, $line);
        $pdf->Line(75, 262, 127, 262, $line);
        $pdf->Line(132, 262, 185, 262, $line);

        $pdf->SetFont('helvetica', 'B', 8, '', true);
        $pdf->SetXY(28, 262);
        $pdf->MultiCell(40, 4, 'SERVICIO AL CLIENTE', 0, 'L', 0, 0, '', '', true);
        $pdf->SetXY(86, 262);
        $pdf->MultiCell(40, 4, 'TRANSPORTACIÓN', 0, 'L', 0, 0, '', '', true);
        $pdf->SetXY(148, 262);
        $pdf->MultiCell(40, 4, 'DESTINATARIO', 0, 'L', 0, 0, '', '', true);

        $pdf->SetXY(18, 268);
        $pdf->MultiCell(40, 4, 'FECHA:', 0, 'L', 0, 0, '', '', true);
        $pdf->Line(33, 271, 70, 271, $line);

        $productos = $this->agrupar_productos($datos['despacho']);
        $this->cebecera_guia_remision($datos['codido_cliente'], &$pdf);
        $this->detalle_guia_remision($productos, &$pdf);

        $nombre_archivo = utf8_decode("guia_remision.pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    function cebecera_guia_remision($codido_cliente, &$pdf)
    {
        $dato_cliente = $this->Cliente->get_cliente_by_codigo($codido_cliente);
        $fecha = date('Y-m-d');
        $pdf->SetFont('helvetica', '', 8, '', true);
        $pdf->SetXY(40, 56);
        $pdf->MultiCell(23, 4, $fecha, 0, 'L', 0, 0, '', '', true);
        $pdf->SetXY(47, 103);
        $pdf->MultiCell(23, 4, $fecha, 0, 'L', 0, 0, '', '', true);
        $pdf->SetXY(55, 117);
        $pdf->MultiCell(50, 4, $dato_cliente->nombre_cliente, 0, 'L', 0, 0, '', '', true);
        $pdf->SetXY(45, 122);
        $pdf->MultiCell(30, 4, $codido_cliente, 0, 'L', 0, 0, '', '', true);

    }

    function detalle_guia_remision ($data, &$pdf)
    {
      $y = 145;
      foreach ($data as $key => $producto) {
        $dato_producto = $this->Producto->get_producto_by_codigo($producto['codigo_producto']);
        $y = $y + 5;
        $pdf->SetXY(20, $y);
        $pdf->MultiCell(15, 4, number_format($producto['cantidad_dispatch'], 2), 0, 'R', 0, 0, '', '', true);
        $pdf->SetXY(45, $y);
        $pdf->MultiCell(50, 4, $dato_producto['descripcion'], 0, 'L', 0, 0, '', '', true);
        $pdf->ln();
        
      }

    }

    function guardar_guia_remision_post()
    {
        $data = $this->post();
        $datos = $data['data'];
        $this->secuencial_guia_get();
        $productos = $this->agrupar_productos($datos['despacho']);
        $this->guardar_cabecera_guia($datos['codido_cliente']);
        $this->guardar_productos_guia($productos);
        $this->update_numero_guia();
        echo format_response($this->numero_guia);

    }

    function guardar_cabecera_guia($codido_cliente)
    {
        $dato_cliente = $this->Cliente->get_cliente_by_codigo($codido_cliente);
        $guia = array(
            'numero_guia_remision'      => $this->numero_guia,
            'tipo_documento_origen'     => '',
            'numero_documento_origen'   => '',
            'codigo_cliente'            => $dato_cliente->codigo_cliente,
            'nombre_cliente'            => $dato_cliente->nombre_cliente,
            'direccion_origen'          => '',
            'direccion_destino'         => '',
            'ruc_transportista'         => '',
            'nombre_transportista'      => '',
            'numero_placa'              => '',
            'descripcion_mecarderia'    => '',
            'cantidad_mercaderia'       => $this->cantidad_mercaderia,
            'motivo_traslado'           => 'Transporte de Alimentos',
            'numero_autorizacion'       => '',
            'fecha_emision'             => date('Y-m-d H:i:s'),
            'fecha_vencimiento'         => date('Y-m-d H:i:s'),
            'numero_declaracion_aduanera'=> '',
            'fecha_inicio_transporte'   => date('Y-m-d H:i:s'),
            'fecha_fin_transporte'      => date('Y-m-d H:i:s'),
            'ruta_transporte'           => '',
            'codigo_destinatario'       => '',
            'codigo_numerico'           => '',
            'observacion'               => '',
            'bodega_origen'             => $this->IDB,
            'bodega_destino'            => '',
            'rechazada'                 => '',
            'autorizacion'              => '',
            'fecha_autorizacion'        => '',
            'telefono'                  => $dato_cliente->telefono,
            'email'                     => $dato_cliente->email,
            'usuario'                   => $this->uid,
            'estado_electronico'        => ''
            );
        $this->dispatch->insertar_cabecera_guia($guia);

    }

    function guardar_productos_guia($productos)
    {
        foreach ($productos as $key => $producto) {
            $dato_producto = $this->Producto->get_producto_by_codigo($producto['codigo_producto']);
            $detalle = array(
                'numero_guia_remision'      => $this->numero_guia,
                'codigo_producto'           => $dato_producto['codigo'],
                'nombre_producto'           => $dato_producto['descripcion'],
                'cantidad'                  => $producto['cantidad_dispatch'],
                'ocurrencia'                => str_pad($key, 2, '0', STR_PAD_LEFT),
                'numero_pedido'             => ''
                );
            $this->dispatch->insertar_productos_guia($detalle);
        }

    }

    function validaciones_generales_get($codido_cliente)
    {
        $this->validacion['validacion'] = true;
        $this->secuencial_guia_get();
        if ($this->secuencial_guia_val == '' || $this->secuencial_guia_val == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No esta configurado secuencial de Guia de Remision (71 - 99)";
        }

        $dato_cliente = $this->Cliente->get_cliente_by_codigo($codido_cliente);
        if ($dato_cliente->email == '' || $dato_cliente->email == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " El Cliente no tiene e-mail";
        }
        echo format_response($this->validacion);

    }

    function secuencial_guia_get()
    {
        $this->secuencial_guia_val = $this->plan_diario->get_numero_documento('71', '99');
        $this->secuencial_guia = (int) $this->secuencial_guia_val + 1;
        $this->numero_guia = $this->serie_sri . str_pad($this->secuencial_guia,9,'0',STR_PAD_LEFT);
    }

    function update_numero_guia()
    {
        $this->plan_diario->update_numero_documento('71', '99', $this->secuencial_guia);
    }
    
}
?>
