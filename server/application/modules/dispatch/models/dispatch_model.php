<?php
include_once(APPPATH ."models/base_model.php");
Class Dispatch_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }

    function guardar_distpatch($array_insert)
    {
        $this->db->insert('planificacion_produccion',$array_insert);
    }

    function actualizar_distpatch($valor, $dia)
    {
      $array_update = array(
               'cantidad_dispatch' => $valor['cantidad_dispatch'],
               'cantidad_entregada' => $valor['cantidad_entregada'],
               'estado_despacho' => 0
          );              

      $this->db->where("desde", $valor['desde']);
      $this->db->where("hasta", $valor['hasta']);
      $this->db->where("id_cliente", $valor['id_cliente']);
      $this->db->where("id_producto", $valor['id_producto']);
      $this->db->where("opcion", $valor['opcion']);
      $this->db->where("estado_despacho", 0);
      $this->db->where("estado_preplan", 1);
      $this->db->where("dia", $dia);
      $this->db->update('planificacion_produccion',$array_update);

    }

    function consulta_dispatch($datos){

    	
        $dia = date('w', strtotime($datos['fecha']));
        $dia = ($dia != "0") ? $dia : '7' ;

    	$this->db->select('*')->from("planificacion_produccion");
     
        $this->db->where("desde", $datos['desde']);
        $this->db->where("hasta", $datos['hasta']);
        $this->db->where("id_cliente", $datos['id_cliente']);
        $this->db->where("id_producto", $datos['id_producto']);
        $this->db->where("opcion", $datos['opcion']);
        //$this->db->where("estado_despacho", 0);
        $this->db->where("estado_preplan", 1);
        $this->db->where("dia", $dia);
        
        $result = $this->db->get();
         //echo $this->db->last_query();
        $flag = 0;
        if(count($result->result_array()) > 0){

        		$flag = 1;
        }

        return $flag;

    }

    function consulta_requisicion_sin_produccion($producto, $fecha)
    {
        $fecha .= " 00:00:00";
        $result = $this->db->query('SELECT numdoc, codprod, desprod, cantsol, cantent, cant_a_devolver, origen FROM '.$this->matriz.'.reqprod WHERE `fecreg` >= "'.$fecha.'" and codprod = "'.$producto.'" and origen = "dispatch"');

        //echo $this->db->last_query();
        if(count($result->result_array()) > 0)
        {
            $resultado =  $result->result_array();
            return $resultado[0];
        }else{
            return null;
        }

    }

    function consulta_receta_ultima_opcion($datos){     
        $dia = date('w', strtotime($datos['fecha']));
        $dia = ($dia != "0") ? $dia : '7' ;

        $this->db->select('id_receta')->from("planificacion_produccion");
     
        $this->db->where("id_producto", $datos['id_producto']);
        $this->db->where("opcion", $datos['opcion']);
        $this->db->where("estado_preplan", 1);
        $this->db->where("dia", $dia);
        $this->db->order_by("opcion", "desc");
        $this->db->limit(1);
        
        $result = $this->db->get();
        //echo $this->db->last_query();
        
        return $result->result_array();
    }


    function inserta_pedido($datos){

        $sql = $this->db->insert_batch("maeped30",$datos); 

        return $sql;       
        
    }


     function iva_default(){
       $sql = $this->db->query("select ad1tab as iva from ".$this->matriz.".maetab where numtab = '01' and codtab='23'");
       $iva = $sql->result_array();
       return $iva[0]['iva'];

    }

     function secuencial_pedido(){
       $sql = $this->db->query("select ad1tab from maetab where numtab = '71' and codtab='91'");
       $secuencial = $sql->result_array();
       return $secuencial[0]['ad1tab'] + 1;

    }

    function actualiza_secuencial_pedido($num){
       $this->db->query("update maetab set ad1tab =".$num."  where numtab = '71' and codtab='91'");     

    }

    function actualiza_estado_dispatch($dato,$usuario){
        $dia = date('w', strtotime($dato['fecha']));
        $dia = ($dia != "0") ? $dia : '7' ;

        $array_update = array(                         
                         'estado_despacho' => 1,
                         'fecha_dispatch' => date('Y-m-d H:i:s'),
                         'usuario_dispatch' => $usuario,
                         'cantidad_entregada' => $dato['cantidad_entregada']
                        );              

        $this->db->where("desde", $dato['desde']);
        $this->db->where("hasta", $dato['hasta']);
        $this->db->where("id_cliente", $dato['id_cliente']);
        $this->db->where("id_producto", $dato['id_producto']);
        $this->db->where("opcion", $dato['opcion']);
        $this->db->where("estado_despacho", 0);
        $this->db->where("estado_preplan", 1);
        $this->db->where("dia", $dia);
        $this->db->update('planificacion_produccion',$array_update);
    }

    function eliminar_opciones($data){

        foreach ($data as $valor) {

            $dia = date('w', strtotime($valor['dia']));
            $dia = ($dia != "0") ? $dia : '7' ;

            $this->db->query('delete from planificacion_produccion 
                              where id_producto = "'.$valor['codigo_producto'].'" 
                              and dia = "'.$dia.'" 
                              and opcion = "'.$valor['opcion'].'" 
                              and id_cliente = "'.$valor['codigo_cliente'].'"');
        }

    }

    function eliminar_clientes($data){
        
        foreach ($data as $valor) {

            $dia = date('w', strtotime($valor['dia']));
            $dia = ($dia != "0") ? $dia : '7' ;

            $this->db->query('delete from planificacion_produccion 
                              where id_cliente = "'.$valor['codigo_cliente'].'" 
                              and dia = "'.$dia.'"');
            
        }
    }

    function eliminar_productos($data){
        
        foreach ($data as $valor) {

            $dia = date('w', strtotime($valor['dia']));
            $dia = ($dia != "0") ? $dia : '7' ;

            $this->db->query('delete from planificacion_produccion 
                              where id_cliente = "'.$valor['codigo_cliente'].'" 
                              and id_producto = "'.$valor['codigo_producto'].'"
                              and dia = "'.$dia.'"');
            
        }
    }

    function insertar_cabecera_guia($datos)
    {
        $this->db->insert("guia_remision_electronica", $datos);
        return $this->db->affected_rows();
    }

    function insertar_productos_guia($datos)
    {
        $this->db->insert("productos_guia_remision_electronica", $datos);
        return $this->db->affected_rows();
    }

    function actualizar_producto_requisicion($datos, $numero_requisicion, $producto, $fecha)
    {
        $this->db->where('numdoc', $numero_requisicion);
        $this->db->where('codprod',$producto);
        $this->db->where('fecreg',$fecha);
        $result = $this->db->update($this->matriz.'.reqprod', $datos);
        //echo $this->db->last_query();
        return $result;

    }
    
}
?>
