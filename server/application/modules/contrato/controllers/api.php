<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
 
class api extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Contrato_model', 'Contrato');
    }
 
    function contratos_get( $codigo, $tipo, $desde, $hasta, $idContrato)
    {
        $contratos = $this->Contrato->get_contratos( $codigo, $tipo, $desde, $hasta, $idContrato);
        echo format_response($contratos);
    }
 
    function contrato_put()
    {
        // create a new contrato and respond with a status/errors
        // return data = echo format_response($data);
    }
 
    function contrato_post()
    {
        // update an existing contrato and respond with a status/errors
        // return data = echo format_response($data);
    }
 
    function contrato_delete()
    {
        // delete a contrato and respond with a status/errors
        // return data = echo format_response($data);
    }
    
}
?>
