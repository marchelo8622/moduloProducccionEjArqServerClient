<?php

include_once(APPPATH ."models/base_model.php");

Class Contrato_model extends Base_model
{
    private $matriz;

    function __construct()
    {
        //ini_set("display_errors", 1);
        parent::__construct();
        $this->matriz = parent::get_matrix_name();
    }
   
    function get_contratos( $codigo, $tipo, $desde, $hasta) {

        
        $this->db->select('contratos.codCliente as codigo,maecte.nomcte01 as nomCliente, contratos.categoria as tipo, contratos.finicio as desde, contratos.ffin as hasta, contratos.idContrato as idContrat, contratos.numVuelo as numV, maetab.codtab as aerolinea');
        $this->db->from('contratos');
        $this->db->join($this->matriz.'.maecte','contratos.codCliente = maecte.codcte01');
        $this->db->join($this->matriz.'.maetab','contratos.tipoAvion = maetab.codtab');
        $this->db->group_by('contratos.codCliente, contratos.idContrato');
        $result = $this->db->get();
        return $result->result_array();
       // echo $hits->db->last_query();
    }
    function get_contratos_filtros( $codigo, $tipo, $desde, $hasta) {


        if ($tipo != "undefined" && $codigo == "undefined") {
            $this->db->like('categoria', $tipo);
        }

        if ($codigo != 'undefined' && $tipo == "undefined") {
            $this->db->like('codCliente', $codigo);
        }

        if ($codigo != 'undefined' && $tipo != "undefined") {
            $this->db->like('codCliente', $codigo);
            $this->db->like('categoria', $tipo);
        }

       
        
        $this->db->select('contratos.codCliente as codigo, maecte.nomcte01 as nomCliente, contratos.categoria as tipo, contratos.finicio as desde, contratos.ffin as hasta, contratos.idContrato as idContrat, contratos.numVuelo as numV, maetab.codtab as aerolinea');
        
        $this->db->from('contratos');
        $this->db->join($this->matriz.'.maecte','contratos.codCliente = maecte.codcte01');
        $this->db->join($this->matriz.'.maetab','contratos.tipoAvion = maetab.codtab');
        $this->db->group_by('contratos.codCliente, contratos.idContrato');
        
        $result = $this->db->get();
        //echo $this->db->last_query();
        return $result->result();
    }
    
    function get_contrato_by_id($codigo, $idContrat, $numV, $aerolinea){

        $sql = $this->db->query('SELECT `contratos`.`codcliente`, `contratos`.`numVuelo`, `contratos`.`tipoAvion`, `contratos`.`cantCiclos`, `contratos`.`categoria`, `contratos`.`finicio`, `contratos`.`ffin`, `contratos`.`idContrato`, `frecuenciaContrato`.`idFrecuencia`, `productosContrato`.`idProducto`, `productosContrato`.`descripcion`, `opciones`.`precio`, `opciones`.`opcion`, `opciones`.`servicioAdicional`, `opciones`.`cantidad`, `frecuenciaContrato`.`lunes`, `frecuenciaContrato`.`martes`, `frecuenciaContrato`.`miercoles`, `frecuenciaContrato`.`jueves`, `frecuenciaContrato`.`viernes`, `frecuenciaContrato`.`sabado`, `frecuenciaContrato`.`domingo`, `productosContrato`.`indiceProducto`, `opciones`.`indiceOpciones`, `clienteContrato`.`duracion`, `clienteContrato`.`ciclos` 
            FROM (`contratos`) 

            INNER JOIN clienteContrato ON contratos.codCliente = clienteContrato.idCliente
            INNER JOIN frecuenciaContrato ON frecuenciaContrato.idCliente = contratos.codCliente
            AND frecuenciaContrato.idContrato = contratos.idContrato
            INNER JOIN productosContrato ON productosContrato.idCliente = contratos.codCliente
            AND productosContrato.idContrato = contratos.idContrato
            AND productosContrato.idFrecuencia = frecuenciaContrato.idFrecuencia
            INNER JOIN opciones ON opciones.idCliente = contratos.codCliente
            AND opciones.idContrato = contratos.idContrato
            AND opciones.idProducto = productosContrato.idProducto

            WHERE `contratos`.`idContrato` = '.$idContrat.' AND `contratos`.`codcliente` = '.$codigo.' AND `contratos`.`numVuelo` = '.$numV.'
             AND `contratos`.`tipoAvion` = '.$aerolinea.' 
            ORDER BY `frecuenciaContrato`.`idFrecuencia` and productosContrato.idProducto and opciones.opcion and productosContrato.indiceProducto');    
            //echo $this->db->last_query();
        return $sql->result();
    
    }

    function get_contrato_by_id_contrato($idContrat, $codCliente){

        $sql = $this->db->query('SELECT `contratos`.`codCliente`, `contratos`.`idContrato`, `maecte`.`nomcte01` as nomCliente, `contratos`.`numVuelo`, `contratos`.`tipoAvion`, `contratos`.`cantCiclos`, `contratos`.`categoria`, `configurar_productos`.`fecha_inicio`, `configurar_productos`.`fecha_fin`,`configurar_productos`.`codigo_producto`, `contratos`.`idContrato`, `frecuenciaContrato`.`idFrecuencia`, `productosContrato`.`idProducto`, `configurar_productos`.`descripcion`,`opciones`.`opcion`, `opciones`.`servicioAdicional`, `opciones`.`cantidad`, `frecuenciaContrato`.`lunes`, `frecuenciaContrato`.`martes`, `frecuenciaContrato`.`miercoles`, `frecuenciaContrato`.`jueves`, `frecuenciaContrato`.`viernes`, `frecuenciaContrato`.`sabado`, `frecuenciaContrato`.`domingo`, `productosContrato`.`indiceProducto`, `opciones`.`indiceOpciones`,`configurar_productos`.`fecha_creacion`,`configurar_productos`.`precio`, `maetab`.`nomtab`,`configurar_productos`.`cantidad`, `opciones`.`precio` as `opprecio`  
            FROM (`contratos`) 
            INNER JOIN clienteContrato ON contratos.codCliente = clienteContrato.idCliente AND contratos.numVuelo = clienteContrato.numVuelo AND contratos.idContrato = clienteContrato.idContrato
            INNER JOIN frecuenciaContrato ON frecuenciaContrato.idCliente = contratos.codCliente AND frecuenciaContrato.idContrato = contratos.idContrato INNER JOIN productosContrato ON productosContrato.idCliente = contratos.codCliente AND productosContrato.idContrato = contratos.idContrato AND productosContrato.idFrecuencia = frecuenciaContrato.idFrecuencia 
            INNER JOIN opciones ON opciones.idCliente = contratos.codCliente AND opciones.idContrato = contratos.idContrato AND opciones.idProducto = productosContrato.idProducto
            inner join '.$this->matriz.'.maecte on maecte.codcte01 = contratos.codCliente
            INNER JOIN configurar_productos on configurar_productos.id_cliente = contratos.codCliente AND configurar_productos.numero_vuelo = contratos.numVuelo AND productosContrato.idProducto = configurar_productos.codigo
            INNER JOIN maepro ON configurar_productos.codigo = maepro.codprod01
            INNER JOIN '.$this->matriz.'.maetab ON maetab.numtab = 46 AND maetab.codtab = maepro.orden01
  

            WHERE `contratos`.`idContrato` = '.$idContrat.' AND `contratos`.`codCliente` = '.$codCliente.'
            ORDER BY `frecuenciaContrato`.`idFrecuencia` and productosContrato.idProducto and opciones.opcion and productosContrato.indiceProducto');
        //echo $this->db->last_query();
        return $sql->result_array();
    }

    function get_contrato_by_datos($codigo){
        $this->db->select('*');
        $this->db->from("contratos");
        $this->db->where('codCliente', $codigo);
        $result = $this->db->get();
        return $result->result();

    }


    function update($data,$idContrato, $codCliente){
        $this->db->where('idContrato', $idContrato);
        $this->db->where('codCliente', $codCliente);       
        $this->db->update('contratos', $data);

    }

    function post_guardar($datos) {
            $this->db->insert("contratos", $datos);
            //echo $this->db->last_query();
    }
    
    function get_contrato_by_client($codigo){
        $sql = $this->db->query('Select idContrato from contratos  where codCliente ="'.$codigo.'" order by idContrato desc limit 1');
        
        return $sql->result_array();
    }

    function delete($data_delete,$idContrato, $codCliente){
        $this->db->where('idContrato', $idContrato);
        $this->db->where('codCliente', $codCliente);
        $this->db->delete("contratos");
        
    }

    function get_precio_contrato($contrato,$cliente,$producto){

        $this->db->select('precio');
        $this->db->from("opciones ");
        $this->db->where('idCliente ', $cliente);
        $this->db->where('idContrato', $contrato);
        $this->db->where('idProducto', $producto);
        $result = $this->db->get();
        return $result->result_array();

    }
}
?>
