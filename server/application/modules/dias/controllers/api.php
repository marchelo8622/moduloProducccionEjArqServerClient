<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Dia_model', 'Dia');
    }
    
    function dia_get()
    {
        $dias = $this->Dia->get_dia();
        echo format_response($dias);
    }
    
}
