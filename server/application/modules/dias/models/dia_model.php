<?php
include_once(APPPATH ."models/base_model.php");
Class Dia_model extends Base_model
{
    function __construct()
    {
        //ini_set("display_errors", 1);
        parent::__construct();
    }
    
    function get_dia($datos)
    {
        $this->db->select('Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo');
        $this->db->from('dias');
        $this->db->join('frecuenciaContrato', 'dias.idCliente = frecuenciaContrato.idCliente AND dias.numVuelo = frecuenciaContrato.numVuelo AND dias.idFrecuencia = frecuenciaContrato.idFrecuencia');
        $this->db->where('dias.idCliente', $datos['idCliente']);
        $this->db->where('dias.idFrecuencia', $datos['frecuencia']);
        $this->db->where('dias.numVuelo', $datos['numVuelo']);
        $result = $this->db->get();
        return $result->result();
    }

    function post_guardar($datos) {
        foreach ($datos as $opcion) {
        $this->db->insert("dias", $opcion);
         }
    }

    function delete($data_delete){
        $this->db->where('idCliente', $data_delete['idCliente']);
        $this->db->where('numVuelo', $data_delete['numVuelo']);
        $this->db->where('idFrecuencia', $data_delete['idFrecuencia']);
        $this->db->delete("dias");
    }

}
?>
