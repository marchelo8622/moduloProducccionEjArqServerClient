<?php
include_once(APPPATH ."models/base_model.php");
Class Frecuencia_contrato_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_frecuencia_contrato()
    {

        if ($idCliente != "undefined" && $numVuelo != "undefined") {
            $this->db->limit(20);
        }

        if ($idCliente != "undefined") {
            $this->db->like('id_Cliente', $idCliente);
        }

        if ($numVuelo != "undefined") {
            $this->db->like('numVuelo', $numVuelo);
        }
        $this->db->select('*');
        $this->db->from("frecuenciaContrato");
        $result = $this->db->get();
        return $result->result();
    }

    function delete($data_delete,$idContrato, $codCliente){

            $this->db->where('idContrato', $idContrato);
            $this->db->where('idCliente', $codCliente);
            $sql=$this->db->delete("frecuenciaContrato");

    }

    function post_guardar($frecuencias) {
        foreach ($frecuencias as $frecuencia) {
            $this->db->insert("frecuenciaContrato", $frecuencia);
        }
    }
}
?>
