<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Frecuencia_contrato_model', 'Frecuencia_contrato');
    }
    
    function frecuencia_contrato_get()
    {
        $frecuencia_contrato = $this->Frecuencia_contrato->get_frecuencia_contrato();
        echo format_response($frecuencia_contrato);
    }
    
}
