<script src="<?= base_url()?>../client/src/app/planificacion_semanal/module.js"></script>
<script src="<?= base_url()?>../client/src/app/planificacion_semanal/planificacion_semanal_model.js"></script>
<script src="<?= base_url()?>../client/src/app/planificacion_semanal/controllers/planificacion_semanal.controller.js"></script>

<div ng-view ng-cloak class="container" style="width:100%">
    <div ng-controller="Notifier"><div>
</div>

<script>
    function base_recargar() {
        window.location.reload();
    }

    function base_salir() {
        window.parent.close();
    }

    function base_imprimir() {
       window.print();  
    }
    
</script>