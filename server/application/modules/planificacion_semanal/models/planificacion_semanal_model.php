<?php

include_once(APPPATH . "models/base_model.php");

Class Planificacion_semanal_model extends Base_model {

    function __construct() {
        parent::__construct();
    }

    

    function get_planificacion_by_from($desde, $hasta) {       

        $sql = $this->db->query("select planificacion_produccion.*,desprod01,codprod01,SUM(cantidad) AS cantidad_total 
                                 from planificacion_produccion
                                 join maepro on id_producto = codprod01
                                 where '".$desde."' >= desde and '".$hasta."' <= hasta
                                 group by id_producto,opcion,dia
                                 order by dia,id_producto, opcion
                                 ");
        return  $sql->result();
    }

    function post_guardar($datos) {
        $IDB = $this->input->get('IDB');

       // print_r($datos);
        $array_data = array();
        foreach ($datos['data'] as $valor) {

              $array_productos_receta = json_decode($valor['id_receta'],true);

              $array_id_receta = array();
              foreach ($array_productos_receta as $prod) {
                      $array_id_receta[] = array(
                              'cat' => $prod['cat'],
                              'receta' => $prod['receta'],
                              'nom_receta' => $this->get_nombre_producto($prod['receta'])

                        );
              }

              $json_id_receta = json_encode($array_id_receta);

              
              
              $array_data[] = array(

                       'id_cliente' => $valor['id_cliente'],                                   
                       'id_producto' => $valor['id_producto'],
                       'id_receta' => $json_id_receta,
                       'dia' => $valor['dia'],
                       'codigo' => $valor['codigo'],
                       'opcion' => $valor['opcion'],
                       'cantidad' => $valor['cantidad'],
                       'desde' => $valor['desde'],
                       'hasta' => $valor['hasta']

                );
        }

       // var_dump($array_data);        
        
        if(count($array_data) > 0){
          $this->elimina_planificacion_produccion($array_data[0]['desde'],$array_data[0]['hasta']);
          foreach ($array_data as $key => $producto) {
              $this->db->replace("planificacion_produccion", $producto);            
          }

          $this->actualizar_estado_pre_plan($array_data[0]['desde'],$array_data[0]['hasta']);
          $this->add_requisicion_pac($array_data,$IDB);
        }
        
        
    }

    function add_requisicion_pac($datos,$IDB) {


        session_start();
        $uid = $_SESSION['usuario'];
        foreach ($datos as  $producto) {
          $array_receta = json_decode($producto['id_receta'],true);
          //var_dump($array_receta);
           foreach($array_receta as $receta){
           $datosProductoCompuesto = $this->get_productos_compuestos($receta['receta']);
            
           $ocu =0;
           foreach ($datosProductoCompuesto as $datoProductoCompuesto) {
            
            $arreglo_requisicion[] =Array(
                   'nopedido31' => 'plan-'.str_pad(round($this->get_ultimo_secuencial_requisicion_produccion(),0),9,'0',STR_PAD_LEFT),
                   'codprod31' =>  $datoProductoCompuesto['codprod20'],
                   'ocurren31' =>  str_pad(round($ocu,0),2,'0',STR_PAD_LEFT),
                   'fecpedido31' => date('Y-m-d H:i:s'),
                   'referencia31' => 'NA',
                   'localid31' => '01',
                   'status31' => '01',
                   'cantped31' => $producto['cantidad'],
                   'cantafac31' => $producto['cantidad'],
                   'uid31' => $uid,
                   'bodega31' => $IDB,
                   'fecestado31' =>date('Y-m-d H:i:s'),
                   'observ31' => 'Semana-'.$producto['desde'].'_'.$producto['hasta']
                ); 

             $ocu++;

           }

          }
        }

        //var_dump($arreglo_requisicion);                
//
        $this->db->insert_batch("req_plan_semanal_tmp", $arreglo_requisicion);        


        $nopedidoReq = 'plan-'.str_pad(round($this->get_ultimo_secuencial_requisicion_produccion(),0),9,'0',STR_PAD_LEFT);
        $existeRequisicion = $this->consulta_existe_requsicion($nopedidoReq);
        $datosTempRequisicion = $this->get_temp_requisicion($uid,$existeRequisicion);
        
        foreach ($datosTempRequisicion as  $valoresRequisicion) {          

          if($existeRequisicion == 1){

              $flag = 'update';
             
              if($this->actualiza_requisicion_pac($valoresRequisicion['nopedido31'],$valoresRequisicion['ocurren31'],$valoresRequisicion['codprod31'],$valoresRequisicion)){

              }else{
                 $this->db->insert("maereq31", $valoresRequisicion);
              }

          }else{ 

              $flag = 'insert';
              $this->db->insert("maereq31", $valoresRequisicion);
          }

        }

        if($flag == 'insert'){
          $secuencialRequisicion = $this->get_secuencial_requisicion_produccion();
          $this->actualiza_secuencial_requisicion_produccion($secuencialRequisicion);
        }        
        $this->eliminar_datos_temp_requisiscion($uid);
    

    }

    function get_temp_requisicion($uid,$existeRequisicion){

      $sql = $this->db->query("select 
                               nopedido31,codprod31,ocurren31,fecpedido31,referencia31,localid31,status31,SUM(cantped31) AS cantped31,SUM(cantafac31) AS cantafac31,uid31,bodega31,fecestado31,observ31
                               from req_plan_semanal_tmp 
                               where uid31 = '".$uid."'  
                               group by codprod31");

      $datosTempReq = $sql->result_array();
      $ocu = 0;


      if($existeRequisicion == 0){
        
        $secuencialRequisicion = $this->get_secuencial_requisicion_produccion();

      }else{
        $secuencialRequisicion = $this->get_ultimo_secuencial_requisicion_produccion();

      }
      
      $arreglo_datos_requisicion = array();
      if(count($datosTempReq) > 0){

        foreach ($datosTempReq as $valorReq) {

          $arreglo_datos_requisicion[] =Array(
                     'nopedido31' => 'plan-'.str_pad(round($secuencialRequisicion,0),9,'0',STR_PAD_LEFT),
                     'codprod31' =>  $valorReq['codprod31'],
                     'ocurren31' =>  str_pad(round($ocu,0),2,'0',STR_PAD_LEFT),
                     'fecpedido31' => $valorReq['fecpedido31'],
                     'referencia31' => $valorReq['referencia31'],
                     'localid31' => $valorReq['localid31'],
                     'status31' => $valorReq['status31'],
                     'cantped31' => $valorReq['cantped31'],
                     'cantafac31' => $valorReq['cantafac31'],
                     'uid31' => $valorReq['uid31'],
                     'bodega31' => $valorReq['bodega31'],
                     'fecestado31' =>$valorReq['fecestado31'],
                     'observ31' => $valorReq['observ31']
                  ); 

               $ocu++;
          
        }
      }

      return  $arreglo_datos_requisicion;

    }

    function get_productos_compuestos($codigo){

      $this->db->where("codprod20t", $codigo); 
      $this->db->select('codprod20,desprod20,unidmed20,cantidad20')->from("lismae");
      $resultado = $this->db->get();
      return $resultado->result_array();

    }

    function eliminar_datos_temp_requisiscion($uid){

      $this->db->where('uid31',$uid);
      $this->db->delete('req_plan_semanal_tmp');

    }

    function aviso_terminacion_ciclo($desde){
      $this->db->select('clienteContrato.idCliente, clienteContrato.ciclos, clienteContrato.duracion,  maecte.nomcte01, clienteContrato.terminacionCiclo');
      $this->db->from('clienteContrato');
      $this->db->join('maecte','clienteContrato.idCliente = codcte01');
      $this->db->where('clienteContrato.terminacionCiclo', $desde);
      $dato = $this->db->get();
      //echo $this->db->last_query();
      return $dato->result();
    }

    function clonar_planificacion($prePlan,$desdeClonar,$hastaClonar){      
       
       foreach ($prePlan as  $valor) {

       // echo $valor->desde;
            $prodCompuestoParaClonar = $this->get_opcion_plansemanal($desdeClonar,$hastaClonar, $valor->dia, $valor->id_producto, $valor->opcion);
             
             $jsonCompuesto = "";
             
             if($prodCompuestoParaClonar != ""){
               
                $jsonCompuesto =  $prodCompuestoParaClonar; 

             }

            $arrayDatosPrePlan [] = array(
                      'id_cliente' => $valor->desde,
                      'id_producto' => $valor->id_producto,
                      'dia' => $valor->dia,
                      'opcion' => $valor->opcion,
                      'cantidad' => $valor->cantidad,
                      'desde' => $valor->desde,
                      'hasta' => $valor->hasta,
                      'codigo' => $valor->codigo,
                      'estado' => $valor->estado,
                      'cantidad_total' => $valor->cantidad_total,
                      'desprod01' => $valor->desprod01,
                      'id_receta' => $jsonCompuesto
              ); 

       }
       

       return $arrayDatosPrePlan;
      
    }


    function get_opcion_plansemanal($fechadesde,$fechahasta,$dia,$producto,$opcion){

       $this->db->where("desde =", $fechadesde);
       $this->db->where("hasta =", $fechahasta);           
       $this->db->where("id_producto =", $producto);
       $this->db->where("dia =", $dia);
       $this->db->where("opcion =", $opcion);
       $this->db->where("estado_preplan =",1);
        //$this->db->select('*,SUM(cantidad) AS cantidad_total')->from("plan_semanal");

       $this->db->select('planificacion_produccion.*,desprod01,codprod01,SUM(cantidad) AS cantidad_total')->from("planificacion_produccion");
        //$this->db->join('maecte', 'id_cliente = codcte01');
        $this->db->join('maepro', 'id_producto = codprod01');
        $this->db->group_by('id_producto,opcion,dia');
        $this->db->order_by('dia');
        $sql = $this->db->get();
        $resultado = $sql->result_array();
        $idReceta = "";

        if(count($resultado) > 0){
          $idReceta = $resultado[0]['id_receta'];           
        }

        return  $idReceta;
    }

    function consulta_existe_requsicion($numRequisicion){

        $this->db->where("nopedido31", $numRequisicion);
        $this->db->where("status31", '01');
        $this->db->select('*')->from("maereq31");
        $sql = $this->db->get();      
        $resultado = $sql->result_array();

        $flag = 0;

        if(count($resultado) > 0){
           $flag = 1;
        }

        return $flag;

    }

    function get_secuencial_requisicion_produccion(){
      
        $this->db->where("numtab", '2525');
        $this->db->where("codtab", '');
        $this->db->select('ad1tab')->from("maetab");
        $sql = $this->db->get();
        $resultado = $sql->result_array();

        return $resultado[0]['ad1tab'] + 1;

    }

    function get_ultimo_secuencial_requisicion_produccion(){
      
        $this->db->where("numtab", '2525');
        $this->db->where("codtab", '');
        $this->db->select('ad1tab')->from("maetab");
        $sql = $this->db->get();
        $resultado = $sql->result_array();

        return $resultado[0]['ad1tab'];

    }

    function actualiza_secuencial_requisicion_produccion($valor){

      $data = array(
               'ad1tab' => $valor               
              );

      $this->db->where("numtab", '2525');
      $this->db->where("codtab", '');
      $this->db->update('maetab', $data); 
    }


    function actualiza_requisicion_pac($nopedido31,$ocurrencia,$codprod31,$valoresRequisicion){

       $this->db->where('nopedido31', $nopedido31);
       $this->db->where('ocurren31', $ocurrencia);
       $this->db->where('codprod31', $codprod31);
       $update = $this->db->update('maereq31', $valoresRequisicion);
       return $update;

    }

    function actualizar_estado_pre_plan($fechaDesde,$fechaHasta){
       

       $this->db->query("update planificacion_produccion set estado_preplan = 1 where '".$fechaDesde."' >= desde and '".$fechaHasta."' <= hasta");
       //return $update;

    }

    function get_nombre_producto($codigo){

      
        $this->db->where("codprod01", $codigo);
        $this->db->select('desprod01')->from("maepro");
        $sql = $this->db->get();
        $resultado = $sql->result_array();

        return $resultado[0]['desprod01'];


    }

    function elimina_planificacion_produccion($desde,$hasta){

      $this->db->query("delete from planificacion_produccion where '".$desde."' >= desde and '".$hasta."' <= hasta");


    }

    

}

?>
