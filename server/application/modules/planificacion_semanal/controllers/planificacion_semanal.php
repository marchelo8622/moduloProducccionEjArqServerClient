<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class planificacion_semanal extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Planificacion_semanal";
		$data["angular_app"] = "Planificacion_semanal";
		$this->layout->view("planificacion_semanal", $data);
	}
}

