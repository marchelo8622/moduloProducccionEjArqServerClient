<?php
//ini_set('display_errors', 1);
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";

class api extends REST_Controller {

    private $validacion = array();

    function __construct() {
        parent::__construct();
        $this->load->model("Planificacion_semanal_model", "Planificacion_semanal");
        $this->load->model('semana_planificar/Semana_planificar_model', 'Semana_planificar');
        $this->load->model('plan_diario/plan_diario_model', 'plan_diario');
        $this->load->model('producto/Producto_model', 'Producto');
        $this->load->model('Pre_plan/Pre_plan_model', 'Pre_plan');
        $this->load->model('contrato/Contrato_model', 'Contrato');
        $this->load->model('clientes/Cliente_model', 'Cliente');
        $this->load->model("configurar_producto_venta/Configurar_producto_venta_model", "configurar_producto_venta");
    }

   
    function productos_tipo_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_tipo($codigo, urldecode($descripcion), 'null');
        foreach ($productos as  $valor) {
            $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($valor->codigo);
            $json = json_encode($config_prod_final);

            $data[] = array(
                    'codigo' => $valor->codigo,
                    'descripcion' => $valor->descripcion,
                    'stock' => $valor->stock,                    
                    'configuraciones_prod_final' => $json
            );
        }
        echo format_response($data);
    }

    function planificacion_by_from_get($desde, $hasta) {
        $plan = $this->Planificacion_semanal->get_planificacion_by_from($desde, $hasta);
        //print_r($plan);
        $data = array();
        if (count($plan) > 0)
        {
        foreach ($plan as  $valor) {

            $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($valor->codprod01);
            $json = json_encode($config_prod_final);


            $data[] = array(
                    'id_producto' => $valor->id_producto,
                    'id_receta' => $valor->id_receta,
                    'id_cliente' => $valor->id_cliente,
                    'dia' => $valor->dia,
                    'opcion' => $valor->opcion,
                    'cantidad' => $valor->cantidad,
                    'desde' => $valor->desde,
                    'codigo' => $valor->codigo,
                    'hasta' => $valor->id_producto,
                    'desprod01' => $valor->desprod01,
                    'codprod01' => $valor->codprod01,
                    'cantidad_total' => $valor->cantidad_total,
                    'configuraciones_prod_final' => $json
            );
        }
        }
        
        echo format_response($data);
    }

   

    function guardar_post() {
        $filter = $this->post();
        $productos = $this->Planificacion_semanal->post_guardar($filter);
        echo format_response($productos);
    }

    function calcular_costo_semana_post() {
        $filter = $this->post();
        $datos_semana = $filter['data'];
        //print_r($datos_semana);
        $sum_costo_productos_semana = 0;
        $resultReceta = 0;
        //$resultSubRecetas = 0;
        $id_producto = '';
        foreach ($datos_semana as $dato_semana){

            $array_receta = json_decode($dato_semana['id_receta'],true);
            $resultSubRecetas = 0;
            foreach ($array_receta as  $valor){
                $costo = $this->Producto->get_costo_producto_by_codigo($valor['receta'],$tabla = 'lismae');

                $resultSubRecetas += ($costo * $dato_semana['cantidad']);
            }
                $sum_costo_productos_semana += $resultSubRecetas;
        } 

        echo format_response($sum_costo_productos_semana);       

        
    }


    function calcular_costo_diario_post($dia,$tabla = 'lismae') {
        $filter = $this->post();        
        $datos_dia = $this->set_planificacion_por_dia($filter['data'],$dia);
        //print_r($datos_semana);
        $sum_costo_productos_semana = 0;
        $resultReceta = 0;
        //$resultSubRecetas = 0;
        $id_producto = '';
        $datos_por_opcion = array();
        foreach ($datos_dia as $dato_dia){

            $array_receta = json_decode($dato_dia['id_receta'],true);
            $resultSubRecetas = 0;
            foreach ($array_receta as  $valor){

                $costo = $this->Producto->get_costo_producto_by_codigo($valor['receta'],$tabla);

                $resultSubRecetas += ($costo * $dato_dia['cantidad']);
            }
              

                $sum_costo_productos_semana += $resultSubRecetas;
                
                
                      
        } 

        $array_costos = array(

                   'costo_dia' =>  $sum_costo_productos_semana,
                   'costos_opciones' =>  $this->calcular_costo_opcion($datos_dia,$dia,$tabla)
            );

        echo format_response($array_costos);      

        
    }

    function calcular_costo_opcion($datos_dia,$dia,$tabla) { 
            
            $costo_productos_opciones = array();
            
            //$resultSubRecetas = 0;
            $id_producto = '';
            foreach ($datos_dia as $dato_dia){

                $array_receta = json_decode($dato_dia['id_receta'],true);
                $resultSubRecetas = 0;
                $resultSubRecetas_unitario = 0;
                foreach ($array_receta as  $valor){

                    $costo = $this->Producto->get_costo_producto_by_codigo($valor['receta'],$tabla);

                    $resultSubRecetas += ($costo * $dato_dia['cantidad']);
                    $resultSubRecetas_unitario += $costo;
                }
                  

                    $costo_productos_opciones [] = array(
                          'dia' => $dato_dia['dia'] ,
                          'opcion' => $dato_dia['opcion'] ,
                          'producto' => $dato_dia['id_producto'] ,
                          'costo' => $resultSubRecetas,
                          'costo_unitario' => $resultSubRecetas_unitario


                          );
                          
            }


            foreach ($costo_productos_opciones as $dato_dia) {
                

                $new_costo_productos_opciones [] = array(
                          'dia' => $dato_dia['dia'] ,
                          'opcion' => $dato_dia['opcion'] ,
                          'producto' => $dato_dia['producto'] ,
                          'costo' => $dato_dia['costo'],
                          'costo_unitario' => $dato_dia['costo_unitario'],
                          'suma_por_producto' => $this->sum_opciones_por_producto($costo_productos_opciones,$dato_dia['producto'])
                          );
                
            }


       

        return $new_costo_productos_opciones;
        
    }

    function sum_opciones_por_producto($costo_productos_opciones,$producto){
        
            $sumOpciones = 0;
            foreach ($costo_productos_opciones as $dato_dia) {                

                if($producto == $dato_dia['producto']){
                  $sumOpciones += $dato_dia['costo'];
                }                
                
            }

            return $sumOpciones;
    }

    function set_planificacion_por_dia($datos_semana,$dia){
        $nuevo_array_dia = array();
        foreach ($datos_semana as $valor) {

            if($valor['dia'] == $dia){
                 $nuevo_array_dia[] = $valor;

            }
            
        }
        return $nuevo_array_dia;
    }

    function set_planificacion_por_opcion($datos_semana,$dia,$producto,$opcion){
        $array_dia = array();
        

            if($datos_semana['dia'] == $dia && $datos_semana['opcion'] == $opcion && $datos_semana['id_producto'] == $producto){
                 $array_dia = $datos_semana;

            }
            
        
        return $array_dia;
    }


    function productos_compuestos_get($codigo, $descripcion) {
        $productos = $this->Producto->get_productos_compuestos($codigo, $descripcion);
        echo format_response($productos);
    }
    
    function pre_plan_get($desde, $hasta) {
        $prePlan = $this->Pre_plan->get_planificacion_by_from($desde, $hasta);
         $data = array();

         if(count( $prePlan) > 0){
                foreach ($prePlan as  $valor) {

                    $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($valor->codprod01);
                    $json = json_encode($config_prod_final);

                    $id_receta = array();
                    $data[] = array(
                            'id_producto' => $valor->id_producto,
                            'id_receta' => json_encode($id_receta),
                            'id_cliente' => $valor->id_cliente,
                            'dia' => $valor->dia,
                            'opcion' => $valor->opcion,
                            'cantidad' => $valor->cantidad,
                            'desde' => $valor->desde,
                            'hasta' => $valor->hasta,
                            'codigo' => $valor->codigo,
                            'desprod01' => $valor->desprod01,
                            'codprod01' => $valor->codprod01,
                            'cantidad_total' => $valor->cantidad_total,
                            'configuraciones_prod_final' => $json
                    );
                }
            }
        echo format_response($data);
    }

    function aviso_terminacion_ciclo_get($desde){
        $aviso = $this->Planificacion_semanal->aviso_terminacion_ciclo($desde);
        echo format_response($aviso);
    }

    function clonar_planificacion_get($desde,$hasta,$desdeClonar,$hastaClonar) {


        $prePlan = $this->Pre_plan->get_planificacion_by_from($desde, $hasta);
        //$planificacion = $this->Planificacion_semanal->get_planificacion_by_from($desdeClonar);

        foreach ($prePlan as  $valor) {

       // echo $valor->desde;

            $config_prod_final = $this->configurar_producto_venta->consulta_categorias_by_producto_final($valor->codprod01);
            $json = json_encode($config_prod_final);
            $prodCompuestoParaClonar = $this->Planificacion_semanal->get_opcion_plansemanal($desdeClonar,$hastaClonar, $valor->dia, $valor->id_producto, $valor->opcion);
             
             $jsonCompuesto = "";
             
             if($prodCompuestoParaClonar != ""){
               
                $jsonCompuesto =  $prodCompuestoParaClonar; 

             }

            $arrayDatosPrePlan [] = array(
                      'id_cliente' => $valor->id_cliente,
                      'id_producto' => $valor->id_producto,
                      'dia' => $valor->dia,
                      'opcion' => $valor->opcion,
                      'cantidad' => $valor->cantidad,
                      'desde' => $valor->desde,
                      'hasta' => $valor->hasta,
                      'codigo' => $valor->codigo,
                      'estado_preplan' => $valor->estado_preplan,
                      'cantidad_total' => $valor->cantidad_total,
                      'desprod01' => $valor->desprod01,
                      'id_receta' => $jsonCompuesto,
                      'configuraciones_prod_final' => $json
              ); 

       }

        echo format_response($arrayDatosPrePlan);
    }


    function cliente_interno_get(){

        echo format_response($this->Cliente->cliente_interno());


    }

    function validaciones_generales_get()
    {
        $this->validacion['validacion'] = true;
        $this->secuencial_requisicion();
        if ($this->secuencial_requisicion == '' || $this->secuencial_requisicion == null)
        {
            $this->validacion['validacion'] = false;
            $this->validacion['error'] = " No esta configurado secuencial de Requisiciones de Compra";
        }
        echo format_response($this->validacion);

    }

    function secuencial_requisicion()
    {
        $this->secuencial_requisicion = $this->Planificacion_semanal->get_ultimo_secuencial_requisicion_produccion();
    }
}

?>
