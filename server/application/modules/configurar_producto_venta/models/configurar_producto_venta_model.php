<?php
include_once(APPPATH ."models/base_model.php");
Class Configurar_producto_venta_model extends Base_model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_producto_final($codigo){

        $this->db->select("codprod01,desprod01");
        $this->db->where("codprod01", $codigo);
        $this->db->from("maepro");
        $result = $this->db->get();
        return $result->result();
    }


    function get_categorias_producto_final(){

        $categorias_padre = $this->get_categorias_padre_producto_final();


        foreach ($categorias_padre as  $valor) {

           $array_categorias[] = $this->get_categorias_hijos_producto_final($valor['codcate']);
          
        }       

        return $array_categorias;
        

    }

    function get_categorias_hijos_producto_final($codigo){

        $this->db->select("desccate,codcate");
        $this->db->where("codcatep", $codigo);
        $this->db->from("categorias");
        $result = $this->db->get();
        return $result->result_array();        

    }


    function get_categorias_padre_producto_final(){

        $this->db->select("codcate");
        $this->db->where("comentario", 'PF');
        $this->db->from("categorias");
        $result = $this->db->get();
        return $result->result_array();       

    }

    function guartar_categorias_producto_final($datos,$producto){

        $this->eliminar_categorias_producto_final($producto);

        $valida =$this->consulta_categorias_producto_final($producto);

        if($valida == 0){
            foreach ($datos as  $dato) {                

                    $this->inserta_categorias_producto_final($dato);
            }
            
        }

        

    }

    function inserta_categorias_producto_final($datos){

       $this->db->insert('config_producto_final',$datos);

    }

    function consulta_categorias_producto_final($producto){

       $this->db->select("*");
       //$this->db->where('codigo_categoria',$datos['codigo_categoria']);
        $this->db->where('codigo_producto',$producto);
       $this->db->from("config_producto_final");
       $result = $this->db->get();

       $flag = 0;
       if(count($result->result_array()) > 0){
            $flag = 1;
       } 

       return $flag; 

    }

    function consulta_categorias_by_producto_final($producto){

       $this->db->select("codigo_categoria as codigo,cantidad,desccate as descripcion");       
       $this->db->where('codigo_producto',$producto);
       $this->db->join('categorias','codigo_categoria = codcate ');
       $this->db->join('maepro','maepro.codprod01 = config_producto_final.codigo_producto and maepro.productoFinal = "S"');       
       $this->db->from("config_producto_final");
       $result = $this->db->get();      

       return $result->result(); 

    }

    function eliminar_categorias_producto_final($codigo_producto){
        $this->db->where('codigo_producto',$codigo_producto);
        $this->db->delete('config_producto_final');        
    }

}
?>
