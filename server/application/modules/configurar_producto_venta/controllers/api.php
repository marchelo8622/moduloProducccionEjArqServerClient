<?php
defined("BASEPATH") OR exit("No direct script access allowed");

require APPPATH . "/libraries/REST_Controller.php";
 
class api extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("Configurar_producto_venta_model", "configurar_producto_venta");
    }
 
    

    function nombre_producto_final_get($codigo_producto){

        $dato_producto = $this->configurar_producto_venta->get_producto_final($codigo_producto);
        echo format_response($dato_producto);
       
    }

    function categorias_producto_final_get(){

        $categorias = $this->configurar_producto_venta->get_categorias_producto_final();

        $array_categorias = array();
        foreach ($categorias as $categoria) {
            foreach ($categoria as $valor) {
                $array_categorias[] = array(
                    'desccate' => $valor['desccate'],
                    'codcate' => $valor['codcate']
                    );
            }
        }
        echo format_response($array_categorias);


    }

    function categorias_by_producto_final_get($codigo_producto){

       $dato_categoria = $this->configurar_producto_venta->consulta_categorias_by_producto_final($codigo_producto);
        echo format_response($dato_categoria);

    }


    function guardar_categorias_post($producto){


        $filter = $this->post();
        //var_dump($filter);
        $categories = $this->configurar_producto_venta->guartar_categorias_producto_final($filter['data'],$producto);
        echo format_response($categories);


    }
}
?>
