<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class configurar_producto_venta extends MX_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$data["title_for_layout"] = "Configurar_producto_venta";
		$data["angular_app"] = "Configurar_producto_venta";
		$this->layout->view("configurar_producto_venta", $data);
	}
}

