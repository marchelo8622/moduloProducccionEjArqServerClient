<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


//PERSONALIZADO PARA VER SOBRE QUE INFOSAC ESTA ACTUANDO

$pac_ini_path = dirname(__FILE__)."/../../../../pac.ini";

$pac_ini = fopen($pac_ini_path, "r");
$DBU = "infosac";

if ($pac_ini) {
	
    while(!feof($pac_ini)) {
      $config_item = explode("=", fgets($pac_ini, 255));
      if ($config_item[0] == "[DBU]") {
          $DBU = trim($config_item[1]);
      }
    }

	fclose($pac_ini);
}

define('INFOSAC_DB', $DBU);

/* End of file constants.php */
/* Location: ./application/config/constants.php */