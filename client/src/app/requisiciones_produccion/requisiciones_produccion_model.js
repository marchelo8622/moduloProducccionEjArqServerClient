(function() {
  "use strict";

  angular.module("Requisiciones_produccion").factory("requisiciones_produccion_model", function(model) {
    return model("requisiciones_produccion");
  });
})();

