(function() {

    "use strict";

    angular.module("Requisiciones_produccion").controller("detalle_produccion.controller", 
        function($scope, $http, $location, $route, requisiciones_produccion_model, detalle_produccion) {
            $scope.orden_produccion = $route.current.params.orden_produccion;
            $scope.detalle_produccion = detalle_produccion;
            $scope.productos_requeridos = [];
            $scope._producto = "";
            $scope._codigo_producto = "";
            $scope._cantidad = 1;
            $scope._observacion = '';

            var branch_selected, tree;
            $scope.my_tree_handler = function(branch) {
              $scope._producto = branch.label;
              $scope._codigo_producto = branch.codigo_producto;
              $scope._cantidad = 1;
            };
            branch_selected = function(branch) {
              return $scope._producto = branch.label;
            };

            $scope.try_pre_solicitar = function () {
                var b;
                b = tree.get_selected_branch();
                $scope.agregar_producto(b);
            };

            $scope.agregar_producto = function(branch)
            {
                if (branch != null && branch != undefined)
                {
                $scope.productos_requeridos.push(
                            {
                                descripcion : branch.label,
                                codigo_producto : branch.codigo_producto,
                                cantidad_solicitada : $scope._cantidad
                            }
                    );
                }
            };

            $scope.eliminar_ingrediente = function (index, producto) {
                $scope.productos_requeridos.splice(index, 1);
            }

            $scope.get_productos_requeridos = function() {
             if ($scope.productos_requeridos.length > 0)
               return true;
              else 
               return false;
            };

            $scope.generar_requisicion = function () {
                if ($scope.get_productos_requeridos()){

                    var params = {
                            data : {  
                                orden_produccion : $scope.orden_produccion,
                                productos : $scope.productos_requeridos,
                                observacion : $scope._observacion
                                }
                        };
                        requisiciones_produccion_model.create('requisiciones_produccion', params).then(function () {
                        $scope.$emit('success', {action: 'Guardado con exito', element: 'Requisicion', identifier: ''});
                        window.history.back();
                    });

                        
                }else{
                    $scope.$emit('warning', {action: 'Solicite un producto!', element: '', message: ''});
                }
            }

            $scope.cancelar = function (){
                window.history.back();
            }


            $scope.my_data = detalle_produccion;
            $scope.my_tree = tree = {};

            return $scope.try_adding_a_branch = function() {
              var b;
              b = tree.get_selected_branch();
              return tree.add_branch(b, {
                label: 'New Product',
                data: {
                  something: 42,
                  "else": 43
                }
              });
            };


        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/requisiciones_produccion/detalle/:orden_produccion", {
                    templateUrl: globalApp.base_url + "../client/src/app/requisiciones_produccion/views/detalle_produccion.html",
                    controller: "detalle_produccion.controller",
                    resolve: {
                        detalle_produccion: function ($location, requisiciones_produccion_model, $route) {
                                    var params = {url_params : {orden_produccion: $route.current.params.orden_produccion}};
                                    return requisiciones_produccion_model.get('detalle_orden_produccion', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }
                    }
                });
        });
})();

