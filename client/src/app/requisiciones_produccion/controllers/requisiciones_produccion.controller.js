(function() {

    "use strict";

    angular.module("Requisiciones_produccion").controller("requisiciones_produccion.controller", 
        function($scope, $http, $location, requisiciones_produccion_model) {
            $scope.open = {};
            $scope.open.week = false;
            $scope.day = '';
            $scope.ordenes_produccion = {};
            $scope.mostrar_listado = {value : false};
            $scope.mensaje_listado = '';
            $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };
            $scope.changeDay = function () {
                    $scope.open.week = true;
                };
            $scope.$watch('week', function () {
                if ($scope.week) {
                    $scope.day = moment($scope.week).format('YYYY-MM-DD');                    
                }
            });

            $scope.buscar_orden_produccion = function () {
                if ($scope.week) {
                    var params = {
                        url_params: {dia: $scope.day}
                        };

                    requisiciones_produccion_model.get('ordenes_produccion', params).then(function (datos) {
                    if (datos.length)
                        $scope.ordenes_produccion = datos;
                    $scope.mostrar_tabla();
                });
                }
            }

            $scope.mostrar_tabla = function () {
                $scope.mostrar_listado.value = false;
                $scope.mensaje_listado = 'No existen Ordenes de Produccion en esta Fecha!';
                if ($scope.ordenes_produccion.length > 0)
                {
                    $scope.mostrar_listado.value = true;
                    $scope.mensaje_listado = '';
                }
            }

            $scope.abrir_orden_produccion = function (orden_produccion) {
                $location.path('requisiciones_produccion/detalle/' + orden_produccion + '/');
            }

        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/requisiciones_produccion/views/requisiciones_produccion.html",
                    controller: "requisiciones_produccion.controller"
                });
        });
})();

