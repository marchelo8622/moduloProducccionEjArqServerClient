(function() {
  "use strict";

  angular.module("Creador_recetas").factory("creador_recetas_model", function(model) {
    return model("creador_recetas");
  });
})();

