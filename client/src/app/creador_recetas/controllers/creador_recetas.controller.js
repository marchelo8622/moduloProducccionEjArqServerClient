(function() {

    "use strict";

    angular.module("Creador_recetas").controller("creador_recetas.controller", 
        function($scope, _, $http, creador_recetas_model, productos_con_receta, productos_para_receta, umedidas, tipos_cocina, bodega_mp) {
            $scope.productos_con_receta = productos_con_receta;
            $scope.productos_para_receta = productos_para_receta;
            $scope.umedidas = umedidas;
            $scope.receta_producto = {
                codigo_producto : '',
                codigo_ingrediente: '',
                descripcion_ingrediente : '',
                unidad_medida : '',
                cantidad_ingrediente : '',
                cantidad_total : '',
                cantidad_unitaria : '',
                costo_ingrediente : '',
                costo_producto : '',
                costo_total : '',
                fecha_ingreso : '',
                tipo_ingrediente : '',
                equivalencia : ''
            };
            $scope.ingredientes_a_eliminar = [];
            $scope.mensaje_receta = '';
            $scope.mostrar_receta = {value : false};
            $scope.tiene_receta = {value : false};
            $scope.cantidad_a_servir = 1;
            $scope.parametros_de_busqueda = {codigo : '', descripcion : ''};
            $scope.modal_buscar_producto = '';
            $scope.producto_seleccionado = {value: false};
            $scope.tipos_cocina = tipos_cocina;
            $scope.total_receta = 0.00;
            $scope.total_receta_por_unidad = 0.00;
            if (bodega_mp == "" || bodega_mp == null || bodega_mp == undefined)
            {
                $scope.$emit('warning', {action: 'No esta configurado Bodega Materia Prima!', element: '', message: ''});
                return;
            }
            $scope.buscar_receta = function () {
                limpiar_totales_receta();
                $scope.modal_buscar_producto = 'modal_buscar_producto';
                $scope.producto_seleccionado.value = true;
                var params = {url_params: {codigo: $scope.producto_con_receta_.codigo}};
                    creador_recetas_model.get('receta_by_producto', params).then(function (datos) {
                        $scope.receta_producto = datos;
                        $scope.mostrar_tabla();
                        $scope.calcular_total_receta();
                    });
                var params = {url_params: {codigo: $scope.producto_con_receta_.codigo}};
                    creador_recetas_model.get('info_producto_receta', params).then(function (datos2) {
                        if (datos2.length > 0)
                        {
                        $scope._cantidad_a_servir = parseInt(datos2[0].sirve_a);
                        $scope.tipo_cocina_ = datos2[0].codigo_cocina;
                            
                        }
                    });
            }
            $scope.guardar_recetas = function () {
                if ($scope.modal_buscar_producto != '')
                {
                    if ($scope.receta_producto.length > 0 && ($scope.tipo_cocina_ == '' || $scope.tipo_cocina_ == undefined) )
                    {
                        $scope.$emit('warning', {action: 'Seleccione tipo de Cocina!', element: '', message: ''});
                        return;
                    }
                    var params_receta = [];
                    params_receta.push(
                            {
                                codigo_producto : $scope.producto_con_receta_.codigo,
                                sirve_a : $scope._cantidad_a_servir,
                                tipo_cocina : $scope.tipo_cocina_
                            }
                    );
                    var params_receta = {
                            data: params_receta[0]
                        };
                        creador_recetas_model.create('receta_producto', params_receta).then(function () {});
                    var params = {
                            data : {  
                                data1 : $scope.receta_producto,
                                data2 : $scope.ingredientes_a_eliminar
                                }
                        };
                        creador_recetas_model.create('creador_recetas', params).then(function () {
                        limpiar_valores();
                        $scope.$emit('success', {action: 'Guardado con exito', element: '', identifier: ''});
                    });
                }

            }
            $scope.eliminar_receta = function () {

                var params = {url_params: {parametro: $scope.producto_con_receta_.codigo}};
                    creador_recetas_model.remove('eliminar_receta', params).then(function (datos) {
                    });
            }
            $scope.eliminar_ingrediente = function (index, producto) {
                $scope.ingredientes_a_eliminar.push({
                    codigo_producto: producto.codigo_producto,
                    codigo_ingrediente: producto.codigo_ingrediente
                });
                $scope.receta_producto.splice(index, 1);
                $scope.calcular_total_receta();
            }

            $scope.seleccionar_producto = function (producto) {
                if ($scope.umedidas.length > 0)
                {
                $scope.receta_producto.push({
                    codigo_producto: $scope.producto_con_receta_.codigo,
                    codigo_ingrediente: producto.codigo,
                    descripcion_ingrediente: producto.descripcion,
                    unidad_medida: $scope.umedidas[0].id_um,
                    cantidad_ingrediente: 0,
                    cantidad_total: 0,
                    cantidad_unitaria : 0,
                    costo_ingrediente: producto.costo,
                    costo_producto: producto.costo,
                    costo_total: 0,
                    fecha_ingreso : '',
                    tipo_ingrediente : '',
                    equivalencia : $scope.umedidas[0].equivalencia
                });
                $scope.mostrar_tabla();
                $scope.calcular_cant_total($scope.receta_producto);
                $scope.parametros_de_busqueda = {codigo : '', descripcion : ''};
                $scope.buscar_productos();
                $scope.calcular_total_receta();
                }else{
                    $scope.$emit('warning', {action: 'No existen Unidades de Medida configuradas!', element: '', message: ''});
                }

                };
            $scope.buscar_productos = function () {

                    if ($scope.parametros_de_busqueda.descripcion == '') {
                        $scope.parametros_de_busqueda.descripcion = null;
                    }

                    if ($scope.parametros_de_busqueda.codigo == '') {
                        $scope.parametros_de_busqueda.codigo = null;
                    }

                    var params = {url_params: {codigo: $scope.parametros_de_busqueda.codigo,
                            descripcion: $scope.parametros_de_busqueda.descripcion}};

                    creador_recetas_model.get('productos_para_receta', params).then(function (datos) {
                        $scope.productos_para_receta = datos;
                    });
                };
            $scope.calcular_cant_total = function (receta) {
                $scope.asignar_cantidad_a_servir();
                if ($scope.cantidad_a_servir != 0)
                {
                $scope.cant_aux = receta.cantidad_ingrediente / receta.equivalencia;
                receta.cantidad_total = $scope.cantidad_a_servir * $scope.cant_aux;
                receta.cantidad_total = receta.cantidad_total.toFixed(6);
                receta.cantidad_unitaria = $scope.cant_aux;
                receta.costo_ingrediente = (receta.cantidad_total / $scope.cantidad_a_servir) * receta.costo_producto;
                receta.costo_ingrediente = receta.costo_ingrediente.toFixed(6);
                receta.costo_total = $scope.cantidad_a_servir * receta.costo_ingrediente;
                receta.costo_total = receta.costo_total.toFixed(6);
                $scope.calcular_total_receta();
                }else{
                    receta.cantidad_total = 0;
                }
            }

            $scope.calcular_cant_unitaria = function (receta) {
                $scope.asignar_cantidad_a_servir();
                if ($scope.cantidad_a_servir != 0)
                {
                $scope.cant_aux = receta.cantidad_total * receta.equivalencia;
                receta.cantidad_ingrediente = $scope.cant_aux / $scope.cantidad_a_servir;
                receta.cantidad_ingrediente = receta.cantidad_ingrediente.toFixed(6);
                receta.cantidad_unitaria = receta.cantidad_ingrediente / receta.equivalencia;
                receta.costo_ingrediente = (receta.cantidad_total / $scope.cantidad_a_servir) * receta.costo_producto;
                receta.costo_ingrediente = receta.costo_ingrediente.toFixed(6);
                receta.costo_total = $scope.cantidad_a_servir * receta.costo_ingrediente;
                receta.costo_total = receta.costo_total.toFixed(6);
                $scope.calcular_total_receta();
                }
            }

            $scope.recalcular_cantidades = function () {
                if ($scope.producto_seleccionado.value)
                {
                _($scope.receta_producto).each(function (receta) {
                        $scope.asignar_cantidad_a_servir();
                        $scope.calcular_cant_total(receta);
                        $scope.calcular_cant_unitaria(receta);
                       
                    });
                $scope.calcular_total_receta();
                }
            }

            $scope.calcular_total_receta = function () {
                if ($scope.receta_producto.length > 0)
                {
                    limpiar_totales_receta();
                    _($scope.receta_producto).each(function (receta) {
                        $scope.total_receta += parseFloat(receta.costo_total);
                        $scope.total_receta_por_unidad += parseFloat(receta.costo_ingrediente);

                    });

                    $scope.total_receta = $scope.total_receta.toFixed(6);
                    $scope.total_receta_por_unidad = $scope.total_receta_por_unidad.toFixed(6);
                }
            }

            $scope.asignar_cantidad_a_servir = function () {
                if ($scope._cantidad_a_servir == '' || $scope._cantidad_a_servir == undefined || $scope._cantidad_a_servir <= 0)
                            $scope.cantidad_a_servir = 0;
                        else
                            $scope.cantidad_a_servir = $scope._cantidad_a_servir;
            }

            $scope.mostrar_tabla = function () {
                $scope.mostrar_receta.value = false;
                $scope.tiene_receta.value = false;
                $scope.mensaje_receta = 'No tiene creada la receta!';
                if ($scope.receta_producto.length > 0)
                {
                    $scope.mostrar_receta.value = true;
                    $scope.tiene_receta.value = true;
                    $scope.mensaje_receta = '';
                }
            }

            $scope.eligio_producto = function () {
                if ($scope.modal_buscar_producto == '')
                    $scope.$emit('warning', {action: 'Seleccione un producto!', element: '', message: ''});
            }

            $scope.cancelar_guardar = function () {
                if ($scope.modal_buscar_producto != '')
                {
                $scope.buscar_receta();
                $scope.receta_producto = {
                    codigo_producto : '',
                    codigo_ingrediente: '',
                    descripcion_ingrediente : '',
                    unidad_medida : '',
                    cantidad_ingrediente : '',
                    cantidad_total : '',
                    costo_ingrediente : '',
                    costo_producto : '',
                    costo_total : '',
                    fecha_ingreso : '',
                    tipo_ingrediente : '',
                    equivalencia : ''
                    };
                $scope.mensaje_receta = '';
                $scope.mostrar_receta = {value : false};
                $scope.tiene_receta = {value : false};
                $scope.cantidad_a_servir = 1;
                $scope.parametros_de_busqueda = {codigo : '', descripcion : ''};
                }
            }

            $scope.imprimir_receta = function () {
                if ($scope.producto_seleccionado.value)
                {
                    $http({
                        method: 'POST',
                        url: full_path('creador_recetas', 'genera_pdf_receta', undefined),
                        data: { codigo: $scope.producto_con_receta_.codigo},
                        responseType: 'arraybuffer'
                    }).then(function (response) {
                        var pdfName = 'receta-'+ $scope.producto_con_receta_.descripcion +'-'+ moment().format('YYYYMMDD') + '.pdf';
                        saveAs(new Blob([response.data], { type: 'application/pdf' }), pdfName);
                    });
                }
            }

            function full_path(resource, action, params) {
                var path = globalApp.base_url + resource + '/api/' + action;
                if (params !== undefined && params.url_params) {
                  _(_.values(params.url_params)).each(function(param) {
                    path += '/' + param;
                  })
                }

                path += document.location.search;

                if (params !== undefined && params.fideicomiso) {
                  path += '&fideicomiso=' + params.fideicomiso;
                }

                return path;
            }

            function limpiar_valores(){
                $scope.ingredientes_a_eliminar = [];
            }

            function limpiar_totales_receta(){
                $scope.total_receta = 0.00;
                $scope.total_receta_por_unidad = 0.00;
            }

            $scope.imprimir_excel = function () {

                if ($scope.producto_seleccionado.value)
                {
                    if ($scope.receta_producto.length > 0)
                    {
                    $http({
                        method: 'POST',
                        url: full_path('creador_recetas', 'genera_excel_receta', undefined),
                        data: { codigo: $scope.producto_con_receta_.codigo},
                        responseType: 'arraybuffer'
                    }).then(function (response) {
                        var pdfName = 'receta-'+ $scope.producto_con_receta_.descripcion +'-'+ moment().format('YYYYMMDD') + '.xlsx';
                        saveAs(new Blob([response.data], { type: 'application/excel' }), pdfName);
                    });
                    }else{
                        $scope.$emit('warning', {action: 'El Producto no tiene Receta!', element: '', message: ''});
                    }
                }

            }
        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/creador_recetas/views/creador_recetas.html",
                    controller: "creador_recetas.controller",
                    resolve: {
                            productos_con_receta: function ($location, creador_recetas_model) {
                                return creador_recetas_model.get('productos_con_recetas')
                                        .catch(function () {
                                            $location.path('/');
                                        });
                            },
                            productos_para_receta: function ($location, creador_recetas_model) {
                                var params = {url_params: {descripcion: null, codigo: null}};
                                return creador_recetas_model.get('productos_para_receta', params)
                                        .catch(function () {
                                            $location.path('/');
                                        });
                            },
                            umedidas: function ($location, creador_recetas_model) {
                                return creador_recetas_model.get('unidades_medidas')
                                        .catch(function () {
                                            $location.path('/');
                                        });
                            },
                            tipos_cocina: function ($location, creador_recetas_model) {
                                return creador_recetas_model.get('tipo_cocinas')
                                        .catch(function () {
                                            $location.path('/');
                                        });
                            },
                            bodega_mp: function ($location, creador_recetas_model) {
                                return creador_recetas_model.get('bodega_costos')
                                        .catch(function () {
                                            $location.path('/');
                                        });
                            }
                        }
                });
        });
})();

