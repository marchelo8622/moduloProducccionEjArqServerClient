(function() {

    "use strict";

    angular.module("Portfee").controller("portfee.controller", function($scope, _, $http, portfee_model) {
        $scope.open = {};
        $scope.open.desde = false;
        $scope.open.hasta = false;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        var date = new Date();
        var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
        $scope.desde = primerDia;
        $scope.hasta = date;
        inicializar_valores();

        $scope.changeDesde = function () {
            $scope.open.desde = true;
        };

        $scope.changeHasta = function () {
            $scope.open.hasta = true;
        };

        $scope.buscar_facturas = function () {
            inicializar_valores();
            if(!$scope.desde)
                $scope.desde = null;

            if(!$scope.hasta)
                $scope.hasta = null;

            if(!$scope.codigo_cliente)
                $scope.codigo_cliente = null;

            var params = {
                    url_params: {
                                    fecha_desde: moment($scope.desde).format('YYYY-MM-DD'), 
                                    fecha_hasta: moment($scope.hasta).format('YYYY-MM-DD'), 
                                    codigo_cliente: $scope.codigo_cliente
                                }
                            };

                    portfee_model.get('facturas', params).then(function (datos) {
                    $scope.facturas_portfee = datos;
                    $scope.numero_facturas = datos.length;
                });

        };

        $scope.checkallChange = function(){
            $scope.checkall = !$scope.checkall;
            
            if ($scope.checkall)
            {
                $scope.datos_facturas.facturas = angular.copy($scope.facturas_portfee);
            }else{
                $scope.datos_facturas.facturas = [];
            }
            calcular_portfee();
        };

        $scope.modificar_listado = function(value, checked) {
            var found = false;
            var copias_fac = angular.copy($scope.datos_facturas.facturas);
            _(copias_fac).map(function (factura, index) {
                if (factura.numero_factura == value.numero_factura)
                {
                    found = true;
                    $scope.datos_facturas.facturas.splice(index, 1);
                    $scope.checkall = false;
                }
            });

            if (!found && checked) {
                $scope.datos_facturas.facturas.push(value);
            }
            calcular_portfee();
          };

        $scope.generar_anticipo = function () {
            var facturas = [];
            _($scope.datos_facturas.facturas).each(function (factura) {
                facturas.push({
                    numero_factura : factura.numero_factura,
                    codigo_cliente : factura.codigo_cliente,
                    valor_portfee: factura.valor_portfee
                });
            });

            var params = {
                    data: {
                                total_portfee: $scope.total_portfee,
                                facturas: facturas 
                            }
                        };

            portfee_model.create('crear_anticipo', params).then(function (datos) {
                if (datos.respuesta)
                {
                    angular.forEach(datos.documentos, function(value, key) {
                        recontabilizar(value.tipo_documento, value.numero_documento, "61", "31");
                    });

                    $scope.$emit('success', {action: 'Creado con exito!', element: 'Anticipo', identifier: ''});
                    $scope.buscar_facturas();

                }else{
                    $scope.$emit('warning', {action: datos.error, element: '', message: ''});
                }

            });

        }

        function calcular_portfee ()
        {
            $scope.total_portfee = 0;
            _($scope.datos_facturas.facturas).each(function (factura) {
                $scope.total_portfee += parseFloat(factura.valor_portfee);
            });
        }

        function inicializar_valores()
        {
            $scope.facturas_portfee = [];
            $scope.numero_facturas = 0;
            $scope.checkall = false;
            $scope.datos_facturas = {
                facturas : []
            };
            $scope.total_portfee = 0;
        }

        function recontabilizar(tipodoc, numdoc, grupo, modulo){
             $.ajax({
                    url: "../../../itprog/it52i.php"+document.location.search,
                    type: "POST",
                    data:{tipodoc:tipodoc,numdoc:numdoc,campo1:tipodoc,grupo:grupo,modulo:modulo,accion:"Procesar"},
                    async:false
                }
            ).responseText;
        }

        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/portfee/views/portfee.html",
                    controller: "portfee.controller"
                });
        });
})();

