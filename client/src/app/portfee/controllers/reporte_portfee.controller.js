(function() {

    "use strict";

    angular.module("Portfee").controller("reporte_portfee.controller", function($scope, _, $http, portfee_model) {
            $scope.open = {};
            $scope.open.desde = false;
            $scope.open.hasta = false;
            $scope.filtro_estado_anticipo = [{
                valor: 'T',
                desc: '- Todos -'
            }, {
                valor: 0,
                desc: 'NO'
            }, {
                valor: 1,
                desc: 'SI'
            }];

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            var date = new Date();
            var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
            $scope.desde = primerDia;
            $scope.hasta = date;
            inicializar_valores();

            $scope.changeDesde = function() {
                $scope.open.desde = true;
            };

            $scope.changeHasta = function() {
                $scope.open.hasta = true;
            };

            $scope.buscar_facturas = function() {
                //console.log($scope.estado_anticipo.filtro_estado_anticipo, 'hola');
                if ($scope.estado_anticipo) {
                    inicializar_valores();
                    if (!$scope.desde)
                        $scope.desde = null;

                    if (!$scope.hasta)
                        $scope.hasta = null;

                    if (!$scope.codigo_cliente)
                        $scope.codigo_cliente = null;

                    var params = {
                        url_params: {
                            fecha_desde: moment($scope.desde).format('YYYY-MM-DD'),
                            fecha_hasta: moment($scope.hasta).format('YYYY-MM-DD'),
                            codigo_cliente: $scope.codigo_cliente,
                            estado: $scope.estado_anticipo.filtro_estado_anticipo
                        }
                    };

                    portfee_model.get('facturas_reporte', params).then(function(datos) {
                        $scope.facturas_portfee = datos;
                        $scope.numero_facturas = datos.length;
                    });
                } else {

                    $scope.$emit('warning', {
                        action: 'Falló la búsqueda!',
                        element: 'El filtro anticipo generado es obligatorio.',
                        message: ''
                    });

                }

            };

            function calcular_portfee() {
                $scope.total_portfee = 0;
                _($scope.datos_facturas.facturas).each(function(factura) {
                    $scope.total_portfee += parseFloat(factura.valor_portfee);
                });
            }

            function inicializar_valores() {
                $scope.facturas_portfee = [];
                $scope.numero_facturas = 0;
            }

            $scope.exportar_excel = function() {

                if ($scope.estado_anticipo) {

                    $http({
                        method: 'POST',
                    url: full_path('portfee', 'genera_excel', undefined),
                        data: {
                            fecha_desde: moment($scope.desde).format('YYYY-MM-DD'),
                            fecha_hasta: moment($scope.hasta).format('YYYY-MM-DD'),
                            codigo_cliente: $scope.codigo_cliente,
                            estado: $scope.estado_anticipo.filtro_estado_anticipo
                        },
                        responseType: 'arraybuffer'
                    }).then(function(response) {
                        var fileName = 'reporte-portfee' + moment(primerDia).format('YYYY-MM-DD') + '.xlsx';
                        saveAs(new Blob([response.data], {
                            type: 'application/excel'
                        }), fileName);
                    });

                } else {

                    $scope.$emit('warning', {
                        action: 'No existe resultados!',
                        element: 'El filtro anticipo generado es obligatorio.',
                        message: ''
                    });

                }

            };

            $scope.exportar_pdf = function() {

                if ($scope.estado_anticipo) {

                    $http({
                        method: 'POST',
                        url: full_path('portfee', 'genera_pdf', undefined),
                        data: {
                            fecha_desde: moment($scope.desde).format('YYYY-MM-DD'),
                            fecha_hasta: moment($scope.hasta).format('YYYY-MM-DD'),
                            codigo_cliente: $scope.codigo_cliente,
                            estado: $scope.estado_anticipo.filtro_estado_anticipo
                        },
                        responseType: 'arraybuffer'
                    }).then(function(response) {
                        var fileName = 'reporte-portfee' + moment(primerDia).format('YYYY-MM-DD') + '.pdf';
                        saveAs(new Blob([response.data], {
                            type: 'application/pdf'
                        }), fileName);
                    });

                } else {

                    $scope.$emit('warning', {
                        action: 'No existe resultados!',
                        element: 'El filtro anticipo generado es obligatorio.',
                        message: ''
                    });

                }

            };
            
            function full_path(resource, action, params) {
                var path = globalApp.base_url + resource + '/api/' + action;
                if (params !== undefined && params.url_params) {
                    _(_.values(params.url_params)).each(function(param) {
                        path += '/' + param;
                    })
                }

                path += document.location.search;

                if (params !== undefined && params.fideicomiso) {
                    path += '&fideicomiso=' + params.fideicomiso;
                }

                return path;
            }

        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/portfee/views/reporte_portfee.html",
                    controller: "reporte_portfee.controller"
                });
        });
})();