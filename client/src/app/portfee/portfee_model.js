(function() {
  "use strict";

  angular.module("Portfee").factory("portfee_model", function(model) {
    return model("portfee");
  });
})();

