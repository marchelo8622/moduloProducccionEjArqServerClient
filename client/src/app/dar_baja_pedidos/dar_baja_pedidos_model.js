(function() {
  "use strict";

  angular.module("Dar_baja_pedidos").factory("dar_baja_pedidos_model", function(model) {
    return model("dar_baja_pedidos");
  });
})();

