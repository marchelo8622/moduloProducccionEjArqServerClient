(function() {

    "use strict";

    angular.module("Dar_baja_pedidos").controller("dar_baja_pedidos.controller", function($scope, $http, _, dar_baja_pedidos_model, pedidos_semidespachados, $location) {

            $scope.datos_pedidos = pedidos_semidespachados;

            $scope.items_seleccionados = [];

            $scope.btnProcesar = false;

            $scope.buscar = function() {

                if ($scope.nombre_cliente || $scope.codigo_cliente || $scope.pedido) {

                    if (!$scope.nombre_cliente) {
                        $scope.nombre_cliente = null;
                    }

                    if (!$scope.codigo_cliente) {
                        $scope.codigo_cliente = null;
                    }

                    if (!$scope.pedido) {
                        $scope.pedido = null;
                    }

                    var params = {
                        url_params: {
                            codigo_cliente: $scope.codigo_cliente,
                            nombre_cliente: $scope.nombre_cliente,
                            pedido: $scope.pedido
                        }
                    };

                    dar_baja_pedidos_model.get('pedidos_semidespachados', params).then(function(response) {
                        $scope.datos_pedidos = response;
                    });

                } else {

                    $scope.$emit('warning', {
                        action: 'No se puede realizar esta accion!',
                        element: 'Ingrese un criterio de búsqueda.',
                        message: ''
                    });

                }
            };

            $scope.refresh = function() {
                window.location.reload();
            };

            $scope.abrir_pedidos = function(numPedido) {
                var url = globalApp.base_url + "../../peprog/pe61fr.php?ID=" + globalApp.id + "&IDB=" + globalApp.idb + "&tipfac=&numdoc=" + numPedido + "&fechaal=&fechadel=";
                window.open(url, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=auto,left=auto,width=auto,height=auto");
            };

            $scope.pedidos_seleccionados = function(checkAll) {
                $scope.datos_pedidos.map(function(value) {
                    value.checked = checkAll.value;
                });

            };


            $scope.procesar = function() {
                _($scope.datos_pedidos).each(function(dato) {
                    if (dato.checked) {
                        $scope.items_seleccionados.push(dato);
                    }
                });

                if ($scope.items_seleccionados.length == 0) {

                    $scope.$emit('warning', {
                        custom_message: 'Seleccione al menos un pedido!.'
                    });

                } else {
                    if (confirm('¿Estas seguro de realizar esta accion?')) {
                        var params = {
                            data: $scope.items_seleccionados
                        };
                        $scope.btnProcesar = true;
                        dar_baja_pedidos_model.create('dar_baja_pedidos', params).then(function(datos) {

                            $scope.$emit('success', {
                                action: 'Pocesado con exito!',
                                element: 'Transacciones de egreso de inventario',
                                identifier: ''
                            });
                            window.location.reload();

                        });
                    }
                }
            };
        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/dar_baja_pedidos/views/dar_baja_pedidos.html",
                    controller: "dar_baja_pedidos.controller",
                    resolve: {
                        pedidos_semidespachados: function($location, dar_baja_pedidos_model) {
                            return dar_baja_pedidos_model.get('pedidos_semidespachados')
                                .catch(function() {
                                    $location.path('/');
                                });
                        }

                    }
                });
        });
})();