(function () {

    "use strict";
    angular.module("Configurar_producto").controller("configurar_producto.controller",
            function ($scope, _, $http, configurar_producto_model, clientes, aviones, cliente, productos) {
                $scope.clientes = clientes;
                $scope.aviones = aviones;
                $scope.productos = productos;
                $scope.base_url = globalApp.base_url;
                $scope.productos_cliente = [];
                $scope.modal_buscar = "";
                $scope.submitted = false;
                $scope.parametros_de_busqueda = {
                    descripcion: null,
                    codigo: null
                };
                var datos_servidor;
                $scope.status = {openedDesde: false, openedHasta: false};
                $scope.desde = function () {
                    $scope.status.openedDesde = true;
                    $scope.status.openedHasta = false;
                };
                $scope.hasta = function () {
                    $scope.status.openedDesde = false;
                    $scope.status.openedHasta = true;
                };
                $scope.toggleMin = function() {
                    $scope.minDate = $scope.minDate ? null : new Date();
                  };

                  $scope.toggleMin();

                $scope.removeProducto = function (index) {
                    $scope.productos_cliente.splice(index, 1);
                };

                $scope.selecciona_cliente = function () {
                    if ($scope.numero_vuelo_ == undefined
                            || $scope.aviones_ == undefined
                            || $scope.clientes_ == undefined
                            ) {
                        return;
                    }
                    var params = {
                        url_params: {
                            cliente: $scope.clientes_,
                            tipo_avion: $scope.aviones_,
                            numero_vuelo: $scope.numero_vuelo_}
                    };

                    configurar_producto_model.get('configurar_producto', params).then(function (datos) {
                        $scope.productos_cliente = datos;

                        if (datos.length) {

                            datos_servidor = get_datos_servidor(datos,
                                    datos[0].id_cliente,
                                    datos[0].id_tipo_avion,
                                    datos[0].numero_vuelo,
                                    datos[0].fecha_inicio,
                                    datos[0].fecha_fin);
                            $scope.desde_ = datos[0].fecha_inicio;
                            $scope.hasta_ = datos[0].fecha_fin;
                        } else {
                            $scope.desde_ = "";
                            $scope.hasta_ = "";
                        }
                    });
                };
                $scope.buscar_productos = function () {

                    if ($scope.parametros_de_busqueda.descripcion == '') {
                        $scope.parametros_de_busqueda.descripcion = null;
                    }

                    if ($scope.parametros_de_busqueda.codigo == '') {
                        $scope.parametros_de_busqueda.codigo = null;
                    }

                    var params = {url_params: {codigo: $scope.parametros_de_busqueda.codigo,
                            descripcion: $scope.parametros_de_busqueda.descripcion}};

                    configurar_producto_model.get('productos_tipo', params).then(function (datos) {
                        $scope.productos = datos;
                    });
                };

                $scope.validate = function () {
                    if (!$scope.modal_buscar){
                        $scope.submitted = true;
                    }
                };

                $scope.seleccionar_producto = function (producto) {
                    $scope.productos_cliente.push({'codigo': producto.codigo,
                        'descripcion': producto.descripcion,
                        'codigo_producto': producto.codigo,
                        'cantidad': 0,
                        'precio': 0});

                };
                watch('numero_vuelo_');
                watch('aviones_');
                watch('clientes_');
                watch('desde_');
                watch('hasta_');

                function watch(inp) {
                    $scope.$watch(inp, function () {
                        if (need_value()) {
                            $scope.modal_buscar = "";
                        } else {
                            $scope.modal_buscar = "modal_buscar";
                        }
                    });


                }
                function need_value() {
                    return ($scope.numero_vuelo_ == undefined
                            || $scope.aviones_ == undefined
                            || $scope.clientes_ == undefined
                            || $scope.desde_ == undefined
                            || $scope.hasta_ == undefined
                            || $scope.numero_vuelo_ == ""
                            || $scope.aviones_ == ""
                            || $scope.clientes_ == ""
                            || $scope.desde_ == ""
                            || $scope.hasta_ == "")
                }

                $scope.cancelar = function () {
                    limpiarValores();
                };

                function limpiarValores() {
                    $scope.aviones_ = "";
                    $scope.numero_vuelo_ = "";
                    $scope.desde_ = "";
                    $scope.hasta_ = "";
                    $scope.productos_cliente = "";
                    $scope.clientes_ = "";

                }

                $scope.guardar = function () {
                    var params = {
                        data: datos()
                    };

                    if (!_.isEqual(datos(), datos_servidor)) {
                        configurar_producto_model.create('guardar_configuracion', params).then(function () {
                            $scope.$emit('success', {action: 'actualizados', element: 'Productos', identifier: ''});
                            datos_servidor = datos();
                        });
                    } else {
                        $scope.$emit('warning', {custom_message: 'No hay datos modificados.'});
                    }
                    limpiarValores();
                };

                function datos() {
                    var data = [];
                    _($scope.productos_cliente).each(function (producto) {
                        data.push({'codigo': producto.codigo,
                            'descripcion': producto.descripcion,
                            'codigo_producto': producto.codigo_producto,
                            'cantidad': producto.cantidad,
                            'precio': producto.precio,
                            'id_cliente': $scope.clientes_,
                            'id_tipo_avion': $scope.aviones_,
                            'numero_vuelo': $scope.numero_vuelo_,
                            'fecha_inicio': moment($scope.desde_).format('YYYY-MM-DD'),
                            'fecha_fin': moment($scope.hasta_).format('YYYY-MM-DD')
                        });
                    });
                    return data;
                }
                ;


                function get_datos_servidor(datos, id_cliente,
                        id_tipo_avion,
                        numero_vuelo,
                        fecha_inicio,
                        fecha_fin) {
                    var data = [];
                    _(datos).each(function (producto) {
                        data.push({'codigo': producto.codigo,
                            'descripcion': producto.descripcion,
                            'codigo_producto': producto.codigo_producto,
                            'cantidad': producto.cantidad,
                            'precio': producto.precio,
                            'id_cliente': id_cliente,
                            'id_tipo_avion': id_tipo_avion,
                            'numero_vuelo': numero_vuelo,
                            'fecha_inicio': fecha_inicio,
                            'fecha_fin': fecha_fin
                        });
                    });
                    return data;
                }
                ;
            })
            .config(function ($routeProvider) {
                $routeProvider
                        .when("/", {
                            templateUrl: globalApp.base_url + "../client/src/app/configurar_producto/views/configurar_producto.html",
                            controller: "configurar_producto.controller",
                            resolve: {
                                clientes: function ($location, configurar_producto_model) {
                                    return configurar_producto_model.get('clientes')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                aviones: function ($location, configurar_producto_model) {
                                    return configurar_producto_model.get('avion')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                cliente: function ($location, configurar_producto_model) {
                                    return configurar_producto_model.create('cliente', {data: "1"})
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                productos: function ($location, configurar_producto_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return configurar_producto_model.get('productos_tipo', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }
                            }
                        });
            });
})();

