(function() {
  "use strict";

  angular.module("Reporte_dispatch").factory("reporte_dispatch_model", function(model) {
    return model("reporte_dispatch");
  });
})();

