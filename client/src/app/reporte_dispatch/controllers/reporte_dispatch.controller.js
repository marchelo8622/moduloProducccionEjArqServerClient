(function() {

    "use strict";

    angular.module("Reporte_dispatch").controller("reporte_dispatch.controller", function($scope, _, $http, reporte_dispatch_model, clientes, productos) {
            $scope.clientes = clientes;
            $scope.productos = productos;
            $scope.open = {};
            $scope.open.dayDispatch = false;
            $scope.changeDayDispatch = function() {
                $scope.open.dayDispatch = true;
            };

            $scope.filtro_orden = [{
                valor: 'maecte.nomcte01',
                desc: 'Nombre Cliente -> Asc'
            }, {
                valor: 'maepro.desprod01',
                desc: 'Nom. Prod -> Asc'
            }, {
                valor: 'fecha_dispatch',
                desc: 'Fecha Despacho -> Asc'
            }];

            $scope.buscar = function() {

                if ($scope.dayDispatch) {
                    $scope.fecha = moment($scope.dayDispatch).format('YYYY-MM-DD');

                    var cliente = undefined;
                    if ($scope.dispatch_productos) {
                        cliente = $scope.dispatch_productos.clientes;
                    }
                    var producto = undefined;
                    if ($scope.dispatch_productos) {
                        producto = $scope.dispatch_productos.productos;
                    }
                    var filtro_orden = undefined;
                    if ($scope.dispatch_productos) {
                        filtro_orden = $scope.dispatch_productos.filtro_orden;
                    }
                    var params = {
                        url_params: {
                            dia: $scope.fecha,
                            cliente: cliente,
                            producto: producto,
                            filtro_orden: filtro_orden
                        }
                    };

                    reporte_dispatch_model.get('dispatch_filtros', params).then(function(response) {
                        $scope.datos_dispatch = response;
                    });
                } else {

                    $scope.$emit('warning', {
                        action: 'No existe resultados!',
                        element: 'El filtro dia es obligatorio.',
                        message: ''
                    });

                }

            };

            $scope.refresh = function() {
                window.location.reload();
            };

            function full_path(resource, action, params) {
                var path = globalApp.base_url + resource + '/api/' + action;
                if (params !== undefined && params.url_params) {
                    _(_.values(params.url_params)).each(function(param) {
                        path += '/' + param;
                    })
                }

                path += document.location.search;

                if (params !== undefined && params.fideicomiso) {
                    path += '&fideicomiso=' + params.fideicomiso;
                }

                return path;
            }

            $scope.exportar_excel = function() {

                if ($scope.dayDispatch) {
                    $scope.fecha = moment($scope.dayDispatch).format('YYYY-MM-DD');
                    var cliente = 'undefined';
                    if ($scope.dispatch_productos) {
                        if ($scope.dispatch_productos.clientes) {
                            cliente = $scope.dispatch_productos.clientes;
                        } else {
                            cliente = 'undefined';
                        }
                    }

                    var producto = 'undefined';
                    if ($scope.dispatch_productos) {
                        if ($scope.dispatch_productos.productos) {
                            producto = $scope.dispatch_productos.productos;
                        } else {

                            producto = 'undefined';

                        }
                    }

                    var filtro_orden = 'undefined';
                    if ($scope.dispatch_productos) {
                        if ($scope.dispatch_productos.filtro_orden) {
                            filtro_orden = $scope.dispatch_productos.filtro_orden;
                        } else {
                            filtro_orden = 'undefined';
                        }
                    }

                    $http({
                        method: 'POST',
                    url: full_path('reporte_dispatch', 'genera_excel_despacho', undefined),
                        data: {
                            cliente: cliente,
                            producto: producto,
                            fecha: $scope.fecha,
                            filtro_orden: filtro_orden
                        },
                        responseType: 'arraybuffer'
                    }).then(function(response) {
                        var fileName = 'reporte-despacho'+ $scope.fecha + '.xlsx';
                        saveAs(new Blob([response.data], { type: 'application/excel' }), fileName);
                    });

                } else {

                    $scope.$emit('warning', {
                        action: 'No existe resultados!',
                        element: 'El filtro dia es obligatorio.',
                        message: ''
                    });

                }

            };


        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/reporte_dispatch/views/reporte_dispatch.html",
                    controller: "reporte_dispatch.controller",
                    resolve: {
                        clientes: function($location, reporte_dispatch_model) {
                            return reporte_dispatch_model.get('clientes')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        productos: function($location, reporte_dispatch_model) {
                            var params = {
                                url_params: {
                                    descripcion: null,
                                    codigo: null
                                }
                            };
                            return reporte_dispatch_model.get('productos_tipo', params)
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                    }
                });
        });
})();