(function() {
  "use strict";

  angular.module("Configurador_umedidas").factory("configurador_umedidas_model", function(model) {
    return model("configurador_umedidas");
  });
})();

