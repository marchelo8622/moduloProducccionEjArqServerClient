(function() {

    "use strict";

    angular.module("Configurador_umedidas").controller("configurador_umedidas.controller", 
        function($scope, $http, configurador_umedidas_model, umedidas) {
            $scope.umedidas = umedidas;
            $scope.modal_crear_um = "";
            $scope.parametros_de_busqueda = {
                nombre_umedida: null
            };

            $scope.modal_guardar = {um_principal_nombre : '', 
                                    um_principal_abreviatura: '',
                                    um_secundaria_nombre: '',
                                    um_secundaria_abreviatura: '',
                                    equivalencia: '',
                                    id_um: ''};

            $scope.buscar_umedidas = function () {
                    var param = $scope.parametros_de_busqueda.nombre_umedida;
                if (param != '' && param != null && param != undefined) {
                    var params = {url_params: {descripcion: $scope.parametros_de_busqueda.nombre_umedida}};
                    configurador_umedidas_model.get('buscar_unidades_medidas', params).then(function (datos) {
                        console.log(datos);
                        $scope.umedidas = datos;
                    });
                }else{
                    configurador_umedidas_model.get('unidades_medidas').then(function (datos) {
                        console.log(datos);
                        $scope.umedidas = datos;
                    });
                }
            };

            $scope.cancelar_guardar = function () {
                    limpiarValores();
                };

            $scope.cancelar_actualizar = function () {
                    limpiarValores();
                    $('#modal_editar_umedida').modal('hide');
                };

            function limpiarValores() {
                $scope.modal_guardar = {um_principal_nombre : '', 
                                    um_principal_abreviatura: '',
                                    um_secundaria_nombre: '',
                                    um_secundaria_abreviatura: '',
                                    equivalencia: '',
                                    id_um: ''};
            }

            $scope.guardar_umedida = function () {
            if (!validar_datos_um())
            {

                var params = [];
                params.push(
                        {
                            unidad_medida_principal : $scope.modal_guardar.um_principal_nombre,
                            abreviatura_um_principal : $scope.modal_guardar.um_principal_abreviatura,
                            unidad_medida_secundaria : $scope.modal_guardar.um_secundaria_nombre,
                            abreviatura_um_secundaria : $scope.modal_guardar.um_secundaria_abreviatura,
                            equivalencia : $scope.modal_guardar.equivalencia
                        }
                );
                var params = {
                        data: params[0]
                    };
                configurador_umedidas_model.create('unidades_medidas', params).then(function () {
                    $scope.$emit('success', {action: 'Guardado con exito', element: '', identifier: ''});
                    limpiarValores();

                });
                $scope.buscar_umedidas();
            }else{
                    $scope.$emit('warning', {action: 'Los campos con (*) son obligatorios', element: '', message: ''});                
            }
            }

            $scope.eliminar_um = function (umedida) {
                var params = {url_params: {codigo: umedida.id_um}};
                    configurador_umedidas_model.get('unidad_medida_utilizada', params).then(function (datos) {
                        if (parseInt( datos ) <= 0)
                        {
                            var accion = confirm('Desea Eliminar ?');
                            if (accion)
                            {
                            var params = {url_params: {parametro: umedida.id_um}};
                                configurador_umedidas_model.remove('unidades_medidas', params).then(function (datos) {
                                    $scope.$emit('success', {action: 'Eliminado con exito', element: '', identifier: ''});
                                    $scope.buscar_umedidas();
                                });
                            }
                        }else{
                            $scope.$emit('warning', {action: 'No se puede eliminar', element: umedida.unidad_medida_principal, message: 'Esta siendo usada en una receta'});
                        }
                    
                    });
            }

            $scope.editar_umedida = function () {

                if (!validar_datos_um())
                {

                    var params = [];
                    params.push(
                        {
                            id_um : $scope.modal_guardar.id_um,
                            unidad_medida_principal : $scope.modal_guardar.um_principal_nombre,
                            abreviatura_um_principal : $scope.modal_guardar.um_principal_abreviatura,
                            unidad_medida_secundaria : $scope.modal_guardar.um_secundaria_nombre,
                            abreviatura_um_secundaria : $scope.modal_guardar.um_secundaria_abreviatura,
                            equivalencia : $scope.modal_guardar.equivalencia
                        }
                    );
                    var params = {
                            data: params[0]
                        };
                    configurador_umedidas_model.create('actualizar_unidades_medidas', params).then(function () {
                        $scope.$emit('success', {action: 'Actualizado con exito', element: '', identifier: ''});
                        $('#modal_editar_umedida').modal('hide');
                        limpiarValores();
                    });
                    $scope.buscar_umedidas();
                }else{
                        $scope.$emit('warning', {action: 'Los campos con (*) son obligatorios', element: '', message: ''});                
                }
                
            }

            $scope.cargar_datos = function (umedida) {
                $scope.modal_guardar = {um_principal_nombre : umedida.unidad_medida_principal, 
                                    um_principal_abreviatura: umedida.abreviatura_um_principal,
                                    um_secundaria_nombre: umedida.unidad_medida_secundaria,
                                    um_secundaria_abreviatura: umedida.abreviatura_um_secundaria,
                                    equivalencia: umedida.equivalencia,
                                    id_um: umedida. id_um};
            }
            
            function validar_datos_um()
            {
                return ($scope.modal_guardar.um_principal_nombre == undefined
                        || $scope.modal_guardar.um_principal_abreviatura == undefined
                        || $scope.modal_guardar.um_secundaria_nombre == undefined
                        || $scope.modal_guardar.um_secundaria_abreviatura == undefined
                        || $scope.modal_guardar.equivalencia == undefined
                        || $scope.modal_guardar.um_principal_nombre == ''
                        || $scope.modal_guardar.um_principal_abreviatura == ''
                        || $scope.modal_guardar.um_secundaria_nombre == ''
                        || $scope.modal_guardar.um_secundaria_abreviatura == ''
                        || $scope.modal_guardar.equivalencia == ''
                        )
            }

        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/configurador_umedidas/views/configurador_umedidas.html",
                    controller: "configurador_umedidas.controller",
                    resolve: {
                            umedidas: function ($location, configurador_umedidas_model) {
                                return configurador_umedidas_model.get('unidades_medidas')
                                        .catch(function () {
                                            $location.path('/');
                                        });
                            }
                        }
                });
        });
})();

