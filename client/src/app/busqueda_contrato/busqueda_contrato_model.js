(function() {
  "use strict";

  angular.module("Busqueda_contrato").factory("busqueda_contrato_model", function(model) {
    return model("busqueda_contrato");
  });
})();

