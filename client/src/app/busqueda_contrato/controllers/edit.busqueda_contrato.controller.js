(function() {

    "use strict";

    angular.module("Busqueda_contrato").controller("edit.busqueda_contrato.controller", 
        function($scope, _, $http, $location, busqueda_contrato_model, clientes, data_edit, aviones, productos, categoriabycliente) {
                console.log(data_edit);
                $scope.accion = 0;
                $scope.codCategoria     =   categoriabycliente[0].codcate;
                $scope.aliasCategoria   =   categoriabycliente[0].alias;
                $scope.clientes         =   clientes;                
                $scope.contratos        =   data_edit;
                $scope.aviones          =   aviones;
                $scope.productos        =   productos;
                $scope.status           =   {openedD: false, openedH: false}; 
                $scope.cliente          =    $scope.contratos[0].codcliente;
                $scope.avion            =   $scope.contratos[0].tipoAvion;                
                $scope.catefinal        =   $scope.contratos[0].categoria;
                $scope.idContrato       =   $scope.contratos[0].idContrato;
                $scope.finicio          =   new Date($scope.contratos[0].finicio);
                $scope.ffin             =   new Date($scope.contratos[0].ffin);
                $scope.finicio.setDate($scope.finicio.getDate()+1);
                $scope.ffin.setDate($scope.ffin.getDate()+1); 
                $scope.vuelo            =   parseInt($scope.contratos[0].numVuelo);
                $scope.tipoAvion        =   $scope.contratos[0].tipoAvion;
                $scope.duracion         =   parseInt($scope.contratos[0].duracion);

                $scope.ciclos           =   parseInt($scope.contratos[0].ciclos);
                if(aviones.length > 0){
                    $scope.cantidadBc       =   parseInt($scope.aviones[0].ad1tab);
                    $scope.cantidadEc       =   parseInt($scope.aviones[0].ad2tab);
                    $scope.cantidadTm       =   parseInt($scope.aviones[0].ad3tab);
                    $scope.cantidadPi       =   parseInt($scope.aviones[0].ad4tab);
                    $scope.cantidadCo       =   parseInt($scope.aviones[0].ad5tab);
                }else{

                    $scope.cantidadBc       =   0;
                    $scope.cantidadEc       =   0;
                    $scope.cantidadTm       =   0;
                    $scope.cantidadPi       =   0;
                    $scope.cantidadCo       =   0;


                }


                $scope.descripcion      =   $scope.contratos[0].descripcion;
                $scope.inputCounter     =   0;
                $scope.disableSelect = true;
                $scope.disableSelectg = false;
                $scope.productos_industrial = [];
                $scope.parametros_de_busqueda = {
                    descripcion: null,
                    codigo: null
                };

               $scope.ciclos_opciones = [{valor:7,desc:'Semanas'},{valor:1,desc:'Dias'}];
               
                var lunes =  $scope.contratos[0].lunes;
                var martes =    $scope.contratos[0].martes;
                var miercoles =    $scope.contratos[0].miercoles;
                var jueves =    $scope.contratos[0].jueves;
                var viernes =    $scope.contratos[0].viernes;
                var sabado =   $scope.contratos[0].sabado;
                var domingo =    $scope.contratos[0].domingo;

                var diasPre = {
                    lunes, martes, miercoles, jueves, viernes, sabado, domingo
                }
                
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                $scope.cancelar = function () {
                    window.location.reload();
                };

                $scope.limpiarValores = function() {
                     window.location.reload();
                }
                $scope.retornar_pagina = function(){
                    
                    $location.path('/');
                }

        /* Declaraciones necesarias para el datepicker*/               
                $scope.desde = function () {
                    $scope.status.openedDesde = true;
                    $scope.status.openedHasta = false;
                };
                $scope.hasta = function () {
                    $scope.status.openedDesde = false;
                    $scope.status.openedHasta = true;
                };

                $scope.status = {
                    opened: false
                };

                $scope.setDate = function(year, month, week, day) {
                    $scope.dt = new Date(year, month, week, day);
                };

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];
                $scope.status = {
                    opened: false
                };

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                var afterTomorrow = new Date();
                afterTomorrow.setDate(tomorrow.getDate() + 2);
                $scope.events =
                  [
                    {
                      date: tomorrow,
                        status: 'full'
                    },
                    {
                      date: afterTomorrow,
                        status: 'partially'
                    }
                  ];
                $scope.toggleMin = function() {
                    $scope.minDate = $scope.minDate ? null : new Date();
                  };

                  $scope.toggleMin();

                $scope.getDayClass = function(date, mode) {
                    if (mode === 'day') {
                      var dayToCheck = new Date(date).setHours(0,0,0,1);
                      for (var i=0;i<$scope.events.length;i++){
                        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
                        if (dayToCheck === currentDay) {
                          return $scope.events[i].status;
                        }
                      }
                    }
                    return '';
                };

                $scope.selecciona_cliente = function () {
                    if ($scope.cliente == undefined) {
                        return;
                    }else{

                        alert('No se puede modificar el cliente.');

                        //$scope.cliente = $scope.cliente;
                    }
                    
                   /* var params = {
                        url_params: {
                            cliente: $scope.contratos[0].codCliente
                            }
                    };


                    busqueda_contrato_model.get('categoria_by_client', params).then(function(datos){
                        $scope.categorias = datos;
                        //$scope.catefinal = $scope.categorias[0].desccate;
                        $scope.codCategoria = $scope.categorias[0].codcate;
                        $scope.aliasCategoria = $scope.categorias[0].alias;
                        
                    });
                    busqueda_contrato_model.get('contrato_by_client', params).then(function(datos){
                        $scope.idContrato = datos;
                    });*/

                };
                $scope.configuracion = function(producto){
                        $scope.show=true;
                        var params = {url_params : {
                            numVuelo: $scope.vuelo, 
                            tipoAvion: $scope.avion,
                            idProducto: producto.codigo_producto                  
                        }    
                    };
                    busqueda_contrato_model.get('configuracion',params).then(function(datos){
                         if(datos.length > 0){
                             var productos = _($scope.productos).findWhere({codigo:producto.codigo_producto});
                             producto.confiCantidad = datos[0].cantidad;
                             producto.confiPrecio =   datos[0].precio;
                             producto.confiCodProducto = datos[0].codigo_producto;
                             producto.confiDescripcion = datos[0].descripcion;

                         }else{
                            var productos = _($scope.productos).findWhere({codigo:producto.codigo_producto});
                             producto.confiCantidad = datos[0].cantidad;
                             producto.confiPrecio =   datos[0].precio;
                             producto.confiCodProducto = datos[0].codigo_producto;
                             producto.confiDescripcion = datos[0].descripcion;
                         }

                    })
                };

/*Objeto dias para los dias de la semana*/
                $scope.dias = {                    
                                Lunes: false, 
                                Martes: false, 
                                Miercoles: false, 
                                Jueves: false, 
                                Viernes: false, 
                                Sabado: false, 
                                Domingo: false
                               };

/*Agregar Frecuencias, Productos y Opciones*/
                $scope.inputCounter = 0;
                $scope.inputs = [{
                        id: 'input'
                }];

                $scope.inputCounter2 = 1;
                $scope.inputs2 =[{
                    id: 'input2'
                }];
                
                $scope.frecuencias = [];
                
                var datos1 = {
                    id: 0,
                    numero:0,
                    productos: []
                  };

                $scope.frecuencias.push(datos1);

                build_frecuencias($scope.contratos);
                $scope.addFrecuencias = function() {                 
                    var datos2 = {
                    id: 1,
                    numero:1,
                    productos: []
                    };
                  $scope.inputCounter += 1;
                  $scope.frecuencias.push(datos2);               
                };

                $scope.addOpciones = function(productos) {
                  $scope.inputCounter2 += 1;
                  //productos.opciones.push({});
                  var params = {url_params : {
                            idCliente: $scope.cliente,
                            idProducto: productos.codigo_producto,
                            numVuelo: $scope.vuelo, 
                            tipoAvion: $scope.avion                   
                        }    
                    };
                busqueda_contrato_model.get('retornaPrecio',params).then(function(datos){
    
                            
                            productos.opciones.push({cantidad: 0,
                                            precio:0, 
                                            clientePrecio:Number(datos.tipo_precio).toFixed(2)});
                            
                    })
                };

                $scope.removeOpciones = function (productos, idProducto) {
                    productos.opciones.splice(idProducto, 1);
                };

                $scope.removeFrecuencias = function (idFrecuencia) {
                    $scope.frecuencias.splice(idFrecuencia, 1);
                };
                
                $scope.buscar_productos = function (flag) {

                    if ($scope.parametros_de_busqueda.descripcion == '') {
                        $scope.parametros_de_busqueda.descripcion = null;
                    }

                    if ($scope.parametros_de_busqueda.codigo == '') {
                        $scope.parametros_de_busqueda.codigo = null;
                    }

                    var params = {url_params: {
                            descripcion: $scope.parametros_de_busqueda.descripcion,
                            codigo: $scope.parametros_de_busqueda.codigo,                            
                            aliascategorias: $scope.aliasCategoria
                        }};

                    busqueda_contrato_model.get('filtro_productos_by_categoria', params).then(function (datos) {
                        $scope.productoscat = datos;
                    });
                };

                $scope.seleccionar_producto = function (index, tipo, producto) {
                    if(tipo == 'fp'){
                        $scope.frecuencias[index].productos.push({
                            'descripcion': producto.descripcion,
                            'codigo_producto': producto.codigo,
                            'opciones': []});
                    }else{
                        $scope.productos_industrial.push({
                            'descripcion': producto.descripcion,
                            'codigo_producto': producto.codigo,
                            'opcion': '',
                            'cantidad': 0
                            });
                    }
                };

                

                $scope.indiceSelect = function (index, tipo){
                    $scope.indiceFrecuencia = index;
                    $scope.tipo =tipo;
                    $scope.disableSelect = true;

                    if($scope.codCategoria == '001'){

                        var params = {url_params : {productoscate: $scope.codCategoria, aliascategorias: $scope.aliasCategoria,cliente:$scope.cliente}};
                           // console.log(params);
                        busqueda_contrato_model.get('productos_by_categoria2', params).then(function (productosc){
                            $scope.productoscat = productosc;
                        });
                    }else{


                        var params = {url_params : {productoscate: $scope.codCategoria, aliascategorias: $scope.aliasCategoria}};
                        busqueda_contrato_model.get('productos_by_categoria', params).then(function (productosc){
                            $scope.productoscat = productosc;
                            //console.log($scope.productos);
                        });

                   
                    }

                };

                $scope.removeProducto = function (array, idProducto) {
                    if($scope.tipo == 'fp'){
                        array.productos.splice(idProducto, 1);
                    }else{
                        array.splice(idProducto, 1);
                    }
                };


                $scope.guardar = function(){
                    if($scope.idContrato != undefined && $scope.catefinal != undefined && $scope.finicio && $scope.ffin){
                                
                            if($scope.codCategoria == '001'){ // cat aerolinea
                           
                                if(!$scope.ciclos || !$scope.avion || !$scope.vuelo || !$scope.duracion){

                                    alert('# de vuelo, tipo avion, duracion de ciclo son obligatorios para esta categoria.');

                                }else{

                                    ejecuta_guardar();
                                }

                            }else{  
                                    ejecuta_guardar();
                            }
                               
                        
                    }else{
                        alert("Los campos con (*) son obligatorios");
                    }
                }

                function ejecuta_guardar(){
                     var params = {
                                    data: datos(),
                                    url_params: {idContrato:$scope.idContrato, codcliente: $scope.cliente}
                                };

                                var resp_valid = $scope.valida_detalle_contrato();                         

                                if(resp_valid != ''){

                                    alert(resp_valid);

                                }else{
                    
                                    busqueda_contrato_model.update('editar_contratos', params).then(function (response) {
                          
                                        $scope.$emit('success', {custom_message: 'Datos modificados.'});
                                             //window.location.reload();
                                     });
                                    
                                       
                                }

                }

                 $scope.valida_detalle_contrato = function() {

                        
                          var msj = '';
                         if($scope.frecuencias.length > 0){
                              
                              console.log($scope.frecuencias,'data');

                            _($scope.frecuencias).each(function (frecuencia, index) {
                                //_(frecuencia.input).each(function (input, key) {

                                    if(!frecuencia.input){

                                        msj += 'Seleccione por lo menos un dia de la semana en la frecuencia #' + index + '.\n';

                                    }
                                                             
                                //});
                            });

                             if(msj == ''){
                                _($scope.frecuencias).each(function (frecuencia, index) {
                                    if(frecuencia.productos.length == 0){

                                         msj += 'En la frecuencia #'+index+' no se encuentra igresado productos.\n';

                                    }else{

                                        _(frecuencia.productos).each(function (producto, indiceProducto) {                             
                                            if(producto.opciones.length == 0){

                                                msj += '\nEn alguno de los productos no se encuentra igresado opciones.\n';

                                            } else{

                                                 
                                                _(producto.opciones).each(function (opciones, key) {

                                                       if(opciones.cantidad <= 0 ){
                                                           msj += 'En la frecuencia #'+index+' en el producto '+producto.descripcion+' opcion '+(key+1)+', la cantidad tiene que ser mayor a cero.\n';
                                                       }

                                                       if(opciones.clientePrecio <= 0){
                                                           msj += 'En la frecuencia #'+index+' en el producto '+producto.descripcion+' opcion '+(key+1)+', el precio tiene que ser mayor a cero.\n';
                                                       }
                                                 });

                                            }                            
                                        });


                                    }                       
                                });

                             }


                         }else{

                            msj += 'Debe ingresar por lo menos una frecuencia.';
                         }


                         return msj;

                    };



                    $scope.valida_frecuencias = function(valor,dia,frecu) {
                    //alert('hola');
 
                    switch (dia) {
                        case 'Lunes':
                           var obj = {Lunes:valor};
                           break
                        case 'Martes':
                           var obj = {Martes:valor};
                           break
                        case 'Miercoles':
                           var obj = {Miercoles:valor};
                           break
                        case 'Jueves':
                           var obj = {Jueves:valor};
                           break
                        case 'Viernes':
                           var obj = {Viernes:valor};
                           break
                        case 'Sabado':
                           var obj = {Sabado:valor};
                           break
                        case 'Domingo':
                           var obj = {Domingo:valor};
                           break
                    }

                    var cont = 0;
                    
                    var data_frec = [];
                    if(valor == true){
                       
                        _($scope.frecuencias).each(function (frecuencia, index) {
                                  if(frecu != index){
                                    data_frec.push(frecuencia.input);
                                  }
                        });

                        if(data_frec.length > 0){

                             var dia_seleccionado = _.findWhere(data_frec, obj);                                
                             
                                 if(dia_seleccionado){
                                   
                                    cont = cont + 1;

                                 }
                            
                        }
                     }
                    if(cont > 0){

                        alert('Usted acaba de cambiar un dia que ya fue seleccionado en otra frecuencia.');
                        switch (dia) {
                                        case 'Lunes':
                                           dia_seleccionado.Lunes = false;
                                           break
                                        case 'Martes':
                                           dia_seleccionado.Martes = false;
                                           break
                                        case 'Miercoles':
                                           dia_seleccionado.Miercoles = false;
                                           break
                                        case 'Jueves':
                                           dia_seleccionado.Jueves = false;
                                           break
                                        case 'Viernes':
                                           dia_seleccionado.Viernes = false;
                                           break
                                        case 'Sabado':
                                           dia_seleccionado.Sabado = false;
                                           break
                                        case 'Domingo':
                                           dia_seleccionado.Domingo = false;
                                           break
                                    } 
                        

                    }
                 };

                
                function datos() {
                    var data = {
                        'data_clienteContrato': data_clienteContrato(),
                        'data_contratos': data_contratos(),
                        'data_frecuencias': data_frecuencias(),
                        'data_productos': data_productos(),
                        'data_opciones': data_opciones()
                    };

                    //console.log(data);

                    return data;
                }
                function data_clienteContrato(){
                            if($scope.cliente == ''){$scope.cliente = null }else{$scope.cliente= $scope.cliente};
                            if($scope.vuelo == undefined){$scope.vuelo = 0 }else{$scope.vuelo = $scope.vuelo};
                            if($scope.avion == undefined ){$scope.avion = '00' }else{$scope.avion = $scope.avion};
                            if($scope.ciclos == undefined){$scope.ciclos = 0 }else{$scope.ciclos = $scope.ciclos};
                            if($scope.duracion == undefined){$scope.duracion = 0 }else{$scope.duracion = $scope.duracion};
                            if($scope.idContrato == undefined){$scope.idContrato = 0 }else{$scope.idContrato = $scope.idContrato};
                    return{
                            idCliente: $scope.cliente,
                            numVuelo: $scope.vuelo,
                            tipoAvion: $scope.avion,
                            ciclos: $scope.ciclos,
                            duracion: $scope.duracion,
                            idContrato : $scope.idContrato
                            
                         }
            }
                function data_contratos(){
                            if($scope.cliente == ''){$scope.cliente = null }else{$scope.cliente = $scope.cliente};
                            if($scope.vuelo == undefined){$scope.vuelo = 0 }else{$scope.vuelo = $scope.vuelo};
                            if($scope.avion == undefined ){$scope.avion = '00' }else{$scope.avion = $scope.avion};
                            if($scope.ciclos == undefined){$scope.ciclos = 0 }else{$scope.ciclos = $scope.ciclos};
                            if($scope.catefinal == undefined){$scope.catefinal = 0 }else{$scope.catefinal = $scope.catefinal};
                            if($scope.idContrato == undefined){$scope.idContrato = 0 }else{$scope.idContrato = $scope.idContrato};
                            
                    return {
                            codcliente: $scope.cliente,
                            numVuelo: $scope.vuelo,
                            tipoAvion: $scope.avion,
                            cantCiclos: $scope.ciclos,
                            categoria: $scope.catefinal,
                            finicio: moment($scope.finicio).format('YYYY-MM-DD'),
                            ffin: moment($scope.ffin).format('YYYY-MM-DD'),
                            idContrato : $scope.idContrato
                        }
                }

                function data_frecuencias() {
                    var data_frecuencia = [];
                    _($scope.frecuencias).each(function (frecuencia, index) {
                        var dias = moment($scope.ffin).diff(moment($scope.finicio),'days');
                                       if(frecuencia.input){                       

                                            if($scope.cliente == ''){$scope.cliente = null }else{$scope.cliente = $scope.cliente};
                                            if($scope.vuelo == undefined){$scope.vuelo = 0 }else{$scope.vuelo = $scope.vuelo};
                                            if($scope.avion == undefined ){$scope.avion = '00' }else{$scope.avion = $scope.avion};
                                            if(dias == ''){dias= 0 }else{dias= dias};
                                            if(frecuencia.input.Lunes == undefined){frecuencia.input.Lunes = 0}else{frecuencia.input.Lunes = frecuencia.input.Lunes};
                                            if(frecuencia.input.Martes == undefined){frecuencia.input.Martes = 0}else{frecuencia.input.Martes = frecuencia.input.Martes};
                                            if(frecuencia.input.Miercoles == undefined){frecuencia.input.Miercoles = 0}else{frecuencia.input.Miercoles = frecuencia.input.Miercoles};
                                            if(frecuencia.input.Jueves == undefined){frecuencia.input.Jueves = 0}else{frecuencia.input.Jueves = frecuencia.input.Jueves};
                                            if(frecuencia.input.Viernes == undefined){frecuencia.input.Viernes = 0}else{frecuencia.input.Viernes = frecuencia.input.Viernes};
                                            if(frecuencia.input.Sabado == undefined){frecuencia.input.Sabado = 0}else{frecuencia.input.Sabado = frecuencia.input.Sabado};
                                            if(frecuencia.input.Domingo == undefined){frecuencia.input.Domingo = 0}else{frecuencia.input.Domingo = frecuencia.input.Domingo};
                                            
                                            data_frecuencia.push({
                                                                idCliente: $scope.cliente,
                                                                numVuelo: $scope.vuelo,
                                                                tipoAvion: $scope.avion,
                                                                dias: dias,
                                                                desde: moment($scope.finicio).format('YYYY-MM-DD'),
                                                                hasta: moment($scope.ffin).format('YYYY-MM-DD'),
                                                                idFrecuencia: index,
                                                                idContrato : $scope.idContrato,
                                                                lunes: frecuencia.input.Lunes==1?true:false,
                                                                martes: frecuencia.input.Martes==1?true:false,
                                                                miercoles: frecuencia.input.Miercoles==1?true:false,
                                                                jueves: frecuencia.input.Jueves==1?true:false,
                                                                viernes: frecuencia.input.Viernes==1?true:false,
                                                                sabado: frecuencia.input.Sabado==1?true:false,
                                                                domingo: frecuencia.input.Domingo==1?true:false
                                            });
                                        }
                    });
                    return data_frecuencia;
                }

                function data_productos() {
                    var data = [];
                    _($scope.frecuencias).each(function (frecuencia, index) {
                        //console.log($scope.frecuencias);
                        _(frecuencia.productos).each(function (producto, indiceProducto) {

                                        if($scope.cliente == ''){$scope.cliente = null }else{$scope.cliente = $scope.cliente};
                                        if($scope.vuelo == undefined){$scope.vuelo = 0 }else{$scope.vuelo = $scope.vuelo};
                                        if($scope.avion == undefined ){$scope.avion = '00' }else{$scope.avion = $scope.avion};
                                        if(producto.codigo_producto == undefined){producto.codigo_producto = ''}else{producto.codigo_producto = producto.codigo_producto}; 
                                        if(producto.descripcion == undefined){producto.descripcion = '' }else{producto.descripcion = producto.descripcion};
                                        if($scope.idContrato == undefined){$scope.idContrato = 0 }else{$scope.idContrato = $scope.idContrato};
                                            
                            data.push({
                                        idCliente: $scope.cliente,
                                        numVuelo: $scope.vuelo,
                                        tipoAvion: $scope.avion,
                                        idProducto: producto.codigo_producto,
                                        descripcion: producto.descripcion,
                                        idFrecuencia: index,
                                        idContrato : $scope.idContrato,
                                        indiceProducto: indiceProducto

                            });
                        });
                    });
                    return data;
                }

                function data_opciones() {
                    var data = [];
                    _($scope.frecuencias).each(function (frecuencia, index) {
                        _(frecuencia.productos).each(function (producto, indiceProducto) {
                            _(producto.opciones).each(function (opcion, indiceOpciones) {
                                        if($scope.cliente == ''){$scope.cliente = null }else{$scope.cliente = $scope.cliente};
                                        if($scope.vuelo == undefined){$scope.vuelo = 0 }else{$scope.vuelo = $scope.vuelo};
                                        if(opcion.clientePrecio == undefined){opcion.clientePrecio = 0 }else{opcion.clientePrecio = opcion.clientePrecio};
                                        if(opcion.opcion == undefined){opcion.opcion = 0 }else{opcion.opcion = opcion.opcion};
                                        if(opcion.servicio == undefined){opcion.servicio = 0 }else{opcion.servicio = opcion.servicio};
                                        if(opcion.cantidad == undefined){opcion.cantidad = 0 }else{opcion.cantidad = opcion.cantidad};
                                        if($scope.idContrato == undefined){$scope.idContrato = 0 }else{$scope.idContrato = $scope.idContrato};
                                data.push({
                                        idCliente: $scope.cliente,
                                        numVuelo: $scope.vuelo,
                                        idFrecuencia: index,
                                        idProducto: producto.codigo_producto,
                                        precio: opcion.clientePrecio,
                                        opcion: opcion.opcion,
                                        servicioAdicional: opcion.servicio,
                                        cantidad: opcion.cantidad,
                                        idContrato : $scope.idContrato,
                                        indiceProducto: indiceProducto,
                                        indiceOpciones: indiceOpciones+1
                                 });
                            });
                        });
                    });
                    return data;
                }
                function build_frecuencias(frecuencia_by_datos){
                    _.each(frecuencia_by_datos, function (frecuencia){
                        $scope.frecuencias[frecuencia.idFrecuencia]={
                             idCliente: frecuencia.idCliente,
                             numVuelo: frecuencia.numVuelo,
                             tipoAvion: frecuencia.avion,
                             dias: frecuencia.dias,
                             desde: moment(frecuencia.desde).format('YYYY-MM-DD'),
                             hasta: moment(frecuencia.hasta).format('YYYY-MM-DD'),
                             idFrecuencia: frecuencia.idFrecuencia,
                             idContrato: frecuencia.idContrato,
                             duracion: frecuencia.duracion,
                             ciclos: frecuencia.ciclos,
                             input:{
                                 Lunes: frecuencia.lunes == 1?true:false,
                                 Martes: frecuencia.martes == 1?true:false,
                                 Miercoles: frecuencia.miercoles == 1?true:false,
                                 Jueves: frecuencia.jueves == 1?true:false,
                                 Viernes: frecuencia.viernes == 1?true:false,
                                 Sabado: frecuencia.sabado == 1?true:false,
                                 Domingo: frecuencia.domingo == 1?true:false,
                             },
                            productos:[]

                        }
                    })

                    _.each(frecuencia_by_datos, function (frecuencia){
                        $scope.frecuencias[frecuencia.idFrecuencia].productos[frecuencia.indiceProducto]={
                                codigo_producto: frecuencia.idProducto,
                                descripcion: frecuencia.descripcion,
                                opciones:[]
                        }
                    })

                    _.each(frecuencia_by_datos, function (frecuencia){
                        $scope.frecuencias[frecuencia.idFrecuencia].productos[frecuencia.indiceProducto].opciones[frecuencia.indiceOpciones-1]={
                                cantidad: parseInt(frecuencia.cantidad),
                                opcion: parseInt(frecuencia.opcion),
                                clientePrecio: parseFloat(frecuencia.precio),
                                servicio: parseInt(frecuencia.servicioAdicional)
                        }
                    })
                    
                }

        })

        .config(function($routeProvider) {
            $routeProvider
                .when("/busqueda_contrato/edit/:_id/:contrato/:vuelo/:aerolinea", {
                    templateUrl: globalApp.base_url + "../client/src/app/busqueda_contrato/views/edit_busqueda_contrato.html",
                    controller: "edit.busqueda_contrato.controller",
                    resolve:{

                        clientes: function ($location, busqueda_contrato_model) {
                                    return busqueda_contrato_model.get('clientes')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                      
                        data_edit: function ($location, busqueda_contrato_model, $route) {
                                    var params = {url_params : {codigo: $route.current.params._id, idContrat: $route.current.params.contrato, numV: $route.current.params.vuelo, aerolinea: $route.current.params.aerolinea}};
                                    return busqueda_contrato_model.get('contratos_by_id', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                        aviones: function ($location, busqueda_contrato_model, $route) {

                                    var params = {url_params : {aerolinea: $route.current.params.aerolinea,codigo: $route.current.params._id}};
                                    return busqueda_contrato_model.get('avion_by_cod', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                        productos: function ($location, busqueda_contrato_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return busqueda_contrato_model.get('productos_tipo', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                        categoriabycliente: function ($location, busqueda_contrato_model, $route){
                                    var params = {url_params : {cliente: $route.current.params._id}};
                                    return busqueda_contrato_model.get('categoria_by_client', params)
                                            .catch(function(){
                                                $location.path('/');    
                                            });
                        }
                        
                    }
                });
        });
})();

