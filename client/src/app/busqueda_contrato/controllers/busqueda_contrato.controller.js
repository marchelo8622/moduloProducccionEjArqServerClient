(function () {

    "use strict";

    angular.module("Busqueda_contrato").controller("busqueda_contrato.controller",
            function ($scope, $http, $location, busqueda_contrato_model, clientes, categorias, contratos, $state) {
                $scope.clientes = clientes;
                $scope.categorias = categorias;
                $scope.contratos = contratos;
                $scope.disableSelect = false;
               
               // $scope.currentContratos = contratos[0].tipo;
                $scope.location = location;
                $scope.contratos_cliente = [];                
                $scope.status ={openedD: false, openedH: false};
                $scope.base_url = globalApp.base_url;
                $scope.parametros_de_busqueda = {
                    codigo: undefined,
                    tipo: undefined,
                    desde: undefined,
                    hasta: undefined,
                    // idContrat: undefined,
                    // numVuelo: undefined, 
                    //frecuen: undefined
                };
                
                $scope.desde = function () {
                    $scope.status.openedDesde = true;
                    $scope.status.openedHasta = false;
                };
                $scope.hasta = function () {
                    $scope.status.openedDesde = false;
                    $scope.status.openedHasta = true;
                };
                $scope.status = {
                    opened: false
                };
                $scope.toggleMin = function() {
                    $scope.minDate = $scope.minDate ? null : new Date();
                  };

                  //$scope.toggleMin();
                $scope.setDate = function(year, month, week, day) {
                    $scope.dt = new Date(year, month, week, day);
                };

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                $scope.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];

                $scope.status = {
                    opened: false
                };

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                var afterTomorrow = new Date();
                afterTomorrow.setDate(tomorrow.getDate() + 2);
                $scope.events =
                  [
                    {
                      date: tomorrow,
                        status: 'full'
                    },
                    {
                      date: afterTomorrow,
                        status: 'partially'
                    }
                  ];

                $scope.getDayClass = function(date, mode) {
                    if (mode === 'day') {
                      var dayToCheck = new Date(date).setHours(0,0,0,0);

                      for (var i=0;i<$scope.events.length;i++){
                        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                        if (dayToCheck === currentDay) {
                          return $scope.events[i].status;
                        }
                      }
                    }

                    return '';
                };

               

                 $scope.selecciona_cliente = function () {
                    if ($scope.cliente == undefined) {
                        return;
                    }
                    
                };

                $scope.genera_pdf = function (contrato) {
                    //console.log(contrato.tipo);
                    if(contrato.tipo == 'AEROLINEAS'){
                        $http({
                            method: 'POST',
                            url: full_path('busqueda_contrato', 'genera_pdf_contratos', undefined),
                            data: { data: contrato.idContrat, codigo: contrato.codigo, tipo: contrato.tipo, desde: contrato.desde, hasta: contrato.hasta},
                            responseType: 'arraybuffer'
                        }).then(function (response) {

                            var pdfName = 'contrato'+ contrato.idContrat +'-'+ moment().format('YYYYMMDD') + '.pdf';
                            saveAs(new Blob([response.data], { type: 'application/pdf' }), pdfName);
                        });
                    }else{
                        //$scope.disableSelect = true;
                        alert('Para la categoria Industrial no se puede generar el pdf');
                    }
                };

                function full_path(resource, action, params) {
      
                    var path = globalApp.base_url + resource + '/api/' + action;

                    if (params !== undefined && params.url_params) {
                      _(_.values(params.url_params)).each(function(param) {
                        path += '/' + param;
                      })
                    }

                    path += document.location.search;

                    if (params !== undefined && params.fideicomiso) {
                      path += '&fideicomiso=' + params.fideicomiso;
                    }

                    return path;
                }

                $scope.buscar = function(){
                  
                 
                    if($scope.busqueda_contrato != undefined || ($scope.finicio != undefined && $scope.ffin != undefined)){
                        var formato_desde = undefined;
                        var formato_hasta = undefined;
                        var codigo = undefined;
                        var tipo = undefined;
                        if($scope.finicio != undefined){
                        formato_desde = moment($scope.finicio).format('YYYY-MM-DD');
                            }
                        if($scope.ffin != undefined){
                        formato_hasta = moment($scope.ffin).format('YYYY-MM-DD');
                            }

                        if($scope.busqueda_contrato){
                          codigo = $scope.busqueda_contrato.clientes;
                          tipo = $scope.busqueda_contrato.categorias; 
                        }
                        
                        var params = {url_params : {
                            codigo: codigo, 
                            tipo: tipo, 
                            desde: formato_desde,
                            hasta: formato_hasta                           
                            
                            }    
                        }
    
                        busqueda_contrato_model.get('contratos_filtros', params).then(function(response){
                            console.log(response,'yes');
                            $scope.contratos = response;
                        });
                    }else{
                       $scope.$emit('warning', { custom_message: 'No se a seleccionado ningun criterio de b\u00FAsqueda' });  
                    }
                };

                $scope.edit_busqueda_contrato = function(contrato){
                    

                    $location.path('busqueda_contrato/edit/' + contrato.codigo + '/' + contrato.idContrat + '/' + contrato.numV + '/' + contrato.aerolinea + '/');
                    
                }

                $scope.new_busqueda_contrato = function(){
                    $location.path('busqueda_contrato/new/');

                }

                $scope.delete_contrato = function(contrato){
                    
                    var params = {
                        url_params: {idContrato:contrato.idContrat, codCliente: contrato.codigo}
                    };

                    var answer = confirm("Deseas eliminar este registro?");
                if (answer){

                    busqueda_contrato_model.remove('eliminar_contratos', params).then(function (response) {

          
                            $scope.$emit('warning', {custom_message: 'Datos eliminados.'});
                     });
                    limpiarValores();
                    }
                    
                        
                }

                function limpiarValores() {
                    window.location.reload();
                }
                function retornar_pagina(){
                    limpiarValores();
                    $location.path('/');
                }

                $scope.seleccionar_contrato = function(contrato){
                     $scope.contratos_cliente.push({
                        'codigo': contrato.codigo,
                        'tipo': contrato.tipo,
                        'desde': contrato.desde,    
                        'hasta': contrato.hasta,
                        'idContrat': contrato.idContrato,
                        'numV': contrato.numV,
                        'nomCliente': contrato.nomCliente
                    });
                }        

                function datos(contrato, tipo, desde, hasta, idContrat, numV) {
                    var data = [];
                    _($scope.contratos_cliente).each(function (contrato) {
                        data.push({ 
                                    'codigo': contrato.codigo,
                                    'tipo': contrato.tipo,
                                    'fecha_inicio': desde,
                                    'fecha_fin': hasta,
                                    'idContrat': contrato.idContrat,
                                    'numV': contrato.numV,
                                    'nomCliente': contrato.nomCliente

                                    //'frecuen': contrato.frecuencia
                        });
                    });
                    return data;
                };
                $scope.refresh = function(){
                    window.location.reload();
                };
                $scope.cancel = function(){

                }
                                
            })
            .config(function ($routeProvider) {
                $routeProvider
                        .when("/", {
                            templateUrl: globalApp.base_url + '../client/src/app/busqueda_contrato/views/busqueda_contrato.html',
                            controller: "busqueda_contrato.controller",
                            resolve: {
                                clientes: function ($location, busqueda_contrato_model) {
                                    return busqueda_contrato_model.get('clientes')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                categorias: function ($location, busqueda_contrato_model) {
                                    return busqueda_contrato_model.get('categoria')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                contratos: function ($location, busqueda_contrato_model) {
                                    var params = {url_params : { codigo: undefined, tipo: undefined, desde: undefined, hasta: undefined, idContrat: undefined, numV: undefined, aerolinea: undefined}};
                                    return busqueda_contrato_model.get('contratos', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }                    

                            }
                        });

            })
            
})();

