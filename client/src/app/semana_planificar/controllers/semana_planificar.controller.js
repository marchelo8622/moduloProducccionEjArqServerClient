(function () {

    "use strict";

    angular.module("Semana_planificar").controller("semana_planificar.controller",
            function ($scope, _, $http, semana_planificar_model, clientes, productos, semanas_planificar) {
                $scope.clientes = clientes;
                $scope.cliente_selected = {};
                $scope.productos = productos;
                $scope.open = {};
                $scope.open.week = false;
                $scope.clone = false;
                $scope.parametros_de_busqueda = {
                    nombre: null,
                    id: null
                };
                
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                function clean() {
                    $scope.clientes = clientes;
                    $scope.cliente_selected = {};
                    $scope.productos = productos;
                    $scope.open = {};
                    $scope.open.week = false;
                    $scope.clone = false;
                    $scope.parametros_de_busqueda = {
                        nombre: null,
                        id: null
                    };
                    $scope.semanas = {};
                    $scope.week = "";
                    $scope.desde = "";
                    $scope.hasta = "";
                }
                function load_week(semanas_planificar_) {
                    var semana = [];
                    var cant_client = 0;
                    var cant_product = 0;
                    var id_cliente = "";
                    var id_producto = "";
                    _.each(semanas_planificar_, function (cliente) {
                        if (id_cliente != cliente.id_cliente) {
                            id_cliente = cliente.id_cliente;
                            semana.push(
                                    {
                                        id: cant_client++,
                                        cliente: cliente.id_cliente,
                                        nombre: cliente.nomcte01,
                                        productos: []
                                    }
                            );
                            cant_product = 0;
                            id_producto = "";
                        }

                        if (id_producto != cliente.id_producto) {
                            var s = _.last(semana).productos.push({
                                id: cant_product++,
                                codigo: cliente.id_producto,
                                nombre: cliente.desprod01,
                                opciones: [
                                    {opcion: cliente.opcion, cantidad: cliente.cantidad}
                                ]
                            });
                            id_producto = cliente.id_producto;

                        } else {
                            var sm = _.last(semana).productos;
                            var cc = _.last(sm).opciones.push({opcion: parseInt(cliente.opcion), cantidad: parseFloat(cliente.cantidad)});
                        }
                    });
                    return semana;
                }

                $scope.semanas = load_week(semanas_planificar);

                $scope.changeWeek = function () {
                    $scope.open.week = true;
                };

                $scope.buscar_clientes = function () {

                    if ($scope.parametros_de_busqueda.nombre == '') {
                        $scope.parametros_de_busqueda.nombre = null;
                    }

                    if ($scope.parametros_de_busqueda.id == '') {
                        $scope.parametros_de_busqueda.id = null;
                    }

                    var params = {url_params: {id: $scope.parametros_de_busqueda.id,
                            nombre: $scope.parametros_de_busqueda.nombre}};

                    semana_planificar_model.get('clientes', params).then(function (datos) {
                        $scope.clientes = datos;
                    });
                };

                $scope.set_cliente = function (cliente) {
                    $scope.cliente_selected = cliente;
                };


                $scope.seleccionar_cliente = function (cliente) {
                    $scope.semanas.push({
                        id: $scope.semanas.length,
                        cliente: cliente.codcte01,
                        nombre: cliente.nomcte01,
                        productos: [
                        ]});
                };

                $scope.setWeek = function (semana) {
                    $scope.cliente = semana;
                };

                $scope.removeClient = function () {
                    $scope.semanas = _.without($scope.semanas, $scope.cliente);
                };

                $scope.guardar = function () {
                    var params = {url_params: []};
                    _.each($scope.semanas, function (cliente) {
                        _.each(cliente.productos, function (producto) {
                            _.each(producto.opciones, function (opcion) {
                                params.url_params.push(
                                        {
                                            id_cliente: cliente.cliente,
                                            id_producto: producto.codigo,
                                            opcion: opcion.opcion,
                                            cantidad: opcion.cantidad,
                                            desde: $scope.desde,
                                            hasta: $scope.hasta
                                        }
                                );

                            });
                        });
                    });
                    var params = {
                        data: params
                    };
                    semana_planificar_model.create('guardar', params).then(function () {
                        clean();
                        $scope.$emit('success', {action: 'actualizados', element: 'Productos', identifier: ''});
                    });

                };

                $scope.seleccionar_producto = function (producto) {
                    $scope.semanas[$scope.cliente_selected.id].productos.push({
                        id: $scope.semanas[$scope.cliente_selected.id].productos.length,
                        nombre: producto.descripcion,
                        codigo: producto.codigo,
                        opciones: [
                            {opcion: 1, cantidad: 0}
                        ]
                    });
                };

                $scope.removeProduct = function (cliente, index) {
                    $scope.semanas[cliente.id].productos.splice(index, 1);
                };

                $scope.add_option = function (producto, cliente) {
                    var cl = _.findWhere($scope.semanas, {id: cliente.id});
                    var pr = _.findWhere(cl.productos, {id: producto.id});
                    var id = pr.opciones.length > 0 ? parseInt(_.last(pr.opciones).opcion) + 1 : 1;

                    pr.opciones.push(
                            {opcion: id, cantidad: 0}
                    );

                };

                $scope.removeOption = function (producto, cliente, opcion) {
                    var cl = _.findWhere($scope.semanas, {id: cliente.id});
                    var pr = _.findWhere(cl.productos, {id: producto.id});
                    pr.opciones = _.without(pr.opciones, opcion);
                };

                $scope.$watch('week', function () {
                    if ($scope.week) {
                        $scope.desde = moment($scope.week).weekday(1).format('YYYY-MM-DD');
                        $scope.hasta = moment($scope.week).weekday(7).format('YYYY-MM-DD');
                        if (!$scope.clone) {
                            var params = {url_params: {desde: $scope.desde}};
                            semana_planificar_model.get('semana_planificar', params).then(function (datos) {
                                $scope.semanas = load_week(datos);
                            });
                        }
                    }
                });
            })
            .config(function ($routeProvider) {
                $routeProvider
                        .when("/", {
                            templateUrl: globalApp.base_url + "../client/src/app/semana_planificar/views/semana_planificar.html",
                            controller: "semana_planificar.controller",
                            resolve: {
                                clientes: function ($location, semana_planificar_model) {
                                    var params = {url_params: {id: null, nombre: null}};
                                    return semana_planificar_model.get('clientes', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                productos: function ($location, semana_planificar_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return semana_planificar_model.get('productos_tipo', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                semanas_planificar: function ($location, semana_planificar_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return semana_planificar_model.get('semana_planificar', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }
                            }
                        });
            });
})();

