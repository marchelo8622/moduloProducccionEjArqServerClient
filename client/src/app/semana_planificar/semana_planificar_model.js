(function() {
  "use strict";

  angular.module("Semana_planificar").factory("semana_planificar_model", function(model) {
    return model("semana_planificar");
  });
})();

