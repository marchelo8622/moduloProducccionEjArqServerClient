(function() {
  "use strict";

  angular.module("Pre_plan").factory("pre_plan_model", function(model) {
    return model("pre_plan");
  });
})();

