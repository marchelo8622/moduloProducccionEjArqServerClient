(function () {

    "use strict";

    angular.module("Pre_plan").controller("pre_plan.controller",
            function ($scope, $http, _, pre_plan_model, clientes, productos, pre_plan_data) {
                $scope.clientes = clientes;
                $scope.productos = productos;
                $scope.isApproved = false;
                $scope.parametros_de_busqueda = {
                    nombre: null,
                    id: null
                };
                $scope.open = {};
                $scope.open.week = false;
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };
                $scope.parametros_de_busqueda = {
                    descripcion: null,
                    codigo: null
                };

                $scope.changeWeek = function () {
                    $scope.open.week = true;
                };

                $scope.listDays = [
                    {id: 1, name: 'Lunes', active: true, clients: []},
                    {id: 2, name: 'Martes', clients: []},
                    {id: 3, name: 'Miercoles', clients: []},
                    {id: 4, name: 'Jueves', clients: []},
                    {id: 5, name: 'Viernes', clients: []},
                    {id: 6, name: 'Sabado', clients: []},
                    {id: 7, name: 'Domingo', clients: []}
                ];

                $scope.buscar_productos = function (flag) {

                    if ($scope.parametros_de_busqueda.descripcion == '') {
                        $scope.parametros_de_busqueda.descripcion = null;
                    }

                    if ($scope.parametros_de_busqueda.codigo == '') {
                        $scope.parametros_de_busqueda.codigo = null;
                    }

                    if(flag){
                     var params = {url_params: {codigo: $scope.parametros_de_busqueda.codigo,
                            descripcion: $scope.parametros_de_busqueda.descripcion}};

                    }else{

                     var params = {url_params: {descripcion: null, codigo: null}};
                     $scope.parametros_de_busqueda.descripcion = null;
                     $scope.parametros_de_busqueda.codigo = null;
                    }


                    pre_plan_model.get('productos_tipo', params).then(function (datos) {
                        $scope.productos = datos;
                    });
                };

                function prePlanData(pre_plan_data) {

                    //console.log(pre_plan_data);
                    _.each(pre_plan_data, function (obj) {
                        obj.dia = parseInt(obj.dia);
                    });

                    var week = [];
                    var idClient = 0, idProduct = 0, idOption = 0;
                    var statusDay = -1, statusClient = -1, statusProduct = -1, statusOption = -1;
                    _.each(pre_plan_data, function (data) {
                        
                        var dayBusqueda = _.findWhere(week, {id: parseInt(data.dia)}); 
                        if (!dayBusqueda) {
                             var wDay = _.findWhere($scope.listDays, {id: parseInt(data.dia)});
                       
                            week.push(
                                    {
                                        id: wDay.id,
                                        name: wDay.name,
                                        clients: []
                                    }
                            );
                            
                        }

                         //console.log(week);

                        

                        var addClient = _.findWhere(week, {id: parseInt(data.dia)});
                        var addDay = _.findWhere(addClient.clients, {code: data.id_cliente});

                       if (!addDay) {

                         
                        
                            
                            addClient.clients.push(
                                    {
                                        id: ++idClient,
                                        code: data.id_cliente,
                                        name: data.name_client,
                                        contrato: data.codigo,
                                        products: []
                                    }
                            );                         
                           

                        }                      


                        var addProduct = _.findWhere(week, {id: parseInt(data.dia)});
                        var addClientes = _.findWhere(addClient.clients, {code: data.id_cliente});
                        var addOpciones = _.findWhere(addClientes.products, {code: data.id_producto});

                        if (!addOpciones) {
                            
                            addClientes.products.push(
                                    {
                                        id: ++idProduct,
                                        code: data.id_producto,
                                        name: data.name_product,
                                        options: []
                                    }
                            );
                            
                        }


                        var addProduct = _.findWhere(week, {id: parseInt(data.dia)});
                        var addClientes = _.findWhere(addClient.clients, {code: data.id_cliente});
                        var addOpciones = _.findWhere(addClientes.products, {code: data.id_producto});
                        if (addOpciones) {
                        addOpciones.options.push(
                                    {
                                        id: data.opcion,
                                        option: data.opcion,
                                        cantidad: data.cantidad
                                    }
                            );

                        }


                        
                    });
                    return week;
                }


         

                $scope.guardar = function () {
                    var params = {url_params: parseDataForSave()};

                    var params = {
                        data: params
                    };

                    console.log(params);
                    //return;

                    pre_plan_model.create('guardar', params).then(function () {
                        
                        $scope.$emit('success', {action: 'actualizados', element: 'Productos', identifier: ''});
                    });

                    //clean();

                };

                function clean() {
                    window.location.reload();

                }

                $scope.cancelar = function () {
                    window.location.reload();
                };


                function parseDataForSave() {
                    var parseData = [];
                    _.each($scope.days, function (day) {
                        _.each(day.clients, function (client) {
                            //console.log(client);
                            _.each(client.products, function (product) {
                                _.each(product.options, function (option) {
                                    parseData.push(
                                            {
                                                id_cliente: client.code,
//                                                name_client: client.name,
                                                id_producto: product.code,
//                                                name_product: product.name,
                                                dia: day.id,
                                                opcion: option.option,
                                                cantidad: option.cantidad,
                                                codigo: client.contrato,
                                                estado_preplan: 0,
                                                desde: $scope.desde,
                                                hasta: $scope.hasta
                                            }
                                    );
                                });
                            });
                        });
                    });
                    //console.log(parseData);
                    return parseData;
                }



                function mergeDays(listDays, week) {
                    if (week.length == 0) {
                        $scope.isApproved = true;
                    }
                    var val = 0;
                    var arrayNew = [];

                    _.each(listDays, function (day) {
                        var wDay = _.findWhere(week, {id: parseInt(day.id)});
                        if (wDay) {
                            arrayNew[val++] = wDay;
                        } else {
                            arrayNew[val++] = (day);
                        }

                    });

                    return arrayNew;
                }

                $scope.setDay = function (day) {
                    $scope.selectedDay = day;
                };

                $scope.setDataProduct = function (day, client) {
                    $scope.selectedDay = day;
                    $scope.selectedClient = client;

                };

                $scope.removeItem = function (day, client, product, option) {

                    var parentDay = _.findWhere($scope.days, {id: day.id});
                    var parentClient = _.findWhere(parentDay.clients, {id: client.id});

                    if (!product) {
                        parentDay.clients = _.without(parentDay.clients, client);
                    } else if (!option) {
                        parentClient.products = _.without(parentClient.products, product);
                    } else {
                        var parentProduct = _.findWhere(parentClient.products, {id: product.id});
                        parentProduct.options = _.without(parentProduct.options, option);
                    }
                };

                $scope.addClient = function (client) {
                    var parentDay = _.findWhere($scope.days, {id: $scope.selectedDay.id});
                    var id = 0;

                    if (_.last(parentDay.clients))
                        id = _.last(parentDay.clients).id;

                    var selectClient = {
                        id: parseInt(id) + 1,
                        code: client.codcte01,
                        name: client.nomcte01,
                        products: []
                    };
                    parentDay.clients.push(selectClient);
                };

                $scope.addProduct = function (product) {
                    console.log(product);
                    var parentDay = _.findWhere($scope.days, {id: $scope.selectedDay.id});
                    var parentClient = _.findWhere(parentDay.clients, {id: $scope.selectedClient.id});

                    var id = 0;

                    if (_.last(parentClient.products))
                        id = _.last(parentClient.products).id;

                    var selectProduct = {
                        id: parseInt(id) + 1,
                        code: product.codigo,
                        name: product.descripcion,
                        options: []
                    };
                    parentClient.products.push(selectProduct);
                };

                $scope.addOption = function (day, client, product) {
                    var parentDay = _.findWhere($scope.days, {id: day.id});
                    var parentClient = _.findWhere(parentDay.clients, {id: client.id});
                    var parentProduct = _.findWhere(parentClient.products, {id: product.id});
                    var id = 0;

                    if (_.last(parentProduct.options)) {
                        id = _.last(parentProduct.options).id;
                    }

                    var selectOption = {
                        id: parseInt(id) + 1,
                        option: parseInt(id) + 1,
                        cantidad: 0
                    };
                    parentProduct.options.push(selectOption);
                };

                $scope.imprimir = function () {
                    console.log($scope.days);
                };

                $scope.buscar_clientes = function (flag) {

                    if ($scope.parametros_de_busqueda.nombre == undefined) {
                        $scope.parametros_de_busqueda.nombre = null;
                    }

                    if ($scope.parametros_de_busqueda.id == undefined) {
                        $scope.parametros_de_busqueda.id = null;
                    }

                    if(flag){
                     var params = {url_params: {id: $scope.parametros_de_busqueda.id,
                            nombre: $scope.parametros_de_busqueda.nombre}};

                    }else{

                     var params = {url_params: {id: null, nombre: null}};
                     $scope.parametros_de_busqueda.nombre = null;
                     $scope.parametros_de_busqueda.id = null;
                    }


                    pre_plan_model.get('clientes', params).then(function (datos) {
                        $scope.clientes = datos;
                    });
                };
                $scope.days = mergeDays($scope.listDays, []);
                $scope.$watch('week', function () {
                    
                    if ($scope.week) {
                        $scope.desde = moment($scope.week).weekday(1).format('YYYY-MM-DD');
                        $scope.hasta = moment($scope.week).weekday(7).format('YYYY-MM-DD');
//                        if (!$scope.clone) {
                        var params = {url_params: {desde: $scope.desde, hasta: $scope.hasta}};

                        pre_plan_model.get('pre_plan', params).then(function (datos) {
                            $scope.isApproved = datos ? datos[0] ? datos[0].estado == 0 : false : false;

                            var week = prePlanData(datos);
                            $scope.days = mergeDays($scope.listDays, week);
                        });
                    }
                });

            })
            .config(function ($routeProvider) {
                $routeProvider
                        .when("/", {
                            templateUrl: globalApp.base_url + "../client/src/app/pre_plan/views/pre_plan.html",
                            controller: "pre_plan.controller",
                            resolve: {
                                clientes: function ($location, pre_plan_model) {
                                    var params = {url_params: {id: null, nombre: null}};
                                    return pre_plan_model.get('clientes', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                productos: function ($location, pre_plan_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return pre_plan_model.get('productos_tipo', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                pre_plan_data: function ($location, pre_plan_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return pre_plan_model.get('pre_plan', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }
                            }
                        });
            });
})();

