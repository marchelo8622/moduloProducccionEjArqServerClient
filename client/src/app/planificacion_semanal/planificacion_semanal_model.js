(function() {
  "use strict";

  angular.module("Planificacion_semanal").factory("planificacion_semanal_model", function(model) {
    return model("planificacion_semanal");
  });
})();

