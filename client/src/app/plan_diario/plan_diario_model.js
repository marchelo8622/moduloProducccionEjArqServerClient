(function() {
  "use strict";

  angular.module("Plan_diario").factory("plan_diario_model", function(model) {
    return model("plan_diario");
  });
})();

