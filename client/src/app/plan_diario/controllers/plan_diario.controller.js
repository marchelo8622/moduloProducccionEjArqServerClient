(function() {

    "use strict";

    angular.module("Plan_diario").controller("plan_diario.controller", 
        function($scope, _,$http, plan_diario_model, categorias, creador_recetas_model, umedidas,planificacion_semanal_model,productos_para_receta) {
            $scope.categorias = categorias;
            $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

            $scope.parametros_de_busqueda = {
                    nombre: null,
                    id: null,
                    codigo : null, 
                    descripcion : null
                };
            $scope.open = {};

            var opciones_eliminadas = [];
            var clientes_eliminados = [];
            $scope.open.week = false;
            $scope.day = '';
            $scope.productos_plan_diario = {};
            $scope.clientes_plan_diario = {};
            $scope.seleccion_categoria = {value: false};
            $scope.seleccion_producto = {value: false};
            $scope.seleccion_clientes_plan_diario = {value: false};
            $scope.mensaje_receta = '';
            $scope.mostrar_receta = {value : false};
            $scope.numOpcion = 0;
            $scope.costo_diario = 0;
            $scope.datos_plan_diario = [];
            $scope.orden_produccion = {};
            $scope.requisiciones_creadas = {value: false};
            $scope.categorias_producto_final = {};
            $scope.productos_con_receta = {};
            $scope.receta_opcion = {};
            $scope.categoria_subreceta_seleccionada = [];
            $scope.producto_receta_ = {value: ''};
            var new_id_receta = [];
            $scope.receta_producto = [];
            $scope.umedidas = umedidas;
            $scope.ingredientes_a_eliminar = [];
            $scope.cantidad_a_servir = 1;
            $scope.productos_para_receta = productos_para_receta;
            $scope.modificar_receta = {value : false};
            $scope.modificar_produccion = {value : false};
            $scope.numero_produccion = {value : ''};
            $scope.productos_receta_a_eliminar = [];
            $scope.tabla = 'lismae'; 
            if ($scope.numero_produccion.value != ""){
                        $scope.tabla = 'lismaehis';
                    }

            $scope.changeDay = function () {
                    $scope.open.week = true;
                };

            $scope.$watch('week', function () {
                if ($scope.week) {
                    $scope.day = moment($scope.week).format('YYYY-MM-DD');
                    if ($scope.seleccion_categoria.value)
                        $scope.buscar_productos_dia();
                    
                     var params = {url_params: {
                                            dia: $scope.day                                            
                                        }
                            };

                    plan_diario_model.get('total_productos_por_dia', params).then(function (datos) {
                    var d = _.groupBy(datos, 'codcate');
                });
                    
                }
            });
            

            $scope.removeItem = function (opcion, cliente) {
                    var parentOption = _.findWhere($scope.datos_plan_diario, {id: opcion.id});                   
                    var dia = $scope.day;
                    var prod = $scope.producto_seleccionado.id_producto;
                    var msj = '';
                    
                     if (!cliente) {
                        msj = "Seguro que desea eliminar la opcion "+opcion.id;
                     }else{

                        msj = "Seguro que desea eliminar el cliente "+cliente.name+" de la opcion "+opcion.id;

                     }

                    var confirmacion= confirm(msj);                    
                        
                         if(confirmacion){                               

                                if (!cliente) {                        
                                    $scope.datos_plan_diario = _.without($scope.datos_plan_diario, opcion);

                                    opciones_eliminadas.push({dia:dia,producto:prod,opcion:opcion});
                                
                                } else {                    
                                   
                                    parentOption.clientes = _.without(parentOption.clientes, cliente); 
                                    clientes_eliminados.push({
                                                              dia: dia,
                                                              producto: prod,
                                                              opcion:opcion.id, 
                                                              cliente:cliente
                                                             });                        
                                }
                        }                    
                    
            };

            $scope.addOption = function () {                                        
                    
                    if($scope.producto_seleccionado != undefined){
                        if($scope.producto_seleccionado.name_product == ''){

                            $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                       
                        }else{
                            
                            if($scope.producto_receta_.value != ''){
                                new_id_receta = _.without(new_id_receta, _.findWhere(new_id_receta, {cat: $scope.producto_receta_.elemento_receta.categoria}));
                                new_id_receta.push({
                                                    cat: $scope.producto_receta_.elemento_receta.categoria,
                                                    receta: $scope.producto_receta_.elemento_receta.codigo,
                                                    nom_receta: $scope.producto_receta_.elemento_receta.descripcion
                                                    }
                                                  );
                                $scope.receta_opcion = new_id_receta;
                                
                                if($scope.categorias_producto_final.length == new_id_receta.length){
                                    var id = 0;

                                    if (_.last($scope.datos_plan_diario))
                                        id = _.last($scope.datos_plan_diario).id;                        

                                    var opcion = {
                                        id: parseInt(id) + 1,
                                        code: parseInt(id) + 1,
                                        id_receta: new_id_receta,                            
                                        clientes: []
                                    };
                                    $scope.datos_plan_diario.push(opcion);
                                    $scope.$emit('success', {action: 'La opcion se agrego correctamente!', element: '', identifier: ''});
                                    $('#btnDescartar').click();
                                }else{

                                    $scope.$emit('warning', {action: 'Listo!! Le falta agregar un producto en las otras categorias de la receta!', element: '', message: ''});

                                }
                            }else{

                                   $scope.$emit('warning', {action: 'Seleccione un producto de la lista!', element: '', message: ''});


                            }

                        }
                    }else{

                        $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                   


                    }
                        
            };

            $scope.calcular_costo_diario = function (dia,tabla) {
                
                    if(dia == '' || $scope.categoria_seleccionada == undefined || $scope.producto_seleccionado.id_producto == ''){

                        $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});

                    }else{
                        var params = {
                            url_params: {dia:dia,tabla:tabla},
                            data: parseDataForSave()
                        };
                         
                        planificacion_semanal_model.create('calcular_costo_diario', params).then(function (data) {
                        
                            $scope.costo_diario = data.costo_dia;                            
                             
                             _.each(data.costos_opciones, function (datos_opcion) {
                                var id_span_op = '#op'+ datos_opcion.opcion;
                                //var id_span_prod = '#prod'+datos_opcion.dia + datos_opcion.producto;                        
                                $(id_span_op).text(' (C.U: $' + datos_opcion.costo_unitario+') (C.T: $' + datos_opcion.costo.toFixed(2)+') ');
                                //$(id_span_prod).text(' ($' + datos_opcion.suma_por_producto+') ');

                            });

                       });

                   }

                      
                   
            };


            $scope.buscar_productos_dia = function () {
                $scope.seleccion_categoria.value = true;
                $scope.limpiar_plan_diario();
                if ($scope.week) {
                    var params = {url_params: {
                                            dia: $scope.day, 
                                            codigo_categoria: $scope.categoria_seleccionada.codcate
                                        }
                            };

                    plan_diario_model.get('productos_plan_diario', params).then(function (datos) {
                    if (datos.length)
                        $scope.productos_plan_diario = datos;
                });
                }
            }

            $scope.Cargar_plan_producto = function () {
                if ($scope.producto_seleccionado.id_producto != '')
                {

                $scope.seleccion_producto.value = true;
                var params = {
                    url_params: {
                                    dia: $scope.day, 
                                    codigo_producto: $scope.producto_seleccionado.id_producto
                                }
                            };

                    plan_diario_model.get('clientes_plan_diario', params).then(function (datos) {


                    $scope.datos_plan_diario = planDiarioData(datos);
                    $scope.nombre_producto = $scope.producto_seleccionado;
                    $scope.buscar_orden_produccion();
                    consultar_categorias_producto_final();
                    
                });
                }

            }

            $scope.buscar_orden_produccion = function (){
                $scope.orden_produccion  = {};
                $scope.requisiciones_creadas = {value: false};
                var params = {url_params: {
                                            dia: $scope.day, 
                                            codigo_categoria: $scope.categoria_seleccionada.codcate
                                        }
                            };
                plan_diario_model.get('buscar_orden_produccion_plan_diario', params).then(function (datos) {
                    $scope.orden_produccion = datos;
                    if ($scope.orden_produccion[0].numero_produccion != "")          
                        {
                            $scope.numero_produccion.value = "Produccion N: " + $scope.orden_produccion[0].numero_produccion;
                            var params = {url_params: {orden_produccion: $scope.orden_produccion[0].numero_produccion}};
                            plan_diario_model.get('num_requisicion_by_produccion', params).then(function (datos) {
                                $scope.requisiciones_creadas.value = datos;
                            });

                        }
                    $scope.calcular_costo_diario($scope.day,$scope.tabla);
                });
            }

            function parseDataForSave() {
                    $scope.fecha = moment($scope.week).format('YYYY-MM-DD');
                    var parseData = [];
                    
                        _.each($scope.datos_plan_diario, function (opcion) {
                            _.each(opcion.clientes, function (cliente) {
                                    parseData.push(
                                            {
                                                id_cliente: cliente.code,
                                                id_producto: $scope.producto_seleccionado.id_producto,
                                                opcion: opcion.code,
                                                cantidad: cliente.cantidad,
                                                cantidad_dispatch: cliente.cantidad_dispatch,
                                                cantidad_entregada: cliente.cantidad_entregada,
                                                id_receta: JSON.stringify(opcion.id_receta),                                               
                                                codigo: cliente.contrato,
                                                fecha: $scope.fecha,
                                                dia: $scope.fecha,
                                                desde: cliente.desde,
                                                hasta: cliente.hasta,
                                                estado_produccion: $scope.orden_produccion[0].estado_produccion,
                                                orden_produccion: $scope.orden_produccion[0].numero_produccion
                                            }
                                    );
                            });
                        });
                    
                    return parseData;
                }

            function setDataEdicion() {
                    
                    var parseData = [];
                    
                        _.each($scope.datos_plan_diario, function (opcion) {
                            _.each(opcion.clientes, function (cliente) {
                                    parseData.push(
                                            {
                                                id_cliente: cliente.code,
                                                id_producto: $scope.producto_seleccionado.id_producto,
                                                opcion: opcion.code,
                                                cantidad: cliente.cantidad,
                                                cantidad_dispatch: cliente.cantidad_dispatch,
                                                cantidad_entregada: cliente.cantidad_entregada,
                                                id_receta: opcion.id_receta,                                               
                                                codigo: cliente.contrato,                                                
                                                dia: $scope.fecha,
                                                desde: cliente.desde,
                                                hasta: cliente.hasta
                                                
                                                
                                            }
                                    );
                            });
                        });
                    
                    return parseData;
                }

            $scope.guardar = function () {

                    if($scope.day == '' || $scope.categoria_seleccionada == undefined || $scope.producto_seleccionado.id_producto == ''){
                        $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                    }else{ 

                        if (!validar_edicion_plan_diario())
                        {
                            $scope.$emit('warning', {action: 'No se puede modificar, el dia ya fue producido', element: '', message: ''});
                            return;
                        }


                        var params = {

                            data : {
                                data1 : parseDataForSave(),
                                fecha : $scope.day, 
                                codigo_categoria:$scope.categoria_seleccionada.codcate,
                                numero_produccion: $scope.orden_produccion[0].numero_produccion,
                                data_opciones_eliminadas: opciones_eliminadas,
                                data_clientes_eliminados: clientes_eliminados
                                }
                        };
                        var valida_recetas = validar_recetas();
                        if (!valida_recetas)
                            return;
                        var valida_clasificacion = validar_clasificacion();
                        if (!valida_clasificacion)
                            return;

                         $('#btnGuardar').attr("disabled", true);                  
                        plan_diario_model.create('guardar', params).then(function () {
                            
                            $scope.$emit('success', {action: 'Actualizado', element: 'Plan Diario', identifier: ''});
                            $scope.modificar_receta = {value : false};
                            opciones_eliminadas = [];
                            clientes_eliminados = [];
                            $scope.Cargar_plan_producto();
                            $('#btnGuardar').attr("disabled", false);
                            $scope.productos_receta_a_eliminar = [];
                            if ($scope.orden_produccion[0].numero_produccion != "") return;

                            var params = {
                                    data: {fecha:$scope.day,codigo_categoria:$scope.categoria_seleccionada.codcate}
                                        };

                                plan_diario_model.create('crear_op_de_producto_receta', params).then(function (datos) {

                                if (datos.respuesta)
                                {
                                    $scope.$emit('success', {action: 'Creada con exito!', element: 'Orden Produccion', identifier: ''});
                                    $scope.buscar_orden_produccion();    
                                }else{
                                    $scope.$emit('warning', {action: datos.error, element: '', message: ''});
                                }
                                //
                            });
                        });
                    
                    }

                    //clean();

                };

            $scope.producir = function () {

                if($scope.day == '' || $scope.categoria_seleccionada == undefined || $scope.producto_seleccionado.id_producto == ''){
                    $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                }else{      


                    if ($scope.orden_produccion[0].numero_produccion !== '')
                    {
                        if (!$scope.requisiciones_creadas.value)
                        {
                            $scope.$emit('warning', {action: 'Falta crear!', element: 'Requisiciones', message: ''});
                            return;
                        }

                        if (!validar_produccion_plan_diario())
                        {
                            $scope.$emit('warning', {action: 'El dia ya fue producido', element: '', message: ''});
                            return;
                        }
                        var params = {
                                  url_params: {orden_produccion: $scope.orden_produccion[0].numero_produccion}
                                    };
                            plan_diario_model.get('validar_entrega_requisiciones', params).then(function (datos) {
                                if (datos.validacion)
                                {
                                var answer = confirm("Seguro que desea realizar esta accion?");
                                if (answer){
                                    var params = {
                                          url_params: {numero_produccion: $scope.orden_produccion[0].numero_produccion}
                                            };
                                    plan_diario_model.get('producir_plan_diario', params).then(function (datos) {
                                        angular.forEach(datos.documentos, function(value, key) {
                                            recontabilizar(value.tipo_documento, value.numero_documento, "41", "22");
                                        });

                                        if (datos.recetas)
                                            $scope.$emit('success', {action: 'producido', element: 'Recetas', identifier: ''});

                                        if (datos.finales)
                                            $scope.$emit('success', {action: 'producido', element: 'Productos Finales', identifier: ''});
                                        else
                                            $scope.$emit('warning', {action: datos.error, element: datos.producto_error, message: ''});
                                        $scope.buscar_orden_produccion();
                                    });

                                    }
                                }else{
                                    $scope.$emit('warning', {action: datos.error, element: '', message: ''});
                                    
                                }

                            });
                    }else{
                        $scope.$emit('warning', {action: 'No existe!', element: 'Orden de Produccion', message: ''});
                    }
                }                

            }

            function recontabilizar(tipodoc, numdoc, grupo, modulo){
                 $.ajax({
                        url: "../../../itprog/it52i.php"+document.location.search,
                        type: "POST",
                        data:{tipodoc:tipodoc,numdoc:numdoc,campo1:tipodoc,grupo:grupo,modulo:modulo,accion:"Procesar"},
                        async:false
                    }
                ).responseText;
            }

              $scope.requisicion = function () { 
                if($scope.day == '' || $scope.categoria_seleccionada == undefined || $scope.producto_seleccionado.id_producto == ''){
                    $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                }else{

                    if ($scope.requisiciones_creadas.value)
                    {
                        $scope.$emit('warning', {action: 'Ya existe para este dia!', element: 'Requisiciones', message: ''});
                        return;
                    }

                    if ($scope.orden_produccion[0].numero_produccion != "")          
                    {

                    var valida_clasificacion = validar_clasificacion();
                    if (!valida_clasificacion)
                        return;
                 
                    var answer = confirm("Seguro que desea realizar esta accion?");
                    if (answer){
                        var params = {
                                data: {numero_produccion: $scope.orden_produccion[0].numero_produccion, fecha:$scope.day}
                                    };
                        plan_diario_model.create('generar_requisiciones_by_produccion', params).then(function (datos) {

                            if (datos.respuesta)
                            {
                                $scope.$emit('success', {action: 'Creadas con exito!', element: 'Requisiciones', identifier: ''});
                                $scope.buscar_orden_produccion();
                            }else{
                                $scope.$emit('warning', {action: datos.error, element: '', message: ''});
                            }
                        });

                    }
                    }else{
                        $scope.$emit('warning', {action: 'No existe para este dia!', element: 'Orden de Produccion', message: ''});
                    }
                }

            }

            $scope.pdf_requisicion = function () { 
                if($scope.day == '' || $scope.categoria_seleccionada == undefined || $scope.producto_seleccionado.id_producto == ''){
                    $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                }else{

                    if ($scope.requisiciones_creadas.value)
                    {
                        $http({
                        method: 'POST',
                        url: full_path('plan_diario', 'genera_pdf_requisicion', undefined),
                        data: { codigo: $scope.orden_produccion[0].numero_produccion, fecha:$scope.day},
                        responseType: 'arraybuffer'
                        }).then(function (response) {
                            var pdfName = 'requisicion-'+ $scope.orden_produccion[0].numero_produccion +'-'+ moment().format('YYYYMMDD') + '.pdf';
                            saveAs(new Blob([response.data], { type: 'application/pdf' }), pdfName);
                        });

                    }else{
                        $scope.$emit('warning', {action: 'No existe una Requisicion para este dia!', element: '', message: ''});
                    }
                }
            }

            function full_path(resource, action, params) {
                var path = globalApp.base_url + resource + '/api/' + action;
                if (params !== undefined && params.url_params) {
                  _(_.values(params.url_params)).each(function(param) {
                    path += '/' + param;
                  })
                }

                path += document.location.search;

                if (params !== undefined && params.fideicomiso) {
                  path += '&fideicomiso=' + params.fideicomiso;
                }

                return path;
            }

             function planDiarioData(plan_diario_data) {

                    var planDiario = [];
                    var idOpcion = 0, idClient = 0, idOption = 0;
                    //var statusDay = -1, statusClient = -1, statusProduct = -1, statusOption = -1;
                    //console.log(plan_diario_data,'entrada');
                    _.each(plan_diario_data, function (data) {                      
                       
                        var day = _.findWhere(planDiario, {code: data.opcion});

                         var cadena = "";
                            _(JSON.parse(data.id_receta)).each(function(val, key){                                    
                                cadena += val.nom_receta+' | ';     
                            });
                       
                       if (!day) {

                            planDiario.push(
                                    {
                                        id: ++idOpcion,
                                        code: data.opcion,
                                        id_receta: JSON.parse(data.id_receta),
                                        string_recetas: cadena,                                    
                                        clientes: []
                                    }
                            );
                        }

                        var addClientes = _.findWhere(planDiario, {code: data.opcion});
                        var addOpciones = _.findWhere(addClientes.clientes, {code: data.id_cliente});

                        if (!addOpciones) {
                            data.cantidad_entregada = (data.cantidad_entregada == "" || data.cantidad_entregada == undefined) ? 0 : data.cantidad_entregada;
                            data.cantidad_dispatch = (data.cantidad_dispatch == "" || data.cantidad_dispatch == undefined) ? 0 : data.cantidad_dispatch;
                            addClientes.clientes.push(
                                    {
                                        id: ++idClient,
                                        code: data.id_cliente,
                                        name: data.name_client,
                                        cantidad: data.cantidad,
                                        cantidad_dispatch: data.cantidad_dispatch,
                                        cantidad_entregada: data.cantidad_entregada,                                        
                                        desde: data.desde,
                                        hasta: data.hasta                                      
                                    }
                            );
                            
                        }
                        

                    });
                    //console.log(planDiario,'salida');
                    return planDiario;
                };



            function recargaPlanDiarioData(plan_diario_data,opcion,new_id_receta) {

                    var planDiario = [];
                    var idOpcion = 0, idClient = 0, idOption = 0;
                    //var statusDay = -1, statusClient = -1, statusProduct = -1, statusOption = -1;
                    //console.log(plan_diario_data,'entrada');
                    _.each(plan_diario_data, function (data) { 

                         if(data.opcion == opcion){
                             data.id_receta = new_id_receta;
                         }                     
                       
                        var day = _.findWhere(planDiario, {code: data.opcion});

                         var cadena = "";
                            _(data.id_receta).each(function(val, key){                                    
                                cadena += val.nom_receta+' | ';     
                            });
                       
                       if (!day) {

                            planDiario.push(
                                    {
                                        id: ++idOpcion,
                                        code: data.opcion,
                                        id_receta: data.id_receta,
                                        string_recetas: cadena,                                    
                                        clientes: []
                                    }
                            );
                        }

                        var addClientes = _.findWhere(planDiario, {code: data.opcion});
                        var addOpciones = _.findWhere(addClientes.clientes, {code: data.id_cliente});

                        if (!addOpciones) {

                            data.cantidad_entregada = (data.cantidad_entregada == "" || data.cantidad_entregada == undefined) ? 0 : data.cantidad_entregada;
                            data.cantidad_dispatch = (data.cantidad_dispatch == "" || data.cantidad_dispatch == undefined) ? 0 : data.cantidad_dispatch;
                           
                            
                            addClientes.clientes.push(
                                    {
                                        id: ++idClient,
                                        code: data.id_cliente,
                                        name: data.name_client,
                                        cantidad: data.cantidad,
                                        cantidad_dispatch: data.cantidad_dispatch,
                                        cantidad_entregada: data.cantidad_entregada,                                        
                                        desde: data.desde,
                                        hasta: data.hasta                                      
                                    }
                            );
                            
                        }
                        

                    });
                    //console.log(planDiario,'salida');
                    return planDiario;
                };

             

            $scope.cancelar = function () {
                    window.location.reload();
                };

            

            $scope.addClient = function (client,opcion) {

                
                if($scope.producto_seleccionado != undefined){
                    if($scope.producto_seleccionado.name_product == ''){

                        $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                   
                    }else{
                    
                        
                        var parentOption = _.findWhere($scope.datos_plan_diario, {id: opcion});

                        var validaCliente = _.findWhere(parentOption.clientes,{code:client.codcte01});

                        if(!validaCliente){
                                var id = 0;
                                var id_receta = {};

                                

                                if (_.last(parentOption.clientes)){
                                    id = _.last(parentOption.clientes).id;
                                    //id_receta = _.last(parentOption.clientes).id_receta;
                                }

                                
                                
                                var selectCliente = {
                                    id: parseInt(id) + 1,                                
                                    code: client.codcte01,
                                    name: client.nomcte01,
                                    cantidad: 0,
                                    cantidad_dispatch: 0,
                                    cantidad_entregada: 0,
                                    //id_receta: id_receta,
                                    desde: $scope.day,
                                    hasta: $scope.day                                
                                    
                                };
                                
                                parentOption.clientes.push(selectCliente);

                        }else{

                                $scope.$emit('warning', {action: 'El cliente seleccionado ya existe para la opcion que desea añadir', element: '', message: ''});
                   
                        }

                     }
                }else{

                     $scope.$emit('warning', {action: 'Seleccione dia, categoria y producto!', element: '', message: ''});
                   
                }

                //console.log(selectClient);
                };


               

                $scope.buscar_clientes = function (flag,opcion) {
                    if (!validar_edicion_plan_diario())
                    {
                        $scope.$emit('warning', {action: 'No se puede modificar, el dia ya fue producido', element: '', message: ''});
                        return;
                    }
                    $scope.modal_buscar_cliente = "modal_buscar_cliente";
                     $scope.obj_opcion = opcion;
                    $scope.numOpcion = opcion.id;

                    if ($scope.parametros_de_busqueda.nombre == undefined) {
                        $scope.parametros_de_busqueda.nombre = null;
                    }

                    if ($scope.parametros_de_busqueda.id == undefined) {
                        $scope.parametros_de_busqueda.id = null;
                    }                  


                    if(flag){
                     var params = {url_params: {id: $scope.parametros_de_busqueda.id,
                            nombre: $scope.parametros_de_busqueda.nombre,
                            categoria: $scope.categoria_seleccionada.codcate}};

                    }else{

                     var params = {url_params: {id: null, nombre: null, categoria: $scope.categoria_seleccionada.codcate}};
                     $scope.parametros_de_busqueda.nombre = null;
                     $scope.parametros_de_busqueda.id = null;
                    }

                    plan_diario_model.get('clientes', params).then(function (datos) {
                        $scope.clientes = datos;
                    });
                };




                $scope.limpiar_plan_diario = function () {
                    $scope.producto_seleccionado = {
                        dia : '',
                        id_producto : '',
                        name_product : ''
                    }
                    $scope.productos_plan_diario = {
                        dia : '',
                        id_producto : '',
                        name_product : ''
                    }

                    $scope.datos_plan_diario = {}

                    $scope.numero_produccion = {value : ''};
                };

                $scope.mostrar_recetas = function (opcion)
                {                   
                    
                    limpiar_totales_receta();
                    if (opcion.clientes.length > 0)
                    {
                        new_id_receta = [];
                        $scope.modal_recetas = "modal_recetas";
                        $scope.numOpcion = opcion.id;
                        $scope.titulo = 'EDITAR RECETA ';
                        $scope.btnAddOption = 1;
                        $scope.receta_opcion = opcion.id_receta;
                        consultar_categorias_producto_final();
                    }else{
                        $scope.modal_recetas = "";
                        $scope.$emit('warning', {action: 'No tiene ningun Cliente Asignado a esta Opcion!', element: '', message: ''});
                    }
                }

                $scope.cargar_receta = function (categoria)
                {
                    if(categoria != undefined)
                    {
                        $scope.categoria_subreceta_seleccionada = categoria;
                        var params = {
                                      url_params: {categoria: categoria.codigo}
                                    };
                        plan_diario_model.get('productos_con_recetas_by_categoria', params).then(function (datos) {
                                $scope.productos_con_receta = datos;
                                seleccionar_producto_receta();
                            });
                    }
                };

                $scope.data_id_receta = function(data){                    
                   var obj = _.findWhere($scope.productos_con_receta, {codigo: data.value});
                   data.elemento_receta = obj;                   
                };

                $scope.buscar_receta = function ()
                {
                    limpiar_totales_receta();
                    $scope.modal_buscar_producto = 'modal_buscar_producto';
                    if ($scope.orden_produccion[0].numero_produccion != "")
                    {
                        var params = {url_params: {codigo: $scope.producto_receta_.value, numero_produccion: $scope.orden_produccion[0].numero_produccion}};
                        creador_recetas_model.get('receta_produccion_by_producto', params).then(function (datos) {
                            $scope.receta_producto = datos;
                            calcular_total_receta();
                        });
                    }else{
                        var params = {url_params: {codigo: $scope.producto_receta_.value}};
                        creador_recetas_model.get('receta_by_producto', params).then(function (datos) {
                            $scope.receta_producto = datos;
                            calcular_total_receta();
                        });
                    }
                    var params = {url_params: {codigo: $scope.producto_receta_.value}};
                    creador_recetas_model.get('info_producto_receta', params).then(function (datos2) {
                        if (datos2.length > 0)
                        {
                        $scope._cantidad_a_servir = parseInt(datos2[0].sirve_a);
                        $scope.tipo_cocina_ = datos2[0].nombre_cocina;
                        $scope.codigo_cocina_ = datos2[0].codigo_cocina;
                        }
                    });
                }

                $scope.eliminar_ingrediente = function (index, producto) {
                    /*$scope.ingredientes_a_eliminar.push({
                        codigo_producto: producto.codigo_producto,
                        codigo_ingrediente: producto.codigo_ingrediente
                    });*/
                    //$scope.receta_producto.splice(index, 1);
                    producto.cantidad_total = 0;
                    $scope.calcular_cant_unitaria(producto);
                    calcular_total_receta();
                    $scope.modificar_receta = {value : true};
                }

                $scope.calcular_cant_total = function (receta) {
                    asignar_cantidad_a_servir();
                    if ($scope.cantidad_a_servir != 0)
                    {
                    $scope.cant_aux = receta.cantidad_ingrediente / receta.equivalencia;
                    receta.cantidad_total = $scope.cantidad_a_servir * $scope.cant_aux;
                    receta.cantidad_total = receta.cantidad_total.toFixed(6);
                    receta.cantidad_unitaria = $scope.cant_aux;
                    receta.costo_ingrediente = (receta.cantidad_total / $scope.cantidad_a_servir) * receta.costo_producto;
                    receta.costo_ingrediente = receta.costo_ingrediente.toFixed(6);
                    receta.costo_total = $scope.cantidad_a_servir * receta.costo_ingrediente;
                    receta.costo_total = receta.costo_total.toFixed(6);
                    $scope.modificar_receta = {value : true};
                    calcular_total_receta();
                    }else{
                        receta.cantidad_total = 0;
                    }
                }

                $scope.calcular_cant_unitaria = function (receta) {
                    asignar_cantidad_a_servir();
                    if ($scope.cantidad_a_servir != 0)
                    {
                    $scope.cant_aux = receta.cantidad_total * receta.equivalencia;
                    receta.cantidad_ingrediente = $scope.cant_aux / $scope.cantidad_a_servir;
                    receta.cantidad_ingrediente = receta.cantidad_ingrediente.toFixed(6);
                    receta.cantidad_unitaria = receta.cantidad_ingrediente / receta.equivalencia;
                    receta.costo_ingrediente = (receta.cantidad_total / $scope.cantidad_a_servir) * receta.costo_producto;
                    receta.costo_ingrediente = receta.costo_ingrediente.toFixed(6);
                    receta.costo_total = $scope.cantidad_a_servir * receta.costo_ingrediente;
                    receta.costo_total = receta.costo_total.toFixed(6);
                    $scope.modificar_receta = {value : true};
                    calcular_total_receta();
                    }
                }

                $scope.guardar_receta_producto = function () 
                {
                    var params_receta = [];
                    params_receta.push(
                            {
                                codigo_producto : $scope.producto_receta_.value,
                                sirve_a : $scope._cantidad_a_servir,
                                tipo_cocina : $scope.codigo_cocina_
                            }
                    );
                    var params_receta = {
                            data: params_receta[0]
                        };
                        creador_recetas_model.create('receta_producto', params_receta).then(function () {});
                    var params = {
                            data : {  
                                data1 : $scope.receta_producto,
                                data2 : $scope.ingredientes_a_eliminar
                                }
                        };
                        creador_recetas_model.create('creador_recetas', params).then(function () {
                        $scope.$emit('success', {action: 'Guardado con exito', element: '', identifier: '', time: 1});
                    });
                }

                $scope.guardar_receta_produccion = function () 
                {
                    if ($scope.orden_produccion[0].numero_produccion != ""){
                    var params = {
                            data : {  
                                data1 : $scope.receta_producto,
                                data2 : $scope.ingredientes_a_eliminar,
                                numero_produccion: $scope.orden_produccion[0].numero_produccion
                                }
                        };
                        plan_diario_model.create('modificar_receta_produccion', params).then(function () {
                        $scope.$emit('success', {action: 'Modificado con exito', element: '', identifier: '', time: 1});
                        $scope.$emit('warning', {action: 'Se realizaron modificaciones!', element: '', message: 'Guarde el Plan Diario', closeButton: true});
                        $scope.modificar_receta = {value : false};
                        $scope.modificar_produccion = {value : true};
                    });
                    }
                }

                $scope.buscar_productos = function () {

                    if ($scope.parametros_de_busqueda.descripcion == '') {
                        $scope.parametros_de_busqueda.descripcion = null;
                    }

                    if ($scope.parametros_de_busqueda.codigo == '') {
                        $scope.parametros_de_busqueda.codigo = null;
                    }

                    var params = {url_params: {codigo: $scope.parametros_de_busqueda.codigo,
                            descripcion: $scope.parametros_de_busqueda.descripcion}};

                    creador_recetas_model.get('productos_para_receta', params).then(function (datos) {
                        $scope.productos_para_receta = datos;
                        $("#modal_buscar_producto").modal();
                    });
                }

                $scope.seleccionar_producto = function (producto) {
                    if ($scope.umedidas.length > 0)
                    {
                    $scope.receta_producto.push({
                        codigo_producto: $scope.producto_receta_.value,
                        codigo_ingrediente: producto.codigo,
                        descripcion_ingrediente: producto.descripcion,
                        unidad_medida: $scope.umedidas[0].id_um,
                        cantidad_ingrediente: 0,
                        cantidad_total: 0,
                        cantidad_unitaria : 0,
                        costo_ingrediente: producto.costo,
                        costo_producto: producto.costo,
                        costo_total: 0,
                        fecha_ingreso : '',
                        tipo_ingrediente : '',
                        equivalencia : $scope.umedidas[0].equivalencia
                    });
                    $scope.calcular_cant_total($scope.receta_producto);
                    calcular_total_receta();
                    }else{
                        $scope.$emit('warning', {action: 'No existen Unidades de Medida configuradas!', element: '', message: ''});
                    }

                }

                $scope.buscar_ingredientes = function () {
                    $scope.parametros_de_busqueda = {codigo : '', descripcion : ''};
                    $scope.buscar_productos();
                }

                function asignar_cantidad_a_servir() 
                {
                    if ($scope._cantidad_a_servir == '' || $scope._cantidad_a_servir == undefined || $scope._cantidad_a_servir <= 0)
                                $scope.cantidad_a_servir = 0;
                            else
                                $scope.cantidad_a_servir = $scope._cantidad_a_servir;
                }

                function calcular_total_receta() 
                {
                    if ($scope.receta_producto.length > 0)
                    {
                        limpiar_totales_receta();
                        _($scope.receta_producto).each(function (receta) {
                            $scope.total_receta += parseFloat(receta.costo_total);
                            $scope.total_receta_por_unidad += parseFloat(receta.costo_ingrediente);

                        });

                        $scope.total_receta = $scope.total_receta.toFixed(6);
                        $scope.total_receta_por_unidad = $scope.total_receta_por_unidad.toFixed(6);
                    }
                }


                function consultar_categorias_producto_final() 
                {
                    if ($scope.producto_seleccionado.id_producto != '')
                    {
                        var params = {
                                  url_params: {codigo_producto: $scope.producto_seleccionado.id_producto}
                                };
                        plan_diario_model.get('categorias_producto_final', params).then(function (datos) {
                            $scope.categorias_producto_final = datos;
                            if ($scope.categoria_subreceta_seleccionada.length <= 0){
                                $scope.categoria_subreceta_seleccionada = $scope.categorias_producto_final[0];
                            }
                            seleccionar_producto_receta();
                        });
                    }
                }

                function seleccionar_producto_receta()
                {
                    $scope.producto_receta_.value = '';
                    $scope.producto_receta_.elemento_receta = [];
                    limpiar_valores();
                    if ($scope.receta_opcion.length > 0)
                    {
                    var receta = _.findWhere($scope.receta_opcion, {cat: $scope.categoria_subreceta_seleccionada.codigo});
                        if(receta){
                            $scope.producto_receta_.value = receta.receta;
                            $scope.buscar_receta();
                            $scope.modificar_receta = {value : false};
                        }
                    }
                }

                function limpiar_totales_receta(){
                    $scope.total_receta = 0.00;
                    $scope.total_receta_por_unidad = 0.00;
                }

                function limpiar_valores() 
                {
                    $scope.ingredientes_a_eliminar = [];
                    $scope.receta_producto = [];
                    $scope.mensaje_receta = '';
                    $scope.mostrar_receta = {value : false};
                    $scope.tiene_receta = {value : false};
                    $scope.cantidad_a_servir = 1;
                    $scope.parametros_de_busqueda = {codigo : '', descripcion : ''};
                }

                $scope.add_nueva_receta = function ()
                {
                    if (!validar_edicion_plan_diario())
                    {
                        $scope.$emit('warning', {action: 'No se puede modificar, el dia ya fue producido', element: '', message: ''});
                        return;
                    }
                    var id = 0;

                            if (_.last($scope.datos_plan_diario))
                                id = _.last($scope.datos_plan_diario).id;

                    limpiar_totales_receta();
                    if (id != '')
                    {
                        new_id_receta = [];
                        $scope.modal_recetas = "modal_recetas";
                        $scope.titulo = 'AGREGAR RECETA A ';
                        $scope.btnAddOption = 0;
                        $scope.numOpcion = parseInt(id) + 1;
                        $scope.receta_opcion = [{}]; //opcion.id_receta
                        consultar_categorias_producto_final();
                    }else{
                        $scope.modal_recetas = "";
                        $scope.$emit('warning', {action: 'Existe un error al ingresar una nueva opcion. Intente nuevamente!', element: '', message: ''});
                    }
                };

                $scope.updateOption = function(receta_opcion,opcion){
                    if (!validar_edicion_plan_diario())
                    {
                        $scope.$emit('warning', {action: 'No se puede modificar, el dia ya fue producido', element: '', message: ''});
                        return;
                    }
                   var data_receta_editar = _.findWhere($scope.datos_plan_diario, {id: opcion});
                    
                    if($scope.producto_receta_.elemento_receta.categoria){
                        /*var producto_a_eliminar = _.findWhere(data_receta_editar.id_receta, {cat: $scope.producto_receta_.elemento_receta.categoria});
                        $scope.productos_receta_a_eliminar = _.without($scope.productos_receta_a_eliminar, _.findWhere($scope.productos_receta_a_eliminar, {codigo_producto: producto_a_eliminar.receta}));
                        $scope.productos_receta_a_eliminar.push({
                            codigo_producto: producto_a_eliminar.receta,
                            opcion: opcion
                        });*/
                        data_receta_editar.id_receta = _.without(data_receta_editar.id_receta, _.findWhere(data_receta_editar.id_receta, {cat: $scope.producto_receta_.elemento_receta.categoria}));
                           
                            if(data_receta_editar.id_receta){
                                data_receta_editar.id_receta.push({
                                    cat: $scope.producto_receta_.elemento_receta.categoria,
                                    receta: $scope.producto_receta_.elemento_receta.codigo,
                                    nom_receta: $scope.producto_receta_.elemento_receta.descripcion
                                });
                            }
                            $scope.receta_opcion = data_receta_editar.id_receta;
                            var data_plan = setDataEdicion();
                            $scope.datos_plan_diario = recargaPlanDiarioData(data_plan,opcion,data_receta_editar.id_receta);
                            $scope.$emit('success', {action: 'Producto de receta editado con exito!', element: '', identifier: ''});                   
                            
                    }else{
                         $scope.$emit('warning', {action: 'No ha modificado el producto de la receta!', element: '', message: ''});                   
                    }                  

                };

                function validar_clasificacion()
                {
                    var valida_clasificacion = true;
                    angular.forEach($scope.productos_plan_diario, function(value, key) {

                        if (value.clasificacion == "")
                        {
                            $scope.$emit('warning', {action: 'No tiene Clasificacion!', element: value.name_product, message: ''});
                            valida_clasificacion = false;
                        }
                    });

                    return valida_clasificacion;
                }

                function validar_produccion_plan_diario()
                {
                   var valida = true;
                    if ($scope.orden_produccion[0].estado_produccion == 2)
                        valida = false;
                    return valida;

                }

                function validar_edicion_plan_diario()
                {
                    var valida = true;
                    if ($scope.orden_produccion[0].estado_produccion != 0)
                        valida = false;
                    return valida;

                }

                $scope.get_estado_requisiciones = function () {
                    var params = {
                          url_params: {orden_produccion: $scope.orden_produccion[0].numero_produccion}
                        };

                    plan_diario_model.get('estado_requisiciones', params).then(function (datos) {
                        var msg = "";
                        if (datos.length > 0)
                        {

                        _.each(datos, function (requisicion) {
                            msg += requisicion.requisicion + ': ' + requisicion.estado + '\n';
                            });
                    }else{
                        msg = 'No existen requisiciones creadas';
                    }
                            alert(msg);
                    });
                }

                function validar_recetas()
                {
                    var valida_receta = true;
                    _.each($scope.datos_plan_diario, function (opcion) {
                        _.each($scope.categorias_producto_final, function (categoria) {
                            var receta_in = _.findWhere(opcion.id_receta, {cat: categoria.codigo});
                            if (receta_in == "" || receta_in == undefined || receta_in == null)
                            {
                                $scope.$emit('warning', {action: 'Falta Completar Receta! Opcion: ', element: opcion.code, message: ''});
                                valida_receta = false;
                            }
                        });
                    });

                    return valida_receta;
                }

        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/plan_diario/views/plan_diario.html",
                    controller: "plan_diario.controller",
                    resolve:{
                                categorias: function ($location, plan_diario_model) {
                                    return plan_diario_model.get('categoria')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                umedidas: function ($location, creador_recetas_model) {
                                    return creador_recetas_model.get('unidades_medidas')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                productos_para_receta: function ($location, creador_recetas_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return creador_recetas_model.get('productos_para_receta', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }
                    }
                });
        });
})();

