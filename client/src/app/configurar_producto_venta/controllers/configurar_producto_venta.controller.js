(function() {

    "use strict";

    angular.module("Configurar_producto_venta").controller("configurar_producto_venta.controller", 
        function($scope,_ ,$location, $http, configurar_producto_venta_model,datos_producto,categorias_producto_final,categorias_by_producto_final) {
        
        //console.log(datos_producto);
        if(datos_producto.length > 0){
            $scope.nombre_producto = datos_producto[0].desprod01;
            $scope.codigo_producto = datos_producto[0].codprod01;
        }else{

             $scope.nombre_producto = globalApp.nomProd;
             $scope.codigo_producto = globalApp.codProd;

        }

        //console.log($scope.codigo_producto);
        $scope.categorias_producto_final = categorias_producto_final;
        $scope.cate_prod_final = categorias_by_producto_final;       


        $scope.addCategory = function () {
                    
            
                    if(($scope.cantidad_ != undefined) && ($scope.categorias_producto_final_ != undefined)){

                        var i;
                        var array_actual = $scope.cate_prod_final;

                        for(i=0;i<array_actual.length;i++){

                               if(array_actual[i].codigo == $scope.categorias_producto_final_.codcate){
                                     alert('Elemento ya existe. Editar desde la caja de texto en la columna cantidad.');
                                     limpiarValores();
                                     return;
                                     
                               }
                        }

                        $scope.cate_prod_final.push({'codigo': $scope.categorias_producto_final_.codcate,
                            'descripcion': $scope.categorias_producto_final_.desccate,
                            'cantidad': $scope.cantidad_
                        });

                         limpiarValores();

                    }else{

                        alert('Categoria y cantidad son campos obligatorios.');
                    }              

        };

        $scope.removeCategory = function(index) {
                    $scope.cate_prod_final.splice(index, 1);
        };


         function data_categories() {
                    var data = [];
                    var cont = 0;

                    //console.log($scope.cate_prod_final);
                    _($scope.cate_prod_final).each(function (categories) {

                         if(categories.cantidad == ''){
                           
                           cont = Number(cont) + 1;                          
                           
                         }
                       
                    });


                     if(cont == 0){

                         _($scope.cate_prod_final).each(function (categories) {
                             
                                data.push({'codigo_producto': $scope.codigo_producto,
                                    'codigo_categoria': categories.codigo,
                                    'cantidad': categories.cantidad                                
                                });
                            
                        });

                    }else{

                         $scope.$emit('warning', {custom_message: 'Existen datos con cantidad en blanco.'});
                         return;

                    }
                    return data;
                };


        $scope.save = function () {
                    var params = {
                        url_params: {producto: $scope.codigo_producto },
                        data: data_categories()
                    };

                    var array_data = data_categories();
                    //console.log(array_data);

                    if(array_data.length > 0){

                        configurar_producto_venta_model.create('guardar_categorias', params).then(function () {
                            $scope.$emit('success', {action: 'actualizadas', element: 'Categorias', identifier: ''});
                            limpiarValores();
                            window.close();
                        });                    
                        


                    }else{

                        $scope.$emit('warning', {custom_message: 'No existen registros para ser guardados.'});
                        configurar_producto_venta_model.create('guardar_categorias', params).then(function () {
                            
                        });
                        

                        
                    }
                    
        };



        $scope.cancelar = function () {
                    limpiarValores();
                };


        $scope.salir = function () {

               window.close();
                    
                };

        function limpiarValores() {
                    
                    $scope.categorias_producto_final_ = undefined;
                    $scope.cantidad_ = undefined;

        }


        

        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/configurar_producto_venta/views/configurar_producto_venta.html",
                    controller: "configurar_producto_venta.controller",
                    resolve: {

                        datos_producto: function ($location, configurar_producto_venta_model) {
                         
                                    var params = {url_params: {producto: globalApp.codProd }};
                                    return configurar_producto_venta_model.get('nombre_producto_final', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },


                        categorias_producto_final: function ($location, configurar_producto_venta_model) {                        
                                   
                                    return configurar_producto_venta_model.get('categorias_producto_final')
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },

                        categorias_by_producto_final: function ($location, configurar_producto_venta_model) {                        
                                   var params = {url_params: {producto: globalApp.codProd }};
                                    return configurar_producto_venta_model.get('categorias_by_producto_final',params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }

                    }
                });
        });
})();

