(function() {
  "use strict";

  angular.module("Configurar_producto_venta").factory("configurar_producto_venta_model", function(model) {
    return model("configurar_producto_venta");
  });
})();

