(function() {

    "use strict";

    angular.module("Dispatch").controller("dispatch.controller", function($scope, $http, _, dispatch_model) {

                $scope.clientes = [];
                $scope.productos = [];                
                $scope.parametros_de_busqueda = {
                    nombre: null,
                    id: null
                };
                $scope.open = {};
                $scope.open.dayDispatch = false;
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };
                $scope.parametros_de_busqueda = {
                    descripcion: null,
                    codigo: null
                };

                $scope.datosDispatchDay = [];

                $scope.changeDayDispatch = function () {
                    $scope.open.dayDispatch = true;
                };

                
                $scope.buscar_productos = function (flag) {

                    if ($scope.parametros_de_busqueda.descripcion == '') {
                        $scope.parametros_de_busqueda.descripcion = null;
                    }

                    if ($scope.parametros_de_busqueda.codigo == '') {
                        $scope.parametros_de_busqueda.codigo = null;
                    }


                    if(flag){
                     var params = {url_params: {codigo: $scope.parametros_de_busqueda.codigo,
                            descripcion: $scope.parametros_de_busqueda.descripcion}};

                    }else{

                     var params = {url_params: {descripcion: null, codigo: null}};
                     $scope.parametros_de_busqueda.descripcion = null;
                     $scope.parametros_de_busqueda.codigo = null;
                    }

                    

                    dispatch_model.get('productos_tipo', params).then(function (datos) {
                        $scope.productos = datos;
                    });
                };

                function dispatchData(dispatch_data) {

                    
                    var dispatch = [];
                    var idClient = 0, idProduct = 0, idOption = 0;
                    var statusDay = -1, statusClient = -1, statusProduct = -1, statusOption = -1;
                    
                    _.each(dispatch_data, function (data) {                      
                       
                        var day = _.findWhere(dispatch, {code: data.id_cliente});
                       
                       if (!day) {

                            dispatch.push(
                                    {
                                        id: ++idClient,
                                        code: data.id_cliente,
                                        name: data.name_client,
                                        contrato: data.codigo,                                        
                                        products: []
                                    }
                            );
                        }
                        
                        var addClientes = _.findWhere(dispatch, {code: data.id_cliente});
                        var addOpciones = _.findWhere(addClientes.products, {code: data.id_producto});

                        if (!addOpciones) {
                            
                            addClientes.products.push(
                                    {
                                        id: ++idProduct,
                                        code: data.id_producto,
                                        name: data.name_product,
                                        estado_dispatch: data.estado_despacho,
                                        options: []
                                    }
                            );
                            
                        }                        
                        var addClientes = _.findWhere(dispatch, {code: data.id_cliente});
                        var addOpciones = _.findWhere(addClientes.products, {code: data.id_producto});
                        if (addOpciones) {
                            //console.log(data.cantidad_dispatch,'eee');
                            if(!data.cantidad_dispatch){
                                    data.cantidad_dispatch = 0.00;
                            }
                            if(!data.cantidad_entregada){
                                    data.cantidad_entregada = 0.00;
                            }
                        addOpciones.options.push(
                                    {
                                        id: data.opcion,
                                        option: data.opcion,
                                        cantidad: data.cantidad,
                                        cantidad_dispatch: data.cantidad_dispatch,
                                        cantidad_entregada: data.cantidad_entregada,
                                        contrato: data.codigo,
                                        estado_dispatch: data.estado_despacho,
                                        desde: data.desde,
                                        hasta: data.hasta
                                    }
                            );

                        }


                    });
                    //console.log(dispatch);
                    return dispatch;
                }


                $scope.guardar = function () {

                    if($scope.dayDispatch == undefined){

                        $scope.$emit('warning', {action: 'No existe!', element: 'una dia seleccionado', message: ''});

                    }else{

                        var validar_opciones = validarDataForSaveAll();

                        if (validar_opciones == 0)
                        {
                        var params = {
                            data : {
                                data1 : parseDataForSaveAll(),
                                clientes: $scope.clientes_a_eliminar,
                                productos: $scope.productos_a_eliminar,
                                opciones: $scope.opciones_a_eliminar
                                }
                        };

                        dispatch_model.create('guardar_dispatch', params).then(function () {
                            cargar_vista();
                            $scope.$emit('success', {action: 'actualizado.', element: 'Dispatch', identifier: ''});
                        });
                        }
                    }

                };

                 function parseDataForSaveAll() {
                     //console.log($scope.datosDispatchDay);
                    $scope.fecha = moment($scope.dayDispatch).format('YYYY-MM-DD');
                    var parseData = [];
                          _.each($scope.datosDispatchDay, function (client) {                                                 
                            _.each(client.products, function (product) {
                                _.each(product.options, function (option) {
                                    parseData.push(
                                            {
                                                id_cliente: client.code,
//                                                name_client: client.name,
                                                id_producto: product.code,
//                                                name_product: product.name,
                                                //dia: day.id,
                                                opcion: option.option,
                                                cantidad_dispatch: option.cantidad_dispatch,
                                                cantidad_entregada: option.cantidad_entregada,
                                                fecha: $scope.fecha,
                                                desde: option.desde,
                                                hasta: option.hasta
                                                                                                
                                            }
                                    );
                                });
                            });
                      });
                      //console.log(parseData);
                    return parseData;
                }

                function validarDataForSaveAll() {
                     //console.log($scope.datosDispatchDay);
                    var cont = 0;
                    $scope.fecha = moment($scope.dayDispatch).format('YYYY-MM-DD');
                    var parseData = [];
                          _.each($scope.datosDispatchDay, function (client) {                                                 
                            _.each(client.products, function (product) {
                                _.each(product.options, function (option) {
                                    if (option.cantidad_dispatch <= 0)
                                    {
                                        cont =  cont + 1;

                                    }
                                });
                            });
                      });

                    if (cont > 0)
                        $scope.$emit('warning', {action: 'No se puede crear opciones con valores en 0, en Cant.Dispatch', element: '', message: ''});
                    return cont;
                }

                function clean() {
                    window.location.reload();

                }

                $scope.cancelar = function () {
                    window.location.reload();
                };


                function parseDataForDispatch(clients,productos_seleccionados) {
                    
                    $scope.fecha = moment($scope.dayDispatch).format('YYYY-MM-DD');
                    var parseData = [];
                                                                           
                            _.each(clients.products, function (product) {

                                 _.each(productos_seleccionados, function (product_select) {

                                    if(product.code == product_select){
                                          var prod = '';
                                        _.each(product.options, function (option) {

                                            
                                            parseData.push(
                                                    {
                                                        id_cliente: clients.code,
                                                        id_producto: product.code,                                                       
                                                        cantidad_dispatch: option.cantidad_dispatch,
                                                        cantidad_entregada: option.cantidad_entregada,
                                                        opcion: option.option,
                                                        codigo:option.contrato,
                                                        fecha: $scope.fecha,
                                                        desde: option.desde,
                                                        hasta: option.hasta                                                        
                                                                                                        
                                                    }
                                            );
                                        });


                                    }
                        
                                    
                                });
                            });
                      
                    return parseData;
                }



                function valida_cantidad_dispatch(clients,productos_seleccionados) {                  
                    
                    var cont = 0;                                            
                    _.each(clients.products, function (product) {

                         _.each(productos_seleccionados, function (product_select) {

                            if(product.code == product_select){
                                _.each(product.options, function (option) {

                                    if(option.cantidad_dispatch == 0){
                                       cont = cont+1;
                                    }
                                });
                            }
                        });
                    });

                    if (cont > 0)
                        $scope.$emit('warning', {action: 'Existen valores en cero en el campo Cant.Dispatch.', element: '', message: ''});
                      
                    return cont;
                }

                function valida_cantidad_entregada(clients,productos_seleccionados) {                  
                    
                    var cont = 0;                                            
                    _.each(clients.products, function (product) {

                         _.each(productos_seleccionados, function (product_select) {

                            if(product.code == product_select){
                                _.each(product.options, function (option) {

                                    if(option.cantidad_entregada == 0){
                                       cont = cont+1;
                                    }
                                    if(option.cantidad_dispatch == 0){
                                       cont = cont+1;
                                    }
                                });
                            }
                        });
                    });
                    if (cont > 0)
                        $scope.$emit('warning', {action: 'Existen valores en cero en el campo Cant.Entregada. o Cant.Dispatch', element: '', message: ''});
                      
                    return cont;
                }



                


                $scope.setDataProduct = function (client) {                    
                    $scope.selectedClient = client;

                };

                $scope.removeItem = function (client, product, option) {
                    var parentClient = _.findWhere($scope.datosDispatchDay, {id: client.id});
                    if (!product) {
                        $scope.datosDispatchDay = _.without($scope.datosDispatchDay, client);
                        $scope.clientes_a_eliminar.push({
                            codigo_cliente : parentClient.code,
                            dia: moment($scope.dayDispatch).format('YYYY-MM-DD')
                        });
                    } else if (!option) {
                        parentClient.products = _.without(parentClient.products, product);
                        $scope.productos_a_eliminar.push({
                            codigo_cliente : parentClient.code,
                            codigo_producto : product.code,
                            dia: moment($scope.dayDispatch).format('YYYY-MM-DD')
                        });
                    } else {
                        var parentProduct = _.findWhere(parentClient.products, {id: product.id});
                        parentProduct.options = _.without(parentProduct.options, option);
                        $scope.opciones_a_eliminar.push({
                            codigo_cliente : parentClient.code,
                            codigo_producto : parentProduct.code,
                            opcion : option.option,
                            dia: moment($scope.dayDispatch).format('YYYY-MM-DD')
                        });
                    }

                };

                $scope.addClient = function (client) {

                     if ($scope.dayDispatch) {                       

                     var validaCliente = _.findWhere($scope.datosDispatchDay,{code:client.codcte01});

                        if(!validaCliente){    
                    
                            var id = 0;

                            if (_.last($scope.datosDispatchDay))
                                id = _.last($scope.datosDispatchDay).id;

                            var selectClient = {
                                id: parseInt(id) + 1,
                                code: client.codcte01,
                                name: client.nomcte01,
                                products: []
                            };
                            $scope.datosDispatchDay.push(selectClient);
                        }else{

                             $scope.$emit('warning', {action: 'El cliente ya existe en el dia seleccionado', element: '', message: ''});
                   
                        }

                    }else{

                        $scope.$emit('warning', {action: 'No existe!', element: 'una dia seleccionado para esta accion', message: ''});

                    }
                };

                $scope.addProduct = function (product) {
                    
                    
                    var parentClient = _.findWhere($scope.datosDispatchDay, {id: $scope.selectedClient.id});

                    var id = 0;

                    var validaProducto = _.findWhere(parentClient.products,{code:product.codigo});

                    if(!validaProducto){ 

                    if (_.last(parentClient.products))
                        id = _.last(parentClient.products).id;

                    var selectProduct = {
                        id: parseInt(id) + 1,
                        code: product.codigo,
                        name: product.descripcion,
                        options: []
                    };
                    parentClient.products.push(selectProduct);
                   }else{

                        $scope.$emit('warning', {action: 'El producto seleccionado ya existe para este cliente', element: '', message: ''});
                   

                   }
                };

                $scope.addOption = function (client, product) {
                    
                    var parentClient = _.findWhere($scope.datosDispatchDay, {id: client.id});
                    var parentProduct = _.findWhere(parentClient.products, {id: product.id});
                    var id = 0;

                    if (_.last(parentProduct.options)) {
                        id = _.last(parentProduct.options).id;
                    }

                    var selectOption = {
                        id: parseInt(id) + 1,
                        option: parseInt(id) + 1,
                        cantidad: 0,
                        cantidad_dispatch: 0,
                        cantidad_entregada: 0
                    };
                    parentProduct.options.push(selectOption);
                };

                 $scope.generar_pedido = function (client, tipo) {

                    //if(flag == undefined || flag == 0) {
                        var productos_seleccionados = [];
                        $("input[name="+client.code+"]").each(function (index) { 
                           if($(this).is(':checked')){
                              productos_seleccionados.push($(this).val());
                           }
                        });

                        if(productos_seleccionados.length > 0){

                            var params = {
                                data: parseDataForDispatch(client,productos_seleccionados)
                            };
                            if (tipo == 'pedido'){
                                var valida_cant_entregada = valida_cantidad_entregada(client,productos_seleccionados);
                                if(valida_cant_entregada == 0)
                                    generar_pedido(params);
                            }

                            if (tipo == 'guia'){
                                var valida_cant_dispatch = valida_cantidad_dispatch(client,productos_seleccionados);
                                if(valida_cant_dispatch == 0)
                                generar_guia(params, client);
                            }
                        }else{

                            $scope.$emit('warning', {action: 'Seleccione al menos un producto para Generar Pedido.', element: '', message: ''});
                   
                        }
                    /*}else{                   
                    
                        alert('Este producto ya fue despachado');
                    }*/
                };
                

                $scope.buscar_clientes = function (flag) {

                    if ($scope.parametros_de_busqueda.nombre == undefined) {
                        $scope.parametros_de_busqueda.nombre = null;
                    }

                    if ($scope.parametros_de_busqueda.id == undefined) {
                        $scope.parametros_de_busqueda.id = null;
                    }

                    


                    if(flag){
                     var params = {url_params: {id: $scope.parametros_de_busqueda.id,
                            nombre: $scope.parametros_de_busqueda.nombre}};

                    }else{

                     var params = {url_params: {id: null, nombre: null}};
                     $scope.parametros_de_busqueda.nombre = null;
                     $scope.parametros_de_busqueda.id = null;
                    }

                    dispatch_model.get('clientes', params).then(function (datos) {
                        $scope.clientes = datos;
                    });
                };
                
                
                $scope.$watch('dayDispatch', function () {
                    
                    if ($scope.dayDispatch) {
                        cargar_vista();
                        iniciar_valores();
                    }
                });

                function cargar_vista(){

                    $scope.fecha = moment($scope.dayDispatch).format('YYYY-MM-DD');
                        var params = {url_params: {fecha: $scope.fecha}};                       

                        dispatch_model.get('pre_plan_por_dia', params).then(function (datos) {
                             
                             $scope.datosDispatchDay = dispatchData(datos);
                            
                       });
                }

                function iniciar_valores(){
                    $scope.opciones_a_eliminar = [];
                    $scope.productos_a_eliminar = [];
                    $scope.clientes_a_eliminar = [];
                }

                function generar_pedido(params)
                {
                    dispatch_model.create('dispatch', params).then(function () {                                
                        $scope.$emit('success', {action: 'Generado', element: 'Pedido', identifier: ''});
                        $scope.guardar();
                    });

                }

                function generar_guia(detalle, client)
                {
                    var params = {url_params: {codido_cliente: client.code}}; 
                    dispatch_model.get('validaciones_generales', params).then(function (datos) {
                        if (datos.validacion)
                        {
                            var params = {
                                    data : {
                                        codido_cliente : client.code,
                                        despacho : detalle
                                        }
                                };

                            dispatch_model.create('guardar_guia_remision', params).then(function (data) {

                            $http({
                                method: 'POST',
                                url: full_path('dispatch', 'guia_remision', undefined),
                                data: {despacho: detalle, codido_cliente: client.code, guia_remision: data},
                                responseType: 'arraybuffer'
                                }).then(function (response) {
                                    var pdfName = 'guia_remision-'+ moment($scope.fecha).format('YYYYMMDD') + '.pdf';
                                    saveAs(new Blob([response.data], { type: 'application/pdf' }), pdfName);
                                });
                            $scope.guardar();
                            });
                        }else{
                            $scope.$emit('warning', {action: datos.error, element: '', message: ''});

                        }
                    });
                }

            function full_path(resource, action, params) {
                var path = globalApp.base_url + resource + '/api/' + action;
                if (params !== undefined && params.url_params) {
                  _(_.values(params.url_params)).each(function(param) {
                    path += '/' + param;
                  })
                }

                path += document.location.search;

                if (params !== undefined && params.fideicomiso) {
                  path += '&fideicomiso=' + params.fideicomiso;
                }

                return path;
            }

        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: globalApp.base_url + "../client/src/app/dispatch/views/dispatch.html",
                    controller: "dispatch.controller"
                    
                });
        });
})();

