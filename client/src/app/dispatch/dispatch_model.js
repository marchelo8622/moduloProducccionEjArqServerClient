(function() {
  "use strict";

  angular.module("Dispatch").factory("dispatch_model", function(model) {
    return model("dispatch");
  });
})();

