(function () {

    "use strict";

    angular.module("Reporte_plan_semanal").controller("reporte_plan_semanal.controller",
            function ($scope, _, $http, reporte_plan_semanal_model, productos, productos_compuestos,planificacion_semanal_model) {
                $scope.productos_compuestos = productos_compuestos;
                //$scope.semanas_planificar = semanas_planificar;
                $scope.open = {};
                $scope.open.week = false;
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };
                $scope.weeks = [
                ];
                $scope.costo_semanal = 0;
                $scope.costo_diario = 0;
                $scope.productos = productos;
                $scope.listDays = [
                    {id: 1, name: 'Lunes', active: true},
                    {id: 2, name: 'Martes'},
                    {id: 3, name: 'Miercoles'},
                    {id: 4, name: 'Jueves'},
                    {id: 5, name: 'Viernes'},
                    {id: 6, name: 'Sabado'},
                    {id: 7, name: 'Domingo'}
                ];
                $scope.add_option = function (day, product) {
                   //console.log(product.options);
                    var dy = _.findWhere($scope.weeks, {name: day.name});
                    var pr = _.findWhere(dy.products, {id: product.id});
                    //var val_option = 0;
                    var id = 0;
                    var cate = [];
                    
                    if (_.last(pr.options)) {
                        id = _.last(pr.options).option;
                        cate = angular.copy(_.last(pr.options).categorias);

                    }
                    _(cate).each(function(v){
                        v.receta = "";
                    })
                    
                    pr.options.push(
                            {
                                //id: ++id,
                                id:parseInt(id) + 1,
                                option: parseInt(id) + 1,
                                cantidad: null,
                                receta: "",
                                categorias: cate 
                            }
                    );
                };

                var planDia = [];
                var id_producto = "";
                var id_op = "";
                var id = 0, id_pr = 0;



                var id_da = "";
                function build_plan(semanas_planificar) {

                    $scope.weeks = [];
                    id_da = 0;
                    id_producto = "";
                    id = 0;
                    id_op = 0;
                    var otherDay = 0;

                    //console.log(semanas_planificar);
                    _.each(semanas_planificar, function (plan, key_plan) {

                        
                        var dayBusqueda = _.findWhere($scope.weeks, {id: parseInt(plan.dia)});                        
                            

                        if (!dayBusqueda) {                            
                            
                            var day = _.findWhere($scope.listDays, {id: parseInt(plan.dia)});                        
                             
                            $scope.weeks.push({id: day.id, name: day.name, products: []});  
                            //console.log($scope.weeks);
                            id_da = plan.dia; 

                        }

                        var addProductos = _.findWhere($scope.weeks, {id: parseInt(plan.dia)});
                        var addOpciones = _.findWhere(addProductos.products, {codigo: plan.id_producto});
                        if (!addOpciones) {
                        
                            addProductos.products.push({
                                name: plan.desprod01,
                                codigo: plan.id_producto,
                                id: ++id_pr,
                                options: []
                            }); 
                        }

                      
                        var addProductos = _.findWhere( $scope.weeks, {id: parseInt(plan.dia)});
                        var addOpciones = _.findWhere(addProductos.products, {codigo: plan.id_producto});
                           
                        if (addOpciones) {  
                            //console.log(plan.id_receta);  
                            var id = key_plan;                    
                            addOpciones.options.push({
                                id: key_plan,
                                option: plan.opcion,
                                cantidad: plan.cantidad_total,
                                receta: JSON.parse(plan.id_receta),                               
                                categorias: JSON.parse(plan.configuraciones_prod_final)
                            });  

                            var categoriasModificadas = _.findWhere(addOpciones.options, {id: key_plan});
                            
                            _(categoriasModificadas.categorias).each(function(v, k){
//                                console.log(v, "categoria");
                                _(JSON.parse(plan.id_receta)).each(function(val, key){

                                    if(v.codigo == val.cat){
                                        v.receta = val.receta;
                                    }
                                })
                            })
                            

                        }

                    });
                    
                }

                $scope.removeOption = function (day, product, option) {
                    var cl = _.findWhere($scope.weeks, {id: day.id});
                    var pr = _.findWhere(cl.products, {id: product.id});
                    pr.options = _.without(pr.options, option);
                };

                function loadData() {
                    var params = {url_params: {desde: $scope.desde, hasta: $scope.hasta}};

                    reporte_plan_semanal_model.get('planificacion_by_from', params).then(function (datos) {
                        
                        if (datos.length) {
                            build_plan(datos);
                        } else {
                            reporte_plan_semanal_model.get('pre_plan', params).then(function (datos) {
                                if (datos.length) {
                                    build_plan(datos);
                                } else {
                                    toastr.warning('No existen planificaciones guardadas', 'Datos');
                                }
                            });
                        }
                    });


                }


                $scope.calcular_costo_semana = function () {

                    
                    var params = {
                        data: set_data()
                    };
                     //console.log(params);
                    planificacion_semanal_model.create('calcular_costo_semana', params).then(function (data) {
                       // console.log(data);
                        $scope.costo_semanal = data;
                    });

                };

                $scope.calcular_costo_diario = function (dia) {
                    var params = {
                        url_params: {dia:dia},
                        data: set_data()
                    };
                    
                    planificacion_semanal_model.create('calcular_costo_diario', params).then(function (data) {
                    
                         $scope.costo_diario = data.costo_dia;
                         var total_producto = 0;
                         var prod = '';
                         var data_sum_opciones = [];
                         _.each(data.costos_opciones, function (datos_opcion) {
                            var id_span_op = '#op'+datos_opcion.dia + datos_opcion.producto + datos_opcion.opcion;
                            var id_span_prod = '#prod'+datos_opcion.dia + datos_opcion.producto;                        
                            $(id_span_op).text(' (C.U: $' + datos_opcion.costo_unitario+') (C.T: $' + datos_opcion.costo.toFixed(2)+') ');
                            $(id_span_prod).text(' ($' + datos_opcion.suma_por_producto+') '); 
                        });                   


                   });

                };

                function set_data() {
                    var data = [];
                    var recetas = [];             
                    _.each($scope.weeks, function (day) {                       
                        
                        _.each(day.products, function (product) {

                            _.each(product.options, function (option) {
                                
                                  recetas = [];

                                _.each(option.categorias, function (cat) {
                                     //console.log(option.categorias);
                                     recetas.push({
                                        cat: cat.codigo,                                        
                                        receta: cat.receta
                                     });

                                });

                                 if(product.id_cliente == '' || product.id_cliente == undefined || product.id_cliente == null){
                                    product.id_cliente = $scope.cliente_interno;
                                 }

                                data.push({
                                    id_producto: product.codigo,
                                    id_cliente: product.id_cliente,
                                    id_receta: JSON.stringify(recetas),
                                    dia: day.id,
                                    opcion: option.option,
                                    cantidad: option.cantidad,
                                    //id_cliente: option.id_cliente,
                                    desde: $scope.desde,
                                    hasta: $scope.hasta
                                    //estado: 1
                                });


                            });
                        });
                        
                    });

                    //console.log(data);
                    return data;
                }



                function save_data(dia) {
                    var data = [];
                    var recetas = [];             
                    _.each($scope.weeks, function (day) {                       
                        
                        _.each(day.products, function (product) {

                            _.each(product.options, function (option) {
                                
                                  recetas = [];

                                _.each(option.categorias, function (cat) {
                                     console.log(option);
                                     recetas.push({
                                        cat: cat.codigo,
                                        receta: cat.receta
                                     });

                                });

                                if(day.id == dia){
                                    data.push({
                                        id_producto: product.codigo,
                                        id_receta: JSON.stringify(recetas),
                                        dia: day.id,
                                        opcion: option.option,
                                        cantidad: option.cantidad,
                                        //id_cliente: option.id_cliente,
                                        desde: $scope.desde,
                                        hasta: $scope.hasta
                                        //estado: 1
                                    });
                                }


                            });
                        });
                        
                    });

                    
                    return data;
                }

                $scope.generar_receta_diaria_pdf = function () {
                    if($scope.idDia == undefined){
                        alert('Seleccione el dia de la semana a imprimir');
                    }else{
                    
                     var arrayDatos = save_data($scope.idDia);
                     var jsonDatos = angular.toJson(arrayDatos);                   
                    
                    /*reporte_plan_semanal_model.create('imprimir_plan_semanal', params).then(function () {
                        $scope.$emit('success', {action: 'actualizados', element: 'Productos', identifier: ''});
                    });*/

                     $http({
                        method: 'POST',
                        url: full_path('reporte_plan_semanal', 'imprimir_plan_semanal', undefined),
                        data: { datos : jsonDatos },
                        responseType: 'arraybuffer'
                    }).then(function (response) {

                        var pdfName = 'planSemanal-'+ moment().format('YYYYMMDD') + '.pdf';
                        saveAs(new Blob([response.data], { type: 'application/pdf' }), pdfName);
                    });


                    }
                    
                }

                $scope.imprimir = function (){
                    if($scope.idDia == undefined){
                        alert('Seleccione el dia de la semana a imprimir');
                    }else{
                    
                    
                     var arrayDatos = save_data($scope.idDia);
                     var jsonDatos = angular.toJson(arrayDatos);                     
                    /*reporte_plan_semanal_model.create('imprimir_plan_semanal', params).then(function () {
                        $scope.$emit('success', {action: 'actualizados', element: 'Productos', identifier: ''});
                    });*/

                     $http({
                        method: 'POST',
                        url: full_path('reporte_plan_semanal', 'imprimir_consolidado', undefined),
                        data: { datos : jsonDatos },
                        responseType: 'arraybuffer'
                    }).then(function (response) {

                        var pdfName = 'Consolidado-'+ moment().format('YYYYMMDD') + '.pdf';
                        saveAs(new Blob([response.data], { type: 'application/pdf' }), pdfName);
                    });


                    }
                }

                

                function full_path(resource, action, params) {
      
                    var path = globalApp.base_url + resource + '/api/' + action;

                    if (params !== undefined && params.url_params) {
                      _(_.values(params.url_params)).each(function(param) {
                        path += '/' + param;
                      })
                    }

                    path += document.location.search;

                    if (params !== undefined && params.fideicomiso) {
                      path += '&fideicomiso=' + params.fideicomiso;
                    }

                    return path;
                }

                $scope.captura_dia = function (dia) {
                    //alert(dia);
                    $scope.idDia = dia;                    
                }

                $scope.cancelar = function () {
                    window.location.reload();
                };

                $scope.seleccionar_producto = function (producto) {
                    var dy = _.findWhere($scope.weeks, {name: $scope.day_selected.name});
                    dy.products.push({
                        name: producto.descripcion,
                        codigo: producto.codigo,
                        id: ++id_pr,
                        options: [
                            {
                                id: ++id,
                                option: 1,
                                cantidad: 0,
                                receta: "0",
                                categorias: JSON.parse(producto.configuraciones_prod_final)
                            }
                        ]
                    })
                };

                $scope.set_day = function (day) {
                    $scope.day_selected = day;
                };

                $scope.changeWeek = function () {
                    $scope.open.week = true;
                };

                $scope.$watch('week', function () {
                    //console.log($scope.week);
                    if ($scope.week) {
                        $scope.desde = moment($scope.week).weekday(1).format('YYYY-MM-DD');
                        $scope.hasta = moment($scope.week).weekday(7).format('YYYY-MM-DD');
                        $scope.weeks = [
                        ];
                        planDia = [];
                        id_producto = "";
                        id = 0;
                        id = id_pr;
                        loadData();
                    }
                });

            })
            .config(function ($routeProvider) {
                $routeProvider
                        .when("/", {
                            templateUrl: globalApp.base_url + "../client/src/app/reporte_plan_semanal/views/reporte_plan_semanal.html",
                            controller: "reporte_plan_semanal.controller",
                            resolve: {
                                 
                                productos: function ($location, reporte_plan_semanal_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return reporte_plan_semanal_model.get('productos_tipo', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                },
                                productos_compuestos: function ($location, reporte_plan_semanal_model) {
                                    var params = {url_params: {descripcion: null, codigo: null}};
                                    return reporte_plan_semanal_model.get('productos_compuestos', params)
                                            .catch(function () {
                                                $location.path('/');
                                            });
                                }
                            }
                        });
            });
})();

