(function() {
  "use strict";

  angular.module("Facturacion_pedidos").factory("facturacion_pedidos_model", function(model) {
    return model("facturacion_pedidos");
  });
})();

