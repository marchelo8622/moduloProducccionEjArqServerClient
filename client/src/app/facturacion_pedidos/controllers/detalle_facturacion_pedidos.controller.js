(function() {

    "use strict";

    angular.module("Facturacion_pedidos").controller("detalle_facturacion_pedidos.controller", function(_, $route, $location, $scope, $http, facturacion_pedidos_model, pedidos_aprobados_by_cliente, vendedores, condiciones_pago, impresoras, formatos, localidades, transportistas, ayudantes, datos_cliente, iva_default, formas_pago) {

            $scope.cliente_pedido_original = $route.current.params.codigo_cliente;
            $scope.cond_pago_original = $route.current.params.cond_pago;
            $scope.lista_pedidos = pedidos_aprobados_by_cliente;
            $scope.nocomp = '';

            $scope.pedidos_check = [];
            $scope.vendedores = vendedores;
            $scope.flag_mod_a_facturar = 0;
            $scope.dividir_factura = false;
            $scope.condiciones_pago = condiciones_pago;
            $scope.impresoras = impresoras;
            $scope.formatos = formatos;
            $scope.localidades = localidades;
            $scope.transportistas = transportistas;
            $scope.ayudantes = ayudantes;
            $scope.datos_cliente = datos_cliente;
            $scope.items_seleccionados = [];
            $scope.parametros_de_busqueda = {
                nombre: null,
                id: null
            };

            $scope.deshabilitar_boton_fact = true;

            $scope.habilitarCondPago = false;
            $scope.num_dias = 0;
            $scope.flag = 1;

            $scope.nombre_cliente = $scope.datos_cliente.nombre_cliente;
            $scope.codigo_cliente = $scope.datos_cliente.codigo_cliente;
            $scope.ci_ruc = $scope.datos_cliente.ci_ruc;
            $scope.telefono = $scope.datos_cliente.telefono;
            $scope.direccion = $scope.datos_cliente.direccion;
            $scope.vendedor = $scope.datos_cliente.vendedor;
            $scope.condPago = '';
            $scope.localidad = $scope.datos_cliente.localidad;
            $scope.contacto = $scope.datos_cliente.contacto;
            $scope.formas_pago = formas_pago;
            $scope.porcentaje_portfee = $scope.datos_cliente.porcentaje_portfee;


            carga_inicial_cond_pago();

            $scope.total = 0;
            $scope.otrosSinIva = 0;
            $scope.iva = iva_default.iva;
            $scope.fecha = iva_default.fecha_formato1;
            $scope.fecha2 = iva_default.fecha_formato2;
            $scope.valor_iva = 0;
            $scope.recargos = 0;
            $scope.descFp = 0;
            $scope.decC = 0;
            $scope.subtotal = 0;
            $scope.totalafac = 0;

            $scope.checkall = false;
            $scope.detalle_formas_pago = [{
                    formasPago: '',
                    num_cheq_tarj: '',
                    banc_cta_emi: '',
                    valor: $scope.total
                }

            ];


            if ($scope.pedidos_check.length == 0) {
                $scope.flag_detalles = 0;
            } else {
                $scope.flag_detalles = 1;
            }

            $scope.pedidos_seleccionados = function(pedido, index) {
                $scope.pedidos_check = [];
                pedido.bloqueado = false;
                var items_checked = 0;
                var descuentoG_S = (pedido.descto30 / pedido.totalpedido) * 100;
                $scope.condPago = pedido.condpag30;
                _($scope.lista_pedidos).each(function(dato, key) {
                    if (index != key) {

                        var descuentoG_P = (dato.descto30 / dato.totalpedido) * 100;
                        if (dato.condpag30 == pedido.condpag30 && pedido.iva30 == dato.iva30 && descuentoG_P == descuentoG_S) {
                            dato.bloqueado = false;
                        } else {
                            dato.bloqueado = true;
                        }
                    }
                    if (dato.checked) {
                        items_checked = items_checked + 1;
                    }
                });

                if (items_checked == 0) {
                    _($scope.lista_pedidos).each(function(dato) {
                        dato.bloqueado = false;
                    });
                }

                var cont = 0;

                _($scope.lista_pedidos).each(function(dato, key) {

                    if (dato.checked) {
                        $scope.pedidos_check.push(dato);
                        cont = cont + 1;
                    }
                });

                if (cont == 0) {
                    $scope.items_pedido = [];
                    $scope.flag_detalles = 0;
                    $scope.condPago = '';
                }

                carga_inicial_cond_pago();
            };

            $scope.detalle_pedidos = function() {

                if ($scope.pedidos_check.length > 0) {
                    //bloquear pedidos
                    bloquear_pedidos();

                    $scope.flag_detalles = 1;

                    var params = {
                        url_params: {
                            cliente: $scope.cliente_pedido_original
                        },
                        data: $scope.pedidos_check
                    };
                    //console.log(params);
                    facturacion_pedidos_model.create('pedidos_by_condicion', params).then(function(datos) {
                        //console.log(datos,'data');                   
                        if (datos.length) {
                            $scope.items_pedido = datos;
                        }
                    });

                } else {
                    $scope.$emit('warning', {
                        custom_message: 'No ha seleccionado pedidos!'
                    });
                }

            };


            $scope.refresh = function() {
                window.location.reload();
            };

            $scope.buscar_productos = function(flag) {

                $scope.bandera_producto = 0;

                if ($scope.parametros_de_busqueda.descripcion == '') {
                    $scope.parametros_de_busqueda.descripcion = null;
                }

                if ($scope.parametros_de_busqueda.codigo == '') {
                    $scope.parametros_de_busqueda.codigo = null;
                }

                if (flag) {
                    var params = {
                        url_params: {
                            codigo: $scope.parametros_de_busqueda.codigo,
                            descripcion: $scope.parametros_de_busqueda.descripcion,
                            flag: 0
                        }
                    };

                } else {

                    var params = {
                        url_params: {
                            descripcion: null,
                            codigo: null,
                            flag: 0
                        }
                    };
                    $scope.parametros_de_busqueda.descripcion = null;
                    $scope.parametros_de_busqueda.codigo = null;
                }


                facturacion_pedidos_model.get('productos_tipo', params).then(function(datos) {
                    $scope.productos = datos;
                });
            };

            $scope.buscar_productos_equivalente = function(flag, cod_producto) {

                $scope.bandera_producto = 1;
                if ($scope.parametros_de_busqueda.descripcion == '') {
                    $scope.parametros_de_busqueda.descripcion = null;
                }

                if ($scope.parametros_de_busqueda.codigo == '') {
                    $scope.parametros_de_busqueda.codigo = null;
                }

                //if(flag){
                var params = {
                    url_params: {
                        codigo: $scope.parametros_de_busqueda.codigo,
                        descripcion: $scope.parametros_de_busqueda.descripcion,
                        flag: 1,
                        cod_producto_principal: cod_producto
                    }
                };

                facturacion_pedidos_model.get('productos_tipo', params).then(function(datos) {
                    $scope.productos = datos;
                    $scope.producto_principal_equivalente = cod_producto;
                });
            };

            $scope.buscar_clientes = function(flag) {

                if ($scope.parametros_de_busqueda.nombre == undefined) {
                    $scope.parametros_de_busqueda.nombre = null;
                }

                if ($scope.parametros_de_busqueda.id == undefined) {
                    $scope.parametros_de_busqueda.id = null;
                }

                if (flag) {
                    var params = {
                        url_params: {
                            id: $scope.parametros_de_busqueda.id,
                            nombre: $scope.parametros_de_busqueda.nombre
                        }
                    };

                } else {

                    var params = {
                        url_params: {
                            id: null,
                            nombre: null
                        }
                    };
                    $scope.parametros_de_busqueda.nombre = null;
                    $scope.parametros_de_busqueda.id = null;
                }


                facturacion_pedidos_model.get('clientes', params).then(function(datos) {
                    $scope.clientes = datos;
                });
            };

            $scope.addClient = function(datos_nuevo_cliente) {

                $scope.datos_cliente = datos_nuevo_cliente;

                $scope.nombre_cliente = $scope.datos_cliente.nombre_cliente;
                $scope.codigo_cliente = $scope.datos_cliente.codigo_cliente;
                $scope.telefono = $scope.datos_cliente.telefono;
                $scope.ci_ruc = $scope.datos_cliente.ci_ruc;
                $scope.direccion = $scope.datos_cliente.direccion;
                $scope.vendedor = $scope.datos_cliente.vendedor;
                //$scope.condPago = $scope.datos_cliente.condPago;
                $scope.localidad = $scope.datos_cliente.localidad;
                $scope.contacto = $scope.datos_cliente.contacto;
                $scope.porcentaje_portfee = $scope.datos_cliente.porcentaje_portfee;
            };

            $scope.addProduct = function(product, producto_principal_equivalente = '') {
                if (producto_principal_equivalente == '') {
                    $scope.producto_principal_equivalente = '';
                }

                var selectProduct = {
                    bodega30: '',
                    cantafac30: 0,
                    cantfac30: 0,
                    cantped30: 0,
                    codcte30: '',
                    codprod30: product.codigo,
                    condpag30: '',
                    descto30: 0,
                    desctofp30: 0,
                    recargos30: 0,
                    dividir_pedido: '',
                    fecpedido30: '',
                    iva30: product.porciva01,
                    localid30: '',
                    nom_producto: product.descripcion,
                    desctoxprod30: 0,
                    nomcte30: '',
                    nopedido30: '',
                    ocurren30: '',
                    novend30: '',
                    precuni30: 0,
                    status30: '',
                    total: 0,
                    aFacturar: 0,
                    uid: '',
                    piva30: product.porciva01,
                    prod_principal_equiv: producto_principal_equivalente
                };

                $scope.items_pedido.push(selectProduct);


            };

            $scope.eliminar_producto = function(datos_fac, index_vista) {

                var copias_items = angular.copy($scope.items_pedido);
                _(copias_items).map(function(item_pedidos, index) {
                    if (index_vista == index) {
                        $scope.items_pedido.splice(index, 1);
                    }
                });

            };



            $scope.calcular_totales = function() {

                if ($scope.nocomp == '') {

                    get_nocomp_movpro_temporales();

                } else {
                    proceso_calcular_totales();
                }

            };

            function proceso_calcular_totales() {
                $scope.flag = 1;
                $scope.subtotal = 0;
                $scope.totalafac = 0;
                $scope.sTotSinIva = 0;
                $scope.sTotConIva = 0;

                _($scope.items_pedido).each(function(dato, index) {
                    dato.total = 0;
                    if (dato.checked) {
                        if ($scope.flag_mod_a_facturar == 0) {
                            dato.aFacturar = formatear_valor_decimal(parseFloat(dato.cantped30) - parseFloat(dato.cantfac30), 2);
                        }
                        dato.total = formatear_valor_decimal((dato.aFacturar * dato.precuni30) - dato.desctoxprod30, 2);
                        $scope.subtotal += parseFloat(dato.total);
                        $scope.totalafac += parseFloat(dato.aFacturar);
                        if (dato.piva30 == 0) {
                            $scope.sTotSinIva += parseFloat(dato.total);
                        } else {

                            $scope.sTotConIva += parseFloat(dato.total);

                        }
                        $scope.descto30 = formatear_valor_decimal(dato.descto30, 2);
                        $scope.desctofp30 = formatear_valor_decimal(dato.desctofp30, 2);
                        $scope.recargos = formatear_valor_decimal(dato.recargos30, 2);
                        $scope.decC = formatear_valor_decimal(dato.descto30, 2);
                        $scope.descFp = formatear_valor_decimal(dato.desctofp30, 2);

                        inserta_temporales($scope.nocomp, index, dato.codprod30, dato.aFacturar, dato.bodega30);

                    } else {
                        elimina_temporales($scope.nocomp, index, dato.codprod30, dato.aFacturar, dato.bodega30);
                    }
                });

                $scope.otrosSinIva = formatear_valor_decimal(($scope.subtotal * ($scope.porcentaje_portfee / 100)), 2);

                $scope.subtotal = formatear_valor_decimal($scope.subtotal, 2);
                $scope.totalafac = formatear_valor_decimal($scope.totalafac, 2);
                $scope.sTotSinIva = formatear_valor_decimal($scope.sTotSinIva, 2);
                $scope.sTotConIva = formatear_valor_decimal($scope.sTotConIva, 2);


                $scope.valor_iva = formatear_valor_decimal(parseFloat($scope.sTotConIva) * (parseFloat($scope.iva) / 100), 2);
                $scope.total = formatear_valor_decimal((((parseFloat($scope.subtotal) + parseFloat($scope.valor_iva) + parseFloat($scope.otrosSinIva)) - parseFloat($scope.decC)) - parseFloat($scope.descFp)) + parseFloat($scope.recargos), 2);
                var detalle_formas_pago_bkp = angular.copy($scope.detalle_formas_pago);
                $scope.detalle_formas_pago = [];
                _(detalle_formas_pago_bkp).each(function(dato, index) {
                    if (index == 0) {
                        dato.valor = formatear_valor_decimal($scope.total, 2);
                        $scope.detalle_formas_pago.push(dato);
                    }
                });
            }

            $scope.items_pedidos_seleccionados = function(checkAll) {

                $scope.items_pedido.map(function(value) {
                    value.checked = checkAll.value;
                });
                $scope.calcular_totales();
            };


            $scope.validar_cantidad_a_facturar = function(valores_item) {


                var aFacturarActual = parseFloat(valores_item.cantped30) - parseFloat(valores_item.cantfac30);

                if (valores_item.checked) {

                    if (valores_item.aFacturar > aFacturarActual) {

                        $scope.$emit('warning', {
                            custom_message: 'No puede facturar una cantidad superior a la disponible ' + aFacturarActual + '.'
                        });

                        valores_item.aFacturar = aFacturarActual;

                    } else {
                        $scope.flag_mod_a_facturar = 1;
                        $scope.calcular_totales();
                    }

                } else {

                    $scope.$emit('warning', {
                        custom_message: 'Este item de la factura no se ecuentra seleccionado'
                    });

                    valores_item.aFacturar = aFacturarActual;

                }

            };

            $scope.add_forma_pago = function(dato) {

                var valores_formas_pago = valida_valor_formas_pago();
                if (valores_formas_pago < $scope.total) {
                    var data_forma_pago = {
                        formasPago: '',
                        num_cheq_tarj: '',
                        banc_cta_emi: '',
                        valor: calcular_valor_restante_forma_pago()
                    }

                    $scope.detalle_formas_pago.push(data_forma_pago);
                } else {

                    $scope.$emit('warning', {
                        custom_message: 'El pago esta completo.'
                    });

                }

            };

            $scope.eliminar_forma_pago = function(datos_fac, index_vista) {

                var copias_items = angular.copy($scope.items_pedido);
                _(copias_items).map(function(item_forma_pago, index) {
                    if (index_vista == index) {
                        $scope.detalle_formas_pago.splice(index, 1);
                        //$scope.calcular_totales();
                    }
                });

            };



            function valida_valor_formas_pago() {

                var sumValor = 0;

                _($scope.detalle_formas_pago).each(function(dato) {
                    sumValor = sumValor + dato.valor;
                });

                return sumValor;

            }

            function calcular_valor_restante_forma_pago() {

                var sumValor = 0;

                _($scope.detalle_formas_pago).each(function(dato) {
                    sumValor = sumValor + dato.valor;
                });

                var valor_restante = $scope.total - sumValor;

                return valor_restante;

            }



            function sumaFecha(d, fecha) {
                var Fecha = new Date();
                var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getFullYear());
                var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
                var aFecha = sFecha.split(sep);
                var fecha = aFecha[2] + '/' + aFecha[1] + '/' + aFecha[0];
                fecha = new Date(fecha);
                fecha.setDate(fecha.getDate() + parseInt(d));
                var anno = fecha.getFullYear();
                var mes = fecha.getMonth() + 1;
                var dia = fecha.getDate();
                mes = (mes < 10) ? ("0" + mes) : mes;
                dia = (dia < 10) ? ("0" + dia) : dia;
                var fechaFinal = dia + sep + mes + sep + anno;
                return (fechaFinal);
            }


            $scope.ingresar_factura = function() {

                if ($scope.items_pedido) {

                    var elementos_seleccionados = validar_elementos_seleccionados();

                    if (elementos_seleccionados > 0) {
                        var com;
                        $scope.flag = 0;

                        var valor_vencimiento = $scope.total / $scope.num_cuotas;

                        $scope.array_vencimientos = [];
                        $scope.suma_vencimientos = 0;

                        for (var i = 1; i <= $scope.num_cuotas; i++) {
                            com = i + '/' + parseInt($scope.num_cuotas);
                            var datos = {
                                fecha: sumaFecha(parseInt($scope.num_dias), $scope.fecha2),
                                valor: valor_vencimiento,
                                comentario: com
                            }
                            $scope.array_vencimientos.push(datos);
                            $scope.fecha2 = sumaFecha(parseInt($scope.num_dias), $scope.fecha2);
                            $scope.suma_vencimientos = $scope.suma_vencimientos + parseFloat(datos.valor);
                        }

                        $scope.deshabilitar_boton_fact = false;

                    } else {

                        $scope.$emit('warning', {
                            custom_message: 'Seleccione al menos un item del detalle del pedido.'
                        });

                    }

                } else {
                    $scope.$emit('warning', {
                        custom_message: 'No ha seleccionado ningun pedido.'
                    });
                }
            };

            $scope.data_condicion_pago = function() {
                var params = {
                    url_params: {
                        codigo: $scope.condPago
                    }
                };
                $scope.flag = 1;
                $scope.fecha2 = iva_default.fecha_formato2;
                $scope.array_vencimientos = [];
                facturacion_pedidos_model.get('data_condicion_pago', params).then(function(datos) {
                    $scope.num_dias = datos[0].dias;
                    $scope.num_cuotas = datos[0].num_cuotas;
                    $scope.tipo_pago = datos[0].tipo_pago;
                });


            };

            $scope.cancelar = function() {

                $http({
                    method: 'POST',
                    url: full_path('facturacion_pedidos', 'elimina_movimientos_temporales_producto_by_nocomp', undefined),
                    data: {
                        nocomp: $scope.nocomp
                    }
                }).then(function(response) {
                    $location.path('/');
                });


            };

            function validar_elementos_seleccionados() {
                var cont = 0;
                if ($scope.items_pedido.length > 0) {
                    _($scope.items_pedido).each(function(dato) {
                        if (dato.checked) {
                            cont = cont + 1;
                        }
                    });
                }

                return cont;
            }

            function carga_inicial_cond_pago() {

                var params = {
                    url_params: {
                        condPago: $scope.condPago

                    }
                };

                facturacion_pedidos_model.get('data_condicion_pago', params).then(function(datos) {
                    $scope.num_cuotas = datos[0].num_cuotas;
                    $scope.tipo_pago = datos[0].tipo_pago;
                });

            }

            function bloquear_pedidos() {

                _($scope.lista_pedidos).each(function(dato, key) {
                    //if (dato.checked) {
                    dato.bloqueado = true;
                    //}
                });

                $scope.habilitarCondPago = true;
            }

            $scope.facturar = function() {

                if ($scope.direccion != '' && $scope.vendedor != '' && $scope.localidad != '' && $scope.condPago != '' && $scope.ci_ruc != '' && $scope.impre != '') {
                    var params = {
                        data: {
                            items: get_items_solo_checkeados(),
                            nocomp: $scope.nocomp
                        }

                    };

                    facturacion_pedidos_model.create('validacion_stock', params).then(function(datos) {
                        if (datos.length > 0) {
                            var cadena = '';
                            _(datos).each(function(dato) {
                                cadena += dato.codProductoSinStock + ', ';
                            });
                            $scope.$emit('warning', {
                                custom_message: 'Los siguientes codigos de producto ' + cadena + ' superan a la cantidad en stock.'
                            });

                        } else {
                            //facturar
                            if (confirm('¿Estas seguro de realizar esta accion?')) {

                                var params = {
                                    data: {
                                        items: data_for_save(),
                                        nocomp: $scope.nocomp
                                    }

                                };
                                facturacion_pedidos_model.create('facturacion_pedidos', params).then(function(datos) {
                                    if (datos.respuesta) {
                                        $scope.deshabilitar_boton_fact = true;
                                        recontabilizar('02', datos.respuesta, '71', '21');

                                        $scope.$emit('success', {
                                            action: 'Creada con exito!',
                                            element: 'Factura',
                                            identifier: ''
                                        });

                                        if ($scope.dividir_factura) {
                                            alert('Usted seleccionó la opcion dividir factura para los pedidos que se van a marcar con *.');
                                            $location.path('facturacion_pedidos/detalle/' + $scope.cliente_pedido_original + '/' + $scope.cond_pago_original + '/');
                                        } else {
                                            $location.path('/');
                                        }

                                    } else {
                                        $scope.deshabilitar_boton_fact = false;
                                        $scope.$emit('warning', {
                                            action: datos.error,
                                            element: '',
                                            message: ''
                                        });
                                    }
                                });

                            }
                        }
                    });

                } else {

                    $scope.$emit('warning', {
                        custom_message: 'En la seccion informacion de cliente los campos con (*) son obligatorios.'
                    });

                }

            };

            function get_items_solo_checkeados() {
                var new_items_pedido = [];
                _($scope.items_pedido).each(function(dato) {
                    if (dato.checked) {
                        new_items_pedido.push(dato);
                    }
                });

                return new_items_pedido;
            }

            function data_for_save() {

                var data_factura = [];

                data_factura.push({
                    codigo_cliente: $scope.codigo_cliente,
                    venta_bruta: $scope.subtotal,
                    subtotal_coniva: $scope.sTotConIva,
                    subtotal_siniva: $scope.sTotSinIva,
                    total_iva: $scope.valor_iva,
                    iva: $scope.iva,
                    desc_cliente: $scope.decC,
                    desc_forma_pago: $scope.descFp,
                    desc_total: 0,
                    recargos: $scope.recargos,
                    valor_portfee: $scope.otrosSinIva,
                    total_factura: $scope.total,
                    localidad: $scope.localidad,
                    codigo_vendedor: $scope.vendedor,
                    condicion_pago: $scope.condPago,
                    numero_pagos: $scope.num_cuotas,
                    forma_pago: '4',
                    dividir_factura: $scope.dividir_factura,
                    productos: paseDataProductosSave(),
                    vencimientos: paseDataVencimientoSave(),
                    pagos: paseDataPagosSave()
                });

                //console.log(data_factura);

                return data_factura;

            }

            function paseDataProductosSave() {
                var con_iva = 'N';
                var new_items_pedido = [];
                _($scope.items_pedido).each(function(dato) {

                    if (dato.checked) {

                        if (dato.piva30 > 0) {
                            con_iva = 'S';
                        }
                        new_items_pedido.push({
                            numero_pedido: dato.nopedido30,
                            codigo_producto: dato.codprod30,
                            ocurrencia: dato.ocurren30,
                            con_iva: con_iva,
                            cantidad_pedida: dato.cantped30,
                            cantidad_facturar: dato.aFacturar,
                            cantidad_facturada: dato.cantfac30,
                            valor: dato.precuni30,
                            descuento: dato.desctoxprod30,
                            total: dato.total,
                            prod_principal_equiv: dato.prod_principal_equiv
                        });
                    }
                });

                return new_items_pedido;
            }

            function paseDataVencimientoSave() {
                var new_array_vencimientos = [];

                _($scope.array_vencimientos).each(function(dato) {
                    if ($scope.tipo_pago == 0) {
                        var saldo = 0;
                    } else {
                        var saldo = dato.valor;

                    }
                    new_array_vencimientos.push({
                        fecha_cobro: formatea_fecha(dato.fecha),
                        cobro: dato.valor,
                        saldo: saldo,
                        detalle: dato.comentario
                    });
                });

                return new_array_vencimientos;
            }

            function formatea_fecha(fecha) {
                var array_fecha = fecha.split('-');
                var new_formato_fecha = array_fecha[2] + '-' + array_fecha[1] + '-' + array_fecha[0];
                return new_formato_fecha;
            }

            function paseDataPagosSave() {
                var new_detalle_formas_pago = [];
                if ($scope.tipo_pago == 0 && $scope.flag == 0) {
                    _($scope.detalle_formas_pago).each(function(dato) {
                        new_detalle_formas_pago.push({
                            valor: dato.valor,
                            forma_pago: dato.formasPago
                        });
                    });
                }
                return new_detalle_formas_pago;
            }

            function full_path(resource, action, params) {
                var path = globalApp.base_url + resource + '/api/' + action;
                if (params !== undefined && params.url_params) {
                    _(_.values(params.url_params)).each(function(param) {
                        path += '/' + param;
                    })
                }

                path += document.location.search;

                return path;
            }

            function formatear_valor_decimal(valor, numero_decimales) {
                var valor_formateado;
                valor_formateado = Number(valor).toFixed(numero_decimales);
                return valor_formateado;
            }

            function recontabilizar(tipodoc, numdoc, grupo, modulo) {
                $.ajax({
                    url: "../../../itprog/it52i.php" + document.location.search,
                    type: "POST",
                    data: {
                        tipodoc: tipodoc,
                        numdoc: numdoc,
                        campo1: tipodoc,
                        grupo: grupo,
                        modulo: modulo,
                        accion: "Procesar"
                    },
                    async: false
                }).responseText;
            }

            function inserta_temporales(nocomp, ocurrencia, codProd, cant, localidad) {
                $http({
                    method: 'POST',
                    url: full_path('facturacion_pedidos', 'inserta_movimientos_temporales_producto', undefined),
                    data: {
                        nocomp: nocomp,
                        ocurrencia: ocurrencia,
                        codProd: codProd,
                        cant: cant,
                        localidad: localidad
                    }
                }).then(function(response) {
                    //$scope.nocomp = response.data;
                });
            };

            function elimina_temporales(nocomp, ocurrencia, codProd, cant, localidad) {
                $http({
                    method: 'POST',
                    url: full_path('facturacion_pedidos', 'elimina_movimientos_temporales_producto', undefined),
                    data: {
                        nocomp: nocomp,
                        ocurrencia: ocurrencia,
                        codProd: codProd,
                        cant: cant,
                        localidad: localidad
                    }
                }).then(function(response) {
                    //console.log(response.data);
                });
            };

            function get_nocomp_movpro_temporales() {
                $http({
                    method: 'POST',
                    url: full_path('facturacion_pedidos', 'nocomp_temporal_producto', undefined),
                    data: {}
                }).then(function(response) {
                    $scope.nocomp = response.data;
                    if ($scope.nocomp != '') {
                        proceso_calcular_totales();
                    }
                });
            };



        })
        .config(function($routeProvider) {
            $routeProvider
                .when("/facturacion_pedidos/detalle/:codigo_cliente/:cond_pago", {
                    templateUrl: globalApp.base_url + "../client/src/app/facturacion_pedidos/views/detalle_facturacion_pedidos.html",
                    controller: "detalle_facturacion_pedidos.controller",
                    resolve: {
                        pedidos_aprobados_by_cliente: function($location, facturacion_pedidos_model, $route) {
                            var params = {
                                url_params: {
                                    codigo_cliente: $route.current.params.codigo_cliente
                                }
                            };
                            return facturacion_pedidos_model.get('pedidos_aprobados_by_cliente', params)
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        datos_cliente: function($location, facturacion_pedidos_model, $route) {
                            var params = {
                                url_params: {
                                    codigo_cliente: $route.current.params.codigo_cliente
                                }
                            };
                            return facturacion_pedidos_model.get('cliente_by_codigo', params)
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        vendedores: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('vendedores')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        condiciones_pago: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('condiciones_pago')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        impresoras: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('impresoras')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        formatos: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('formatos')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        localidades: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('localidades')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        transportistas: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('transportistas')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        ayudantes: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('ayudantes')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        iva_default: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('iva_default')
                                .catch(function() {
                                    $location.path('/');
                                });
                        },
                        formas_pago: function($location, facturacion_pedidos_model, $route) {
                            return facturacion_pedidos_model.get('formas_pago')
                                .catch(function() {
                                    $location.path('/');
                                });
                        }

                    }
                });
        });
})();