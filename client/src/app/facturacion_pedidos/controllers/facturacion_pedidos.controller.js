(function() {

  "use strict";

  angular.module("Facturacion_pedidos").controller("facturacion_pedidos.controller", function(_, $location, $scope, $http, facturacion_pedidos_model, pedidos_aprobados) {

      $scope.datos_pedidos = pedidos_aprobados;


      $scope.buscar = function() {

        if ($scope.nombre_cliente || $scope.codigo_cliente || $scope.pedido) {

          if (!$scope.nombre_cliente) {
            $scope.nombre_cliente = null;
          }

          if (!$scope.codigo_cliente) {
            $scope.codigo_cliente = null;
          }

          if (!$scope.pedido) {
            $scope.pedido = null;
          }

          var params = {
            url_params: {
              codigo_cliente: $scope.codigo_cliente,
              nombre_cliente: $scope.nombre_cliente,
              pedido: $scope.pedido
            }
          };

          facturacion_pedidos_model.get('pedidos_aprobados', params).then(function(response) {
            $scope.datos_pedidos = response;
          });

        } else {

          $scope.$emit('warning', {
            action: 'No se puede realizar esta accion!',
            element: 'Ingrese un criterio de búsqueda.',
            message: ''
          });

        }
      };

      $scope.refresh = function() {
        window.location.reload();
      };

      $scope.abrir_pedidos = function(codigo_cliente, cond_pago) {

        $location.path('facturacion_pedidos/detalle/' + codigo_cliente + '/' + cond_pago + '/');
      //$location.path('requisiciones_produccion/detalle/' + orden_produccion + '/');
      };


    })
    .config(function($routeProvider) {
      $routeProvider
        .when("/", {
          templateUrl: globalApp.base_url + "../client/src/app/facturacion_pedidos/views/facturacion_pedidos.html",
          controller: "facturacion_pedidos.controller",
          resolve: {
            pedidos_aprobados: function($location, facturacion_pedidos_model) {
              return facturacion_pedidos_model.get('pedidos_aprobados')
                .catch(function() {
                  $location.path('/');
                });
            }

          }
        });
    });
})();